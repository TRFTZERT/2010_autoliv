﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.8.3.111 SP?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="Prog_Fst" Source="Prog_Fst.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Sender" Source="Cell2.LibDVFrame1_ST.Sender.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2" />
  <TaskClass Name="Cyclic#3">
    <Task Name="Cell1_Came" Source="Cell1.Cell1_Camera.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell2_Came" Source="Cell2.Cell2_Camera.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell3_Came" Source="Cell3.Cell3_Camera.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell4_Came" Source="Cell4.Cell4_Camera.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#4">
    <Task Name="Prog_St" Source="Prog_St.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Prog_Fbd" Source="Prog_Fbd.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell1_Basi" Source="Cell1.Cell1_Basic.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell2_Basi" Source="Cell2.Cell2_Basic.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell3_Basi" Source="Cell3.Cell3_Basic.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell4_Basi" Source="Cell4.Cell4_Basic.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Server" Source="LibAsTCP1_ST.Server.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Client" Source="LibAsTCP1_ST.Client.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell1_Robo" Source="Cell1.Cell1_Robot.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell2_Robo" Source="Cell2.Cell2_Robot.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell3_Robo" Source="Cell3.Cell3_Robot.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell4_Robo" Source="Cell4.Cell4_Robot.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Alarm" Source="Infrastructure.Alarm.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Audit" Source="Infrastructure.Audit.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="UserMngmnt" Source="Infrastructure.UserMngmnt.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell10_Bas" Source="Cell10.Cell10_Basic.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell10_Cam" Source="Cell10.Cell10_Camera.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell_Robot" Source="Cell10.Cell_Robot.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell7_Basi" Source="Cell7.Cell7_Basic.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell7_Came" Source="Cell7.Cell7_Camera.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cell7_Robo" Source="Cell7.Cell7_Robot.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#5">
    <Task Name="mvHMICtrl" Source="Infrastructure.mvHMICtrl.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#6">
    <Task Name="Blink" Source="Infrastructure.Blink.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8" />
  <DataObjects>
    <DataObject Name="assl1" Source="" Memory="UserROM" Language="Binary" />
  </DataObjects>
  <Binaries>
    <BinaryObject Name="FWRules" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arsvcreg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asnxdb1" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="ABB_TRFTZ1" Source="Libraries.ABB_TRFTZ1.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="ABBTR1" Source="Libraries.ABBTR1.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="AsTCP" Source="Libraries.AsTCP.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="dvframe" Source="Libraries.dvframe.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsSafety" Source="Libraries.AsSafety.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="FileIO" Source="Libraries.FileIO.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="DataObj" Source="Libraries.DataObj.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsXml" Source="Libraries.AsXml.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsIOAcc" Source="Libraries.AsIOAcc.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrMath" Source="Libraries.AsBrMath.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsEPL" Source="Libraries.AsEPL.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.AsBrStr.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="SafeReset" Source="Libraries.SafeReset.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIecCon" Source="Libraries.AsIecCon.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="mvLib" Source="Libraries.mvLib.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="RFID_Trp" Source="Libraries.RFID_Trp.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="AsUSB" Source="Libraries.AsUSB.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="Acp10man" Source="Libraries.Acp10man.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="NcGlobal" Source="Libraries.NcGlobal.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10par" Source="Libraries.Acp10par.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpAlarmX" Source="Libraries.MpAlarmX.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpBase" Source="Libraries.MpBase.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpAudit" Source="Libraries.MpAudit.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpData" Source="Libraries.MpData.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpFile" Source="Libraries.MpFile.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpRecipe" Source="Libraries.MpRecipe.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpServer" Source="Libraries.MpServer.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpUserX" Source="Libraries.MpUserX.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10_MC" Source="Libraries.Acp10_MC.lby" Memory="UserROM" Language="binary" Debugging="true" />
  </Libraries>
</SwConfiguration>