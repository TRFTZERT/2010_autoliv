define(['system/widgets/KeyBoard/KeyBoard',
    'brease/events/BreaseEvent',
    'brease/controller/KeyboardManager',
    'system/widgets/KeyBoard/libs/KeyCollector',
    'system/widgets/KeyBoard/libs/InputProcessor',
    'system/widgets/KeyBoard/libs/InputValidator',
    'system/widgets/KeyBoard/libs/FocusManager',
    'brease/events/EventDispatcher',
    'system/widgets/common/keyboards/AutoSize',
    'system/widgets/common/keyboards/KeyboardType',
    'brease/controller/ZoomManager'
], function (SuperClass,
    BreaseEvent,
    keyboardManager,
    KeyCollector,
    InputProcessor,
    InputValidator,
    FocusManager,
    EventDispatcher,
    AutoSize,
    KeyboardType,
    zoomManager) {

    'use strict';

    /**
    * @class widgets.__WIDGET_LIBRARY__.__WIDGET_NAME__
    * #Description
    *   
    * @breaseNote
    * @extends system.widgets.KeyBoard
    *
    * @iatMeta category:Category
    * Keyboards
    * @iatMeta description:short
    * Custom keyboard
    * @iatMeta description:de
    * Custom keyboard
    * @iatMeta description:en
    * Custom keyboard
    */

    var defaultSettings = {
            html: 'widgets/__WIDGET_LIBRARY__/__WIDGET_NAME__/__WIDGET_NAME__.html',
            stylePrefix: 'widgets___WIDGET_LIBRARY_____WIDGET_NAME__',
            width: __WIDTH__,
            height: __HEIGHT__,
            scale2fit: true
        },
        WidgetClass = SuperClass.extend(function __WIDGET_NAME__(elem, options, deferredInit, inherited) {
            if (inherited === true) {
                SuperClass.call(this, null, null, true, true);
                _loadHTML(this);
            } else {
                if (instance === undefined) {
                    SuperClass.call(this, null, null, true, true);
                    _loadHTML(this);
                    instance = this;
                } else {
                    return instance;
                }
            }
        }, defaultSettings),
        instance,
        p = WidgetClass.prototype;
        
    p.init = function () {
        this.focusManager = new FocusManager();
        this.inputProcessor = new InputProcessor();
        this.keyCollector = new KeyCollector();
        this.inputValidator = new InputValidator();
        SuperClass.prototype.init.apply(this, arguments);
    };

    p.getValue = function () {
        return this.inputValidator.getValue();
    };

    p.validate = function (value) {
        return this.inputValidator.validate(value);
    };

    p.show = function (options, refElement) {
        SuperClass.prototype.show.call(this, options, refElement);
        this.closeOnLostContent(refElement);
        this.inputProcessor.setOptions(options);
        this.eventDispatcher.dispatchEvent({
            type: 'Collector.Set',
            detail: {
                'value': this.settings.text
            }
        });
        this.inputValidator.setRestriction(this.settings.restrict);
        this.eventDispatcher.dispatchEvent({
            type: 'Keyboard.Show'
        });
    };

    p.hide = function () {
        SuperClass.prototype.hide.apply(this, arguments);
        keyboardManager.getLayoutSelector().close();
        this.eventDispatcher.dispatchEvent({
            type: 'Keyboard.Hide'
        });
    };

    p.dispose = function () {
        if (this.eventDispatcher) {
            this.eventDispatcher.removeEventListener('Processor.Submit', this._bind(_onSubmit));
            this.eventDispatcher.removeEventListener('Collector.Close', this._bind(_onClose));
        }
        this.focusManager.dispose();
        this.inputProcessor.dispose();
        this.keyCollector.dispose();
        this.inputValidator.dispose();
        SuperClass.prototype.dispose.apply(this, arguments);
        instance = undefined;
    };

    p._applyScale = function (factor) {
        var options = AutoSize.getOptions(KeyboardType.ALPHANUMERIC);
        if (options.autoSize === true) {
            var limits = AutoSize.getLimits(this.dimensions, options),
                appZoom = zoomManager.getAppZoom();

            factor = AutoSize.range(appZoom, limits.min, limits.max);
        }
        SuperClass.prototype._applyScale.call(this, factor);
    };

    function _loadHTML(widget) {
        require(['text!' + widget.settings.html], function (html) {
            widget.deferredInit(document.body, html, true);
            _appendLayoutSelector(widget);
            widget.inputEl = widget.el.find('.ValueOutput');
            widget.inputElem = widget.inputEl.get(0);
            if (brease.config.detection.mobile === false) {
                widget.inputElem.removeAttribute('readonly');
            }
            widget.eventDispatcher = new EventDispatcher();
            widget.eventDispatcher.addEventListener('Processor.Submit', widget._bind(_onSubmit));
            widget.eventDispatcher.addEventListener('Collector.Close', widget._bind(_onClose));
            widget.keyCollector.init(widget.eventDispatcher, widget);
            var cursor = {};
            widget.focusManager.init(widget.eventDispatcher, widget, cursor);
            widget.inputProcessor.init(widget.eventDispatcher, widget, cursor);
            widget.inputValidator.init(widget.eventDispatcher, widget);
            widget.readyHandler();
        });
    }
    function _appendLayoutSelector(widget) {
        var layoutSelector = keyboardManager.getLayoutSelector().el,
            targetContainer = widget.el.find('.breaseLayoutSelector');
        if (targetContainer.length > 0) {
            layoutSelector.appendTo(targetContainer);
        }
    }
    function _onSubmit(e) {
        /**
        * @event value_submit
        * Fired after user clicks 'enter' to submit value    
        * @param {Object} detail current value
        * @param {String} type {@link brease.events.BreaseEvent#static-property-SUBMIT BreaseEvent.SUBMIT}
        * @param {HTMLElement} target element of widget
        */
        this.dispatchEvent(new CustomEvent(BreaseEvent.SUBMIT, { detail: e.detail.value }));
        this.hide();
    }

    function _onClose() {
        this.hide();
    }

    return WidgetClass;

});
