define([
    'widgets/brease/TableWidget/libs/Model',
    'brease/enum/Enum'
], function (SuperClass, Enum) {
    
    'use strict';
    /** 
     * @class widgets.brease.AuditList.libs.Model
     * Class for storing data and manipulating this data
     */

    var defaultSettings = {},
    
        ModelClass = SuperClass.extend(function Model(widget) {
            SuperClass.apply(this, arguments);
            this.widget = widget;
            this.settings = widget.settings;
            this.firstConnection = true;
            this.currentData = [];
        }, defaultSettings),

        p = ModelClass.prototype;

    /**
     * @method updateNextData
     * @private
     * Updates the data model but without calling the rerender function of the table, only updates the data in the datatable
     * @param {Object} telegram
     */
    p.updateNextData = function (telegram) {
        if ((this.prevTelegram && this.prevTelegram.parameter.ToRecordIndex >= telegram.parameter.FromRecordIndex) || this.prevTelegram === undefined) {
            this.updateTable(telegram.data.List);
            //Add Alarm pictures to the alarm list and fix timestamp at the same time
            for (var itr = 0; itr < telegram.data.List.length; itr += 1) {
                telegram.data.List[itr].tim = this._fixTimestamp(telegram.data.List[itr].tim);
            } 
            this.prevTelegram = telegram;
            this.updateFixedData(telegram);
            this.widget.renderer.updateNextData(telegram.data.List);
        }
    };

    /**
     * @method updateTable
     * @private
     * This method will store the retrieved data in the variable currentData. 
     * It will then create a dictionary with the specified key of the two 
     * arrays and then merge these together. This way no double entries will
     * be allowed. New data will overwrite old data.
     * 
     * @param {Object[]} data An array of data as defined by the /docs/MpAudit_WidgetConnection.docx
     */
    p.updateTable = function (data) {
        this.currentData = this.currentData.concat($.extend(true, [], data));
        this.currentData = _.uniqBy(this.currentData, 'idx');
    };

    /**
     * @method updateFixedData
     * @private
     * This method will store the retrieved data in the variable currentData. 
     * It will then create a dictionary with the specified key of the two 
     * arrays and then merge these together. This way no double entries will
     * be allowed. New data will overwrite old data.
     * 
     * @param {Object[]} data An array of data as defined by the /docs/MpAlarmHistory_WidgetConnection.docx
     */
    p.updateFixedData = function (data) {
        this.data = this.data.concat($.extend(true, [], data));
        this.data = _.uniqBy(this.data, 'idx');
    };

    /**
     * @method prepareData
     * @private
     * This method will go through the stored in the this.data variable and fix the image paths for the columns Status Old, Status New, Categories 
     * and the timestamp for Timestamp column. This data will overwrite the original data so that it can be displayed as the user has defined in the
     * property grid. The function will then call the updateData method which will update the DataTable with the latest data.
     * @param {Object[]} data 
     */
    p.prepareData = function (data) {
        for (var itr = 0; itr < data.length; itr += 1) {
            data[itr].tim = this._fixTimestamp(data[itr].tim);
            this.data.push(data[itr]);
        } 

        //To minimize the number of iterations where we repeat the fixTimestamp for new data
        //we only check new data.
        this.data = _.uniqBy(this.data, 'idx');
        
        if (!brease.config.editMode) {
            this.widget.renderer.updateData();
        }

    };
    
    /**
     * @method prepareTelegram
     * @private
     * This method will go through the stored in the this.data variable and fix the image paths for the columns Status Old, Status New, Categories 
     * and the timestamp for Timestamp column. This data will overwrite the original data so that it can be displayed as the user has defined in the
     * property grid. The function will then call the updateData method which will update the DataTable with the latest data.
     */
    p.prepareTelegram = function (data) {
        this.telegram = $.extend(true, [], data);
        
        //Add Alarm pictures to the alarm list and fix timestamp at the same time
        for (var itr = 0; itr < this.telegram.length; itr += 1) {
            this.telegram[itr].tim = this._fixTimestamp(this.telegram[itr].tim);
        }
        
    };

    /**
     * @method resetTable
     * This method resets the data that has been stored and updates the renderer so that the table becomes empty.
     */
    p.resetTable = function () {
        this.currentData = [];
        this.data = [];
        this.widget.renderer.updateData();
    };

    /**
     * @method setMockData
     * Only to be called in the editor. Will grab some mock data to display in the editor.
     */
    p.setMockData = function () {
        if (!brease.config.editMode) { return false; }
        this.fetchData(_mockEditorData());
    };

    /**
     * @method setData
     * intermediary function to set data in the fetchData function.
     * @param {Object} telegram
     */
    p.setData = function (telegram) {
        this.fetchData(telegram);
    };

    /**
     * @method fetchData
     * @private
     * The fetchData is the brain behind the AuditList. It will decided where data should go, if more should be retrieved. When the methodId for the telegram is
     * 'Getidx' the method will start fetching data if 
     * non is available, interrupt the data fetch if new data has been made available in the backend, continue fetching data where it left of if a page change is
     * made while loading data, or if it should reset the fetch from the very beginning (for example at a language change). If data is returned with the methodId
     * 'GetList' then the data is passed to a function that stores the data and continues the retrieve.
     * @param {Object} telegram
     */
    p.fetchData = function (telegram) {
        if (telegram === null) { return; }
        var telegramCopy = $.extend(true, [], telegram);

        if (telegramCopy.methodID === 'GetRecordIndex') {
            //Start
            this.widget.renderer._showBusyIndicator();
            if (this.widget.settings.counter === 0) {
                this.startFetch(telegramCopy);

            //Reset data if telegram data is 0
            } else if (telegramCopy.data === 0) {
                this.resetFetch();
            //Refetches data if value is less than the largest audit value we recieve
            } else if (telegramCopy.data < this.widget.settings.counter) {
                this.widget.settings.counter = telegramCopy.data;
                this.reFetchData();
            //If new audit is set while fetching data  
            } else {
                this.continueFetch(telegramCopy);    
            }
        } else if (telegramCopy.methodID === 'GetList') {
            if (telegramCopy.parameter.NextData) {
                this.updateNextData(telegramCopy);
            } else {
                this.getData(telegramCopy);
            }
        }
    };

    /**
     * @method startFetch
     * @private
     * when there is no data stored, the telegram will receive the total number of rows in the backend on the data parameter, this is used to set the counter
     * to how far we need to count before all data is stored in the front end. Observer though that the count is down and not up - we end on 0. After that the
     * currentIteration is set to 0, and the figure out from where to where in the dataset we should get data from. It all depends on the number of rows and
     * the number of rows we allow per fetch (currently at 1000 rows). Then the sendData to stitch together the necessary telegram the backend needs to 
     * return the right data set.
     * @param {Object} telegram
     */
    p.startFetch = function (telegram) {
        this.widget.settings.counter = telegram.data;
        this.getFrom = telegram.data;
        this.getTo = telegram.data - this.settings.rowsPerFetch;
        this.sendData(this.getFrom, this.getTo);
    };
    
    /**
     * @method continueFetch
     * We continue fetching data where we stopped
     * @param {Object} telegram
     */
    p.continueFetch = function (telegram) {
        this.getFrom = telegram.data;
        this.getTo = this.widget.settings.counter;
        this.widget.settings.counter = telegram.data;
        if (this.currentData.length > this.data.length) {
            this.prepareData($.extend(true, [], this.currentData));   
        }
        this.sendData(this.getFrom, this.getTo);
    };

    /**
     * @method reFetchData
     * This method can be called when all stored data should be reset to scratch and new data retrieved from the backend. One such example is at a
     * language change as the old language needs to be removed from the table. It will however not purge the table from data.
     */
    p.reFetchData = function () {
        this._resetData();
        if (this.widget.settings.counter > 0) {
            this.widget.renderer._showBusyIndicator();
            this.sendData(this.getFrom, this.getTo);
        }
    };

    /**
     * @method resetFetch
     * The resetFetch takes values one step further and resets every variable in the fetch algorithm before also resetting the table and displaying an
     * empty table.
     */
    p.resetFetch = function () {
        this._resetData();
        this.widget.settings.counter = 0;
        this.resetTable();
    };

    /**
     * @method fetchNextData
     * this method will fetch the next set of data, i.e. the next 1000 rows.
     * @param {Boolean} force this parameter forces a read from the backend if set to true
     */
    p.fetchNextData = function (force) {
        this.getFrom = this._getFromData();
        this.getTo = this._getToData();
        this.sendData(this.getFrom, this.getTo, true, force);
    };

    /**
     * @method _resetData
     * @private
     * This method will reset data, should be called by reFetchData and resetFetch
     */
    p._resetData = function () {
        this.currentIteration = 0;
        this.getFrom = this.settings.rowsPerFetch;
        this.getTo = 0;
        this.data = [];
        this.currentData = [];
    };

    /**
     * @method getData
     * stores the data it has just recieved by calling the updateTable. It then increases the iteration, recalculates the to and from variables so that the next
     * iteration and sends a telegram to get the next data set. If there are no more datasets then it will call for a preparation of data before displaying it in
     * the DataTables.
     * @param {Object} telegram
     */
    p.getData = function (telegram) {
        this.widget.settings.langChanged = false;
        this.updateTable(telegram.data.List);
        // if (this.currentData.length <= 1000 && this.currentData.length > this.widget.settings.rowsPerFetch) {
        //     this.sendData(this.getFrom, this.getTo);
        // } else {
        this.prepareData(telegram.data.List);
        // }
    };

    /**
     * @method _getFromData
     * Returns the value from where the data should be selected
     * @returns {UInteger}
     */
    p._getFromData = function () {
        return (this.currentData.length > 0) ? _.minBy(this.currentData, 'idx').idx - 1 : this.settings.rowsPerFetch;
    };
    /**
     * @method _getToData
     * Returns the value to where the data should be selected
     * @returns {UInteger}
     */
    p._getToData = function () {
        return (this.currentData.length > 0) ? Math.max(_.minBy(this.currentData, 'idx').idx - this.settings.rowsPerFetch - 1, 0) : 0;
    };

    /**
     * @method getDataForItem
     * This method get's an integer and returns the object row from the row on that position.
     * @param {Integer} item the current poistion of the original data to be returned
     * @returns {Object} row in the data table of original data
     */
    p.getDataForItem = function (item) {
        return this.currentData[item];
    };
    
    /**
     * @method sendData
     * From is the higher number, to is the lower number
     * @param {Integer} from the number from where in the dataset we get data
     * @param {Integer} to the number form where to in the dataset we get data
     * @param {Boolean} nextData boolean to indicate if an extra parameter is to be added that is used by the front end to determine if data should be added or appended
     * @param {Boolean} force if set to true, all values will be disregarded and a GetList will be sent to the backend no matter what
     */
    p.sendData = function (from, to, nextData, force) {
        var optParameters = [], params;
        optParameters = this._getOptionalParams();

        to = Math.max(to, 0);
        params = Object.assign({ 'FromRecordIndex': from, 'ToRecordIndex': to }, optParameters);

        if (nextData) {
            params['NextData'] = true;
        }
        if (force || from > 0) {
            if (brease.config.preLoadingState) return;
            this.widget.linkHandler.sendRequestAndProvideCallback('GetList', this.widget._updateData, undefined, params);
        } else {
            this.widget.renderer._hideBusyIndicator();
        }
    };

    p._getOptionalParams = function () {
        var i = 1, arrParams = [];
        if (this.settings.config !== undefined && this.settings.config.columns !== undefined) {
            this.settings.config.columns.forEach(function (column) {
                var objParams = {};
                if (column.data === 'new' || column.data === 'old') {
                    objParams[('Parameter' + i)] = column.data + '={&' + column.data + '}{&' + column.data + '[UNIT= %s]}';
                    arrParams.push(objParams);
                    i++;
                }
            });
        }
        return arrParams;
    };

    /**
     * @method exportData
     * This method will call the linkhandler to export the necessary data 
     */
    p.export = function () {
        this.widget.linkHandler.sendDataAndProvideCallback('Export', this.widget._updateData, undefined, {});
    };
    
    /**
     * @method _mockEditorData
     * @private
     * @returns {Object} telegram for editor, not to be used in runtime
     */
    function _mockEditorData() {
        var mockData = {
            'data': {
                'List': [
                    { 'idx': '0', 'opn': 'Mocked Audit Trace', 'tex': 'This audit list is a mocked list and will not reflect the real audit list, it still needs to be configured in the MpAuditTrail', 'tim': '1970-01-01T00:00:00.000+00:00', 'typ': '0', 'new': '10', 'old': '5' },
                    { 'idx': '1', 'opn': 'Mocked: Audit Item 1', 'tex': ' User 1 has logged in', 'tim': '1970-01-01T11:33:15.141+00:00', 'typ': '1', 'new': '10', 'old': '5' },
                    { 'idx': '2', 'opn': 'Mocked: Audit Item 2', 'tex': 'User 1 changed motor speed', 'tim': '1970-01-01T11:33:15.141+00:00', 'typ': '2', 'new': '10', 'old': '5' },
                    { 'idx': '3', 'opn': 'Mocked: Audit Item 3', 'tex': 'User 2 logged in', 'tim': '1970-01-01T11:31:56.541+00:00', 'typ': '3', 'new': '10', 'old': '5' },
                    { 'idx': '4', 'opn': 'Mocked: Audit Item 4', 'tex': 'User 2 changed location to Great Britain.', 'tim': '1970-01-01T11:31:56.341+00:00', 'typ': '0', 'new': '10', 'old': '5' },
                    { 'idx': '5', 'opn': 'Mocked: Audit Item 5', 'tex': 'User 1 logged off', 'tim': '1970-01-01T11:31:55.741+00:00', 'typ': '1', 'new': '10', 'old': '5' },
                    { 'idx': '6', 'opn': 'Mocked: Audit Item 6', 'tex': 'User 2 logged off', 'tim': '1970-01-01T11:31:55.865+00:00', 'typ': '2', 'new': '10', 'old': '5' }
                ],
                'RecordIndex': 6 
            },
            'parameter': {},
            'methodID': 'GetList', 
            'response': 'OK'
        };

        return mockData;
    }

    return ModelClass;

});
