define([
    'widgets/brease/common/libs/redux/view/ListView/ListView',
    'widgets/brease/common/libs/redux/view/ItemView/ItemView',
    'widgets/brease/DropDownBox/libs/reducer/DropDownBoxActions',
    'brease/enum/Enum',
    'brease/core/Utils',
    'brease/events/BreaseEvent',
    'brease/controller/PopUpManager'
], function (ListView, ItemView, DropDownBoxActions, Enum, Utils, BreaseEvent, popupManager) {

    'use strict';

    var DropDownBoxView = function (store, parent, widget) {
        this.el = parent;
        this.store = store;
        this.widget = widget;
        this.render();
    };

    var p = DropDownBoxView.prototype;

    p.render = function render() {

        this.dispose();

        this.el.addClass('breaseDropDownBox DropDownBoxView');
        var state = this.store.getState();

        if (state.status.visible && state.status.active) {

            var actualItem = state.items.itemList[state.items.selectedIndex],
                selectedText = actualItem === undefined ? undefined : state.text.textElements[actualItem.textId],
                selectedImage = actualItem === undefined ? undefined : state.image.imageElements[actualItem.imageId];

            var itemProps = {
                imageIndicator: {
                    showImage: true
                },
                text: {
                    text: selectedText === undefined ? '' : selectedText.displayText,
                    textSettings: state.text.textSettings,
                    showText: state.items.listSettings.showTextsInButton
                },
                image: {
                    image: selectedImage === undefined ? undefined : selectedImage.imagePath,
                    showImage: state.items.listSettings.showImagesInButton
                },
                itemSettings: {
                    itemHeight: '100%',
                    imageAlign: state.items.itemSettings.imageAlign
                },
                status: {
                    enabled: state.status.enabled,
                    visible: state.status.visible,
                    selected: state.items.listOpen,
                    lastItem: true
                },
                onClick: _updateListStatus.bind(this)
            };
            this.itemView = new ItemView(itemProps, this.el);

            if (state.items.listOpen) {
                this.listContainer = $("<div id='" + this.el[0].id + "_listBoxWrapper' class='listBoxContainer " + state.style.styleToApply + "'></div>");
                this.arrow = $("<div class='arrow'></div>");
                this.listContainer.append(this.arrow);
                $(document.body).append(this.listContainer);
                var borderCorrection = _getBorderFromList(this.listContainer);
                var scaleFactor = Utils.getScaleFactor(this.el[0]);
                var buttonSize = { height: this.el[0].offsetHeight, width: this.el[0].offsetWidth };
                var listPosition = _calculateListPosition(this.el, buttonSize,
                    state.items.listSettings.listPosition,
                    state.items.listSettings.listHeight + borderCorrection,
                    state.items.listSettings.listWidth,
                    scaleFactor,
                    state.items.listSettings.cropToParent);
                this._listContainerSizePos(this.listContainer, listPosition, scaleFactor);
                _layoutArrowList(this.listContainer, this.arrow, state.items.listSettings.listPosition, buttonSize, listPosition.width, scaleFactor);
                //Override listSettings
                var listProps = {
                    status: state.status,
                    items: state.items,
                    text: state.text,
                    image: state.image,
                    onClick: (function (store, widget, element) {
                        return function (index, originalEvent) {
                            _dispatchSelectedItem(store, index, widget, originalEvent, element);
                            originalEvent.preventDefault();
                        };
                    })(this.store, this.widget, this.el)
                };
                this.listView = new ListView(listProps, this.listContainer);
                this.closeOnMouseDownBound = _closeListOnMouseDown.bind(this);
                this.closeOnWheelBound = _closeListOnWheel.bind(this);
                $(document.body).on(BreaseEvent.MOUSE_DOWN, this.closeOnMouseDownBound);
                $(document.body).on('wheel', this.closeOnWheelBound);
            }
        }
    };

    p.dispose = function dispose() {
        if (this.listView !== undefined) {
            this.listView.dispose();
        }
        if (this.itemView !== undefined) {
            this.itemView.dispose();
        }
        if (this.arrow !== undefined) {
            this.arrow.remove();
        }
        if (this.listContainer !== undefined) {
            this.listContainer.remove();
        }
        if (this.closeOnMouseDownBound !== undefined) {
            $(document.body).off(BreaseEvent.MOUSE_DOWN, this.closeOnMouseDownBound);
        }
        if (this.closeOnWheelBound !== undefined) {
            $(document.body).off('wheel', this.closeOnWheelBound);
        }
        if (this.closeOnMouseMoveBound !== undefined) {
            $(document.body).off(BreaseEvent.MOUSE_MOVE, this.closeOnMouseMoveBound);
        }
    };

    function _dispatchSelectedItem(store, index, widget, originalEvent, element) {
        //Trigger the widget _clickHandler in order to have the Click event from BaseWidget
        var coordinates = element[0].getBoundingClientRect();
        originalEvent.clientX = coordinates.left;
        originalEvent.clientY = coordinates.top;
        // eslint-disable-next-line no-useless-call
        widget._clickHandler.call(widget, originalEvent);
        if (store.getState().status.enabled) {
            var action = DropDownBoxActions.updateSelectedItem(index);
            store.dispatch(action);
            //Store AS with the new values
            widget.valueChangeFromUI();
        }
    }

    function _updateListStatus(event) {
        //Save the actual timeStamp to avoid inmediate close of the list
        this.openEventTimeStamp = event.timeStamp;
        if (this.store.getState().status.enabled) {
            var action = DropDownBoxActions.toggleListStatus();
            this.store.dispatch(action);
            this.widget.triggerToggleStateChanged();
        }
    }

    function _closeListOnMouseDown(event) {
        //If the event is different from the open event and it is not comming
        // from an element of the widget itself -> close list
        if (this.openEventTimeStamp !== event.timeStamp && !$.contains(this.listContainer.find('.ListView')[0], event.target) &&
                !$.contains(this.el[0], event.target)) {
            _closeList(this.store, this.widget);
        } else if ($.contains(this.el[0], event.target)) {
            this.closeOnMouseMoveBound = _closeOnMouseMove.bind(this);
            $(document.body).on(BreaseEvent.MOUSE_MOVE, this.closeOnMouseMoveBound);
        }
    }

    function _closeOnMouseMove(event) {
        _closeList(this.store, this.widget);
    }

    function _closeListOnWheel(event) {
        if (!$.contains(this.listContainer.find('.ListView')[0], event.target)) {
            _closeList(this.store, this.widget);
        }
    }

    function _closeList(store, widget) {
        if (store.getState().status.enabled) {
            var action = DropDownBoxActions.closeList();
            store.dispatch(action);
            widget.triggerToggleStateChanged();
        }
    }

    p._listContainerSizePos = function (element, position, scaleFactor) {
        var appcontainerWidth = parseInt($('#appContainer').width(), 10),
            endPos = position.left + position.width;

        if (endPos > appcontainerWidth) {
            position.left = appcontainerWidth - position.width;
        }

        element.css('position', 'absolute');
        element.css('height', position.height);
        element.css('width', position.width);
        element.css('top', position.top);
        element.css('left', position.left);
        element.css('z-index', position.zIndex);
        element.css({ 'transform': 'scale(' + scaleFactor + ',' + scaleFactor + ')', 'transform-origin': '0px 0px 0px' });
    };

    function _calculateListPosition(element, size, listPosition, listHeight, listWidth, scaleFactor, cropToParent) {
        var positionList = {},
            offset = element.offset(),
            scaledSize = {
                height: size.height * scaleFactor,
                width: size.width * scaleFactor
            },
            scaledListWidth = listWidth * scaleFactor,
            scaledListHeight = listHeight * scaleFactor,
            correctionHeightArrow = 8,
            correctionHeightLimits = 0,
            topLimitContainer,
            bottomLimitContainer;

        if (cropToParent === Enum.CropToParent.height || cropToParent === Enum.CropToParent.both) {
            bottomLimitContainer = element.parent().offset().top + element.parent().height() * scaleFactor;
            topLimitContainer = element.parent().offset().top;
        } else {
            bottomLimitContainer = $('#appContainer').height();
            topLimitContainer = 0;
        }

        switch (listPosition) {
            case Enum.Position.bottom:
                //top
                positionList.top = offset.top + scaledSize.height;
                //left
                positionList.left = offset.left + scaledSize.width / 2 - scaledListWidth / 2;
                //height
                listHeight = listHeight + correctionHeightArrow;
                correctionHeightLimits = (positionList.top + listHeight * scaleFactor - bottomLimitContainer) / scaleFactor;
                positionList.height = correctionHeightLimits > 0 ? listHeight - correctionHeightLimits : listHeight;
                //width
                positionList.width = listWidth;
                break;
            case Enum.Position.top:
                //top
                listHeight = listHeight + correctionHeightArrow;
                positionList.top = offset.top - (listHeight * scaleFactor);
                if (positionList.top - topLimitContainer < 0) {
                    correctionHeightLimits = (positionList.top - topLimitContainer) / -scaleFactor;
                    positionList.top = topLimitContainer;
                }
                //left
                positionList.left = offset.left + scaledSize.width / 2 - scaledListWidth / 2;
                //height
                positionList.height = listHeight - correctionHeightLimits;
                //width
                positionList.width = listWidth;
                break;
            case Enum.Position.left:
                //top
                positionList.top = offset.top;
                //left
                positionList.left = offset.left - scaledListWidth;
                //height
                correctionHeightLimits = (positionList.top + listHeight * scaleFactor - bottomLimitContainer) / scaleFactor;
                positionList.height = correctionHeightLimits > 0 ? listHeight - correctionHeightLimits : listHeight;
                //width
                positionList.width = listWidth;
                break;
            case Enum.Position.right:
                //top
                positionList.top = offset.top;
                //left
                positionList.left = offset.left + scaledSize.width;
                //height
                correctionHeightLimits = (positionList.top + listHeight * scaleFactor - bottomLimitContainer) / scaleFactor;
                positionList.height = correctionHeightLimits > 0 ? listHeight - correctionHeightLimits : listHeight;
                //width
                positionList.width = listWidth;
                break;
            case Enum.Position.center:
                //top
                positionList.top = offset.top;
                //left
                positionList.left = offset.left + scaledSize.width / 2 - scaledListWidth / 2;
                //height
                correctionHeightLimits = (positionList.top + listHeight * scaleFactor - bottomLimitContainer) / scaleFactor;
                positionList.height = correctionHeightLimits > 0 ? listHeight - correctionHeightLimits : listHeight;
                //width
                positionList.width = listWidth;
                break;
            case Enum.Position.middle:
                //top
                positionList.top = offset.top + scaledSize.height / 2 - scaledListHeight / 2;
                if (positionList.top - topLimitContainer < 0) {
                    positionList.top = topLimitContainer;
                }
                //left
                positionList.left = offset.left + scaledSize.width / 2 - scaledListWidth / 2;
                //height
                correctionHeightLimits = (positionList.top + listHeight * scaleFactor - bottomLimitContainer) / scaleFactor;
                positionList.height = correctionHeightLimits > 0 ? listHeight - correctionHeightLimits : listHeight;
                //width
                positionList.width = listWidth;
                break;
        }
        positionList.zIndex = popupManager.getHighestZindex() + 1;
        return positionList;
    }

    function _layoutArrowList(container, arrow, listPosition, ItemSize, listWidth, scaleFactor) {
        switch (listPosition) {
            case Enum.Position.bottom:
                container.addClass('bottom');
                arrow.addClass('bottom');
                arrow.css('margin-left', listWidth / 2 - 8);
                break;
            case Enum.Position.top:
                container.addClass('top');
                arrow.addClass('top');
                arrow.css('margin-left', listWidth / 2 - 8);
                break;
            case Enum.Position.left:
                container.addClass('left');
                arrow.addClass('left');
                arrow.css('margin-top', ItemSize.height / 2 - 8);
                break;
            case Enum.Position.right:
                container.addClass('right');
                arrow.addClass('right');
                arrow.css('margin-top', ItemSize.height / 2 - 8);
                break;
            case Enum.Position.center:
                container.addClass('center');
                arrow.addClass('center');
                break;
            case Enum.Position.middle:
                container.addClass('right');
                arrow.addClass('middle');
                break;
        }
    }

    function _getBorderFromList(elementParent) {
        var dummyDiv = $('<div class="ListView Container"></div>');
        elementParent.append(dummyDiv);
        var topBorder = parseInt(dummyDiv.css('border-top-width'), 10),
            bottomBorder = parseInt(dummyDiv.css('border-bottom-width'), 10),
            sumBorder = topBorder + bottomBorder;
        dummyDiv.remove();
        return sumBorder;
    }

    return DropDownBoxView;

});
