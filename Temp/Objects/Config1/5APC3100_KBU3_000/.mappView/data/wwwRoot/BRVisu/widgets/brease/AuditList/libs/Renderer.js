define([
    'widgets/brease/TableWidget/libs/Renderer'
], function (SuperClass) {
    
    'use strict';
    /** 
     * @class widgets.brease.AuditList.libs.Renderer
     * Class for rendering the widget.
     */
    
    var RendererClass = SuperClass.extend(function Renderer(widget) {
            SuperClass.call(this);
            this.widget = widget;
        }, null),
    
        p = RendererClass.prototype;

    /**
     * @method _colorMeBlue
     * @override
     * @private
     * Colloquialism: color me blue - make me feel sad. Used in Blues music.
     * This method will iterate over the currently displayed data set in the data table and if a row fits the criteria selected in the styling dialog,
     * it will be given the style class that corresponds to this rule.
     */
    p._colorMeBlue = function () {
    
        if (this.widget.model.currentData === undefined || !this.dt.exists()) { return; }
        if (this.widget.model.currentData.length === 0) { return; }
                
        for (var j = 0; j < this.widget.el.find('tbody').children().length; j += 1) {
            for (var i = 0; i < this.widget.settings.config.style.length; i += 1) {
    
                var offset = (this.dt.getFilteredPage() * this.widget.settings.itemsPerPage) + j,
                    currRow = this.dt.getFilteredOffset(offset);
                if (currRow === undefined) {
                    continue;
                }
                var currTyp = this.widget.model.currentData[currRow].typ,
                    rowEligible = this._rowEligibility(currTyp, this.widget.settings.config.style[i]);
    
                if (rowEligible) {
                    this.widget.el.find('tbody').children('tr:eq(' + j + ')').addClass('widgets_brease_AuditListStyle_style_style' + this.widget.settings.config.style[i].namePos);
                }
            }
        }
    };

    /**
     * @method _rowEligibility
     * @override
     * @private
     * @param {Integer} currState
     * @param {Integer} currSev
     * @param {Object} style
     * @param {Integer} style.statePos
     * @param {Boolean} style.sevOneUse
     * @param {Integer} style.sevOne
     * @param {Integer} style.sevOnePos
     * @param {Boolean} style.sevTwoUse
     * @param {Integer} style.sevTwo
     * @param {Integer} style.sevTwoPos
     * @param {Integer} style.sevTwo
     * @returns {Boolean}
     * Helper method for the _colorMeBlue function 
     */
    p._rowEligibility = function (currState, style) {
        //First check state (act, act ack, inact)
        if (currState === style.statePos) {
            return true;
        } else {
            return false;
        }
    };
    
    return RendererClass;
});
