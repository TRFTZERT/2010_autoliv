define([
    'brease/core/Class',
    'brease/events/BreaseEvent',
    'widgets/brease/TableWidget/libs/TextRetriever',
    'widgets/brease/GenericDialog/GenericDialog',
    'widgets/brease/GenericDialog/libs/config',
    'widgets/brease/Label/Label',
    'widgets/brease/CheckBox/CheckBox',
    'widgets/brease/DateTimeInput/DateTimeInput',
    'widgets/brease/Image/Image',
    'widgets/brease/Rectangle/Rectangle',
    'widgets/brease/DropDownBox/DropDownBox',
    'widgets/brease/NumericInput/NumericInput'
], function (
    SuperClass, BreaseEvent, TextRetriever, Dialog, 
    DialogConfig, Label, CheckBox, DateTimeIn, Image, 
    Rectangle, DropDownBoxLegacy, NumIn
) {
    
    'use strict';
    
    /** 
     * @class widgets.brease.TableWidget.libs.Dialogue
     * @extends brease.core.Class
     * @inheritable
     */

    var DialogueClass = SuperClass.extend(function Dialogue(widget) {
            this.dialog = new Dialog(widget.elem);// widget.configDiag;
            SuperClass.apply(this);
            this.widget = widget;
        }, null),

        p = DialogueClass.prototype;
    
    /**
     * @method initialize
     * @param {String} type which type of dialog should be instantiated 
     * @param {String} lang
     * Should be implemented in the derived widgets, will start a dialog for the filtering
     */
    p.initialize = function (type, lang) {
        //Implement in derived child widget
    };

    /**
     * @method open
     * This method retrieves the name from the getLanguage function and checks if it's german or not. If it's german it stores the internal language as german otherwise
     * as english. The reason for this is that the next function, setUpTexts, retrieves all texts associated with the Dialog window and stores it over the text object
     * in either the english or the german object, hence we don't need to add another object for textkeys coming from the backend. These will automatically, and seamlessly,
     * integrate into Filtering/Sorting/or Styling dialog. It will also listen to the promise being resolved, and when so is done - i.e. the text have been gathered from the
     * backend, it will open the dialog and display it, also handle eventlisteners for closing the dialog.
     * @param {String} type possible options filter, style or sort. 
     */
    p.open = function (type) {
        var actualLang = this._getLanguage();
        this.lang = (actualLang !== 'de') ? 'en' : 'de';
        var def = new $.Deferred();
        this.setUpTexts(def, actualLang, type);
        var self = this;
        $.when(def.promise()).then(function successHandler() {
            
            var conf = self.initialize(type);
            self.dialog.show(conf, self.widget.elem);
            self.dialog.isReady().then(function (arg) {
                $('#' + self.dialog.elem.id).on(BreaseEvent.WIDGET_READY, { type: type }, self._bind('_widgetAdded'));
                $('#' + self.dialog.elem.id).on('window_closing', { type: type }, self._bind('_collectDataBeforeClosing'));
            });
        });

    };

    /**
     * @method _initializeEmptyDialogConfig
     * @private
     * This method will create the basic configuration needed for any dialog, based on the GenericDialog module.
     * @param {String} headerText
     */
    p._initializeEmptyDialogConfig = function (headerText) {
        this.config = new DialogConfig();

        // dialog
        this.config.forceInteraction = true;
        this.config.contentWidth = 600;
        this.config.contentHeight = 480;

        // header
        this.config.header.text = headerText;

        //footer
        this.config.buttons.ok = true;
        this.config.buttons.cancel = true;
    };

    p._widgetAdded = function (e) {
        var type = e.data.type;
        $('#' + this.dialog.elem.id).off(BreaseEvent.WIDGET_READY, this._bind('_widgetAdded'));
        this[type].initialize();
    };
    
    /**
     * @method _collectDataBeforeClosing
     * @private
     * This method will listen for the closing event and once it it is thrown it will collect the filter configuration, store it into the table
     * and redraw the table. Then it will update the backend that there was a change in the filter.
     */
    p._collectDataBeforeClosing = function (e) {
        if (brease.uiController.parentWidgetId(e.target) === this.dialog.elem.id && this.dialog.getDialogResult() === 'ok') {
            var type = e.data.type;
            this.widget.settings.config[type] = this[type]._widgetCollectStateBeforeClosing();
            if (this.widget.controller) {
                this.widget.controller.draw();
            }
            this.widget.sendConfiguration(type);
        }
    };

    /**
     * @method _addRowHandler
     * @private
     * @deprecated
     */
    p._addRowHandler = function (e) {
        //Function to switch between the different tabs avaible for modularity
        this.filter._addRowHandler(e);
    };
    
    /**
     * @method _getLanguage
     * @private
     * This method will determine which langauge is currenly used. If it's not german it will default to English.
     */
    p._getLanguage = function () {
        return brease.language.getCurrentLanguage();
    };

    /**
     * @method _reColourAllObjects
     * @private
     * @param {Object} self 
     * Will recolor all separators (?)
     */
    // eslint-disable-next-line no-unused-vars
    function _reColourAllObjects(self) {
        self.filter._reColourFirstLineSeparator();
    }

    /**
     * @method dispose
     * This method will dispose of/remove the dialog.
     */
    p.dispose = function () {
        this.dialog.dispose();
        SuperClass.prototype.dispose.call(this);
    };

    return DialogueClass;
});
