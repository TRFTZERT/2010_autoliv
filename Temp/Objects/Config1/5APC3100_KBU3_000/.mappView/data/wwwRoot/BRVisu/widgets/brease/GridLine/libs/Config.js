define([
    'brease/enum/Enum'
], function (Enum) {

    'use strict';

    /**
     * @class widgets.brease.GridLine.Config
     * @extends core.javascript.Object
     * @override widgets.brease.GridLine
     */

    /**
     * @cfg {brease.enum.Direction} orientation='horizontal'
     * @iatStudioExposed
     * @iatCategory Appearance
     * Defines the orientation of the grid if it's horizontal or vertical.  
     */

    /**
     * @cfg {PixelVal} gridSize='200px'
     * @iatStudioExposed
     * @iatCategory Layout
     * Defines the width or height of the Grid depending on the orientation.
     */

    /**
     * @cfg {ImagePath} modifyImage=''
     * @iatStudioExposed
     * @iatCategory Appearance
     * @bindable
     * Image which is shown in modify state.  
     */

    /**
     * @cfg {ImagePath} expandImage=''
     * @iatStudioExposed
     * @iatCategory Appearance
     * @bindable
     * Image which is shown when an item is expanded.  
     */

    /**
     * @cfg {ImagePath} collapseImage=''
     * @iatStudioExposed
     * @iatCategory Appearance
     * @bindable
     * Image which is shown when an item is collapsed.  
     */

    /**
     * @cfg {Boolean} modifyAnimation=false
     * @iatStudioExposed
     * @iatCategory Behavior
     * Defines if the Items are shaking in modify state.  
     */

    /**
     * @cfg {UInteger} transitionTime=0
     * @iatStudioExposed
     * @iatCategory Behavior
     * Defines the time (ms) for collapsing / expanding an item.  
     */

    /**
     * @cfg {Boolean} snapToPosition=false
     * @iatStudioExposed
     * @iatCategory Behavior
     * Defines if the Items should scroll automatically to next/previous position when releasing the Item.  
     */

    /**
     * @cfg {String} itemConfiguration=''
     * @iatStudioExposed
     * @iatCategory Data
     * @bindable
     * @not_projectable
     * Returns / Sets the sorting of the Items from / to Widget.  
     */

    /**
     * @property {WidgetList} [children=["widgets.brease.GridLineItem"]]
     * @inheritdoc
     */

    return {
        gridSize: parseInt('200px', 10),
        orientation: Enum.Direction.horizontal,
        modifyImage: '',
        expandImage: '',
        collapseImage: '',
        modifyAnimation: false,
        transitionTime: 0,
        snapToPosition: false,
        itemConfiguration: '',
        childPositioning: 'relative' //NEEDED THAT THE GRIDLINEITEMS ARE POSITONED RELATIVE IN EDITOR
    };

});
