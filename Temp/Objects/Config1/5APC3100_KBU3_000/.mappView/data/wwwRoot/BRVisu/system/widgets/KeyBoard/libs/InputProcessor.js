define(['brease/core/Utils'],
    function (Utils) {

        'use strict';
        function InputProcessor() {
            var self = this,
                cursor;

            self.init = function (eventDispatcher, widget, sharedCursor) {
                cursor = sharedCursor || {};
                self.widget = widget;
                self.eventDispatcher = eventDispatcher;
                self.eventDispatcher.addEventListener('Validator.Set', self.onSet);
                self.eventDispatcher.addEventListener('Validator.Change', self.onChange);
                self.eventDispatcher.addEventListener('Validator.Input', self.onInput);
                self.eventDispatcher.addEventListener('Collector.Delete', self.onDelete);
                self.eventDispatcher.addEventListener('Collector.CursorLeft', self.onCursorLeft);
                self.eventDispatcher.addEventListener('Collector.CursorRight', self.onCursorRight);
                self.eventDispatcher.addEventListener('Collector.Clear', self.onClear);
                self.eventDispatcher.addEventListener('Validator.Submit', self.onSubmit);
                self.inputElem = widget.inputElem;
            };

            self.setOptions = function (options) {
                if (options.type !== undefined) {
                    this.inputElem.setAttribute('type', options.type);
                }
                if (options.maxLength !== undefined) {
                    this.inputElem.setAttribute('maxlength', parseInt(options.maxLength, 10));
                } else {
                    this.inputElem.removeAttribute('maxlength');
                }
            };
            
            self.onSet = function (e) {
                var value = e.detail.value;
                self.inputElem.value = value;
                cursor.pos = value.length;
                _sendChange();
            };

            function _sendChange() {

                self.eventDispatcher.dispatchEvent({
                    type: 'Processor.Change',
                    detail: {
                        'value': self.inputElem.value
                    }
                });
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            }

            self.onChange = function (e) {
                var value = e.detail.value;
                cursor.pos = self.inputElem.selectionStart;

                if (e.detail.originalValue !== undefined && e.detail.value.length !== e.detail.originalValue.length) {
                    cursor.pos -= Math.abs(e.detail.value.length - e.detail.originalValue.length);
                }
                self.inputElem.value = value;
                
                _sendChange();
            };

            self.onInput = function (e) {
                var value = '';
                if (Utils.isString(e.detail.value)) {
                    value = e.detail.value;
                }
                cursor.pos = self.inputElem.selectionStart + value.length;
                self.inputElem.setRangeText(value, self.inputElem.selectionStart, self.inputElem.selectionEnd, 'end');
                self.eventDispatcher.dispatchEvent({
                    type: 'Processor.Change',
                    detail: {
                        'value': self.inputElem.value
                    }
                });
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            };

            self.onDelete = function () {
                if (self.inputElem.selectionEnd > 0) {
                    
                    if (self.inputElem.selectionStart !== self.inputElem.selectionEnd) {
                        cursor.pos = self.inputElem.selectionStart;
                        self.inputElem.setRangeText('', self.inputElem.selectionStart, self.inputElem.selectionEnd, 'start');
                    } else {
                        cursor.pos = self.inputElem.selectionEnd - 1;
                        self.inputElem.setRangeText('', self.inputElem.selectionEnd - 1, self.inputElem.selectionEnd, 'start'); 
                    }
                }
                self.eventDispatcher.dispatchEvent({
                    type: 'Processor.Change',
                    detail: {
                        'value': self.inputElem.value
                    }
                });
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            };

            self.onCursorLeft = function () {
                var cursorPosition = self.inputElem.selectionStart - 1;
                if (cursorPosition >= 0) {
                    self.inputElem.setSelectionRange(cursorPosition, cursorPosition);
                }
                cursor.pos = self.inputElem.selectionStart;
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            };

            self.onCursorRight = function () {
                var cursorPosition = self.inputElem.selectionStart + 1;
                self.inputElem.setSelectionRange(cursorPosition, cursorPosition);
                cursor.pos = self.inputElem.selectionStart;
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            };

            self.setSelectionRange = function (pos1, pos2) {
                self.inputElem.setSelectionRange(pos1, pos2);
                cursor.pos = self.inputElem.selectionEnd;
            };

            self.onClear = function () {
                self.inputElem.value = '';
                cursor.pos = 0;
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            };

            self.onSubmit = function (e) {
                self.eventDispatcher.dispatchEvent({ type: 'Processor.Submit', detail: { 'value': e.detail.value } });
            };

            self.dispose = function () {
                self.eventDispatcher.removeEventListener('Validator.Set', self.onSet);
                self.eventDispatcher.removeEventListener('Validator.Change', self.onChange);
                self.eventDispatcher.removeEventListener('Validator.Input', self.onInput);
                self.eventDispatcher.removeEventListener('Collector.Delete', self.onDelete);
                self.eventDispatcher.removeEventListener('Collector.CursorLeft', self.onCursorLeft);
                self.eventDispatcher.removeEventListener('Collector.CursorRight', self.onCursorRight);
                self.eventDispatcher.removeEventListener('Collector.Clear', self.onClear);
                self.eventDispatcher.removeEventListener('Validator.Submit', self.onSubmit);
                self.inputElem = null;
            };
        }
        
        return InputProcessor;
    });
