define([
    'brease/core/ContainerWidget',
    'brease/events/BreaseEvent',
    'brease/enum/Enum',
    'brease/core/Utils',
    'brease/helper/Scroller',
    'widgets/brease/GridLine/libs/Config',
    'widgets/brease/common/libs/ChildHandling',
    'widgets/brease/common/libs/ShakeElements',
    'widgets/brease/common/libs/external/Sortable',
    'widgets/brease/common/libs/ReorderingByJson',
    'widgets/brease/common/libs/redux/utils/UtilsJSON',
    'brease/decorators/DragAndDropCapability'
], function (SuperClass, BreaseEvent, Enum, Utils, Scroller, Config, ChildHandling, ShakeElements, Sortable, ReorderingByJson, UtilsJSON, dragAndDropCapability) {

    'use strict';

    /**
     * @class widgets.brease.GridLine
     * @extends brease.core.ContainerWidget
     * 
     * @mixins widgets.brease.common.DragDropProperties.libs.DroppablePropertiesEvents
     *
     * @iatMeta studio:isContainer
     * true
     * @iatMeta studio:visible
     * true
     * @iatMeta category:Category
     * Container
     * @iatMeta description:short
     * Container for GridLineItems
     * @iatMeta description:de
     * Definiert einen Container, in dem GridLineItems platziert werden.
     * @iatMeta description:en
     * Defines a container where GridLineItems are placed.
     */

    var defaultSettings = Config,

        WidgetClass = SuperClass.extend(function GridLine() {
            SuperClass.apply(this, arguments);
        }, defaultSettings),

        p = WidgetClass.prototype;

    p.init = function () {
        this.el.addClass('breaseGridLine');
        this.addEventListener();

        this.childHandling = ChildHandling;
        this.reorderingByJson = ReorderingByJson;
        this.utilsJSON = UtilsJSON;
        this.shakeElements = ShakeElements;

        SuperClass.prototype.init.call(this);

        this.settings.childrenInfo = this.childHandling.getChildren(this.el);

        this.setOrientation(this.settings.orientation);
        this.createScroller();

        var widget = this;
        this.childHandling.childrenInitDone(this.settings.childrenInfo.id, function () {
            widget.updateChildren();
            widget.updateScroller();
            widget.initDragDrop();
            widget.activateSort(false);
        });
    };

    /**
     * @method setOrientation
     * Sets orientation
     * @param {brease.enum.Direction} orientation
     */
    p.setOrientation = function (orientation) {
        this.settings.orientation = orientation;
        this.el.removeClass(Enum.Direction.horizontal + ' ' + Enum.Direction.vertical);
        this.el.addClass(orientation);
        if (brease.config.editMode) {
            this.updateChildren();
            if (this.scroller !== undefined) {
                this.destroyScroller();
                this.createScroller();
                this.updateScroller();
            }
        }
    };

    /**
     * @method getOrientation 
     * Returns orientation.
     * @return {brease.enum.Direction}
     */
    p.getOrientation = function () {
        return this.settings.orientation;
    };

    /**
     * @method setGridSize
     * Sets gridSize
     * @param {PixelVal} gridSize
     */
    p.setGridSize = function (gridSize) {
        this.settings.gridSize = parseInt(gridSize, 10);
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setGridSize', this.settings.gridSize);
        this.updateScroller();
    };

    /**
     * @method getGridSize 
     * Returns gridSize.
     * @return {PixelVal}
     */
    p.getGridSize = function () {
        return parseInt(this.settings.gridSize, 10) + 'px';
    };

    /**
     * @method setModifyImage
     * Sets modifyImage
     * @param {ImagePath} modifyImage
     */
    p.setModifyImage = function (modifyImage) {
        this.settings.modifyImage = modifyImage;
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setModifyImage', this.settings.modifyImage);
    };

    /**
     * @method getModifyImage 
     * Returns modifyImage.
     * @return {ImagePath}
     */
    p.getModifyImage = function () {
        return this.settings.modifyImage;
    };

    /**
     * @method setExpandImage
     * Sets expandImage
     * @param {ImagePath} expandImage
     */
    p.setExpandImage = function (expandImage) {
        this.settings.expandImage = expandImage;
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setExpandImage', this.settings.expandImage);
    };

    /**
     * @method getExpandImage 
     * Returns expandImage.
     * @return {ImagePath}
     */
    p.getExpandImage = function () {
        return this.settings.expandImage;
    };

    /**
     * @method setCollapseImage
     * Sets collapseImage
     * @param {ImagePath} collapseImage
     */
    p.setCollapseImage = function (collapseImage) {
        this.settings.collapseImage = collapseImage;
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setCollapseImage', this.settings.collapseImage);
    };

    /**
     * @method getCollapseImage 
     * Returns collapseImage.
     * @return {ImagePath}
     */
    p.getCollapseImage = function () {
        return this.settings.collapseImage;
    };

    /**
     * @method setModifyAnimation
     * Sets modifyAnimation
     * @param {Boolean} modifyAnimation
     */
    p.setModifyAnimation = function (modifyAnimation) {
        this.settings.modifyAnimation = modifyAnimation;
    };

    /**
     * @method getModifyAnimation 
     * Returns modifyAnimation.
     * @return {Boolean}
     */
    p.getModifyAnimation = function () {
        return this.settings.modifyAnimation;
    };

    /**
     * @method setTransitionTime
     * Sets transitionTime
     * @param {UInteger} transitionTime
     */
    p.setTransitionTime = function (transitionTime) {
        this.settings.transitionTime = transitionTime;
    };

    /**
     * @method getTransitionTime 
     * Returns transitionTime.
     * @return {UInteger}
     */
    p.getTransitionTime = function () {
        return this.settings.transitionTime;
    };

    /**
     * @method setSnapToPosition
     * Sets snapToPosition
     * @param {Boolean} snapToPosition
     */
    p.setSnapToPosition = function (snapToPosition) {
        this.settings.snapToPosition = snapToPosition;
    };

    /**
     * @method getSnapToPosition 
     * Returns snapToPosition.
     * @return {Boolean}
     */
    p.getSnapToPosition = function () {
        return this.settings.snapToPosition;
    };

    /**
     * @method setItemConfiguration
     * Sets itemConfiguration
     * @param {String} itemConfiguration
     */
    p.setItemConfiguration = function (itemConfiguration) {
        this.settings.itemConfiguration = itemConfiguration;
        this.reorderingByJson.ordering(itemConfiguration, this.scrollWrapper, this.settings.parentContentId);
        this.expandCollapseItem(this.utilsJSON.convertJSONtoObject(this.settings.itemConfiguration));
    };

    /**
     * @method getItemConfiguration 
     * Returns itemConfiguration.
     * @return {String}
     */
    p.getItemConfiguration = function () {
        this.settings.itemConfiguration = this.reorderingByJson.getOrder(this.scrollWrapper);
        return this.settings.itemConfiguration;
    };

    /**
     * @method ExpandAll
     * @iatStudioExposed
     * Expands all Childs
     */
    p.ExpandAll = function () {
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setExpand');
        this.updateScroller();
        this.submitItemConfiguration();
    };

    /**
     * @method CollapseAll
     * @iatStudioExposed
     * Expands all Childs
     */
    p.CollapseAll = function () {
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setCollapse');
        this.updateScroller();
        this.submitItemConfiguration();
    };

    /**
     * @method Expand
     * @iatStudioExposed
     * Expands item
     * @param {String} item
     */
    p.Expand = function (item) {
        this.childHandling.setParameterForChild(this.settings.parentContentId + '_' + item, 'setExpand');
        this.updateScroller();
        this.submitItemConfiguration();
    };

    /**
     * @method Collapse
     * @iatStudioExposed
     * Collapse item
     * @param {String} item
     */
    p.Collapse = function (item) {
        this.childHandling.setParameterForChild(this.settings.parentContentId + '_' + item, 'setCollapse');
        this.updateScroller();
        this.submitItemConfiguration();
    };

    /**
     * @method Modify
     * @iatStudioExposed
     * Enable the Modification of Items
     * @param {Boolean} value
     */
    p.Modify = function (value) {
        if (this.isEnabled() === false) { value = false; }
        if (value === true) {
            this.CollapseAll();
            if (this.settings.modifyAnimation === true) {
                this.shakeElements.startShaking(this.settings.childrenInfo.id);
            }
        }

        if (value === false) {
            this.shakeElements.stopShaking(this.settings.childrenInfo.id);
        }

        this.activateSort(value);
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setModify', value);
    };

    p._internalEnable = function (initial) {
        SuperClass.prototype._internalEnable.call(this);
        if (this.scroller !== undefined) {
            this.Modify(false);
        }
    };

    p.createScroller = function () {
        var containerChildren = this.container.children().detach(),
            scrollerSettings = { mouseWheel: true, tap: true, scrollY: false, scrollX: false };

        if (this.settings.orientation === Enum.Direction.horizontal) {
            scrollerSettings.scrollX = true;
            this.scrollWrapper = $('<div/>').addClass('scrollWrapper').css('height', '100%').css('width', 'auto');
        } else {
            scrollerSettings.scrollY = true;
            this.scrollWrapper = $('<div/>').addClass('scrollWrapper').css('height', 'auto').css('width', '100%');

        }

        this.scrollWrapper.append(containerChildren);
        this.container.append(this.scrollWrapper);

        this.scroller = Scroller.addScrollbars(this.container[0], scrollerSettings);
        this.scroller.refresh();
        this.scroller.on('scrollEnd', this._bind('scrollToPosition'));

        if (brease.config.editMode) {
            this.scrollerHandlingEditor();
        }

    };

    p.scrollerHandlingEditor = function () {
        var scroller = this.scroller;
        var originalFn = scroller._start;
        this.scroller._start = function (e) {
            if (Utils.closestWidgetElem(e.target).attributes['data-brease-widget'].value === 'widgets/brease/GridLine' || Utils.closestWidgetElem(e.target).attributes['data-brease-widget'].value === 'widgets/brease/GridLineItem') {
                originalFn.call(scroller, e);
            }
        };
    };

    p.scrollToPosition = function () {
        if (this.getSnapToPosition() === false) { return; }
        switch (this.getOrientation()) {
            case Enum.Direction.horizontal:
                this.scrollToPositionHorizontal();
                break;

            case Enum.Direction.vertical:
                this.scrollToPositionVertical();
                break;
        }
    };

    p.scrollToPositionHorizontal = function () {
        var width = 0, oldWidth = 0, padding = parseInt(this.el.css('padding-left'), 10), array = [0];

        for (var i = 0; i < this.settings.childrenInfo.id.length - 1; i += 1) {
            width = $('#' + this.settings.childrenInfo.id[i]).outerWidth();
            array.push(oldWidth + width + padding);
            oldWidth = oldWidth + width;
        }

        var closest = -this.getClosestNum(Math.abs(this.scroller.x), array);
        if (closest < this.scroller.maxScrollX) {
            closest = this.scroller.maxScrollX;
        }
        this.scroller.scrollTo(closest, 0, this.settings.transitionTime, 0);
    };

    p.scrollToPositionVertical = function () {
        var height = 0, oldHeight = 0, padding = parseInt(this.el.css('padding-top'), 10), array = [0];

        for (var i = 0; i < this.settings.childrenInfo.id.length - 1; i += 1) {
            height = $('#' + this.settings.childrenInfo.id[i]).outerHeight();
            array.push(oldHeight + height + padding);
            oldHeight = oldHeight + height;
        }

        var closest = -this.getClosestNum(Math.abs(this.scroller.y), array);
        if (closest < this.scroller.maxScrollY) {
            closest = this.scroller.maxScrollY;
        }
        this.scroller.scrollTo(0, closest, this.settings.transitionTime, 0);
    };

    p.getClosestNum = function (num, ar) {
        var i = 0, closest, closestDiff, currentDiff;

        if (ar.length) {
            closest = ar[0];
            for (i; i < ar.length; i += 1) {
                closestDiff = Math.abs(num - closest);
                currentDiff = Math.abs(num - ar[i]);
                if (currentDiff < closestDiff) {
                    closest = ar[i];
                }
                closestDiff = null;
                currentDiff = null;
            }
            return closest;
        }
    };

    p.updateScroller = function () {
        var scroller = this.scroller;

        setTimeout(function () {
            scroller.refresh();
        }, this.settings.transitionTime + 1);

    };

    p.destroyScroller = function () {
        var scrollWrapperChildren = this.scrollWrapper.children().detach();
        this.container.append(scrollWrapperChildren);

        this.scroller.destroy();
        this.scrollWrapper.remove();
    };

    p.updateChildren = function () {
        this.settings.childrenInfo = this.childHandling.getChildren(this.el);
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setOrientation', this.settings.orientation);
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setGridSize', this.settings.gridSize);
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setTransitionTime', this.settings.transitionTime);
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setCollapseImage', this.settings.collapseImage);
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setExpandImage', this.settings.expandImage);
        this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setModifyImage', this.settings.modifyImage);

        if (brease.config.editMode) {
            this.childHandling.setSameParameterForAllChilds(this.settings.childrenInfo.id, 'setExpand');
        } else {
            var widget = this;
            this.settings.childrenInfo.id.forEach(function (id) {
                var state = brease.callWidget(id, 'getShowExpanded');
                if (state === true) {
                    widget.childHandling.setParameterForChild(id, 'setExpand');
                } else {
                    widget.childHandling.setParameterForChild(id, 'setCollapse');
                }
            });
        }
    };

    p.widgetAddedHandler = function (e) {
        this.updateChildren();
        this.updateScroller();
    };

    p.widgetRemovedHandler = function (e) {
        this.updateChildren();
        this.updateScroller();
    };

    p._setWidth = function (w) {
        SuperClass.prototype._setWidth.call(this, w);
        this.destroyScroller();
        this.createScroller();
        this.updateScroller();
    };

    p._setHeight = function (h) {
        SuperClass.prototype._setHeight.call(this, h);
        this.destroyScroller();
        this.createScroller();
        this.updateScroller();
    };

    p.wake = function () {
        SuperClass.prototype.wake.apply(this, arguments);
    };

    p.suspend = function () {
        if (!brease.config.preLoadingState) {
            this.Modify(false);
        }
        SuperClass.prototype.suspend.apply(this, arguments);
    };

    // override method called in BaseWidget.init
    p._initEditor = function () {
        var widget = this;
        require(['widgets/brease/GridLine/libs/EditorHandles'], function (EditorHandles) {
            var editorHandles = new EditorHandles(widget);
            widget.getHandles = function () {
                return editorHandles.getHandles();
            };
            widget.designer.getSelectionDecoratables = function () {
                return editorHandles.getSelectionDecoratables();
            };
        });
    };

    p.submitItemConfiguration = function () {
        this.sendValueChange({ itemConfiguration: this.getItemConfiguration() });
    };

    p.expandCollapseItem = function (itemConfig) {
        var contentId = this.settings.parentContentId,
            widget = this;

        if (Array.isArray(itemConfig)) {
            itemConfig.forEach(function (item) {
                if (item.s === 1) {
                    widget.childHandling.setParameterForChild(contentId + '_' + item.wRef, 'setExpand');
                } else {
                    widget.childHandling.setParameterForChild(contentId + '_' + item.wRef, 'setCollapse');
                }
            });
        }
    };

    p.triggerModifiedEvent = function () {
        /**
         * @event Modified
         * @iatStudioExposed
         * Fired when the sort-order is changed by User interaction
         */
        var ev = this.createEvent('Modified');
        ev.dispatch();
    };

    p.userUpdate = function () {
        this.submitItemConfiguration();
        this.triggerModifiedEvent();
    };

    p.addEventListener = function () {
        this.el.on(BreaseEvent.DBL_CLICK, this._bind('triggerDoubleClickEvent'));
    };

    p.triggerDoubleClickEvent = function () {
        if (this.isDisabled || brease.config.editMode) { return; }

        /**
         * @event DoubleClick
         * @iatStudioExposed
         * Fired when element has double click.
         */
        var clickEv = this.createEvent('DoubleClick');
        clickEv.dispatch();
    };
    //DragDrop Handling

    p.initDragDrop = function () {
        this.sortable = new Sortable(this.scrollWrapper[0], { scroll: false, draggable: '.breaseGridLineItem', onUpdate: this._bind('userUpdate'), onStart: this._bind('dragStart'), onEnd: this._bind('dragEnd') });
    };

    p.dragStart = function () {
        this.scroller.disable();
    };

    p.dragEnd = function () {
        this.scroller.enable();
    };

    p.activateSort = function (enable) {
        this.sortable.option('disabled', !enable);
    };

    return dragAndDropCapability.decorate(WidgetClass, false);
});
