define([],
    function () {

        'use strict';

        /**
         * @class widgets.brease.GridLineItem.Config
         * @extends core.javascript.Object
         * @override widgets.brease.GridLineItem
         */

        /**
         * @cfg {UInteger} expandedSize=2
         * @iatStudioExposed
         * @iatCategory Appearance
         * Defines how many grid areas are used in expanded mode.  
         */

        /**
         * @cfg {UInteger} collapsedSize=1
         * @iatStudioExposed
         * @iatCategory Appearance
         * Defines how many grid areas are used in collapsed mode.  
         */

        /**
         * @cfg {Boolean} showExpanded=false
         * @iatStudioExposed
         * @iatCategory Behavior
         * Defines the initial state of an item.  
         */

        /**
         * @cfg {brease.enum.HorizontalAlign} imageAlign='right'
         * @iatStudioExposed
         * @iatCategory Appearance
         * Defines where the image should be displayed on top. Possible input: left and right.  
         */

        /**
         * @property {WidgetList} [parents=["widgets.brease.GridLine"]]
         * @inheritdoc
         */

        return {
            expandedSize: 2,
            collapsedSize: 1,
            showExpanded: false,
            imageAlign: 'right',
            status: 0,
            setModify: false
        };

    });
