define(['brease/events/BreaseEvent'],
    function (BreaseEvent) {

        'use strict';
        function FocusManager() {
            var self = this,
                cursor;

            self.init = function (eventDispatcher, widget, sharedCursor) {
                cursor = sharedCursor;
                self.widget = widget;
                self.eventDispatcher = eventDispatcher;
                self.eventDispatcher.addEventListener('Keyboard.Hide', self.onHide);
                self.eventDispatcher.addEventListener('Keyboard.Show', self.onShow);
                self.eventDispatcher.addEventListener('Input.Focus', self.inputFocus);
                self.inputEl = widget.inputEl;
                self.inputElem = widget.inputElem;
            };

            self.onShow = function () {
                _setInitialFocus.call(self);
                self.inputEl.one('focusout', function (e) {
                    inputFocus.call(self);
                });
                self.inputEl.on('click', function (e) {
                    cursor.pos = self.inputElem.selectionStart;
                });
            };

            self.onHide = function () {
                self.inputBlur();
                self.inputEl.off('click');
                window.clearTimeout(self.timeout);
            };

            self.dispose = function () {
                self.eventDispatcher.removeEventListener('Keyboard.Hide', self.onHide);
                self.eventDispatcher.removeEventListener('Keyboard.Show', self.onShow);
                self.eventDispatcher.removeEventListener('Input.Focus', self.inputFocus);
                self.inputEl.off();
                self.inputEl = null;
                self.inputElem = null;
                self.widget = null;
            };

            self.inputFocus = function () {
                inputFocus.call(self);
            };

            self.inputBlur = function () {
                self.inputEl.blur(); 
                window.clearTimeout(self.timeout);
            };

            function inputFocus() {
                var self = this;
                if (self.widget && self.widget.data && self.widget.data.open === true) {
                    self.timeout = window.setTimeout(function () { 
                        if (self.inputElem && self.inputElem !== document.activeElement) {
                            _setFocus.call(self, cursor.pos);
                        }
                    }, 100); 
                }
            }
        }

        function _setInitialFocus() {
            _setFocus.call(this, this.inputElem.value.length);
        }

        function _setFocus(pos) {
            this.inputElem.setSelectionRange(pos, pos);
            if (!brease.config.detection.mobile) {
                this.inputEl.focus();
            }
        }
        
        return FocusManager;
    });
