define([], function () {
    'use strict';
    function DialogueTexts() {
        return {
            en: {
                filter: {
                    title: 'Configuration dialogue to filter the audits',
                    col: 'Column',
                    op: 'Operator',
                    val: 'Value',
                    and: 'and',
                    or: 'or'
                },
                sort: {
                    title: 'Configuration dialog to sort the audits',
                    col: 'Column',
                    sort: 'Sort',
                    by: 'By',
                    first: 'First',
                    then: 'then',
                    inc: 'increasing',
                    dec: 'decreasing',
                    az: 'A - Z',
                    za: 'Z - A',
                    old: 'oldest first',
                    new: 'newest first'
                },
                style: {
                    title: 'Configuration dialogue to style the audits',
                    and: 'and',
                    or: 'or',
                    style: 'Set style',
                    state: 'if audit is',
                    au1: 'diagnostics',
                    au2: 'user management',
                    au3: 'electronic signature',
                    au16: 'value change',
                    au17: 'value change VC4',
                    au18: 'audit custom',
                    au19: 'value change OPC',
                    au20: 'batch start',
                    au32: 'AlarmX state change',
                    au33: 'AlarmX audit',
                    au48: 'recipe audit',
                    au64: 'PackML mode change',
                    au65: 'PackML state change',
                    au80: 'tweet message service',
                    au96: 'hardware import',
                    au112: 'codeBox program',
                    au2457: 'audit cleared'
                }
            },
            de: {
                filter: {
                    title: 'Konfigurationsdialog für das Filtern der Auditliste',
                    col: 'Spalte',
                    op: 'Operator',
                    val: 'Wert',
                    and: 'und',
                    or: 'oder'
                },
                sort: {
                    title: 'Konfigurationsdialog zum Sortieren der Auditliste',
                    col: 'Spalte',
                    sort: 'Sortieren',
                    by: 'Nach',
                    first: 'Zuerst',
                    then: 'dann',
                    inc: 'aufsteigend',
                    dec: 'absteigend',
                    az: 'A - Z',
                    za: 'Z - A',
                    old: 'älteste',
                    new: 'neueste'
                },
                style: {
                    title: 'Konfigurationsdialog zum Stylen der Auditliste',
                    and: 'und',
                    or: 'oder',
                    style: 'Set style',
                    state: 'if event is of type',
                    au1: 'Diagnose',
                    au2: 'Benutzerverwaltung',
                    au3: 'elektronische Unterschrift',
                    au16: 'Wertänderung',
                    au17: 'Wertänderung VC4',
                    au18: 'benutzerdefiniertes Audit',
                    au19: 'Wertänderung OPC',
                    au20: 'Batchstart',
                    au32: 'AlarmX-Statusänderung',
                    au33: 'AlarmX-Audit',
                    au48: 'Recipe Audit',
                    au64: 'PackML-Modusänderung',
                    au65: 'PackML-Statusänderung',
                    au80: 'Tweet-Nachrichtendienst',
                    au96: 'Hardware importieren',
                    au112: 'CodeBox Programm',
                    au999: 'Audit gelöscht'
                }
            }
        };
    }
    
    return new DialogueTexts();
});
