define([
    'widgets/brease/TableColumnWidget/TableColumnWidget',
    'brease/events/BreaseEvent',
    'brease/core/Types',
    'brease/enum/Enum'
], function (SuperClass, BreaseEvent, Types, Enum) {

    'use strict';

    /**
     * @class widgets.brease.AuditListItem
     * @extends widgets.brease.TableColumnWidget
     *
     * @iatMeta category:Category
     * Data
     *
     * @iatMeta description:short
     * AuditListItem, widget used to set the columns wanted to be shown in the AuditList
     * @iatMeta description:de
     * AuditListItem, widget used to set the columns wanted to be shown in the AuditList
     * @iatMeta description:en
     * AuditListItem, widget used to set the columns wanted to be shown in the AuditList
     */

    /**
     * @property {WidgetList} [parents=["widgets.brease.AuditList"]]
     * @inheritdoc  
     */

    /**
     * @cfg {brease.enum.AuditListItemType} columnType='text'
     * @iatStudioExposed
     * @iatCategory Behavior
     * Type the Auditlist column will display
     */

    var defaultSettings = {
            text: '',
            columnType: Enum.AuditListItemType.tex
        },

        WidgetClass = SuperClass.extend(function AuditListItem() {
            SuperClass.apply(this, arguments);
        }, defaultSettings),

        p = WidgetClass.prototype;

    p.init = function () {
        SuperClass.prototype.init.call(this);
        this.el.addClass('breaseAuditListItem');
    };

    /**
     * @method setColumnType
     * Sets columnType
     * @param {brease.enum.AuditListItemType} columnType
     */
    p.setColumnType = function (columnType) {
        this.settings.columnType = columnType;
        var event = new CustomEvent('ColumnTypeChanged', { detail: { columnType: Enum.AuditListItemType.getKeyForValue(columnType) }, bubbles: true, cancelable: true });
        this.dispatchEvent(event);
    };

    /**
     * @method getColumnType 
     * Returns columnType.
     * @return {brease.enum.AuditListItemType}
     */
    p.getColumnType = function () {
        return this.settings.columnType;
    };

    p.getShortColumnType = function () {
        return Enum.AuditListItemType.getKeyForValue(this.settings.columnType);
    };

    p.setData = function (telegram) {
        this.telegram = telegram;
    };

    return WidgetClass;
});
