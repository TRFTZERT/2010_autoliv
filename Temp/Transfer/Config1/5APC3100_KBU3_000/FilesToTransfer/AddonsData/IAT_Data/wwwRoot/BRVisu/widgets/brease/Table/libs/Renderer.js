define([
    'brease/core/Class',
    'widgets/brease/common/libs/external/jquery.dataTables',
    'brease/enum/Enum',
    'brease/events/BreaseEvent',
    'brease/core/Types',
    'widgets/brease/Table/libs/InputHandler',
    'widgets/brease/Table/libs/ScrollHandler',
    'widgets/brease/common/libs/wfUtils/UtilsImage'
], function (
    SuperClass, dataTables, Enum, BreaseEvent, 
    Types, InputHandler, ScrollHandler, UtilsImage
) {

    'use strict';

    /**
     * @class widgets.brease.Table.libs.Renderer
     * @extends brease.core.Class
     */

    var Renderer = SuperClass.extend(function Renderer(widget, options) {
            SuperClass.call(this);
            this.widget = widget;
            this.settings = widget.settings;
            this.options = options;
            this.initialize();
        }, null),

        p = Renderer.prototype;

    p.initialize = function () {
        //change table base class
        $.fn.dataTable.ext.classes.sTable = 'dataTable';

        //set dataTables to throw errors in console
        $.fn.dataTableExt.sErrMode = this.options.errorMode;
        this.debouncedUpdateItemClasses = _.debounce(this.updateItemClassesDebounced.bind(this), 200);

        _addTableNode(this);

        this.busy = false;

        this.initScrollHandler();

        if (!brease.config.editMode) {
            this.inputHandler = new InputHandler(this.widget, this);
        }
    };

    p.initScrollHandler = function () {
        this.scrollHandler = new ScrollHandler(this.widget, this, this.options);
    };

    p.updateEditor = function () {
        if (this.tableHeaderEl !== undefined) {
            this.tableHeaderEl.parent().remove();
        }
        _createHeaderWrapper(this);
        _generate(this);
        this.scrollHandler.initializeScroller(false, true);
        this.scrollHandler._refreshScroller();

        this.updateTable();
    };

    p.setInitData = function (data) {
        if (this.tableHeaderEl !== undefined) {
            this.tableHeaderEl.parent().remove();
            this.scrollHandler.removeScroller();
        }
        _createHeaderWrapper(this);
        _generate(this);
        this.scrollHandler.initializeScroller(true, false);
        this.scrollHandler._refreshScroller();
    };

    p.updateTable = function () {
        if (this.table === undefined) {
            _buildTable(this);
        } else {
            this.updateTableSize();
        }
    };

    p.updateTableSize = function () {
        if (this.table !== undefined) {
            _clearTable(this);
            _addTableNode(this);
            _rebuildTable(this);
        }
    };

    p.updateRow = function (data, rowIndex) {
        this.setRendererBusy();
        var renderer = this;
        _updateRowData(this.settings.tableData, data, rowIndex);
        $.each(this.table.row(rowIndex).nodes().toJQuery().children(), function (index, node) {
            if (node.className.indexOf('hidden') === -1) {
                var t = renderer.settings.tableData[rowIndex][index];
                t = renderer._prepareString(rowIndex, t);
                node.innerHTML = t;
            }
        });

        _drawCallback.call(this);
    };

    p.updateColumn = function (data, columnIndex) {
        this.setRendererBusy();
        var renderer = this;
        _updateColumnData(this.settings.tableData, data, columnIndex);
        $.each(this.table.cells(null, columnIndex).nodes().toArray(), function (index, node) {
            if (node.className.indexOf('hidden') === -1) {
                var t = renderer.settings.tableData[index][columnIndex];
                t = renderer._prepareString(columnIndex, t);
                node.innerHTML = t;
            }
        });

        _drawCallback.call(this);
    };

    p.updateCell = function (rowIndex, columnIndex, value, cellElem) {
        this.setRendererBusy();
        this.table.cell(cellElem._DT_CellIndex.row, cellElem._DT_CellIndex.column).data(value);
        if (typeof rowIndex === 'number' && typeof columnIndex === 'number') {
            cellElem = this.table.cell(rowIndex, columnIndex).node();
        }
        if (cellElem !== undefined && cellElem.className.indexOf('hidden') === -1) {
            value = this._prepareString(columnIndex, value);
            cellElem.innerHTML = value;
        }

        _drawCallback.call(this);
    };

    p._prepareString = function (colIndex, t) {
        if (colIndex === null) {
            colIndex = this.inputHandler.itemIndex;
        }
        if (this.widget.settings.tableItemTypes[colIndex] === 'widgets/brease/TableItem') {
            //Optimize so we only do split/reverse/join x2 if there is a breakline available
            if (t.includes('<br>')) {
                t = t.replace(/<(?!br>)/g, '&lt;'); //Regex for negative lookahead i.e. all < not followed by a 'br />'
                //ECMA5 does not support negative lookbehind, therefore we have to reverse the string and apply a negative lookahead and reverse the string again
                //This is guaranteed a culprit on the speed and should be removed as soon as possible and replaced with the next optimized line
                //t = t.replace(/(?<!\<br \/)/g, '&gt;');  //Regex for negative lookbehind, i.e. all > not preceeded by '<br /'
                t = t.split('').reverse().join('');
                t = t.replace(/>(?!rb<)/g, ';tg&');
                t = t.split('').reverse().join('');
            } else {
                t = t.replace(/</g, '&lt;');                
                t = t.replace(/>/g, '&gt;');
            }
        }
        if (this.settings.multiLine) {
            t = t.replace(/\\n/g, '<br>');
        }
        return t;
    };

    p._itemSelectHandler = function (e) {
        e.stopPropagation();

        var selectedTarget,
            selectedItems;

        if (e.target.tagName !== 'TD') {
            selectedTarget = $(e.target).parent();
        } else {
            selectedTarget = e.target;
        }

        if ((this.widget.isDisabled) || $(selectedTarget).hasClass('disabled')) {
            return;
        }

        if (this.previouslySelectedItems) {
            this.previouslySelectedItems.removeClass('selected');
            this.previouslySelectedItems.children().removeClass('selected');
        }

        if (this.options.selectableItem === 'row' && this.settings.selection) {
            selectedItems = this.table.row(selectedTarget).nodes().toJQuery();
            selectedItems.addClass('selected');
            selectedItems.children().addClass('selected');
        } else if (this.options.selectableItem === 'column' && this.settings.selection) {
            selectedItems = this.table.column(selectedTarget).nodes().toJQuery();
            selectedItems.addClass('selected');
        }
        this.previouslySelectedItems = selectedItems;

        this.widget[this.options.selectionCallbackFn](
            {
                event: e,
                type: this.options.selectableItem,
                rowIndex: this.table['row'](selectedTarget).index(),
                columnIndex: this.table['column'](selectedTarget).index()
            });

        /**
         * @event SelectedRowChanged
         * Triggered when any value in the table is changed
         * Used only by the Database widget!
         */
        var ev = this.widget.createEvent('SelectedRowChanged', { 'itemSelected': selectedItems });
        ev.dispatch();
        
    };

    p.selectItem = function (type, index, deselectOther) {
        var itemsToSelect;

        if (deselectOther) {
            if (this.previouslySelectedItems) {
                this.previouslySelectedItems.removeClass('selected');
                this.previouslySelectedItems.children().removeClass('selected');
            }
        }

        if (type === 'row' && this.settings.selection) {
            itemsToSelect = this.table.row(index).nodes().toJQuery();
            if (itemsToSelect.hasClass('disabled') === false) {
                itemsToSelect.addClass('selected');
                itemsToSelect.children().addClass('selected');
            }
        } else if (type === 'column' && this.settings.selection) {
            itemsToSelect = this.table.column(index).nodes().toJQuery();
            if (itemsToSelect.hasClass('disabled') === false) {
                itemsToSelect.addClass('selected');
            }
        }

        this.previouslySelectedItems = itemsToSelect;

    };

    /**
     * @method updateItemClasses
     * The starting point!
     * This method will iterate over all rows and all columns and apply the correct classes to display the table correctly.
     * This method has turned into a monster, and I appologise for that. The problem is that data flows into the Table from
     * so many different direction that no matter how you try to tackle this it's going to be a mess. First out we have the
     * TableItems, these can either be visible or disabled. Then there is the tableConfiguration, the tableConfiguration can
     * either hide or disable rows from in sections or with individual indicies. When a column is removed, the coltable also 
     * has to be updated with the correct values because otherwise the width of the table will be incorrect and you can either
     * scroll too far or not far enough. The header is another chapter that is decoupled from the other table. But it has the
     * same properties as the table. To add the cherry on top the table also has to work in horizontal and vertical mode.
     */
    p.updateItemClasses = function () {
        this.debouncedUpdateItemClasses();
    };

    p.updateItemClassesDebounced = function () {
        //Stop trailing debounce function in test
        if (!this.tableDataInfo || !this.widget.elem) return;
        
        this.setRendererBusy();

        //Prelude
        var headerCol, headerConfigType, useItem;
        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            headerCol = this.tableDataInfo.columns;
            headerConfigType = 'specColumns';
            useItem = false;
        } else {
            headerCol = this.tableDataInfo.rows;
            headerConfigType = 'specRows';
            useItem = true;
        }

        //Re-add all cols to the colgroup, that way we only have to remove
        var table = this.widget.elem.getElementsByTagName('table')[0];
        var colgroup = this.widget.elem.getElementsByTagName('colgroup')[0];
        this._removeColGroup(colgroup);
        this._addColGroup(table);

        for (var h = 0; h < headerCol; h += 1) {
            var header = this.tableHeaderEl.children()[h],
                vis = this._checkHeader(h, header, 'itemVisibility', headerConfigType, 'visible', 'hidden');
            if (vis) {
                this._checkHeader(h, header, 'itemEnableStates', headerConfigType, 'disable', 'disabled');
                this._applyIndStyle(h, header);
            }
        }

        this.settings.hiddenColumns = [];
        this.settings.hiddenRows = [];
        
        if (this.table === undefined) {
            this.table = this.tableBodyEl.DataTable();
        }

        /*jshint -W018 */
        var optimized = !(typeof this.settings.tableConfiguration === 'object');// && this.settings.filter.length > 0);
        /*jshint +W018 */

        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            this._updateVerticalFunctional(useItem, optimized);
        } else {
            this._updateHorizontalFunctional(useItem, optimized);
        }

        var totRows = this.widget.elem.getElementsByTagName('tr').length,
            totCols = this.widget.elem.getElementsByTagName('td').length / totRows;
            
        // if (!optimized) {
        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            if (!optimized) this._updateVerticalCosmetic(totCols, totRows);
        } else {
            this._updateHorizontalCosmetic(totCols, totRows);
        }
        // }
        
        this.setRendererReady();
        // console.log('function finished...')
        this.scrollHandler._refreshScroller();
    };

    p._updateHorizontalFunctional = function (useItem, optimized) {
        for (var y = 0; y < this.tableDataInfo.rows; y += 1) {
            var node;
            if (optimized) {
                node = this.widget.elem.getElementsByTagName('tr')[y];
            } else {
                node = this.table.row(y).node();
            }
            if (!node) continue;
            var row = this._checkRow(y, node, 'itemVisibility', 'visible', 'hidden', useItem);
            // this._applyOverride(this.table.row(y).node());
            if (row || brease.config.editMode) {
                var rowDis = this._checkRow(y, node.children, 'itemEnableStates', 'disable', 'disabled');

                for (var x = 0; x < this.tableDataInfo.columns; x += 1) {
                    var cellNode;
                    if (optimized) {
                        cellNode = this.widget.elem.getElementsByTagName('td')[((y * this.tableDataInfo.columns) + x)];
                    } else {
                        cellNode = this.table.cell(y, x).node();
                    }
                    var vis = this._checkCell(x, y, cellNode, 'itemVisibility', 'visible', 'hidden');
                    if ((vis && rowDis) || brease.config.editMode) {
                        this._checkCell(x, y, cellNode, 'itemEnableStates', 'disable', 'disabled');
                    }
                }
            }
        }
    };

    p._updateHorizontalCosmetic = function (cols, rows) {
        var cellActual = 0, rowActual = 0;
        for (var y = 0; y < rows; y++) {
            var node = this.widget.elem.getElementsByTagName('tr')[y];
            if (node && !node.classList.contains('hidden')) {
                this._addRowOddEven(rowActual, node);
                for (var x = 0; x < cols; x++) {
                    var cellNode = this.widget.elem.getElementsByTagName('td')[((y * this.tableDataInfo.columns) + x)];
                    if (cellNode && !cellNode.classList.contains('hidden')) {
                        this._addRowOddEven(cellActual, cellNode);
                        this._applyIndStyle(y, cellNode);
                        cellActual += 1;
                    }
                }
                cellActual = 0;
                rowActual += 1;
            }
        }
    };

    p._updateVerticalFunctional = function (useItem, optimized) {
        var rowActual = 0;
        for (var y = 0; y < this.tableDataInfo.rows; y += 1) {
            var node;
            if (optimized) {
                node = this.widget.elem.getElementsByTagName('tr')[y];
            } else {
                node = this.table.row(y).node();
            }
            if (!node) continue;
            var row = this._checkRow(y, node, 'itemVisibility', 'visible', 'hidden', useItem);
            if (row || brease.config.editMode) {
                if (optimized) {
                    this._addRowOddEven(rowActual, node);
                }
                var rowDis = this._checkRow(y, node.children, 'itemEnableStates', 'disable', 'disabled');

                for (var x = 0; x < this.tableDataInfo.columns; x += 1) {
                    //Magic
                    var cellNode;
                    if (optimized) {
                        cellNode = this.widget.elem.getElementsByTagName('td')[((y * this.tableDataInfo.columns) + x)];
                    } else {
                        cellNode = this.table.cell(y, x).node();
                    }
                    var vis = this._checkCell(x, x, cellNode, 'itemVisibility', 'visible', 'hidden');
                    if ((vis && rowDis) || brease.config.editMode) {
                        this._checkCell(x, x, cellNode, 'itemEnableStates', 'disable', 'disabled');
                        if (optimized) {
                            this._addRowOddEven(rowActual, cellNode);
                            this._applyIndStyle(x, cellNode);
                        }
                    }
                }
                rowActual += 1;
            }
        }
    };
    
    p._updateVerticalCosmetic = function (cols, rows) {
        var rowActual = 0;
        for (var y = 0; y < rows; y++) {
            var node = this.widget.elem.getElementsByTagName('tr')[y];
            if (node && !node.classList.contains('hidden')) {
                this._addRowOddEven(rowActual, node);
                for (var x = 0; x < cols; x++) {
                    var cellNode = this.widget.elem.getElementsByTagName('td')[((y * this.tableDataInfo.columns) + x)];
                    this._addRowOddEven(rowActual, cellNode);
                    this._applyIndStyle(x, cellNode);
                }
                rowActual += 1;
            }
        }
    };

    /**
     * @method _checkHeader
     * @private
     * This method will check if a header cell should have a class, disabled or hidden, applied to it - depending on what is passed.
     * @param {UInteger} item the index of the item that should be checked.
     * @param {HTMLElement|HTMLElement[]} cellNode the node as a HTMLElement which should be modified.
     * @param {String} cellType the type of item which should be compared to. Valid entries are 'itemEnableStates' or 'itemVisibility'.
     * @param {String} configType which part of the configuration should be used. Valid entries are 'specRows' or 'specColumns'.
     * @param {String} type the type of check being performed, valid entries are 'disable' and 'visible'.
     * @param {String} cls the class which should be applied to the cellNode.
     * @returns {Boolean} value returned by the _handleClasses.
     */
    p._checkHeader = function (item, cellNode, cellType, configType, type, cls) {
        var show = this._findElementInConfig(item, configType, type);
        return this._handleClasses(cls, cellNode, this.settings[cellType][item] && show);
    };

    /**
     * @method _checkRow
     * @private
     * This method will check if a row should have a class, disabled or hidden, applied to it - depending on what is passed.
     * @param {UInteger} item the index of the item that should be checked.
     * @param {HTMLElement|HTMLElement[]} rowNode the node as a HTMLElement which should be modified.
     * @param {String} type the type of check being performed, valid entries are 'disable' and 'visible'.
     * @param {String} cls the class which should be applied to the cellNode.
     * @param {Boolean} useItem decides if the item state has to be taken into consideration to determine the state of a row or cell
     * @returns {Boolean} value returned by the _handleClasses.
     */
    p._checkRow = function (item, cellNode, cellType, type, cls, useItem) {
        useItem = (useItem) ? this.settings[cellType][item] : true;
        var show = this._findElementInConfig(item, 'specRows', type);
        return this._handleClasses(cls, cellNode, useItem && show, item, 'specRows');
    };

    /**
     * @method _checkCell
     * @private
     * This method will check if a cell should have a class, disabled or hidden, applied to it - depending on what is passed.
     * @param {UInteger} x the current column that is being investigated (OBS! not the same as item if we are in horizontal mode).
     * @param {UInteger} item the index of the item that should be checked.
     * @param {HTMLElement|HTMLElement[]} cellNode the node as a HTMLElement which should be modified.
     * @param {String} cellType the type of item which should be compared to. Valid entries are 'itemEnableStates' or 'itemVisibility'.
     * @param {String} type the type of check being performed, valid entries are 'disable' and 'visible'.
     * @param {String} cls the class which should be applied to the cellNode.
     * @returns {Boolean} value returned by the _handleClasses.
     */
    p._checkCell = function (x, item, cellNode, cellType, type, cls) {
        var show = this._findElementInConfig(x, 'specColumns', type);
        return this._handleClasses(cls, cellNode, this.settings[cellType][item] && show, x, 'specColumns');
    };

    /**
     * @method _addRowOddEven
     * @private
     * This method will add the classes either odd or even depending on the value pass on the parameter val and add this to the node parameter.
     * @param {UInteger} val value that corresponds to the row in the table-
     * @param {HTMLElement|HTMLElement[]} node the node as a HTMLElement which should be modified.
     *
     */
    p._addRowOddEven = function (val, node) {
        var cls = (val % 2 === 0) ? 'odd' : 'even';
        if (node.length !== undefined) {
            for (var i = 0; i < node.length; i += 1) {
                this._addRowOddEvenHelper(cls, node[i]);
            }
        } else {
            this._addRowOddEvenHelper(cls, node);
        }
    };

    p._addRowOddEvenHelper = function (cls, node) {
        node.classList.remove('odd');
        node.classList.remove('even');
        this._handleClasses(cls, node, false);
    };

    /**
     * @method _handleClasses
     * @private
     * This method will add or remove the classes passed on the cls parameter to the node by using the helper function
     * @param {String} cls class to be added to the node.
     * @param {HTMLElement|HTMLElement[]} node the node as a HTMLElement which should be modified.
     * @param {Boolean} comp value used in the helper function to add or remove class passed.
     * @param {String} configDirection tells us which direction the configuration is at currently either specRows or specColumns.
     * @returns {Boolean}
     */
    p._handleClasses = function (cls, node, comp, x, configDirection) {
        if (!node) return;
        if (node.length !== undefined) {
            for (var i = 0; i < node.length; i += 1) {
                this._helperHandleClass(cls, node[i], comp, x);
            }
        } else {
            this._helperHandleClass(cls, node, comp, x);
        }

        if (cls === 'hidden' && configDirection === 'specColumns' && x !== undefined && this.settings.dataOrientation === Enum.Direction.vertical) this._updateColumnsVert(!comp, x);
        return comp;
    };

    /**
     * @method _helperHandleClass
     * @private
     * This method is a helper class to the _handleClasses method. It will add or remove the class to the node that is passed.
     * @param {String} cls class passed to the function.
     * @param {HTMLElement|HTMLElement[]} node the node as a HTMLElement which should be modified.
     * @param {Boolean} comp if true the class will be removed otherwsie the class will be added.
     * @param {UInteger} x position in the array, only necessary for when a class should be hidden and we are in horizontal mode
     * @returns {Boolean} returns the comp value as is to be used further up the foodchain.
     */
    p._helperHandleClass = function (cls, node, comp, x) {
        if (brease.config.editMode && (cls === 'hidden' || cls === 'disabled')) return;
        if (comp) {
            node.classList.remove(cls);
        } else {
            node.classList.add(cls);
        }
    };

    /**
     * @method _applyIndStyle
     * @private
     * This class will apply the inividual style to the corresponding row, if the table is not using table styling (i.e. using table item styling).
     * @param {UInteger} item the corresponding item in the table configuration
     * @param {HTMLElement} node the node as a HTMLE<lement which should be modified.
     */
    p._applyIndStyle = function (item, cellNode) {
        if (!this.settings.useTableStyling) {
            var es = this.settings.tableItemTypes[item].split('/');
            cellNode.classList.add('widgets_' + es[1] + '_' + es[2] + '_style_' + this.settings.itemStyling[item]);
            cellNode.classList.add('override');
        }
    };
    /**
     * @method _applyOverride
     * @private
     * This class will apply the override class to the node that's being passed, if the table is not using table styling (i.e. using table item styling).
     * @param {HTMLElement} node the node as a HTMLE<lement which should be modified.
     */
    p._applyOverride = function (cellNode) {
        if (!this.settings.useTableStyling) {
            cellNode.classList.add('override');
        }
    };

    /**
     * @method _findElementInConfig
     * This function will iterate over all the indeces in the list in the tableConfiguration to find out if the current cell is in the configuration or not
     * @param {UInteger} index the index we want to find in the configuration
     * @param {String} direction the direction of the configuration, can either be specRows or specColumns
     * @param {String} type the type of check being performed, valid entries are 'disable' and 'visible'
     * @returns {Boolean} the value returned is that the cell is either disabled or visible
     */
    p._findElementInConfig = function (index, direction, type) {
        var conf = (this.settings.tableConfiguration !== undefined && this.settings.tableConfiguration[direction] !== undefined) ? this.settings.tableConfiguration[direction] : [];
        var confSingle = conf.find(function (c) { return c.index === index; }),
            confMulti = conf.find(function (c) {
                return (c.from <= index && c.to >= index) || (c.from <= index && c.to === undefined) || (c.from === undefined && c.to >= index);
            }),
            cellDisOrVis;
        confSingle = (confSingle === undefined) ? {} : confSingle;
        confMulti = (confMulti === undefined) ? {} : confMulti;

        if (type === 'disable') {
            cellDisOrVis = (this.widget.isDisabled || confSingle[type] === true || confMulti[type] === true);
        } else {
            cellDisOrVis = (confSingle[type] === false || confMulti[type] === false);
            this._updateColumnsHorz(cellDisOrVis, index, direction);
        }

        return !cellDisOrVis;
    };

    /**
     * @method _updateColumnsHorz
     * @private
     * This method will take information from the _findElementInConfig method and determine if a column needs to be removed from the colgroup
     * so that the width of the table corresponds to the number of visible columns. It then updates the width of the table. ONLY for horizontal
     * direction.
     * @param {Boolean} conf value the configuration has decided, true if it should be hidden else false
     * @param {UInteger} index the index that should be removed. Index is stored so the same column isn't removed twice.
     * @param {String} direction the direction of the configuration, can either be specRows or specColumns
     */
    p._updateColumnsHorz = function (conf, index, direction) {
        if (direction === 'specColumns' && this.settings.dataOrientation === Enum.Direction.horizontal && conf && !this.settings.hiddenColumns.includes(index)) {
            this.settings.hiddenColumns.push(index);
            this.widget.el.find('col:last-child').remove();
            this.widget.el.find('table').width(this.widget.el.find('table').width() - this.settings.columnWidth);
        }
    };
    /**
     * @method _updateColumnsVert
     * @private
     * This method will take information from the _findElementInConfig method and determine if a column needs to be removed from the colgroup
     * so that the width of the table corresponds to the number of visible columns. It then updates the width of the table. ONLY for vertical
     * direction.
     * @param {Boolean} conf value the configuration has decided, true if it should be hidden else false
     * @param {UInteger} index the index that should be removed. Index is stored so the same column isn't removed twice.
     */
    p._updateColumnsVert = function (conf, index) {
        if (brease.config.editMode) return;
        if (conf) {
            if (!this.settings.hiddenColumns.includes(index)) {
                this.settings.hiddenColumns.push(index);
                this.widget.el.find('col[br-index=' + index + ']').remove();
            }
        } else {
            var ind = this.settings.hiddenColumns.indexOf(index);
            if (ind > -1) {
                this.settings.hiddenColumns.splice(ind, 1);
                var col = document.createElement('col');
                col.width = this._getColumnWidth(index) + 'px';
                col.setAttribute('br-index', index);
                var colgrp = this.widget.elem.getElementsByTagName('colgroup')[0];
                colgrp.insertBefore(col, colgrp.children[index]);
            }
        }

        //Update width
        var totalWidth = 0;
        this.widget.el.find('col[br-index]').each(function (index, item) {
            totalWidth += parseInt(item.width);
        });
        this.widget.el.find('table').width(totalWidth);
    };

    /**
     * @method _getColumnWidth
     * @private
     * This method will return the column width for the colgroup for a given item index. If the table is in horizontal mode
     * we don't need to care as it's only the vertical orientation that can take on diffferent column sizes.
     * @param {UInteger} i the index of the column that needs to be looked up in the list 
     * @returns {UInteger} the width of the given item
     */
    p._getColumnWidth = function (i) {
        var w = (this.settings.dataOrientation === Enum.Direction.vertical && 
            this.settings.itemColumnWidths[i] !== undefined &&
            this.settings.itemColumnWidths[i] !== 0) ? this.settings.itemColumnWidths[i] : this.settings.columnWidth;
        return parseInt(w, 10);
    };

    /**
     * @method _removeColGroup
     * This method removes the colgroup and all subsequent children so that we faster can instantiate a correct table
     * @param {HTMLElement} colgroup the colgroup we want to remove
     */
    p._removeColGroup = function (colgroup) {
        if (colgroup === undefined) return;
        colgroup.parentNode.removeChild(colgroup);
    };
    
    /**
     * @method _addColGroup
     * This method adds the colgroup and all subsequent children so that we faster can instantiate a correct table
     * @param {HTMLElement} parent the table we want to add the colgroup to
     */
    p._addColGroup = function (parent) {
        var colgroup = document.createElement('colgroup'), width = 0;
        // for (var i = 0; i < this.settings.tableItemTypes.length; i += 1) {
        for (var i = 0; i < this.tableDataInfo.columns; i += 1) {
            // if (!this.settings.hiddenColumns || !this.settings.hiddenColumns.includes(i)) {
            var col = document.createElement('col');
            col.width = this._getColumnWidth(i) + 'px';
            col.setAttribute('br-index', i);
            colgroup.appendChild(col);
            width += this._getColumnWidth(i);
            // }
        }
        
        this.widget.el.find('table').width(width);
        parent.insertBefore(colgroup, parent.firstChild);
    };

    p.updateTexts = function () {
        this.setRendererBusy();
        var renderer = this;
        $.each(this.tableHeaderEl.children(), function (index, headerNode) {
            $(headerNode).children('span').text(renderer.settings.headerTexts[index]);
            $(headerNode).children('span').html(renderer.widget._breakLine($(headerNode).children('span').html()));
        });

        _drawCallback.call(this);
    };
    
    /**
     * @method _updateColumnsOrRows
     * @private
     * The sole purpose of this function is so that the drawCallback
     * function can update the cell images accordingly. Before it only
     * called the _fitBackGroundImagesToRowHeight which works fine in 
     * vertical mode but not horizontal. For horizontal we might have
     * individual row heights.
     */
    p._updateColumnsOrRows = function () {
        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            _fitBackgroundImagesToRowHeight(this.widget, this.settings.rowHeight);
        } else {
            var self = this;
            $.each(this.settings.itemRowHeights, function (index, height) {

                //update images if necessary
                if (self.settings.tableItemTypes[index].includes('ImageList')) {
                    _fitBackgroundImagesToRowHeight(self.widget, height, index);
                }
            });
        }
    };

    /**
     * @method updateColumnWidths
     * This method will update the width in all columns in the table according to order of 
     * the (first one is only for header elements) the headerSize, then the individual 
     * item columnWidth and finally the cellSize/columnWidth given in the table. From this
     */
    p.updateColumnWidths = function () {
        var headerElements = this.tableHeaderEl.children(),
            colWidth = 0,
            totalcolumnWidth = 0;

        for (var i = 0; i < this.settings.tableItemIds.length; i += 1) {
            colWidth = (this.settings.dataOrientation === Enum.Direction.horizontal && this.settings.headerSize !== 0) ? this.settings.headerBarSize : this._getColumnWidth(i);
                        
            if (this.settings.tableItemTypes[i].includes('ImageList')) {
                _fitBackgroundImagesToRowHeight(this.widget, this.settings.rowHeight);
            }
            $(headerElements[i]).outerWidth(colWidth);
            totalcolumnWidth += colWidth;
        }

        //Finally update the header container of the widget if we are in horizontal mode
        if (this.settings.dataOrientation === Enum.Direction.horizontal) {
            var header = this.widget.el.find('.headerContainer');
            var headerWidth = (this.settings.dataOrientation === Enum.Direction.horizontal) ? colWidth : totalcolumnWidth;
            header.width(headerWidth);
            header.children().width(headerWidth);
        }
    };

    p.updateRowHeights = function (initial) {
        this.setRendererBusy();

        var tableRows = this.tableBodyEl.find('tr'),
            headerElements = this.tableHeaderEl.children(),
            renderer = this;

        if (this.settings.dataOrientation === Enum.Direction.horizontal) {
            $.each(this.settings.itemRowHeights, function (index, height) {
                if (height !== 0 && height !== undefined) {
                    $(tableRows[index]).height(height);
                    $(headerElements[index]).outerHeight($(tableRows[index]).height());
                } else {
                    height = renderer.settings.rowHeight;
                    $(tableRows[index]).height(renderer.settings.rowHeight);
                    $(headerElements[index]).outerHeight($(tableRows[index]).height());
                }

                //update images if necessary
                if (renderer.settings.tableItemTypes[index].includes('ImageList')) {
                    _fitBackgroundImagesToRowHeight(renderer.widget, height, index);
                }
            });
        } else if (this.settings.dataOrientation === Enum.Direction.vertical) {
            tableRows.height(this.settings.rowHeight);
        }

        if (!initial) {
            this.scrollHandler._refreshScroller();
            _drawCallback.call(this);
        }
    };

    p.updateTableOrder = function (index) {
        var currOrder = this.widget.getInternalOrderState(index);
        if (currOrder === undefined) { // Resets order original setup -- I hope
            this.settings.order = '';
        } else {
            this.settings.order = [index, currOrder];
        }

        if (this.settings.dataOrientation === Enum.Direction.horizontal) {
            _sortDataHorizontally(this, index, currOrder);
        }
        _clearTable(this);
        _addTableNode(this);
        _rebuildTable(this);
        this._updateImages(index, currOrder);
    };

    p._updateImages = function (index, currOrder) {
        if (this.settings.showSortingButton) {
            for (var i = 0; i < this.settings.tableItemIds.length; i += 1) {
                var topTriangle = '', bottomTriangle;
                if (i === index) {
                    bottomTriangle = (currOrder === 'asc') ? 'selected' : 'unselected';
                    topTriangle = (currOrder === 'desc') ? 'selected' : 'unselected';
                } else {
                    topTriangle = 'unselected';
                    bottomTriangle = 'unselected';
                }
                this.tableHeaderEl.find('svg > g > path:eq(' + 2 * i + ')').removeClass('selected').removeClass('unselected').addClass(topTriangle);
                this.tableHeaderEl.find('svg > g > path:eq(' + (2 * i + 1) + ')').removeClass('selected').removeClass('unselected').addClass(bottomTriangle);
            }
        }
    };

    p.processStyleChange = function () {
        var renderer = this;
        this.styleChangeProcess = _.defer(function () {
            renderer.updateRowHeights();
        });
    };

    p._inputReadyHandler = function () {
        this.tableBodyEl.off('inputHandlerReady', _.bind(this._inputReadyHandler, this));
        this.tableBodyEl.on(BreaseEvent.CLICK, _.bind(this._cellClickHandler, this));
        this.widget[this.options.rendererReadyCallbackFn]();
    };

    p._cellClickHandler = function (e) {
        if (this.tableReady) {
            if ((e.target.getAttribute('input') === 'true') && (!$(e.target).hasClass('disabled'))) {
                this.inputHandler.requestCellInput(e.target);
            }
        }
    };

    p._headerClickHandler = function (e) {
        var indexOfChild;

        this.tableHeaderEl.children().each(function (index, elem) {
            if ($.contains(elem, e.target)) {
                indexOfChild = index;
                return false;
            } else if (elem === e.target) {
                indexOfChild = index;
                return false;
            }
        });
        if (this.settings.showSortingButton) {
            this.updateTableOrder(indexOfChild);
        }
        this.widget[this.options.headerClickCallbackFn](e, indexOfChild);
    };

    p.setRowState = function (index, state) {
        var rowEl = $(this.table.row(index).node()),
            itemConfig = brease.callWidget(this.settings.tableItemIds[index], 'getItemConfig');

        rowEl.children().toggleClass('disabled', !state).attr('input', itemConfig.input);
        rowEl.prev().children().toggleClass('nextItemDisabled', !state);
        if ((state === true) && (itemConfig.input === true)) {
            rowEl.children().each(function (index, td) {
                if (itemConfig.inputConfig.validDataLength > index) {
                    $(td).attr('input', true);
                } else {
                    $(td).attr('input', false);
                }
            });
        }
        this.tableHeaderEl.children().eq(index).toggleClass('disabled', !state);
        this.tableHeaderEl.children().eq(index).prev().toggleClass('nextItemDisabled', !state);
    };

    p.setColumnState = function (index, state) {
        var columnEl = $(this.table.column(index).nodes()),
            itemConfig = brease.callWidget(this.settings.tableItemIds[index], 'getItemConfig');

        columnEl.toggleClass('disabled', !state).attr('input', itemConfig.input);
        if (index > 0) {
            $(this.table.column(index - 1).nodes()).toggleClass('nextItemDisabled', !state);
        }
        if ((state === true) && (itemConfig.input === true)) {
            columnEl.each(function (index, td) {
                if (itemConfig.inputConfig.validDataLength > index) {
                    $(td).attr('input', true);
                } else {
                    $(td).attr('input', false);
                }
            });
        }
        this.tableHeaderEl.children().eq(index).toggleClass('disabled', !state);
        this.tableHeaderEl.children().eq(index).prev().toggleClass('nextItemDisabled', !state);
    };

    p.enable = function () {
        if (this.scrollHandler.scrollerBody && this.table) {
            this.scrollHandler.scrollerBody.enable();
        }
    };

    p.disable = function () {
        if (this.scrollHandler.scrollerBody && this.table) {
            this.scrollHandler.scrollerBody.disable();
        }
    };

    p.setVisible = function () {
        _resize(this.widget);
        this.scrollHandler._refreshScroller();        
    };

    p.setRendererBusy = function () {
        if (!brease.config.editMode) {
            this.busy = true;
        }
    };

    p.setRendererReady = function () {
        this.busy = false;
        this.widget._hideBusyIndicator();
    };

    p.findElementInConfig = function (config, index) {
        return _findElementInConfig(config, index);
    };

    p._itemResizeHandler = function (e) {
        var totalHeaderSize = 0, itemToUpdate;
        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            for (var i = 0; i < this.settings.tableItemIds.length; i += 1) {
                if (this.settings.tableItemIds[i] === e.detail.id) {
                    this.settings.itemColumnWidths[i] = e.detail.newSize;
                    itemToUpdate = i;
                }
                totalHeaderSize += parseInt(this.settings.itemColumnWidths[i]);
            }
            this.widget.el.find('.headerContainer').width(totalHeaderSize);

        } else {
            for (var j = 0; j < this.settings.tableItemIds.length; j += 1) {
                if (this.settings.tableItemIds[j] === e.detail.id) {
                    this.settings.itemRowHeights[j] = e.detail.newSize;
                    itemToUpdate = j;
                }
                totalHeaderSize += parseInt(this.settings.itemRowHeights[j]);
            }
            this.widget.el.find('.headerContainer').height(totalHeaderSize);
        }

        _updateFromEditHandler(this, itemToUpdate, e.detail.newSize, totalHeaderSize);
        this.scrollHandler._refreshEditorScroller(true);
    };

    p.setFilter = function () {
        var self = this;
        if ($.fn.dataTable.ext.search.length !== 0) {
            $.fn.dataTable.ext.search.pop();
        }

        //The function for filtering WILL ONLY BE CALLED IF THERE IS DATA IN THE TABLE!!!!
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex, row) {
                
                var accVal = (self.widget.settings.filter.length === 0), accAnd = true;
                for (var i = 0; i < self.widget.settings.filter.length; i += 1) {
                    var fil = self.widget.settings.filter[i], compVal, origVal,
                        conf = self.widget.settings.itemConfigs[fil.data];

                    //Check if we are looking at time
                    if (conf.type === 'date') {
                        compVal = self._fixTimestamp(fil.comp.split('Z')[0], conf.inputConfig.format);
                        //origVal = self._fixTimestamp(brease.callWidget(self.widget.settings.tableItemIds[fil.data], 'getValue')[dataIndex], conf.inputConfig.format);
                        origVal = self._fixTimestamp(brease.callWidget(self.widget.settings.tableItemIds[fil.data], 'getValue')[dataIndex].split('Z')[0], conf.inputConfig.format);
                    } else {
                        compVal = fil.comp;
                        origVal = self.widget.settings.tableData[dataIndex][fil.data];
                    }

                    var val = self._getFilterStatement(origVal, fil.opVal, compVal);

                    //Split between whether we are in an 'OR' or 'AND' filter
                    if (fil.logVal === 1 || fil.logical === '') {
                        accVal = accVal || (accAnd && val);
                        accAnd = true;
                    } else if (fil.logVal === 0) {
                        accAnd = accAnd && val;
                    }

                }
                return accVal;
            });
    };

    p._getFilterStatement = function (origVal, op, compVal) {
        /* jshint eqeqeq:false */
        var retValRow;

        if (compVal === 'not supported') {
            return true;
        }

        if (origVal instanceof Date) {
            origVal = origVal.getTime();
            compVal = compVal.getTime();
        }

        switch (op) {
            case 0:
                //retValRow = (origVal !== compVal);
                // eslint-disable-next-line eqeqeq
                retValRow = (origVal != compVal);
                break;

            case 1:
                //retValRow = (origVal === compVal);
                // eslint-disable-next-line eqeqeq
                retValRow = (origVal == compVal);
                break;

            case 2:
                retValRow = (origVal < compVal);
                break;

            case 3:
                retValRow = (origVal <= compVal);
                break;

            case 4:
                retValRow = (origVal > compVal);
                break;

            case 5:
                retValRow = (origVal >= compVal);
                break;

            case 6:
                retValRow = (typeof (origVal) !== 'string') ? false : (origVal.indexOf(compVal) !== -1);
                break;

            case 7:
                retValRow = (typeof (origVal) !== 'string') ? false : (origVal.indexOf(compVal) === -1);
                break;

            default:
                retValRow = true;
        }
        return retValRow;
    };

    p._fixTimestamp = function (tim, f) {
        var d = new Date(tim);

        if (f.indexOf('f') !== -1) {
            // eslint-disable-next-line no-self-assign
            d = d;
        } else if (f.indexOf('s') !== -1) {
            d.setMilliseconds(0);
        } else if (f.indexOf('m') !== -1) {
            d.setMilliseconds(0);
            d.setSeconds(0);
        } else if (f.indexOf('H') !== -1 || f.indexOf('h') !== -1) {
            d.setMilliseconds(0);
            d.setSeconds(0);
            d.setMinutes(0);
        } else if (f.indexOf('d') !== -1) {
            d.setMilliseconds(0);
            d.setSeconds(0);
            d.setMinutes(0);
            d.setHours(0);
        } else if (f.indexOf('M') !== -1) {
            d.setMilliseconds(0);
            d.setSeconds(0);
            d.setMinutes(0);
            d.setHours(0);
            d.setDate(0);
        } else if (f.indexOf('y') !== -1) {
            d.setMilliseconds(0);
            d.setSeconds(0);
            d.setMinutes(0);
            d.setHours(0);
            d.setDate(0);
            d.setMonth(0);
        }
        return d;
    };

    p.dispose = function () {
        if (brease.config.editMode) {
            this.widget.elem.removeEventListener('ItemSizeChanged', this._bind('_itemResizeHandler'));
        }
        _clearTable(this);
        window.clearTimeout(this.styleChangeProcess);
        this.tableReady = null;
        this.scrollHandler.dispose();
        this.inputHandler.dispose();
        SuperClass.prototype.dispose.apply(this, arguments);
    };
    
    p.suspend = function () {
        this.inputHandler.suspend();
    };

    p.wake = function () {
        this.inputHandler.wake();
    };

    // Privates

    /**
     * @method _fitBackgroundImagesToRowHeight
     * This method will fit the image to the height of the row these are placed in
     * @param {Object} widget widget we operate on 
     * @param {UInteger} height optional, the height that should be applied to the image, if none is supplied it will use the rowHeight
     * @param {UInteger} index the row that should be operated upon. May differ in horizontal direction
     */
    function _fitBackgroundImagesToRowHeight(widget, height, index) {
        if (height === undefined || height === 0) {
            height = widget.settings.rowHeight;
        }
        //Set size to one pixel less than row height so appended row padding adds up to the correct size
        if (index) {
            widget.el.find('tr:eq(' + index + ') td > img').css('max-height', height - 1);
        } else {
            widget.el.find('td > img').css('max-height', height - 1);
        }
    }

    function _createHeaderWrapper(renderer) {
        var headerWrapperEl = $('<div class="dataTables_scrollHead"></div>'),
            headerSize = renderer.settings.headerBarSize;

        if (!renderer.widget.settings.showHeader) {
            headerWrapperEl.css('display', 'none');
            headerSize = 0;
        }

        renderer.tableHeaderEl = $('<div class="tableHeader">').on(BreaseEvent.CLICK, renderer._bind('_headerClickHandler'));

        if (renderer.settings.dataOrientation === Enum.Direction.vertical) {
            headerWrapperEl.height(headerSize);
            headerWrapperEl.addClass('verticalOrientation');
        } else if (renderer.settings.dataOrientation === Enum.Direction.horizontal) {
            headerWrapperEl.width(headerSize);
            headerWrapperEl.addClass('horizontalOrientation');
        }

        headerWrapperEl.append(renderer.tableHeaderEl);
        renderer.widget.container.append(headerWrapperEl);
    }

    function _generate(renderer) {
        var headerEl = [];
        var deferredList = [], i = 0;
        $.each(renderer.settings.tableItemIds, function (index) {
            headerEl.push($('<div class="headerElement"/>').append($('<span>').text(renderer.settings.headerTexts[index])));
            if (renderer.settings.showSortingButton) {
                var src = 'widgets/brease/Table/img/sort_arrow.svg';
                UtilsImage.getInlineSvg(src).then(function (svgElement) {
                    var imgEl = $('<svg class="ordering img_vert" />');
                    imgEl.replaceWith(svgElement);
                    imgEl = svgElement;
                    imgEl.addClass('ordering');
                    if (renderer.settings.dataOrientation === Enum.Direction.vertical) {
                        imgEl.addClass('img_vert');
                    } else {
                        imgEl.addClass('img_horz');
                    }
                    headerEl[i].append(imgEl);
                    i += 1;
                });
            }
        });

        $.when(deferredList).done(function () {
            renderer.tableHeaderEl.append(headerEl);
        }).fail();
    }

    function _createBodyWrapper(renderer) {

        var bodyWrapperEl = $('<div class="dataTables_scrollBody"/>'),
            headerWrapperEl = renderer.widget.el.find('.dataTables_scrollHead').detach(),
            headerSize = (renderer.widget.settings.showHeader) ? renderer.settings.headerBarSize : 0;

        renderer.widget.container.find('.dataTable thead').remove();
        renderer.tableWrapper = renderer.widget.container.find('.dataTables_wrapper');
        renderer.tableBodyEl.addClass('tableCells');

        if (renderer.settings.dataOrientation === Enum.Direction.vertical) {
            bodyWrapperEl.css('height', 'calc(100%' + ' - ' + headerSize + 'px)');
            bodyWrapperEl.addClass('verticalOrientation');
            renderer.tableWrapper.addClass('verticalOrientation');
        } else if (renderer.settings.dataOrientation === Enum.Direction.horizontal) {
            bodyWrapperEl.css('width', 'calc(100%' + ' - ' + headerSize + 'px)');
            headerWrapperEl.width(renderer.settings.headerBarSize);
            bodyWrapperEl.addClass('horizontalOrientation');
            renderer.tableWrapper.addClass('horizontalOrientation');
        }

        renderer.tableBodyEl.wrap(bodyWrapperEl);
        renderer.tableWrapper.prepend(headerWrapperEl);
    }

    function _addTableNode(renderer) {
        renderer.tableBodyEl = $('<table>').attr('id', renderer.widget.elem.id + '_table');
        if (!brease.config.editMode) {
            renderer.tableBodyEl.on('inputHandlerReady', _.bind(renderer._inputReadyHandler, renderer));
        }
        renderer.widget.container.append(renderer.tableBodyEl);
    }

    function _buildTable(renderer) {

        if (renderer.widget.settings.tableData.length > 0 && renderer.widget.settings.tableData[0].empty) {
            return;
        } else if (renderer.widget.settings.tableData.length > 0) {
            renderer.setFilter();
        }

        _createColumnObj(renderer);

        var options = {
            data: renderer.settings.tableData,
            autoWidth: false,
            ordering: (renderer.settings.dataOrientation === Enum.Direction.vertical) ? renderer.settings.showSortingButton : false, //Only allow sorting if vertical table,
            order: renderer.settings.order, //set no sorting order to begin with.
            paging: false,
            info: false,
            dom: 't',
            language: { zeroRecords: '' },
            columns: renderer.columnDefinition,
            columnDefs: [
                { targets: '_all', createdCell: _.bind(_createdCellCallback, renderer), orderable: true, searchable: true }
            ],
            initComplete: _.bind(_initCompleteCallback, renderer),
            drawCallback: _.bind(_drawCallback, renderer)
        };

        renderer.tableBodyEl.DataTable(options);
    }

    function _rebuildTable(renderer) {

        //TEST - for memory leakage
        if (!brease.config.editMode) {
            renderer.inputHandler.dispose();
        }

        if (renderer.widget.settings.tableData.length > 0 && renderer.widget.settings.tableData[0].empty) {
            return;
        } else if (renderer.widget.settings.tableData.length > 0) {
            renderer.setFilter();
        }

        _createColumnObj(renderer);
        
        if (renderer.settings.dataOrientation === Enum.Direction.horizontal && renderer.settings.order !== '') {
            _sortDataHorizontally(renderer, renderer.settings.order[0], renderer.settings.order[1]);
        }

        var options = {
            data: renderer.settings.tableData,
            autoWidth: false,
            ordering: (renderer.settings.dataOrientation === Enum.Direction.vertical) ? renderer.settings.showSortingButton : false, //Only allow sorting if vertical table,
            order: renderer.settings.order, //set no sorting order to begin with.
            paging: false,
            info: false,
            dom: 't',
            language: { zeroRecords: '' },
            columns: renderer.columnDefinition,
            columnDefs: [
                { targets: '_all', createdCell: _.bind(_createdCellCallback, renderer), orderable: true, searchable: true }
            ],
            initComplete: _.bind(_rebuildCompleteCallback, renderer),
            drawCallback: _.bind(_drawCallback, renderer)
        };

        renderer.tableBodyEl.DataTable(options);

        //TEST - for memory leakage
        if (!brease.config.editMode) {
            renderer.inputHandler.initialize();
        }
    }

    function _createColumnObj(renderer) {

        renderer.columnDefinition = [];

        if (renderer.settings.dataOrientation === Enum.Direction.vertical) {
            $.each(renderer.settings.headerTexts, function (index, headerText) {
                renderer.columnDefinition.push(null);
            });

        } else if (renderer.settings.dataOrientation === Enum.Direction.horizontal) {
            for (var i = 0; i < renderer.settings.tableData[0].length; i += 1) {
                renderer.columnDefinition.push(null);
            }
        }
    }

    function _initCompleteCallback() {
        _setClasses(this);
        _createBodyWrapper(this);
        _addColumnDefs(this);

        // this.updateItemVisibility(true);
        this.updateItemClassesDebounced();

        this.updateColumnWidths(true);
        this.updateRowHeights(true);
        this.scrollHandler.initializeScroller(false, false);
        this.scrollHandler._refreshScroller();
        _setInitialEnableStates(this);
        _resize(this.widget);
        this.tableReady = true;
        this.setRendererReady();
        this.widget[this.options.tableReadyCallbackFn]();

        if (this.settings.dataOrientation === 'vertical') {
            this.selectItem('row', this.settings.selectedRow, true);
        } else {
            this.selectItem('column', this.settings.selectedColumn, true);
        }
    }

    function _rebuildCompleteCallback() {
        _setClasses(this);
        _createBodyWrapper(this);
        _addColumnDefs(this);

        this.updateItemClassesDebounced();

        this.updateColumnWidths(true);
        this.updateRowHeights(true);
        this.scrollHandler.initializeScroller(false, true);
        this.scrollHandler._refreshScroller();
        _setInitialEnableStates(this);
        _resize(this.widget);
        this.tableReady = true;
        this.setRendererReady();
        this.widget[this.options.tableReadyCallbackFn]();

        if (this.settings.dataOrientation === 'vertical') {
            this.selectItem('row', this.settings.selectedRow, true);
        } else {
            this.selectItem('column', this.settings.selectedColumn, true);
        }
    }

    function _drawCallback() {
        _calcTableDataBoundaries(this);
        this._updateColumnsOrRows();
        this.setRendererReady();
        this.widget[this.options.drawCallbackFn]();
    }

    function _createdCellCallback(cell, cellData, rowData, rowIndex, colIndex) {
        var compIndex = (this.settings.dataOrientation === Enum.Direction.vertical) ? rowIndex : colIndex,
            readIndex = (this.settings.dataOrientation === Enum.Direction.vertical) ? colIndex : rowIndex;

        //Check if cell is of type input type true or false
        if ((this.settings.itemConfigs[readIndex].input) && (this.settings.itemConfigs[readIndex].inputConfig.validDataLength > compIndex)) {
            cell.setAttribute('input', true);
        } else {
            cell.setAttribute('input', false);
        }

        if ((this.settings.itemConfigs[readIndex].type === 'number') || (this.settings.itemConfigs[readIndex].type === 'nodeNumber')) {
            cell.setAttribute('type', 'number');
        } else if (this.settings.itemConfigs[readIndex].type === 'string') {
            cell.setAttribute('type', 'string');
        }
    }

    function _setClasses(renderer) {
        if (renderer.settings.ellipsis) {
            renderer.widget.el.addClass('ellipsis');
        }
        if (renderer.settings.wordWrap && renderer.settings.multiLine) {
            renderer.widget.el.addClass('wordWrap');
        } else if (renderer.settings.multiLine) {
            renderer.widget.el.addClass('mulitLine');
        }
    }

    function _calcTableDataBoundaries(renderer) {
        renderer.tableDataInfo =
            {
                rows: renderer.settings.tableData.length,
                columns: renderer.columnDefinition.length
            };
    }

    function _addColumnDefs(renderer) {
        // var colGroupEl = $('<colgroup>');
        // for (var i = 0; i < renderer.columnDefinition.length; i += 1) {
        //     var colEl = $('<col>');
        //     colGroupEl.append(colEl);
        // }
        // renderer.tableBodyEl.prepend(colGroupEl);
    }

    function _setInitialEnableStates(renderer) {
        $.each(renderer.settings.itemEnableStates, function (index, value) {
            if (value === false) {
                if (renderer.settings.dataOrientation === Enum.Direction.vertical) {
                    renderer.setColumnState(index, false);
                } else if (renderer.settings.dataOrientation === Enum.Direction.horizontal) {
                    renderer.setRowState(index, false);
                }
            }
        });
    }

    //Updates
    function _updateRowData(array, newRow, rowIndex) {
        $.each(array, function (index, data) {
            if (index === rowIndex) {
                $.each(array[index], function (cellIndex, celldata) {
                    array[index][cellIndex] = newRow[cellIndex] !== undefined ? newRow[cellIndex] : '';
                });
            }
        });
    }

    function _updateColumnData(array, newColumn, columnIndex) {
        $.each(array, function (arrayIndex, dataSet) {
            $.each(dataSet, function (index, value) {
                if (index === columnIndex) {
                    array[arrayIndex][index] = newColumn[arrayIndex] !== undefined ? newColumn[arrayIndex] : '';
                }
            });
        });
    }

    // Delete Table
    function _clearTable(renderer) {
        if (renderer.table) {
            var headerWrapper = renderer.widget.container.find('.dataTables_scrollHead').detach();
            renderer.widget.container.append(headerWrapper);
            renderer.scrollHandler.removeScroller();
            renderer.table.destroy(true);
            renderer.table = undefined;
        }
        // eslint-disable-next-line no-unused-expressions
        renderer.tableBodyEl ? renderer.tableBodyEl.off().empty() : null;
    }

    /**
     * @method _sortDataHorizontally
     * This method takes data, transposes it and sorts it accordingly to the order passed on the colIndex.
     * It then re transposes it and stores the sorted data in the tableData to be used by the table.
     * OBS Only to be used in horizontal mode! (For vertical mode, the datatables can sort it by itself)
     * @param {Object} renderer reference to this class
     * @param {UInteger} colIndex the column index that is to be sorted
     * @param {String} order the order can either be asc or desc
     */
    function _sortDataHorizontally(renderer, colIndex, order) {

        //Only transpose if asc/desc ordering, else return original data setup
        if (order !== undefined) {
            //Transpose data to sort in vertical fashion
            var altTable = _.zip.apply(_, renderer.settings.tableData);

            //Anon function to sort data
            altTable.sort(function (a, b) {
                if (a[colIndex] === b[colIndex]) {
                    return 0;
                } else if (isNaN(a[colIndex]) || isNaN(b[colIndex])) {
                    if (order === 'asc') {
                        return (a[colIndex] < b[colIndex]) ? -1 : 1;
                    } else {
                        return (a[colIndex] > b[colIndex]) ? -1 : 1;
                    }
                }
                if (order === 'asc') {
                    return a[colIndex] - b[colIndex];
                } else {
                    return b[colIndex] - a[colIndex];
                }
            });
            //Transpose data back to original format and apply to table
            renderer.settings.tableData = _.zip.apply(_, altTable);
        } else {
            renderer.settings.tableData = renderer.settings.originTableData;
        }
    }

    function _updateFromEditHandler(renderer, index, size, totalSize) {
        var headerElements = renderer.tableHeaderEl.find('.headerElement');

        if (renderer.settings.dataOrientation === Enum.Direction.vertical) {
            var colElements = renderer.tableBodyEl.find('col');
            $(colElements[index]).width(size);
            $(headerElements[index]).width(size);
            renderer.tableHeaderEl.width(totalSize);
            renderer.tableBodyEl.width(totalSize);
        } else {
            var rowElements = renderer.tableBodyEl.find('tr');
            $(rowElements[index]).height(size);
            $(headerElements[index]).height(size);
            renderer.tableHeaderEl.height(totalSize);
            renderer.tableBodyEl.height(totalSize);
        }
    }

    function _resize(widget) {
        if (widget.settings.maxHeight > widget.settings.height) {
            var newHeight = Math.max(Math.min(widget.container.find('.dataTable').outerHeight() + widget.settings.headerBarSize, widget.settings.maxHeight), widget.settings.height);
            widget.el.height(newHeight);
        }
    }

    // Helper
    function _findElementInConfig(array, indexToFind) {
        var elementToReturn;
        $.each(array, function (arrayIndex, element) {
            if ((element.index === indexToFind) || (element.from <= indexToFind && element.to >= indexToFind)) {
                elementToReturn = element;
                return false;
            }
        });
        return elementToReturn;
    }

    return Renderer;
});
