define(['brease/events/BreaseEvent'],
    function (BreaseEvent) {

        'use strict';
        var LoaderPoolStub = function () {
            this.dispatchFragmentShowForContent = function (contenId) {
                document.body.dispatchEvent(new CustomEvent(BreaseEvent.FRAGMENT_SHOW, { detail: { url: '', contentId: contenId }, bubbles: true }));
            };
            this.dispatchFragmentHideForContent = function (contenId) {
                document.body.dispatchEvent(new CustomEvent(BreaseEvent.FRAGMENT_HIDE, { detail: { contentId: contenId }, bubbles: true }));
            };
        };
        LoaderPoolStub.prototype.dispose = function () { };
        LoaderPoolStub.prototype.findContentLoaderWithContent = function () { };
        LoaderPoolStub.prototype.startTagMode = function () { };
        LoaderPoolStub.prototype.endTagMode = function () { };
        LoaderPoolStub.prototype.loadContent = function (contentId, container, deferred) {
            console.log('loadContent', contentId);
            deferred.resolve('', true);
        };
        LoaderPoolStub.prototype.removeLoader = function () { };
        return LoaderPoolStub;
    });
