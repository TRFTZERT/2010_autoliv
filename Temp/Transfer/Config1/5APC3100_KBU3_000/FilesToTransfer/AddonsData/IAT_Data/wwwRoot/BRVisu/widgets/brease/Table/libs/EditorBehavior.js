define([
    'brease/core/Class',
    'widgets/brease/Table/libs/EditorHandles',
    'brease/enum/Enum'
], function (
    SuperClass, EditorHandles, Enum
) {

    'use strict';

    var defaultSettings = {},
    
        EditorBehaviorClass = SuperClass.extend(function Model(widget) {
            SuperClass.apply(this, arguments);
            this.widget = widget;
        }, defaultSettings),

        p = EditorBehaviorClass.prototype;

    p.initialize = function () {
        this.throttledFunc = _.throttle(this.reinitialize, 500);
        _initEditor(this);
    };

    p.reinitialize = function () {
        this.widget.renderer.updateEditor();
    };

    p.childrenInitializedEditor = function () {
        // console.log('%c Children in Editor initialized!', 'background: #222; color: #bada55');
    };

    p.childrenUpdated = function () {
        
        if (this.widget.settings.dataOrientation === Enum.Direction.vertical) {
            this.widget.container.children('.headerContainer').width(this.widget.container.find('.dataTables_scrollBody > .dataTable').width());
            this.widget.container.children('.headerContainer').css('flex-direction', 'row');

            //Update size of the tableitems
            this.headerContainer.width(this.widget.container.find('.dataTables_scrollBody > .dataTable').width());
            this.headerContainer.height(this.widget.settings.headerBarSize);
            var headerChildren = this.headerChildren,
                rowHeight = this.widget.settings.headerBarSize;
            this.widget.container.find('col').each(function (i) {
                $(headerChildren[i]).width($(this).width());
                $(headerChildren[i]).height(rowHeight);
            });

            //console.log('%c' + this.widget.container.children('.headerContainer').width(), 'background-color:purple;color:green;');
        } else {
            this.widget.container.children('.headerContainer').height(this.widget.container.find('.dataTables_scrollBody > .dataTable').height())
                .css('left', '0px')
                .css('top', '0px')
                .css('flex-direction', 'column');

            //Update size of the tableitems
            this.headerContainer.height(this.widget.container.find('.dataTables_scrollBody > .dataTable').height());
            this.headerContainer.width(this.widget.settings.headerBarSize);
            var headerChildrenC = this.headerChildren,
                columnWidth = this.widget.settings.headerBarSize;
            this.widget.container.find('tr').each(function (i) {
                var itemHeight = brease.callWidget(headerChildrenC[i].id, 'getRowHeight');
                $(headerChildrenC[i]).height((itemHeight) > $(this).height() ? itemHeight : $(this).height());
                $(headerChildrenC[i]).width(columnWidth);
            });
        }
        this.throttledFunc();
    };

    p.childrenAdded = function (e) {
        if (this.headerChildren === undefined) {
            this.headerChildren = this.widget.container.find('.breaseWidget[data-brease-widget]').detach();
            this.headerContainer = $('<div class="headerContainer" style="position:absolute;left:0px;right:0px;height:100%;width:100%;display:flex"></div>');
            (this.widget.settings.dataOrientation === Enum.Direction.vertical) ? this.headerContainer.css('flex-direction', 'row') : this.headerContainer.css('flex-direction', 'column');
            this.widget.container.prepend(this.headerContainer);
            this.widget.container.children('.headerContainer').append(this.headerChildren);

        } else {
            this.headerChildren = this.widget.container.find('.breaseWidget[data-brease-widget]').detach();
            this.widget.container.children('.headerContainer').append(this.headerChildren);
        }

        if (this.widget.settings.dataOrientation === Enum.Direction.vertical) {
            this.headerContainer.width(this.widget.container.find('.dataTables_scrollBody > .dataTable').width());
            this.headerContainer.height(this.widget.settings.headerBarSize).css('flex-direction', 'row');
            var headerChildren = this.headerChildren,
                rowHeight = this.widget.settings.headerBarSize;
            this.widget.container.find('col').each(function (i) {
                $(headerChildren[i]).width($(this).width());
                $(headerChildren[i]).height(rowHeight);
            });
        } else {
            this.headerContainer.height(this.widget.container.find('.dataTables_scrollBody > .dataTable').height());
            this.headerContainer.width(this.widget.settings.headerBarSize).css('flex-direction', 'column');
            var headerChildrenA = this.headerChildren,
                columnWidth = this.widget.settings.headerBarSize;
            this.widget.container.find('tr').each(function (i) {
                var itemHeight = brease.callWidget(headerChildrenA[i].id, 'getRowHeight');
                $(headerChildrenA[i]).height((itemHeight) > $(this).height() ? itemHeight : $(this).height());
                $(headerChildrenA[i]).width(columnWidth);
            });
        }
        this.throttledFunc();
    };

    p.childrenRemoved = function (e) {
        this.childrenUpdated();
    };

    p.dispose = function () {
        // Do something
    };

    function _initEditor(that) {
        that.editMode = {};
        that.editMode.itemDefs = [];

        that.widget.el.find('[data-brease-widget]').each(function () {
            var d = $.Deferred();
            that.editMode.itemDefs.push(d);
        });

        $.when.apply($, that.editMode.itemDefs).done(function () {
            that.childrenInitializedEditor();
        });

        that.widget.el.addClass('iat-container-widget');

        var editorHandles = new EditorHandles(that.widget);
        that.widget.getHandles = function () {
            return editorHandles.getHandles();
        };
        that.widget.designer.getSelectionDecoratables = function () {
            return editorHandles.getSelectionDecoratables();
        };
    }

    return EditorBehaviorClass;
});
