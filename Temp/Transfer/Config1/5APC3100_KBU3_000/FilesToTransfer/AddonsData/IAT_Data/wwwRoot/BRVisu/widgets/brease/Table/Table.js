define([
    'brease/core/ContainerWidget',
    'brease/events/BreaseEvent',
    'brease/enum/Enum',
    'widgets/brease/Table/libs/Renderer',
    'brease/core/Utils',
    'widgets/brease/Table/libs/EditorBehavior',
    'widgets/brease/Table/libs/Config',
    'widgets/brease/Table/libs/Dialogue',
    'widgets/brease/Table/libs/ConfigBuilder'
], function (
    SuperClass, BreaseEvent, Enum, Renderer, 
    Utils, EditorBehavior, TableConfig, TableDialogue, ConfigBuilder
) {
    
    'use strict';

    /**
     * @class widgets.brease.Table
     * #Description
     * Widget to show data in a table 
     * @breaseNote 
     * @extends brease.core.ContainerWidget
     * @requires widgets.brease.BusyIndicator
     * @requires widgets.brease.TextInput
     * @requires widgets.brease.Label
     * @requires widgets.brease.NumericInput
     * @requires widgets.brease.CheckBox
     * @requires widgets.brease.DateTimeInput
     * @requires widgets.brease.Image
     * @requires widgets.brease.Rectangle
     * @requires widgets.brease.DropDownBox
     * @requires widgets.brease.GenericDialog
     * @iatMeta studio:isContainer
     * true
    
     * @iatMeta category:Category
     * Data,Container
     * @iatMeta description:short
     * Darstellung von Daten in einer Tabelle
     * @iatMeta description:de
     * Darstellung von Daten in einer Tabelle
     * @iatMeta description:en
     * Displays data in a table
     */

    /**
     * @property {WidgetList} [children=["widgets.brease.TableItem","widgets.brease.TableItemImageList","widgets.brease.TableItemDateTime"]]
     * @inheritdoc  
     */

    var WidgetClass = SuperClass.extend(function Table() {
            SuperClass.apply(this, arguments);
        }, TableConfig),

        p = WidgetClass.prototype;

    p.init = function () {
        if (this.settings.omitClass !== true) {
            this.addInitialClass('breaseTable container');
        }
        if (!brease.config.editMode) {
            SuperClass.prototype.init.call(this);
        }

        this.settings.cellVisibility = [];
        this.settings.cellDisability = [];

        if (brease.config.editMode === true) {
            this.widgetReady = new $.Deferred();
            this.tableReady = new $.Deferred();
            this.el.on(BreaseEvent.WIDGET_READY, this._bind('_widgetReadyHandler'));
            this._startInitialisation();
            this.editorBehavior = new EditorBehavior(this);
            this.addInitialClass('iatd-outline');
            
            SuperClass.prototype.init.call(this);

            this.editorBehavior.initialize();

        }

        this.configBuilder = new ConfigBuilder();

        _scrollSynchronizationSetup(this);

        if (this.settings.dataOrientation === Enum.Direction.horizontal) {
            this.settings.rendererOptions.selectableItem = 'column';
        } else {
            this.settings.rendererOptions.selectableItem = 'row';
        }

        if (!brease.config.editMode) {
            this.busyId = Utils.uniqueID(this.elem.id + '_busyIndicator');
            this.busyWrapper = $('<div class="busyWrapper"/>').addClass('visible');
            this.el.append(this.busyWrapper);
            _addChildWidgets(this);
        }

        if (this.settings.dataOrientation === Enum.Direction.horizontal) {
            this.settings.headerBarSize = (this.settings.headerSize === 0) ? +this.settings.columnWidth : this.settings.headerSize;
        } else {
            this.settings.headerBarSize = (this.settings.headerSize === 0) ? +this.settings.rowHeight : this.settings.headerSize;
        }
        
        if (!this.settings.showScrollbars) {
            this.settings.rendererOptions.scroller.scrollbars = false;
        }
        this.initRenderer();

        if (!brease.config.editMode) {
            this.widgetReady = new $.Deferred();
            this.tableReady = new $.Deferred();
            this.settings.filter = this.configBuilder.parseFilter(this.settings.filterConfiguration);
            this._startInitialisation();
            this.el.on(BreaseEvent.WIDGET_READY, this._bind('_widgetReadyHandler'));
        }

    };

    p.initRenderer = function () {
        this.renderer = new Renderer(this, this.settings.rendererOptions);
    };

    p.widgetAddedHandler = function (e) {

        if (this.settings.editor.first) {
            this.settings.editor.first = false;
            var children = this.container.children('[data-brease-widget]');
            for (var i = 0; i < children.length; i += 1) {
                this.settings.editor.editorItemOrder[i] = children[i].id;
                this.settings.tableItemIds[i] = children[i].id;
                this.settings.tableItemTypes[i] = $(children[i]).attr('data-brease-widget');
            }
            this.settings.editor.added += 1;
        } else {
            this.settings.editor.added += 1;
            //If more than the inital tableitems are added we need to add these to the tableitemids
            if (this.settings.editor.added > this.settings.editor.editorItemOrder.length) {
                this.settings.tableItemIds.push(e.detail.widgetId);
                this.settings.tableItemTypes.push(brease.callWidget(e.detail.widgetId, 'getItemType'));
                this.settings.editor.editorItemOrder.push(e.detail.widgetId);
                //Let newly attached item that the table is already ready
                brease.callWidget(e.detail.widgetId, 'setTableReady', this.elem.id, true);
            }    
        }
        //If all inital table items (and more) are initialized, we build the table 
        if (this.settings.editor.added >= this.settings.editor.editorItemOrder.length) {
            this.childrenInitializedInEditorHandler();
            this.editorBehavior.childrenAdded(e);
            
            $('#' + this.settings.tableItemIds[0]).parent().off('ItemSizeChanged', this._bind('updateChangeInEditor'));
            $('#' + this.settings.tableItemIds[0]).parent().on('ItemSizeChanged', this._bind('updateChangeInEditor'));
        }

    };

    p.widgetRemovedHandler = function (e) {
        var index = this.settings.tableItemIds.indexOf(e.detail.widgetId);
        this.settings.tableItemIds.splice(index, 1);
        this.settings.tableItemTypes.splice(index, 1);
        this.settings.editor.editorItemOrder.splice(index, 1);
        this.settings.editor.added -= 1;

        //Get new data from the items left
        this.childrenInitializedInEditorHandler();
        this.editorBehavior.childrenRemoved(e);
    };  

    p.updateChangeInEditor = function () {
        //this.renderer.updateEditor();

        this.childrenInitializedInEditorHandler();
        this.editorBehavior.childrenUpdated();
    };

    p.setStyle = function (style) {

        SuperClass.prototype.setStyle.call(this, style);
        if (this.renderer) {
            this.renderer.processStyleChange();
        }
    };

    p.setTableData = function (tableData, init) {
        this.settings.tableData = tableData;
        this.settings.originTableData = $.extend(true, [], tableData);
        this.renderer.setFilter();
        if (init) {
            this.renderer.updateTable();        
        } else {
            this.renderer.updateTableSize();
        }
    };

    p.setRowData = function (data, rowIndex) {
        this.renderer.updateRow(data, rowIndex);
    };

    p.setColumnData = function (data, columnIndex) {
        this.renderer.updateColumn(data, columnIndex);
    };

    p.addRows = function (newRows) {
        this.renderer.appendRows(newRows);
    };

    p.addColumns = function (newColumns) {
        this.renderer.appendColumns(newColumns);
    };

    /**
     * @method setRowHeight
     * Sets rowHeight
     * @param {Size} rowHeight
     */
    p.setRowHeight = function (rowHeight) {
        this.settings.rowHeight = rowHeight;
        this.settings.headerBarSize = (this.settings.headerSize === 0) ? (this.settings.dataOrientation === Enum.Direction.vertical) ? +this.settings.rowHeight : +this.settings.columnWidth : this.settings.headerSize;
        
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getRowHeight 
     * Returns rowHeight.
     * @return {Size}
     */
    p.getRowHeight = function () {
        return this.settings.rowHeight;
    };

    /**
     * @method setColumnWidth
     * Sets columnWidth
     * @param {Size} columnWidth
     */
    p.setColumnWidth = function (columnWidth) {
        this.settings.columnWidth = columnWidth;
        this.settings.headerBarSize = (this.settings.headerSize === 0) ? (this.settings.dataOrientation === Enum.Direction.vertical) ? +this.settings.rowHeight : +this.settings.columnWidth : this.settings.headerSize;

        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getColumnWidth 
     * Returns columnWidth.
     * @return {Size}
     */
    p.getColumnWidth = function () {
        return this.settings.columnWidth;
    };

    /**
     * @method setDataOrientation
     * Sets dataOrientation
     * @param {brease.enum.Direction} dataOrientation
     */
    p.setDataOrientation = function (dataOrientation) {
        this.settings.dataOrientation = dataOrientation;

        if (dataOrientation === Enum.Direction.vertical) {
            this.el.find('.container').removeClass('horizontal').addClass('vertical');
            this.settings.rendererOptions.selectableItem = 'row';
            this.settings.headerBarSize = (this.settings.headerSize === 0) ? +this.settings.rowHeight : this.settings.headerSize;
        } else {
            this.el.find('.container').removeClass('vertical').addClass('horizontal');
            this.settings.rendererOptions.selectableItem = 'column';
            this.settings.headerBarSize = (this.settings.headerSize === 0) ? +this.settings.columnWidth : this.settings.headerSize;
        }

        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getDataOrientation 
     * Returns dataOrientation.
     * @return {brease.enum.Direction}
     */
    p.getDataOrientation = function () {
        return this.settings.dataOrientation;
    };

    /**
     * @method setOffsetRow
     * Sets offsetRow
     * @param {Integer} offsetRow
     */
    p.setOffsetRow = function (offsetRow) {
        this.settings.offsetRow = offsetRow;
        if (this.renderer.tableReady) {
            if (offsetRow > this.renderer.tableDataInfo.rows - 1) {
                this.renderer.scrollHandler.calcScrollItemOffset();
                return;
            }
            this.offsetRowDeferID = _.defer(function (widget) {
                widget.omitScrollPositionSubmit = true;
                widget.renderer.scrollHandler.scrollToIndex(widget.settings.offsetRow, widget.settings.offsetColumn);
                if (widget.offsetColumnDeferID !== undefined) {
                    window.clearTimeout(widget.offsetColumnDeferID);
                } 
            }, this);
        }
    };

    /**
     * @method getOffsetRow 
     * Returns offsetRow.
     * @return {Integer}
     */
    p.getOffsetRow = function () {
        return this.settings.offsetRow;
    };

    /**
     * @method setOffsetColumn
     * Sets offsetColumn
     * @param {Integer} offsetColumn
     */
    p.setOffsetColumn = function (offsetColumn) {
        this.settings.offsetColumn = offsetColumn;
        if (this.renderer.tableReady) {
            if (offsetColumn > this.renderer.tableDataInfo.columns - 1) {
                this.renderer.scrollHandler.calcScrollItemOffset();
                return;
            }
            this.offsetColumnDeferID = _.defer(function (widget) {
                widget.omitScrollPositionSubmit = true;
                widget.renderer.scrollHandler.scrollToIndex(widget.settings.offsetRow, widget.settings.offsetColumn);
                if (widget.offsetRowDeferID !== undefined) {
                    window.clearTimeout(widget.offsetRowDeferID);
                }
            }, this);
        }
    };

    /**
     * @method getOffsetColumn 
     * Returns offsetColumn.
     * @return {Integer}
     */
    p.getOffsetColumn = function () {
        return this.settings.offsetColumn;
    };

    /**
     * @method setSelectedRow
     * @iatStudioExposed
     * Sets selectedRow
     * @param {Integer} value
     */
    p.setSelectedRow = function (value) {
        this.settings.selectedRow = value;
        if (this.renderer.tableReady && this.settings.dataOrientation === 'vertical') {
            this.renderer.selectItem('row', value, true);
        }
    };

    /**
     * @method getSelectedRow
     * Returns selectedRow.
     * @return {Integer}
     */
    p.getSelectedRow = function () {
        return this.settings.selectedRow;
    };

    /**
     * @method setSelectedColumn
     * @iatStudioExposed
     * Sets selectedColumn
     * @param {Integer} value
     */
    p.setSelectedColumn = function (value) {
        this.settings.selectedColumn = value;
        if (this.renderer.tableReady && this.settings.dataOrientation === 'horizontal') {
            this.renderer.selectItem('column', value, true);
        }
    };

    /**
     * @method getSelectedColumn 
     * Returns selectedColumn.
     * @return {Integer}
     */
    p.getSelectedColumn = function () {
        return this.settings.selectedColumn;
    };

    /**
     * @method setTableConfiguration
     * Sets the tableConfiguration
     * @param {String} tableConfiguration
     */
    p.setTableConfiguration = function (tableConfiguration) {
        if (Utils.isObject(tableConfiguration)) {
            this.settings.tableConfiguration = tableConfiguration;
        } else if (Utils.isString(tableConfiguration)) {
            try {
                this.settings.tableConfiguration = JSON.parse(tableConfiguration.replace(/'/g, '"'));
                if (this.renderer.tableReady) {
                    this._showBusyIndicator();
                    this.renderer.updateItemClasses();
                }
            } catch (error) {
                console.iatWarn(this.elem.id + ': TableConfiguration String "' + tableConfiguration + '" is invalid!');
                if (this.renderer.tableReady) {
                    this._showBusyIndicator();
                    this.settings.tableConfiguration = undefined;
                    this.renderer.updateItemClasses();
                }
            }
        }
    };

    /**
     * @method getTableConfiguration
     * Returns the tableConfiguration
     * @return {String} tableConfiguration
     */
    p.getTableConfiguration = function () {
        return this.settings.tableConfiguration;
    };

    /**
     * @method setFilterConfiguration
     * Returns the filterConfiguration
     * @param {String} filterConfiguration
     */
    p.setFilterConfiguration = function (filterConfiguration) {
        this.settings.filterConfiguration = filterConfiguration;
        this.settings.filter = this.configBuilder.parseFilter(filterConfiguration);
        this.renderer.updateTable();
    };

    /**
     * @method getFilterConfiguration
     * Returns the filterConfiguration
     * @return {String} filterConfiguration
     */
    p.getFilterConfiguration = function () {
        return this.settings.filterConfiguration;
    };

    /**
     * @method setEllipsis
     * Sets ellipsis
     * @param {Boolean} ellipsis
     */
    p.setEllipsis = function (ellipsis) {
        this.settings.ellipsis = ellipsis;
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getEllipsis 
     * Returns ellipsis.
     * @return {Boolean}
     */
    p.getEllipsis = function () {
        return this.settings.ellipsis;
    };

    /**
     * @method setUseTableStyling
     * Sets useTableStyling
     * @param {Boolean} useTableStyling
     */
    p.setUseTableStyling = function (useTableStyling) {
        this.settings.useTableStyling = useTableStyling;
    };

    /**
     * @method getUseTableStyling 
     * Returns useTableStyling.
     * @return {Boolean}
     */
    p.getUseTableStyling = function () {
        return this.settings.useTableStyling;
    };

    /**
     * @method setShowSortingButton
     * Sets showSortingButton
     * @param {Boolean} showSortingButton
     */
    p.setShowSortingButton = function (showSortingButton) {
        this.settings.showSortingButton = showSortingButton;
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getShowSortingButton 
     * Returns showSortingButton.
     * @return {Boolean}
     */
    p.getShowSortingButton = function () {
        return this.settings.showSortingButton;
    };

    /**
     * @method setMultiLine
     * Sets multiLine
     * @param {Boolean} multiLine
     */
    p.setMultiLine = function (multiLine) {
        this.settings.multiLine = multiLine;
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getMultiLine 
     * Returns multiLine.
     * @return {Boolean}
     */
    p.getMultiLine = function () {
        return this.settings.multiLine;
    };
    
    /**
     * @method setHeaderSize
     * Sets headerSize
     * @param {UInteger} headerSize
     */
    p.setHeaderSize = function (headerSize) {
        this.settings.headerSize = headerSize;
        this.settings.headerBarSize = (headerSize === 0) ? (this.settings.dataOrientation === Enum.Direction.vertical) ? +this.settings.rowHeight : +this.settings.columnWidth : headerSize;
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getHeaderSize 
     * Returns headerSize.
     * @return {UInteger}
     */
    p.getHeaderSize = function () {
        return this.settings.headerSize;
    };

    /**
     * @method setWordWrap
     * Sets wordWrap
     * @param {Boolean} wordWrap
     */
    p.setWordWrap = function (wordWrap) {
        this.settings.wordWrap = wordWrap;
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getWordWrap 
     * Returns wordWrap.
     * @return {Boolean}
     */
    p.getWordWrap = function () {
        return this.settings.wordWrap;
    };

    /**
      * @method setSelection
      * Sets selection.
      * @param {Boolean} selection
      */
    p.setSelection = function (selection) {
        this.settings.selection = selection;
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getSelection 
     * Returns selection.
     * @return {Boolean}
     */
    p.getSelection = function () {
        return this.settings.selection;
    };

    /**
     * @method setShowScrollbars
     * Sets showScrollbars
     * @param {Boolean} showScrollbars
     */
    p.setShowScrollbars = function (showScrollbars) {
        this.settings.showScrollbars = showScrollbars;

        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getShowScrollbars 
     * Returns showScrollbars.
     * @return {Boolean}
     */
    p.getShowScrollbars = function () {
        return this.settings.showScrollbars;
    };

    /**
     * @method setShowHeader
     * Sets showHeader
     * @param {Boolean} showHeader
     */
    p.setShowHeader = function (showHeader) {
        this.settings.showHeader = showHeader;
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getShowHeader 
     * Returns showHeader.
     * @return {Boolean}
     */
    p.getShowHeader = function () {
        return this.settings.showHeader;
    };

    /**
     * @method setMaxHeight
     * Sets maxHeight
     * @param {Integer} maxHeight
     */
    p.setMaxHeight = function (maxHeight) {
        this.settings.maxHeight = maxHeight;
    };

    /**
     * @method getMaxHeight 
     * Returns maxHeight.
     * @return {Integer}
     */
    p.getMaxHeight = function () {
        return this.settings.maxHeight;
    };

    /**
     * @method setScrollLinkYRefId
     * Sets scrollLinkYRefId
     * @param {String} scrollLinkYRefId
     */
    p.setScrollLinkYRefId = function (scrollLinkYRefId) {
        this.settings.scrollLinkYRefId = scrollLinkYRefId;

        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getScrollLinkYRefId
     * Returns scrollLinkYRefId.
     * @return {String}
     */
    p.getScrollLinkYRefId = function () {
        return this.settings.scrollLinkYRefId;

    };

    /**
     * @method setScrollLinkXRefId
     * Sets scrollLinkXRefId
     * @param {String} scrollLinkXRefId
     */
    p.setScrollLinkXRefId = function (scrollLinkXRefId) {
        this.settings.scrollLinkXRefId = scrollLinkXRefId;
        
        if (brease.config.editMode) {
            this.updateChangeInEditor();
        }
    };

    /**
     * @method getScrollLinkXRefId
     * Returns scrollLinkXRefId.
     * @return {String}
     */
    p.getScrollLinkXRefId = function () {
        return this.settings.scrollLinkXRefId;
    };

    /**
     * @method scrollToHorizontal
     * @iatStudioExposed
     * Scroll to horizontal column
     * @param {UInteger} value
     */
    p.scrollToHorizontal = function (value) {
        if (this.renderer.tableReady) {
            if (value !== undefined && value >= 0 && value < this.renderer.tableDataInfo.columns) {
                this.renderer.scrollHandler.scrollToIndex(-1, value);
            }
        }
    };

    /**
     * @method scrollToVertical
     * @iatStudioExposed
     * Scroll to vertical row
     * @param {UInteger} value
     */
    p.scrollToVertical = function (value) {
        if (this.renderer.tableReady) {
            if (value !== undefined && value >= 0 && value < this.renderer.tableDataInfo.rows) {
                this.renderer.scrollHandler.scrollToIndex(value, -1);
            }
        }
    };

    /**
     * @method scrollPageDown
     * @iatStudioExposed
     * Scroll one page down
     */
    p.scrollPageDown = function () {
        if (this.renderer.tableReady) {
            this.renderer.scrollHandler.scrollPageDown();
        }
    };

    /**
     * @method scrollPageUp
     * @iatStudioExposed
     * Scroll one page up
     */
    p.scrollPageUp = function () {
        if (this.renderer.tableReady) {
            this.renderer.scrollHandler.scrollPageUp();
        }
    };

    /**
     * @method scrollPageLeft
     * @iatStudioExposed
     * Scroll one page left
     */
    p.scrollPageLeft = function () {
        if (this.renderer.tableReady) {
            this.renderer.scrollHandler.scrollPageLeft();
        }
    };

    /**
     * @method scrollPageRight
     * @iatStudioExposed
     * Scroll one page right
     */
    p.scrollPageRight = function () {
        if (this.renderer.tableReady) {
            this.renderer.scrollHandler.scrollPageRight();
        }
    };

    /**
     * @method scrollStepDown
     * @iatStudioExposed
     * Scroll one step down
     */
    p.scrollStepDown = function () {
        if (this.renderer.tableReady) {
            this.renderer.scrollHandler.scrollItemDown();
        }
    };

    /**
     * @method scrollStepUp
     * @iatStudioExposed
     * Scroll one step up
     */
    p.scrollStepUp = function () {
        if (this.renderer.tableReady) {
            this.renderer.scrollHandler.scrollItemUp();
        }
    };

    /**
     * @method scrollStepRight
     * @iatStudioExposed
     * Scroll one step right
     */
    p.scrollStepRight = function () {
        if (this.renderer) {
            this.renderer.scrollHandler.scrollItemRight();
        }
    };

    /**
     * @method scrollStepLeft
     * @iatStudioExposed
     * Scroll one step left
     */
    p.scrollStepLeft = function () {
        if (this.renderer.tableReady) {
            this.renderer.scrollHandler.scrollItemLeft();
        }
    };

    /**
     * @method setVerticalScroll
     * Scroll vertical to position
     * @param {Integer} position
     * Position in Y Direction
     * @param {String} origin
     * Original event widget ID
     */
    p.setVerticalScroll = function (position, redirectedBy) {
        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            this.renderer.scrollHandler.scrollTo(position, 'Y', false);
        } else {
            this.renderer.scrollHandler.scrollTo(position, 'Y', true);
        }

        if (this.settings.scrollLinkYRefId !== undefined) {
            if (redirectedBy.indexOf(this.elem.id) === -1 && redirectedBy.indexOf(this.settings.scrollLinkYRefId) === -1) {
                redirectedBy.push(this.elem.id);
                brease.callWidget(this.settings.scrollLinkYRefId, 'setVerticalScroll', position, redirectedBy);
            } else {
                return false;
            }
        }
    };

    /**
     * @method setHorizontalScroll
     * Scroll horizontal to position
     * @param {Integer} position
     * Position in X Direction
     * @param {String} origin
     * Original event widget ID
     */
    p.setHorizontalScroll = function (position, redirectedBy) {
        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            this.renderer.scrollHandler.scrollTo(position, 'X', true);
        } else {
            this.renderer.scrollHandler.scrollTo(position, 'X', false);
        }

        if (this.settings.scrollLinkXRefId !== undefined) {
            if (redirectedBy.indexOf(this.elem.id) === -1 && redirectedBy.indexOf(this.settings.scrollLinkXRefId) === -1) {
                redirectedBy.push(this.elem.id);
                brease.callWidget(this.settings.scrollLinkXRefId, 'setHorizontalScroll', position, redirectedBy);
            } else {
                return false;
            }
        }
    };

    /**
     * @method setRowVisibility
     * @iatStudioExposed
     * Set the visibility for a certain row.
     * @param {Integer} index
     * @param {Boolean} visible
     */
    p.setRowVisibility = function (index, visible) {
        _adjustTableConfiguration(this, 'row', index, visible);
        this.renderer.updateItemClasses();
        this.submitTableConfiguration();
    };

    /**
     * @method setColumnVisibility
     * @iatStudioExposed
     * Set the visibility for a certain column.
     * @param {Integer} index
     * @param {Boolean} visible
     */
    p.setColumnVisibility = function (index, visible) {
        _adjustTableConfiguration(this, 'column', index, visible);
        this.renderer.updateItemClasses();
        this.submitTableConfiguration();
    };

    /**
     * @method openConfiguration
     * @iatStudioExposed
     * @param {TableConfigurationType} type (Supported types: Filtering)
     * Open the filter part of the configuration dialogue
     */
    p.openConfiguration = function (type) {
        if (this.isDisabled) { return; }
        
        if (type === 'Filtering') {
            this.configDialogue = new TableDialogue(this);
            this.configDialogue.openFilter();
        // } else if (type === 'Configuration'){
        //     this.configDialogue = new TableDialogue(this);
        //     this.configDialogue.openConfig();
        } else {
            console.iatWarn('Unsupported configuration type!');
        }
    };

    //-----------------------------------------------------------------
    p._startInitialisation = function (e) {
        var self = this;
        this.init = true;

        $.when(this.widgetReady.promise(), this.tableReady.promise()).then(function successHandler() {
            $.each(self.settings.tableItemIds, function (index, tableItemId) {
                // Notify all TableItems to be ready
                brease.callWidget(tableItemId, 'setTableReady', self.elem.id, true);
            });
        });

        if (brease.config.editMode && this.settings.tableItemIds.length === 0) {
            return false;
        }

        this._childInitializationProgress();
    };

    p._widgetReadyHandler = function (e) {
        if (e.target.id === this.elem.id) {
            this.widgetReady.resolve();
        }
    };

    p._tableReadyCallback = function () {
        this.omitScrollPositionSubmit = false;
        if ((this.settings.offsetColumn <= this.renderer.tableDataInfo.columns - 1) && (this.settings.offsetRow <= this.renderer.tableDataInfo.rows - 1)) {
            if ((this.settings.offsetColumn !== 0) && (this.settings.offsetRow !== 0)) {
                this.renderer.scrollHandler.scrollToIndex(this.settings.offsetRow, this.settings.offsetColumn);
            } else if ((this.settings.offsetColumn !== 0) && (this.settings.offsetRow === 0)) {
                this.renderer.scrollHandler.scrollToIndex(-1, this.settings.offsetColumn);
            } else if ((this.settings.offsetColumn === 0) && (this.settings.offsetRow !== 0)) {
                this.renderer.scrollHandler.scrollToIndex(this.settings.offsetRow, -1);
            }
        } else {
            this.renderer.scrollHandler.calcScrollItemOffset();
        }
        if (this.tableReady.state() !== 'resolved') {
            this.tableReady.resolve();
        }
    };

    p._rendererReadyCallback = function () {
        this._dispatchReady();
    };

    p._selectionCallback = function (selection) {
        if (selection.rowIndex !== undefined) {
            this.settings.selectedRow = selection.rowIndex;
            this.sendValueChange({ selectedRow: this.settings.selectedRow });
        }
        if (selection.columnIndex !== undefined) {
            this.settings.selectedColumn = selection.columnIndex;
            this.sendValueChange({ selectedColumn: this.settings.selectedColumn });
        }
        if (selection.rowIndex !== undefined && selection.columnIndex !== undefined) {
            if (this.settings.dataOrientation === Enum.Direction.vertical) {
                _updateImageIndex(this, selection.rowIndex);
            } else {
                _updateImageIndex(this, selection.columnIndex);
            }
        }

        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            this._forwardClickEvent(selection.event, selection.columnIndex);
        } else {
            this._forwardClickEvent(selection.event, selection.rowIndex);
        }
    };

    p._drawCallback = function () {
        if (this.settings.tableItemIds.length > 0) {
            this._hideBusyIndicator();
        }

        if (this.updatePostponed === true) {
            $.each(this.settings.tableItemIds, function (index, tableItemId) {
                var state = brease.uiController.callWidget(tableItemId, 'getDataUpdateState');
                for (var key in state) {
                    if (state[key] === true) {
                        this['_' + key](tableItemId);
                    }
                }
            });
        }
    };

    p._scrollCallback = function (scrollItemOffset) {

        this.settings.offsetRow = scrollItemOffset.row;
        this.settings.offsetColumn = scrollItemOffset.column;
        if (this.omitScrollPositionSubmit === false) {
            this.submitScrollOffset();
        } else {
            this.omitScrollPositionSubmit = false;
        }

    };

    p._headerClickCallback = function (e, indexOfChild) {
        this._forwardClickEvent(e, indexOfChild);
    };

    p._valueUpdateAvailable = function (tableItemId) {
        var redrawTable = false;
        if (this.renderer.busy === true) {
            this.updatePostponed = true;
        }
        if (this.renderer.busy === false) {
            var tableItemIndex = this.settings.tableItemIds.indexOf(tableItemId),
                data = brease.uiController.callWidget(tableItemId, 'getData');

            var largestPrev = Math.max.apply(Math, this.settings.tableItemLengths);
            this.settings.tableItemLengths[tableItemIndex] = data.length;
            var largest = Math.max.apply(Math, this.settings.tableItemLengths);

            if (largestPrev !== largest) {
                redrawTable = true;
            } else {
                redrawTable = false;
            }

            //redraw is for stopping continuous updates of the table! Do not remove. If a filter has been set, then the table MUST redraw to update the internal model
            if ((redrawTable && this.init === false) || 
                (this.init === false && this.settings.filterConfiguration.length > 3) || 
                (this.init === false && this.settings.showSortingButton)) {
                _fetchTableConfig(this, this.settings.tableItemIds);
                this.settings.tableData = _getData(this.settings.tableItemIds, this.settings.dataOrientation, this);
                this.setTableData(this.settings.tableData, false);
            } else {
                brease.uiController.callWidget(tableItemId, 'valueUpdateApplied');
                if (this.settings.dataOrientation === Enum.Direction.horizontal) {
                    this.setRowData(data, tableItemIndex);
                } else if (this.settings.dataOrientation === Enum.Direction.vertical) {
                    this.setColumnData(data, tableItemIndex);
                }
            }
        }
    };

    p._headerUpdateAvailable = function (tableItemId) {
        if (this.renderer.busy === true) {
            this.updatePostponed = true;
        }
        if (this.renderer.busy === false) {
            var tableItemIndex = this.settings.tableItemIds.indexOf(tableItemId),
                text = brease.uiController.callWidget(tableItemId, 'getHeaderText');

            brease.uiController.callWidget(tableItemId, 'headerUpdateApplied');
            this._showBusyIndicator();
            this.settings.headerTexts[tableItemIndex] = text;
            this.renderer.updateTexts();
        }
    };

    p._visibleUpdateAvailable = function (tableItemId) {
        if (brease.config.editMode) { return; }
        if (this.renderer.busy === true) {
            this.updatePostponed = true;
        }
        if (this.renderer.busy === false) {
            this._showBusyIndicator();
            var tableItemIndex = this.settings.tableItemIds.indexOf(tableItemId),
                visible = brease.uiController.callWidget(tableItemId, 'getVisible');

            brease.uiController.callWidget(tableItemId, 'visibleUpdateApplied');
            this.settings.itemVisibility[tableItemIndex] = visible;
            this.renderer.updateItemClasses();
        }
        this._valueUpdateAvailable(tableItemId);
    };

    p._itemSizeUpdateAvailable = function (tableItemId, type) {
        if (this.renderer.busy === true) {
            this.updatePostponed = true;
        }

        if (this.renderer.busy === false) {
            var tableItemIndex = this.settings.tableItemIds.indexOf(tableItemId),
                size = 0;
            if (type === 'column') {
                size = brease.uiController.callWidget(tableItemId, 'getColumnWidth');
                brease.uiController.callWidget(tableItemId, 'itemSizeUpdateApplied');

                //We don't want to update table in editor if size is the same as this messes up the scrollbars
                //if (brease.config.editMode && this.settings.itemColumnWidths[tableItemIndex] === size) { return;}
                this.settings.itemColumnWidths[tableItemIndex] = size;
                this.renderer.updateColumnWidths();
            } else if (type === 'row') {
                size = brease.uiController.callWidget(tableItemId, 'getRowHeight');
                brease.uiController.callWidget(tableItemId, 'itemSizeUpdateApplied');
                this.settings.itemRowHeights[tableItemIndex] = size;
                this.renderer.updateRowHeights();
            }
            this.renderer.updateItemClasses();
        }
    };

    p._enableUpdateAvailable = function (tableItemId) {
        if (this.renderer.busy === true) {
            this.updatePostponed = true;
        }
        if (this.renderer.busy === false) {
            var tableItemIndex = this.settings.tableItemIds.indexOf(tableItemId),
                enable = brease.uiController.callWidget(tableItemId, 'isEnabled');
            this.settings.itemEnableStates[tableItemIndex] = enable;

            brease.uiController.callWidget(tableItemId, 'enableUpdateApplied');
            this.renderer.updateItemClasses();
        }
    };

    p._formatUpdateAvailable = function (tableItemId) {
        var tableItemIndex = this.settings.tableItemIds.indexOf(tableItemId),
            format = brease.uiController.callWidget(tableItemId, 'getFormat');
        this.settings.itemConfigs[tableItemIndex].inputConfig.format = format;
    };

    p._forwardClickEvent = function (e, indexOfChild) {
        var id = _evaluateClickedChild(this, indexOfChild);
        brease.callWidget(id, '_clickHandler', e, { origin: id });
    };

    p.getInternalOrderState = function (indexOfChild) {
        return brease.callWidget(_evaluateClickedChild(this, indexOfChild), 'updateOrderState');
    };

    p.submitScrollOffset = function () {
        if (this.isDisabled === true) { return false; }
        this.sendValueChange({ offsetRow: this.settings.offsetRow });
        this.sendValueChange({ offsetColumn: this.settings.offsetColumn });
    };

    p.submitUserInput = function (itemIndex, arrayIndex, inputValue) {
        var tableItemDataArray = [],
            tableItemId;

        if (this.settings.dataOrientation === Enum.Direction.horizontal) {
            this.settings.tableData[itemIndex][arrayIndex] = inputValue;
            tableItemId = this.settings.tableItemIds[itemIndex];
            tableItemDataArray = this.settings.tableData[itemIndex];
        } else if (this.settings.dataOrientation === Enum.Direction.vertical) {
            this.settings.tableData[arrayIndex][itemIndex] = inputValue;
            tableItemId = this.settings.tableItemIds[itemIndex];
            $.each(this.settings.tableData, function (index, array) {
                tableItemDataArray.push(array[itemIndex]);
            });
        }

        brease.callWidget(tableItemId, 'submitChange', tableItemDataArray);
        /**
         * @event ValueChanged
         * @iatStudioExposed
         * Triggered when any value in the table is changed
         */
        var ev = this.createEvent('ValueChanged', {});
        ev.dispatch();

    };

    p.submitTableConfiguration = function () {
        this.sendValueChange({ tableConfiguration: JSON.stringify(this.getTableConfiguration()).replace(/"/g, '\'') });
    };

    p.submitFilterConfiguration = function () {
        this.settings.filterConfiguration = this.configBuilder.serializeFilter(this.settings.filter);
        this.sendValueChange({ filterConfiguration: this.settings.filterConfiguration });
    };

    p._childInitializationProgress = function () {
        _initChildren(this);
    };

    p.childrenInitializedHandler = function () {
        
        var self = this;
        this.container.children('.breaseWidget').each(function () {
            if (this.tagName !== 'TABLE') {
                self.settings.tableItemIds.push($(this).attr('id'));
                self.settings.tableItemTypes.push($(this).attr('data-brease-widget'));
            }
        });

        _fetchTableConfig(this, this.settings.tableItemIds);
        this.renderer.setInitData();
        this._showBusyIndicator();
        this.dataInitialization();
    };

    p.childrenInitializedInEditorHandler = function () {
        this.renderer.setInitData();
        this._showBusyIndicator();

        this.dataInitDone(this.settings.editorFirst);
        this.settings.editorFirst = false;
    };

    p.dataInitialization = function () {
        var self = this,
            dataInitStates = [];

        for (var i = 0; i < this.settings.tableItemIds.length; i += 1) {
            dataInitStates[i] = new $.Deferred();
            dataInitStates[i].promise();
        }

        $.when.apply($, dataInitStates).then(function () {
            self.el.off('dataInitDone');
            self.dataInitDone();
            //Reference the scroller to the this.scroller object -> Triggered by the ContainerWidget
            self.scroller = self.renderer.scrollHandler.scrollerBody;
        });

        $.each(this.settings.tableItemIds, function (index, tableItemId) {
            if (brease.callWidget(tableItemId, 'getDataInitState')) {
                dataInitStates[index].resolve();
            } else {
                self.el.one('dataInitDone', function (e) {
                    var tableItemTarget = e.target.id;
                    e.stopImmediatePropagation();
                    dataInitStates[_.indexOf(self.settings.tableItemIds, tableItemTarget)].resolve();
                });
            }
        });
    };

    p.dataUpdateData = function () {
        _fetchTableConfig(this, this.settings.tableItemIds);
        this.settings.tableData = _getData(this.settings.tableItemIds, this.settings.dataOrientation, this);
        this.setTableData(this.settings.tableData, false);
    };

    p.dataInitDone = function (init) {
        init = (init === undefined) ? true : init;

        _fetchTableConfig(this, this.settings.tableItemIds);
        this.settings.tableData = _getData(this.settings.tableItemIds, this.settings.dataOrientation, this);
        this.init = false;
        this.setTableData(this.settings.tableData, init);
    };

    p.setVisible = function () {
        SuperClass.prototype.setVisible.apply(this, arguments);
        this.renderer.setVisible();
    };

    p.disable = function () {
        SuperClass.prototype.disable.apply(this, arguments);
        if (this.renderer) {
            this.renderer.disable();
            this.renderer.updateItemClasses();
        }
    };

    p.enable = function () {
        SuperClass.prototype.enable.apply(this, arguments);
        if (this.renderer) {
            this.renderer.enable();
            this.renderer.updateItemClasses();
        }
    };

    function _getData(tableItemIds, orientation, widget) {
        var dataSets = [],
            tableData = [],
            maxDataSetLength = 0;
        if (tableItemIds.length !== 0) {
            $.each(tableItemIds, function (index, tableItemId) {
                var tmp = brease.callWidget(tableItemId, 'getData'),
                    valueArray = [];
                $.each(tmp, function (ind, val) {
                    if (widget.settings.tableItemTypes[index] === 'widgets/brease/TableItem') {
                        // This line serves to remove < and > tags from a string and replace them with &lt; and &gt;
                        val = widget._removeHTMLTags(val);
                    }
                    valueArray[ind] = widget._breakLine(val);
                });
                dataSets.push(valueArray);
                widget.settings.tableItemLengths[index] = dataSets[index].length;
                if (dataSets[index].length > maxDataSetLength) {
                    maxDataSetLength = dataSets[index].length;
                }
            });
            
            dataSets = dataSets.slice(0);
    
            switch (orientation) {
                case Enum.Direction.horizontal:
                    $.each(dataSets, function (index, dataSet) {
                        tableData.push(dataSet);
                        var dataSetLength = dataSet.length;
                        if (dataSetLength === 0) {
                            // Must be done due to datatables js. Throws an error if array is empty in horizontal direction
                            tableData[index].push('');
                            dataSetLength += 1;
                        }
                        if (dataSetLength < maxDataSetLength) {
                            for (var i = 0; i < maxDataSetLength - dataSetLength; i += 1) {
                                tableData[index].push('');
                            }
                        }
                    });
                    
                    break;
    
                case Enum.Direction.vertical:
                    for (var i = 0; i < maxDataSetLength; i += 1) {
                        tableData.push([]);
                    }
    
                    $.each(dataSets, function (dataSetIndex, dataSet) {
                        for (var i = 0; i < maxDataSetLength; i += 1) {
                            if (i > (dataSet.length - 1)) {
                                tableData[i][dataSetIndex] = '';
                            } else {
                                tableData[i][dataSetIndex] = dataSets[dataSetIndex][i];
                            }
                        }
                    });
    
                    break;
                
                default:
                    break;
            }
        }

        return tableData;
    }

    p._showBusyIndicator = function () {
        if (brease.config.editMode) { return; }
        this.busyWrapper.addClass('visible');

        if (this.settings.dataOrientation === Enum.Direction.vertical) {
            this.busyWrapper.css({
                'top': '0px',
                'margin-top': this.settings.rowHeight + 'px',
                'left': parseInt(this.settings.width, 10) / 2 - 25 + 'px'
            
            });
        } else {
            this.busyWrapper.css({
                'left': '0px',
                'margin-left': this.settings.columnWidth + 'px',
                'top': parseInt(this.settings.height, 10) / 2 - 25 + 'px'
            });
        }
    };

    p._hideBusyIndicator = function () {
        if (brease.config.editMode) { return; }
        this.busyWrapper.removeClass('visible');
    };

    p._onWidgetsReady = function (e) {
        if (e.target.id === this.busyId) {
            this.busyIndicator = brease.uiController.callWidget(this.busyId, 'widget');
            this.busyWrapper.off(BreaseEvent.WIDGET_READY, this._bind('_onWidgetsReady'));
        }
    };

    p._scrollXYHandler = function (e) {
        e.stopImmediatePropagation();
        var redirectedBy = [this.elem.id];

        if (this.settings.scrollLinkYRefId !== undefined && this.settings.scrollLinkXRefId !== undefined) {
            brease.callWidget(this.settings.scrollLinkYRefId, 'setVerticalScroll', e.detail, redirectedBy);
            brease.callWidget(this.settings.scrollLinkXRefId, 'setHorizontalScroll', e.detail, redirectedBy);
        } else if (this.settings.scrollLinkYRefId !== undefined) {
            brease.callWidget(this.settings.scrollLinkYRefId, 'setVerticalScroll', e.detail, redirectedBy);
        } else if (this.settings.scrollLinkXRefId !== undefined) {
            brease.callWidget(this.settings.scrollLinkXRefId, 'setHorizontalScroll', e.detail, redirectedBy);
        }
    };

    p.dispose = function () {
        if (!brease.config.editMode) {
            window.clearTimeout(this.offsetRowDeferID);
            window.clearTimeout(this.offsetColumnDeferID);
            this.elem.removeEventListener('ScrollXY', this._bind('_scrollXYHandler'));
            this.renderer.dispose();
        }
        SuperClass.prototype.dispose.apply(this, arguments);
    };
    
    p.suspend = function () {
        this.renderer.suspend();
        SuperClass.prototype.suspend.apply(this, arguments);
    };

    p.wake = function () {
        this.renderer.wake();
        SuperClass.prototype.wake.apply(this, arguments);
    };

    /***********Private Methods***********/

    function _initChildren(widget) {
        var itemDefs = [];

        widget.el.find('[data-brease-widget]').each(function () {
            var children = this,
                id = children.id,
                d = $.Deferred();

            itemDefs.push(d);

            if (brease.uiController.getWidgetState(id) >= Enum.WidgetState.READY && brease.uiController.getWidgetState(id) !== Enum.WidgetState.SUSPENDED) {
                d.resolve();
            } else {
                children.addEventListener(BreaseEvent.WIDGET_READY, function () {
                    d.resolve();
                });
            }
        });

        $.when.apply($, itemDefs).done(function () {
            widget.childrenInitializedHandler();
        });
    }

    function _fetchTableConfig(widget, tableItemIds) {
        widget.settings.itemVisibility = [];
        widget.settings.itemColumnWidths = [];
        widget.settings.itemRowHeights = [];
        widget.settings.headerTexts = [];
        widget.settings.itemConfigs = [];
        widget.settings.itemEnableStates = [];

        $.each(tableItemIds, function (index, tableItemId) {
            brease.uiController.callWidget(tableItemId, 'valueUpdateApplied');
            widget.settings.itemVisibility.push(brease.callWidget(tableItemId, 'getVisible'));
            brease.uiController.callWidget(tableItemId, 'visibleUpdateApplied');
            widget.settings.itemColumnWidths.push(brease.callWidget(tableItemId, 'getColumnWidth'));
            widget.settings.itemRowHeights.push(brease.callWidget(tableItemId, 'getRowHeight'));
            brease.uiController.callWidget(tableItemId, 'itemSizeUpdateApplied');
            widget.settings.headerTexts.push(brease.callWidget(tableItemId, 'getHeaderText'));
            brease.uiController.callWidget(tableItemId, 'headerUpdateApplied');
            widget.settings.itemConfigs.push(brease.callWidget(tableItemId, 'getItemConfig'));
            widget.settings.itemEnableStates.push(brease.callWidget(tableItemId, 'isEnabled'));
            brease.uiController.callWidget(tableItemId, 'enableUpdateApplied');
            brease.uiController.callWidget(tableItemId, 'setDataOrientation', widget.settings.dataOrientation);
            widget.settings.itemStyling.push(brease.callWidget(tableItemId, 'getStyle'));                
        });
    }

    function _addChildWidgets(widget) {
        widget.busyWrapper.on(BreaseEvent.WIDGET_READY, widget._bind('_onWidgetsReady'));

        brease.uiController.createWidgets(widget.busyWrapper[0], [
            {
                className: 'BusyIndicator',
                id: widget.busyId,
                options: {}
            }
        ], true, widget.settings.parentContentId);
    }

    function _adjustTableConfiguration(widget, type, accordingIndex, visibility) {
        var elementToAccess = 'spec' + type.charAt(0).toUpperCase() + type.slice(1) + 's',
            arrayElement;

        if (widget.settings.tableConfiguration !== undefined && Utils.isObject(widget.settings.tableConfiguration)) {
            if (Array.isArray(widget.settings.tableConfiguration[elementToAccess])) {
                arrayElement = widget.renderer.findElementInConfig(widget.settings.tableConfiguration[elementToAccess], accordingIndex);
                if (arrayElement !== undefined) {
                    arrayElement.visible = visibility;
                } else {
                    widget.settings.tableConfiguration[elementToAccess].push({ index: accordingIndex, visible: visibility });
                }
            } else {
                widget.settings.tableConfiguration[elementToAccess] = [];
                widget.settings.tableConfiguration[elementToAccess].push({ index: accordingIndex, visible: visibility });
            }
        } else {
            widget.settings.tableConfiguration = { specRows: [], specColumns: [] };
            widget.settings.tableConfiguration[elementToAccess].push({ index: accordingIndex, visible: visibility });
        }
    }

    p._breakLine = function (text) {
        // Regexp for replacing \n to <br />
        // Source: http://stackoverflow.com/questions/5076466/javascript-replace-n-with-br
        if (this.settings.multiLine && text.length > 0 && typeof text === 'string') {
            return text.replace(/\\n/g, '<br />');
        } else {
            return text;
        }
        
    };

    p._removeHTMLTags = function (t) {
        if (typeof t === 'string') {
            t = t.replace(/</g, '&lt;');
            t = t.replace(/>/g, '&gt;');
        }
        return t;
    };

    function _updateImageIndex(widget, selection) {

        $.each(widget.settings.tableItemTypes, function (index) {
            if (widget.settings.tableItemTypes[index] === 'widgets/brease/TableItemImageList') {
                brease.callWidget(widget.settings.tableItemIds[index], '_updateSelectedImageIndex', selection);
            }
        });
    }

    function _evaluateClickedChild(widget, index) {
        return widget.settings.tableItemIds[index]; //Returns the ID of the corresponding child
    }

    function _scrollSynchronizationSetup(widget) {
        if (widget.settings.scrollLinkYRefId.length === 0) {
            widget.settings.scrollLinkYRefId = undefined;
        }

        if (widget.settings.scrollLinkXRefId.length === 0) {
            widget.settings.scrollLinkXRefId = undefined;
        }
        widget.elem.addEventListener('ScrollXY', widget._bind('_scrollXYHandler'));
    }

    return WidgetClass;
});
