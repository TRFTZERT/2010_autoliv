define(['widgets/brease/GroupBox/GroupBox',
    'brease/decorators/LanguageDependency'
], function (SuperClass, languageDependency) {

    'use strict';

    /**
     * @class widgets.brease.FlexBoxItem
     * @extends widgets.brease.GroupBox
     * @iatMeta studio:isContainer
     * true
     *
     * @iatMeta category:Category
     * Container
     *
     * @iatMeta description:short
     * Container Widget
     * @iatMeta description:de
     * Flexibler Bereich innerhalb einer FlexBox, der je nach Konfiguration wachsen oder schrumpfen kann
     * @iatMeta description:en
     * Flexible area inside of a FlexBox, which can grow or shrink depending on the configuration
     */

    /**
     * @cfg {UNumber} sizeFactor=1
     * @bindable
     * @iatStudioExposed
     * @iatCategory Appearance
     * Factor used to distribute the free space in the parent container (FlexBox)
     */

    /**
     * @cfg {Integer} maxHeight
     * @hide
     */

    /**
     * @cfg {brease.enum.Direction} alignment
     * @hide
     */

    /**
     * @property {WidgetList} [parents=["widgets.brease.FlexBox"]]
     * @inheritdoc  
     */

    var defaultSettings = {
            sizeFactor: 1
        },

        WidgetClass = SuperClass.extend(function FlexBoxItem() {
            SuperClass.apply(this, arguments);
        }, defaultSettings),

        p = WidgetClass.prototype;

    p.init = function () {

        if (this.settings.omitClass !== true) {
            this.addInitialClass('breaseFlexBoxItem');
        }

        this.settings.omitClass = true;

        SuperClass.prototype.init.call(this);

        _setSizeFactor(this);

    };

    // override method called in BaseWidget.init and defined in SuperClass GroupBox
    p._initEditor = function () {
        var widget = this;
        require(['widgets/brease/FlexBoxItem/libs/EditorHandles'], function (EditorHandles) {
            var editorHandles = new EditorHandles(widget);
            widget.getHandles = function () {
                return editorHandles.getHandles();
            };
            // workaround
            widget.designer.getSelectionDecoratables = function () {
                return editorHandles.getSelectionDecoratables();
            };
        });
    };

    p.visibilityChangeHandler = function (e) {
        if (this.scroller && this.elem.hidden === false) {
            this.debouncedRefresh();
        }
    };

    /**
     * @method setSizeFactor
     * @iatStudioExposed
     * Sets sizeFactor
     * @param {UNumber} sizeFactor
     */
    p.setSizeFactor = function (sizeFactor) {
        this.settings.sizeFactor = sizeFactor;
        _setSizeFactor(this);
    };

    /**
     * @method getSizeFactor
     * Returns sizeFactor.
     * @return {UNumber}
     */
    p.getSizeFactor = function () {
        return this.settings.sizeFactor;
    };

    //Private

    function _setSizeFactor(widget) {
        widget.el.css('flex-grow', widget.getSizeFactor());
    }

    return languageDependency.decorate(WidgetClass, false);

});
