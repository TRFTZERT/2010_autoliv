define([
    'brease/core/ContainerWidget',
    'brease/events/BreaseEvent',
    'brease/enum/Enum',
    'widgets/brease/GridLineItem/libs/Config',
    'widgets/brease/common/libs/ImageHandling',
    'brease/decorators/DragAndDropCapability'
], function (SuperClass, BreaseEvent, Enum, Config, ImageHandling, dragAndDropCapability) {

    'use strict';

    /**
     * @class widgets.brease.GridLineItem
     * @extends brease.core.ContainerWidget
     *
     * @mixins widgets.brease.common.DragDropProperties.libs.DroppablePropertiesEvents
     * 
     * @iatMeta studio:isContainer
     * true
     * @iatMeta studio:visible
     * true
     * @iatMeta category:Category
     * Container
     * @iatMeta description:short
     * Container for Widgets
     * @iatMeta description:de
     * Definiert einen Bereich innerhalb einer GridLine, welcher mit einer definierten Groeße vergroeßert / verkleinert werden kann.
     * @iatMeta description:en
     * Defines an area inside a GridLine, which can be expanded / collapsed with a defined size.
     */

    var defaultSettings = Config,

        WidgetClass = SuperClass.extend(function GridLineItem() {
            SuperClass.apply(this, arguments);
        }, defaultSettings),

        p = WidgetClass.prototype;

    p.init = function () {
        this.el.addClass('breaseGridLineItem');
        this.addEventListener();
        SuperClass.prototype.init.call(this);
        this.setImageHandling(ImageHandling);
    };

    p.setImageHandling = function (ImageHandling) {
        this.imageHandlingModule = new ImageHandling();
    };

    /**
     * @method setCollapsedSize
     * Sets collapsedSize
     * @param {UInteger} collapsedSize
     */
    p.setCollapsedSize = function (collapsedSize) {
        this.settings.collapsedSize = collapsedSize;
        if (brease.config.editMode) {
            brease.callWidget(this.settings.parentRefId, 'updateScroller');
        }
    };

    /**
     * @method getCollapsedSize 
     * Returns collapsedSize.
     * @return {UInteger}
     */
    p.getCollapsedSize = function () {
        return this.settings.collapsedSize;
    };

    /**
     * @method setExpandedSize
     * Sets expandedSize
     * @param {UInteger} expandedSize
     */
    p.setExpandedSize = function (expandedSize) {
        this.settings.expandedSize = expandedSize;
        this.setExpand();
        if (brease.config.editMode) {
            brease.callWidget(this.settings.parentRefId, 'updateScroller');
        }
    };

    /**
     * @method getExpandedSize 
     * Returns expandedSize.
     * @return {UInteger}
     */
    p.getExpandedSize = function () {
        return this.settings.expandedSize;
    };

    /**
     * @method setImageAlign
     * Sets imageAlign
     * @param {brease.enum.HorizontalAlign} imageAlign
     */
    p.setImageAlign = function (imageAlign) {
        this.settings.imageAlign = imageAlign;
        this.imgEl.removeClass(Enum.HorizontalAlign.left + ' ' + Enum.HorizontalAlign.right);
        this.imgEl.addClass(imageAlign);
    };

    /**
     * @method getImageAlign 
     * Returns imageAlign.
     * @return {brease.enum.HorizontalAlign}
     */
    p.getImageAlign = function () {
        return this.settings.imageAlign;
    };

    /**
     * @method setShowExpanded
     * Sets showExpanded
     * @param {Boolean} showExpanded
     */
    p.setShowExpanded = function (showExpanded) {
        this.settings.showExpanded = showExpanded;
    };

    /**
     * @method getShowExpanded 
     * Returns showExpanded.
     * @return {Boolean}
     */
    p.getShowExpanded = function () {
        return this.settings.showExpanded;
    };

    //Internal Methods

    p.setStatus = function (status) {
        this.settings.status = status;
    };

    p.getStatus = function () {
        return this.settings.status;
    };

    // override method called in BaseWidget.init
    p._initEditor = function () {
        var widget = this;
        require(['widgets/brease/GridLineItem/libs/EditorHandles'], function (EditorHandles) {
            var editorHandles = new EditorHandles(widget);
            widget.getHandles = function () {
                return editorHandles.getHandles();
            };
            widget.designer.getSelectionDecoratables = function () {
                return editorHandles.getSelectionDecoratables();
            };
        });
    };

    p.setGridSize = function (gridSize) {
        this.settings.gridSize = parseInt(gridSize, 10);
        if (brease.config.editMode) {
            this.setExpand();
        } else {
            this.setCollapse();
        }
    };

    p.setOrientation = function (orientation) {
        this.settings.orientation = orientation;
        this.el.removeClass(Enum.Direction.horizontal + ' ' + Enum.Direction.vertical);
        this.el.addClass(this.settings.orientation);
    };

    p.setModify = function (value) {
        this.settings.setModify = value;
        if (value === true) {
            if (this.settings.modifyImage !== undefined && this.settings.modifyImage !== '') {
                this.imageHandling(this.settings.modifyImage);
            }
        } else {
            if (this.getStatus() === 1) {
                this.imageHandling(this.settings.expandImage);
            } else {
                this.imageHandling(this.settings.collapseImage);
            }
        }
    };

    p.setExpand = function () {
        if (this.settings.setModify === true) { return; }
        var size = this.multiplicator(this.settings.gridSize, this.settings.expandedSize);

        if (this.settings.orientation === Enum.Direction.horizontal) {
            this.el.css('width', size + 'px');
            this.el.css('height', '100%');
        } else {
            this.el.css('height', size + 'px');
            this.el.css('width', '100%');
        }

        this.imageHandling(this.settings.expandImage);
        this.triggerExpandedEvent();
    };

    p.setCollapse = function () {
        if (this.settings.setModify === true) { return; }
        var size = this.multiplicator(this.settings.gridSize, this.settings.collapsedSize);

        if (this.settings.orientation === Enum.Direction.horizontal) {
            this.el.css('width', size + 'px');
            this.el.css('height', '100%');
        } else {
            this.el.css('height', size + 'px');
            this.el.css('width', '100%');
        }

        this.imageHandling(this.settings.collapseImage);
        this.triggerCollapsedEvent();
    };

    p.setTransitionTime = function (time) {
        this.settings.transitionTime = time;

        var cssTransition = 'width ' + this.settings.transitionTime + 'ms, height ' + this.settings.transitionTime + 'ms';
        this.el.css('transition', cssTransition).css('-webkit-transition', cssTransition);
    };

    p.setExpandImage = function (expandImage) {
        this.settings.expandImage = expandImage;
        if (brease.config.editMode || this.getStatus() === 1) {
            this.imageHandling(expandImage);
        }
    };

    p.setCollapseImage = function (collapseImage) {
        this.settings.collapseImage = collapseImage;
        if (this.getStatus() === 0 && this.settings.setModify === false) {
            this.imageHandling(collapseImage);
        }
    };

    p.setModifyImage = function (modifyImage) {
        this.settings.modifyImage = modifyImage;
        if (this.settings.setModify === true) {
            this.imageHandling(modifyImage);
        }
    };

    p.imageHandling = function (path) {
        this.imageHandlingModule.setImage(path, this._bind('updateImg'));
    };

    p.updateImg = function (tag) {
        this.container.find('> svg:not([data-brease-widget])').remove();
        this.container.find('> img').remove();
        this.imgEl = tag;
        this.setImageAlign(this.settings.imageAlign);
        this.addStatusClass();
        this.container.append(this.imgEl);
        this.imgEl.on(BreaseEvent.CLICK, this._bind('internalExpandCollapse'));
    };

    p.internalExpandCollapse = function () {
        if (!this.getStatus()) {
            this.setExpand();
        } else {
            this.setCollapse();
        }
        brease.callWidget(this.el.closest('[data-brease-widget=widgets/brease/GridLine]')[0].id, 'updateScroller');
        brease.callWidget(this.el.closest('[data-brease-widget=widgets/brease/GridLine]')[0].id, 'submitItemConfiguration');
    };

    p.triggerExpandedEvent = function () {
        /**
         * @event Expanded
         * @iatStudioExposed
         * Fired when widget gets expanded
         */
        var ev = this.createEvent('Expanded');
        ev.dispatch();

        this.setStatus(1);
    };

    p.triggerCollapsedEvent = function () {
        /**
         * @event Collapsed
         * @iatStudioExposed
         * Fired when widget gets collapsed
         */
        var ev = this.createEvent('Collapsed');
        ev.dispatch();

        this.setStatus(0);
    };

    p.addEventListener = function () {
        this.el.on(BreaseEvent.DBL_CLICK, this._bind('triggerDoubleClickEvent'));
    };

    p.triggerDoubleClickEvent = function () {
        if (this.isDisabled || brease.config.editMode) { return; }

        /**
         * @event DoubleClick
         * @iatStudioExposed
         * Fired when element has double click.
         */
        var clickEv = this.createEvent('DoubleClick');
        clickEv.dispatch();
    };

    p.multiplicator = function (in1, in2) {
        return in1 * in2;
    };

    p.addStatusClass = function () {
        this.imgEl.removeClass('expanded collapsed modify');
        if (this.settings.setModify === false) {
            if (this.getStatus() === 1) {
                this.imgEl.addClass('expanded');
            } else {
                this.imgEl.addClass('collapsed');
            }
        } else {
            this.imgEl.addClass('modify');
        }

    };

    p.suspend = function () {
        this.el.removeClass('shake shakeAlternate');
        SuperClass.prototype.suspend.apply(this, arguments);
    };

    return dragAndDropCapability.decorate(WidgetClass, false);

});
