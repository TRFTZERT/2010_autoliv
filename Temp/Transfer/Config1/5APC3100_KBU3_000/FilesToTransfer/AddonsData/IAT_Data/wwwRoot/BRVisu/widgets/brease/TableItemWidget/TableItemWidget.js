define([
    'brease/core/BaseWidget',
    'brease/events/BreaseEvent',
    'brease/decorators/LanguageDependency', 
    'brease/core/Types',
    'brease/enum/Enum'
], function (
    SuperClass, BreaseEvent, languageDependency, 
    Types, Enum
) {
    
    'use strict';

    /**
     * @class widgets.brease.TableItemWidget
     * @extends brease.core.BaseWidget
     * @iatMeta studio:visible
     * false
     */

    /**
     * @cfg {String} text=''
     * @iatStudioExposed
     * @iatCategory Appearance
     * @bindable
     * @localizable
     * Heading for table column.  
     */

    /**
     * @cfg {UInteger} rowHeight=0
     * @iatStudioExposed
     * @iatCategory Appearance
     * @groupRefId CellSize
     * @groupOrder 1
     * @bindable
     * Can individually override rowHeight of Table if !== 0 when TableItem is a row.  
     */

    /**
     * @cfg {UInteger} columnWidth=0
     * @iatStudioExposed
     * @iatCategory Appearance
     * @groupRefId CellSize
     * @groupOrder 2
     * @bindable
     * Can individually override columnWidth of Table if !== 0 when TableItem is a column.  
     */

    /**
     * @cfg {String} tooltip=''
     * @iatStudioExposed
     * @hide.  
     */

    /**
     * @method showTooltip
     * @hide
     */

    var defaultSettings = {
            text: '',
            rowHeight: 0,
            columnWidth: 0,
            order: undefined,
            dataOrientation: Enum.Direction.vertical
        },

        WidgetClass = SuperClass.extend(function TableItemWidget() {
            SuperClass.apply(this, arguments);
        }, defaultSettings),

        p = WidgetClass.prototype;

    p.init = function () {
        if (this.settings.omitClass !== true) {
            this.addInitialClass('breaseTableItemWidget');
        }

        SuperClass.prototype.init.call(this);

        this.tableReady = false;
        
        _textInit(this);

        this.visibleUpdateAvailable = false;
        this.headerUpdateAvailable = false;
        this.itemSizeUpdateAvailable = false;
        this.initialTableReadyCall = false;
    };

    p._updateTable = function () {
        if (this.isDisabled || !this.tableReady) { return; }
        var event = this.createEvent('TableItemChanged', {});
        event.dispatch();
    };

    /**
     * @method setStyle
     */

    /**
     * @method setEnable
     * @iatStudioExposed
     * Enables (true) or disables (false) the TableItem widget
     * @param {Boolean} value
     */
    p.setEnable = function (value) {
        SuperClass.prototype.setEnable.apply(this, arguments);
        this.enableUpdateAvailable = true;
        if (this.tableReady) {
            this.table._enableUpdateAvailable(this.elem.id);
        }
       
    };

    p.enableUpdateApplied = function () {
        this.enableUpdateAvailable = false;
    };

    /**
     * @method setVisible
     * @iatStudioExposed
     * Sets the visibility of the widget.
     * @param {Boolean} value State of visibility (false = hide, true = show)
     */
    p.setVisible = function (value) {
        if (value !== undefined) {
            this.settings.visible = Types.parseValue(value, 'Boolean');
            this.visibleUpdateAvailable = true;
            if (this.tableReady) {
                this.table._visibleUpdateAvailable(this.elem.id);
            }
            //Needed for visibleChanged Event 
            //BaseWidget will add or remove "remove" class => has no effect, because tableItem is display:none
            SuperClass.prototype.setVisible.apply(this, arguments);
        }
    };

    p.visibleUpdateApplied = function () {
        this.visibleUpdateAvailable = false;
    };

    /**
     * @method setText
     * @iatStudioExposed
     * Sets text
     * @param {String} value
     */
    p.setText = function (value) {
        if (brease.language.isKey(value) === false) {
            this.updateText(value);
            this.removeTextKey();
        } else {
            this.setTextKey(brease.language.parseKey(value), true);
        }

        if (brease.config.editMode) {
            this._updateTable();
        }
    };

    /**
     * @method getText 
     * Returns text.
     * @return {String}
     */
    p.getText = function () {
        return this.settings.text;
    };

    /**
     * @method getHeaderText
     * Returns the current header text
     * @return {String}
     */
    p.getHeaderText = function () {
        return this.settings.text;
    };

    /**
     * @method upodateText
     * Updates the settings object and DOM element
     */
    p.updateText = function (text) {
        if (text !== null) {
            this.settings.text = Types.parseValue(text, 'String');
            this.updateTableHeader(this.settings.text);

        }
        
    };

    /**
     * @method setTextKey
     * Sets textkey
     * @param {String} key The new textkey
     */
    p.setTextKey = function (key, invoke) {

        if (key !== undefined) {
            this.settings.textkey = key;
            this.setLangDependency(true);
            if (invoke === true) {
                this.updateText(brease.language.getTextByKey(this.settings.textkey));
            }
        }
    };

    /**
     * @method removeTextKey
     * remove the textkey
     */
    p.removeTextKey = function () {
        this.settings.textkey = null;
        this.setLangDependency(false);
    };

    /**
     * @method getTextKey
     * Returns textkey
     */
    p.getTextKey = function () {
        return this.settings.textkey;
    };

    /**
     * @method setDataOrientation
     * Sets dataOrientation
     * @param {brease.enum.Direction} dataOrientation
     */
    p.setDataOrientation = function (dataOrientation) {
        this.settings.dataOrientation = dataOrientation;
    };

    /**
     * @method setRowHeight
     * @iatStudioExposed
     * Sets rowHeight
     * @param {Size} value
     */
    p.setRowHeight = function (value) {
        if (value !== undefined) {
            this.settings.rowHeight = value;
            this.itemSizeUpdateAvailable = true;
            if (this.tableReady) {
                if (brease.config.editMode) {
                    this.itemUpdate(value);
                } else {
                    this.table._itemSizeUpdateAvailable(this.elem.id, 'column');
                }
            }
        }
    };

    /**
     * @method getRowHeight 
     * Returns rowHeight.
     * @return {Size}
     */
    p.getRowHeight = function () {
        return this.settings.rowHeight;
    };

    /**
     * @method setColumnWidth
     * @iatStudioExposed
     * Sets columnWidth
     * @param {Size} value
     */
    p.setColumnWidth = function (value) {
        if (value !== undefined) {
            this.settings.columnWidth = value;
            this.itemSizeUpdateAvailable = true;
            if (this.tableReady) {
                if (brease.config.editMode) {
                    this.itemUpdate(value);
                } else {
                    this.table._itemSizeUpdateAvailable(this.elem.id, 'column');
                }
            }
        }
    };

    /**
     * @method getColumnWidth 
     * Returns columnWidth.
     * @return {Size}
     */
    p.getColumnWidth = function () {
        return this.settings.columnWidth;
    };

    p.itemSizeUpdateApplied = function () {
        this.itemSizeUpdateAvailable = false;
    };

    /**
     * @method updateTableHeader
     * Invoke drawing function for headers in the table
     */
    p.updateTableHeader = function (text) {
        if (this.tableReady) {
            this.headerUpdateAvailable = true;
            this.table._headerUpdateAvailable(this.elem.id, text);
        } else {
            this.settings.text = text;
        }
    };

    /**
     * @method updateOrderState 
     * Returns the ordering of the TableItemWidget.
     * @return {String}
     */ 
    p.updateOrderState = function () {
        if (this.order === undefined) {
            this.order = 'desc';
        } else if (this.order === 'desc') {
            this.order = 'asc';
        } else if (this.order === 'asc') {
            this.order = undefined;
        }
        return this.order;
    };

    p.headerUpdateApplied = function () {
        this.headerUpdateAvailable = false;
    };

    p.setTableReady = function (tableId, flag) {
        this.tableId = tableId;
        this.tableReady = flag;
        this.table = brease.callWidget(this.tableId, 'widget');
        this.initUpdateTableValues();
    };

    p.langChangeHandler = function () {
        if (this.settings.textkey) {
            this.updateText(brease.language.getTextByKey(this.settings.textkey));
        }
    };

    p.initUpdateTableValues = function () {
        if (!this.initialTableReadyCall) {
            this.updateTableValues();
        }
    };
    
    p.initUpdateTableValues = function () {
        if (!this.initialTableReadyCall) {
            this.updateTableValues();
        }
    };

    /**
     * @method updateTableValues
     * Invoke drawing function for cell values in the table
     */
    p.updateTableValues = function () {
        //Has to be called in derived widget
    };

    /* Editor Size change */
    p.itemUpdate = function (newSize) {
        //this.setColumnWidth(newWidth);
        var event = new CustomEvent('ItemSizeChanged', { detail: { id: this.elem.id, newSize: newSize }, bubbles: true, cancelable: true });
        // console.log('New header width: ', newWidth);

        this.dispatchEvent(event);
    };

    /**
     * @method enable
     * @override
     * Enable widget
     */
    p.enable = function () {
        SuperClass.prototype.enable.apply(this, arguments);
        this.enableUpdateAvailable = true;
        if (this.tableReady) {
            this.table._enableUpdateAvailable(this.elem.id);
        }
    };

    /**
     * @method disable
     * @override
     * Disable widget
     */
    p.disable = function () {
        SuperClass.prototype.disable.apply(this, arguments);
        this.enableUpdateAvailable = true;
        if (this.tableReady) {
            this.table._enableUpdateAvailable(this.elem.id);
        }

    };

    p.dispose = function () {
        this.enableUpdateAvailable = null;
        this.enableUpdateApplied = null;
        this.visibleUpdateAvailable = null;
        this.visibleUpdateApplied = null;
        this.headerUpdateAvailable = null;
        this.itemSizeUpdateAvailable = null;
        this.tableReady = null;
        this.tableId = null;
        this.table = null;
 
        SuperClass.prototype.dispose.apply(this, arguments);
    };

    function _textInit(widget) {

        if (widget.settings.text !== undefined) {
            if (brease.language.isKey(widget.settings.text) === false) {
                widget.setText(widget.settings.text);
            } else {
                widget.setTextKey(brease.language.parseKey(widget.settings.text), true);
            }
        }
    }

    // override method called in BaseWidget.init
    p._initEditor = function () {
        var widget = this;
        require(['widgets/brease/TableItemWidget/libs/EditorHandles'], function (EditorHandles) {
            var editorHandles = new EditorHandles(widget);

            widget.el.addClass('flex-item');

            widget.getHandles = function () {
                return editorHandles.getHandles();
            };
            widget.designer.getSelectionDecoratables = function () {
                return editorHandles.getSelectionDecoratables();
            };
        });
    };

    return languageDependency.decorate(WidgetClass, false);
});
