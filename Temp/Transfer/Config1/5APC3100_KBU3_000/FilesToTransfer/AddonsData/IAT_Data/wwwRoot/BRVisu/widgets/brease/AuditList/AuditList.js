/*global define*/
define([
    'widgets/brease/TableWidget/TableWidget',
    'widgets/brease/AuditList/libs/Model',
    'widgets/brease/AuditList/libs/Controller',
    'widgets/brease/AuditList/libs/Renderer',
    'widgets/brease/AuditList/libs/Config',
    'widgets/brease/common/MpLinkHandler/libs/MpLinkHandler'
], function (SuperClass, Model, Controller, Renderer, AuditConfig, MpLinkHandler) {
    
    'use strict';
    
    /**
    * @class widgets.brease.AuditList
    * @breaseNote 
    * @extends widgets.brease.TableWidget
    * @requires widgets.brease.AuditListStyle
    * @iatMeta studio:isContainer
    * true
    * 
    * @iatMeta category:Category
    * Data
    *
    * @iatMeta description:short
    * AuditList widget that connects to the mpAuditTrail to display alarms set in that function block
    * @iatMeta description:de
    * AuditList widget that connects to the mpAuditTrail to display alarms set in that function block
    * @iatMeta description:en
    * AuditList widget that connects to the mpAuditTrail to display alarms set in that function block
    */

    /**
    * @event ItemClick
    * @param {Integer} id
    * @param {String} operatorName
    * @param {String} text
    * @param {String} timestamp
    * @param {String} type
    * @iatStudioExposed
    * Fired when a row is clicked on.
    */

    /**
    * @event ItemDoubleClick
    * @param {Integer} id
    * @param {String} operatorName
    * @param {String} text
    * @param {String} timestamp
    * @param {String} type
    * @iatStudioExposed
    * Fired when a row is double clicked on.
    */

    /**
    * @property {WidgetList} [children=["widgets.brease.AuditListItem"]]
    * @inheritdoc  
    */

    var defaultSettings = AuditConfig,

        WidgetClass = SuperClass.extend(function AuditList() {
            SuperClass.apply(this, arguments);
        }, defaultSettings),

        p = WidgetClass.prototype;

    /**
     * @method initModel
     * function that initialises the mpLinkHandler and the widget specific model class
     */
    p.initModel = function () {
        this.linkHandler = new MpLinkHandler(this);
        this.model = new Model(this);
        this.model.initialize();
    };

    /**
     * @method initController
     * function that initialises the widget specific controller class
     */
    p.initController = function () {
        this.controller = new Controller(this);
        this.controller.initialize();
    };

    /**
     * @method initRenderer
     * function that initialises the widget specific renderer class
     */
    p.initRenderer = function () {
        this.renderer = new Renderer(this);
        this.renderer.initialize();
    };       

    /**
    * @method setMpLink
    * Data is received from 
    * @param {MpComIdentType} telegram
    */
    p.setMpLink = function (telegram) {
        this.linkHandler.incomingMessage(telegram);
    };

    /**
    * @method getMpLink
    * At initialization it is called, it may not be called later
    * @return {Object}
    */
    p.getMpLink = function () {
        return this.settings.mpLink;
    };

    /**
     * @method _updateData
     * @private
     * Callback function for the mpLinkHandler, passes the data on to the model where it will be analysed
     * @param {String} message
     * @param {Object} telegram
     */
    p._updateData = function (message, telegram) {
        if (!message.includes('Error')) {
            this.model.setData(telegram);
        }
    };
    // Actions
    /**
    * @method export
    * @iatStudioExposed
    * Exports the current archive to pdf format on the backend. See MpAuditTrail for mor information
    */
    p.export = function () {
        if (this.isDisabled) { return; }
        
        this.controller.export();
    };

    /**
     * @method dispose
     * @override
     * calls the _contentDeactivateHandler to unsubscribe from the backend and remove all eventListeners, and updates the superclass method dispose 
     */
    p.dispose = function () {
        this.controller._contentDeactivatedHandler();
        SuperClass.prototype.dispose.apply(this, arguments);
    };

    /**
     * @method wake
     * @override
     * calls the _addClassSpecificEventListeners to in case specific eventlisteners are needed, and updates the superclass method wake 
     */
    p.wake = function () {
        this.controller._addClassSpecificEventListeners(this.controller);
        SuperClass.prototype.wake.apply(this, arguments);
    };

    /**
     * @method suspend
     * @override
     * calls the _contentDeactivateHandler to unsubscribe from the backend and remove all eventListeners, and updates the superclass method suspend 
     */
    p.suspend = function () {
        this.controller._contentDeactivatedHandler();
        this.linkHandler.reset();
        SuperClass.prototype.suspend.apply(this, arguments);
    };

    return WidgetClass;

});
