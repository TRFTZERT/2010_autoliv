define([
    'widgets/brease/TableWidgetStyle/TableWidgetStyle'
], function (SuperClass) {
    
    'use strict';

    /**
     * @class widgets.brease.AuditListStyle
     * #Description
     * AuditListStyle - abstract widget to set styles in the AuditList  
     * Text can be language dependent.  
     * @breaseNote 
     * @extends widgets.brease.TableWidgetStyle
     * @iatMeta studio:visible
     * false
     * @abstract
     */

    var WidgetClass = SuperClass.extend(function AuditListStyle() {
        SuperClass.apply(this, arguments);
    }, false);

    return WidgetClass;

});
