define([
    'widgets/brease/TableWidget/libs/Controller', 
    'brease/events/BreaseEvent',
    'widgets/brease/AuditList/libs/Dialogue'
], function (SuperClass, BreaseEvent, ConfigDialogue) {
    
    'use strict';
    /** 
     * @class widgets.brease.AuditList.libs.Controller
     * Class for controlling user and widget input and directing this either to the model or the renderer directly.
     */

    var ControllerClass = SuperClass.extend(function Controller(widget) {
            SuperClass.call(this);
            this.widget = widget;
        }, null),

        p = ControllerClass.prototype;

    /**
     * @method _addClassSpecificEventListeners
     * @override
     * @private
     * Adds the event listener for content activated handler to the document which will trigger when the content the AlarmHistory resides in get's
     * actived.
     */
    p._addClassSpecificEventListeners = function (listener) {
        document.body.addEventListener(BreaseEvent.CONTENT_ACTIVATED, listener._bind('_contentActivatedHandler'));
    };

    /* Commands */

    /* Handler */
    /**
     * @method _contentActivatedHandler
     * @private
     * @param {Object} e
     * Removes the eventlistener for content activated, sets an error message if the binding to the mplink is missing,
     * finally subscribes to the method GetRecordIndex.
     */
    p._contentActivatedHandler = function (e) {
        if (e.detail.contentId === this.widget.settings.parentContentId && this) {
            document.body.removeEventListener(BreaseEvent.CONTENT_ACTIVATED, this._bind('_contentActivatedHandler'));
            if (this.widget.bindings === undefined || this.widget.bindings['mpLink'] === undefined) {
                this._setMissingBindingErrorMsg();
            }
            this.widget.linkHandler.subscribeWithCallback('GetRecordIndex', this.widget._updateData);
        }    
    };

    /**
     * @method _contentDeactivatedHandler
     * @private
     * This method get's called when the content get's deactivated. It will unsubscribe from the server with the method
     * GetUpdateCount
     */
    p._contentDeactivatedHandler = function () {
        this.widget.linkHandler.unSubscribe('GetRecordIndex');
    };

    /**
     * @method languageChanged
     * This method will force the model to refetch the data from the backend, so the data is available in the correct
     * langauge.
     */
    p.languageChanged = function () {
        this.widget.model.reFetchData();
    };

    /**
     * @method open
     * This method instantiates a new configuration dialog and opens it with the type of configuration passed in the parameter
     * @param {String} type
     */
    p.openConf = function (type) {
        this.configDialogue = new ConfigDialogue(this.widget);
        this.configDialogue.open(type);
    };

    /**
     * @method export
     * this method will craft a telegram for the backend so that it exports the audit trail.
     */
    p.export = function () {
        this.widget.model.export();
    };
    
    /* Public methods */

    /**
     * @method getEventItem
     * @param {UInteger} item
     * @returns {Object}
     * This method will retrieve the original data (unaltered by timestamps) for the given row index, and put it into an object with readable names
     * and return this.
     */
    p.getEventItem = function (item) {
        var data = this.widget.model.getDataForItem(item);

        var value = {
            'id': data.idx,
            'operatorName': data.opn,
            'text': data.tex,
            'timestamp': data.tim,
            'type': data.typ };
        return value;
    };

    /**
     * @method updateData
     * ONLY to be called in the editor
     * Set's mock data in the table and updates the renderer
     */
    p.updateData = function () {
        this.widget.model.setMockData();
        this.widget.renderer.updateData();
    };

    /**
     * @method _scrolledToBottom
     * @private
     * This method will be called by the TableWidget when the scrolling starts to approach the end of the scrollable area
     */
    p._scrolledToBottom = function () {
        this.widget.model.fetchNextData();
    };

    return ControllerClass;
});
