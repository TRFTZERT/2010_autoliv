define(function () {
    
    'use strict';   
    
    /**
     * @class widgets.brease.AuditList.libs.Config
     * @extends core.javascript.Object
     * @override widgets.brease.AuditList
     */

    /**
     * @cfg {MpComIdentType} mpLink=''
     * @iatStudioExposed
     * @bindable
     * @not_projectable
     * @iatCategory Data
     * Link to a MpAuditTrail component
     */
        
    return {
        rowsPerFetch: 1000,
        mpLink: '',
        type: 'AuditList',
        config: {
            columns: [],
            columnWidths: [],
            columnTypes: {
                idx: 'int',
                opn: 'str',
                tex: 'str',
                tim: 'date',
                typ: 'int',
                old: 'str',
                new: 'str'
            },
            style: [],
            order: [[0, 'asc']],
            filter: [],
            hidden: { data: 'idx' },
            sort: [],
            original: {
                order: []
            }
        }
    };
});
