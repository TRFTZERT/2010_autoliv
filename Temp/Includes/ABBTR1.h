/* Automation Studio generated header file */
/* Do not edit ! */
/* ABBTR1  */

#ifndef _ABBTR1_
#define _ABBTR1_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Datatypes and datatypes of function blocks */
typedef struct IskKule_OP
{
	/* VAR (analog) */
	struct TON TON1;
	struct TON TON2;
	struct TP TP1;
	struct TOF TOF1;
	struct TON TON4;
	struct TON TON3;
	struct SR SR1;
	signed char zzInternalMemory[24];
	/* VAR_INPUT (digital) */
	plcbit Hucre_GSOK;
	plcbit Hucre_ASOK;
	plcbit SysOut_AutoON;
	plcbit SysOut_CycleON;
	plcbit UyariVar;
	plcbit AlarmVar;
	plcbit OP_ResetBtn;
	plcbit OP_PPtoMainBtn;
	plcbit OP_StartBtn;
	plcbit OP_StopBtn;
	plcbit OP_GirisIzinBtn;
	plcbit Kapilar_Kilitli;
	/* VAR_OUTPUT (digital) */
	plcbit Kule_Yesil;
	plcbit Kule_Sari;
	plcbit Kule_Kirmizi;
	plcbit Kule_Buzzer;
	plcbit OP_StartLmb;
	plcbit OP_PPtoMainLmb;
	plcbit OP_StopLmb;
	plcbit OP_GirisIzinLmb;
	plcbit OP_ResetLmb;
	plcbit SysIn_PPtoMain;
	plcbit SysIn_MotorOnStart;
	plcbit SysIn_ExeErrReset;
	plcbit SysIn_EmgErrReset;
	plcbit SysIn_Stop;
	plcbit GirisIstegiVar;
	plcbit Kapilar_Ac;
	/* VAR (digital) */
	plcbit b250msTRUE;
	plcbit b250msFALSE;
	plcbit bKule_YesilSurekliYan;
	plcbit bKule_YesilKesikliYan;
	plcbit bKule_SariSurekliYan;
	plcbit bKule_SariKesikliYan;
	plcbit bKule_KirmiziSurekliYan;
	plcbit bKule_KirmiziKesikliYan;
	plcbit bKule_BuzzerSurekliYan;
	plcbit bKule_BuzzerKesikliYan;
	plcbit bOP_GirisIzinSurekiYan;
	plcbit bOP_GirisIzinKesikliYan;
	plcbit bOP_StartSurekiYan;
	plcbit bOP_StartKesikliYan;
	plcbit bOP_StartAtMainSurekiYan;
	plcbit bOP_StartAtMainKesikliYan;
	plcbit bOP_ResetSurekliYan;
	plcbit bOP_ResetKesikliYan;
	plcbit bOP_StopSurekliYan;
	plcbit bOP_StopKesikliYan;
	plcbit bGirisIzinIstekVar;
	plcbit bBuzzerSus;
} IskKule_OP_typ;

typedef struct MOVE_EN_STRING
{
	/* VAR_INPUT (analog) */
	plcstring IN[251];
	/* VAR_OUTPUT (analog) */
	plcstring OUT[251];
	/* VAR_INPUT (digital) */
	plcbit EN;
} MOVE_EN_STRING_typ;

typedef struct Mtr
{
	/* VAR_INPUT (analog) */
	plcstring MotorIsmi[81];
	/* VAR_OUTPUT (analog) */
	plcstring AlarmMesaji[251];
	/* VAR (analog) */
	struct TON TON1;
	struct TON TON2;
	plcstring sHataMesaji1[251];
	plcstring sHataMesaji2[251];
	signed char zzInternalMemory[10];
	struct MOVE_EN_STRING MOVE_EN_STRING1;
	/* VAR_INPUT (digital) */
	plcbit MtrAcik;
	plcbit MtrAcTalep;
	plcbit MtrKptTalep;
	plcbit MtrTersYonTalep;
	plcbit MtrAcManTalep;
	plcbit MtrKptManTalep;
	plcbit OtomatikMod;
	plcbit ManuelMod;
	plcbit GuvenlikOK;
	/* VAR_OUTPUT (digital) */
	plcbit MtrAc;
	plcbit MtrTersYon;
	plcbit AlarmMtrKapanmadi;
	plcbit AlarmMtrAcilmadi;
} Mtr_typ;

typedef struct Pstn_5_3
{
	/* VAR_INPUT (analog) */
	plcstring PistonIsmi[81];
	/* VAR_OUTPUT (analog) */
	plcstring AlarmMesaji[251];
	/* VAR (analog) */
	struct TON TON1;
	struct TON TON2;
	plcstring sHataMesaji1[251];
	plcstring sHataMesaji2[251];
	signed char zzInternalMemory[10];
	struct MOVE_EN_STRING MOVE_EN_STRING1;
	/* VAR_INPUT (digital) */
	plcbit PstnAcik;
	plcbit PstnKpli;
	plcbit PstnAcTalep;
	plcbit PstnKptTalep;
	plcbit PstnAcManTalep;
	plcbit PstnKptManTalep;
	plcbit OtomatikMod;
	plcbit ManuelMod;
	plcbit GuvenlikOK;
	plcbit HavaOK;
	/* VAR_OUTPUT (digital) */
	plcbit PstnAc;
	plcbit PstnKpt;
	plcbit AlarmPstnAcilmadi;
	plcbit AlarmPstnKapanmadi;
} Pstn_5_3_typ;

typedef struct Pstn_5_2
{
	/* VAR_INPUT (analog) */
	plcstring PistonIsmi[81];
	/* VAR_OUTPUT (analog) */
	plcstring AlarmMesaji[251];
	/* VAR (analog) */
	plcstring sHataMesaji1[251];
	plcstring sHataMesaji2[251];
	struct TON TON1;
	struct TON TON2;
	signed char zzInternalMemory[10];
	struct MOVE_EN_STRING MOVE_EN_STRING1;
	/* VAR_INPUT (digital) */
	plcbit PstnAcik;
	plcbit PstnKpli;
	plcbit PstnAcTalep;
	plcbit PstnKptTalep;
	plcbit PstnAcManTalep;
	plcbit PstnKptManTalep;
	plcbit OtomatikMod;
	plcbit ManuelMod;
	plcbit GuvenlikOK;
	plcbit HavaOK;
	/* VAR_OUTPUT (digital) */
	plcbit PstnAc;
	plcbit PstnKpt;
	plcbit AlarmPstnKapanmadi;
	plcbit AlarmPstnAcilmadi;
} Pstn_5_2_typ;

typedef struct Pstn_5_2_AcanTekBob
{
	/* VAR_INPUT (analog) */
	plcstring PistonIsmi[81];
	/* VAR_OUTPUT (analog) */
	plcstring AlarmMesaji[251];
	/* VAR (analog) */
	plcstring sHataMesaji1[251];
	plcstring sHataMesaji2[251];
	struct TON TON1;
	struct TON TON2;
	signed char zzInternalMemory[10];
	struct MOVE_EN_STRING MOVE_EN_STRING1;
	/* VAR_INPUT (digital) */
	plcbit PstnAcik;
	plcbit PstnKpli;
	plcbit PstnAcTalep;
	plcbit PstnKptTalep;
	plcbit PstnAcManTalep;
	plcbit PstnKptManTalep;
	plcbit OtomatikMod;
	plcbit ManuelMod;
	plcbit GuvenlikOK;
	plcbit HavaOK;
	/* VAR_OUTPUT (digital) */
	plcbit PstnAc;
	plcbit AlarmPstnKapanmadi;
	plcbit AlarmPstnAcilmadi;
	/* VAR (digital) */
	plcbit bPstnKapanmali;
} Pstn_5_2_AcanTekBob_typ;

typedef struct Pstn_5_2_KapatanTekBob
{
	/* VAR_INPUT (analog) */
	plcstring PistonIsmi[81];
	/* VAR_OUTPUT (analog) */
	plcstring AlarmMesaji[251];
	/* VAR (analog) */
	plcstring sHataMesaji1[251];
	plcstring sHataMesaji2[251];
	struct TON TON1;
	struct TON TON2;
	signed char zzInternalMemory[10];
	struct MOVE_EN_STRING MOVE_EN_STRING1;
	/* VAR_INPUT (digital) */
	plcbit PstnAcik;
	plcbit PstnKpli;
	plcbit PstnAcTalep;
	plcbit PstnKptTalep;
	plcbit PstnAcManTalep;
	plcbit PstnKptManTalep;
	plcbit OtomatikMod;
	plcbit ManuelMod;
	plcbit GuvenlikOK;
	plcbit HavaOK;
	/* VAR_OUTPUT (digital) */
	plcbit PstnKpt;
	plcbit AlarmPstnKapanmadi;
	plcbit AlarmPstnAcilmadi;
	/* VAR (digital) */
	plcbit bPstnAcilmali;
} Pstn_5_2_KapatanTekBob_typ;

typedef struct AlarmSet
{
	/* VAR_INPUT (analog) */
	plcstring Mesaj[251];
	signed short MesajNo;
	plcstring AlarmListesi300IN[300][251];
	/* VAR_OUTPUT (analog) */
	plcstring AlarmListesi300[300][251];
	/* VAR (analog) */
	plcstring AlarmListesi300LOC[300][251];
	signed char zzInternalMemory[10];
	plcstring sTemp[81];
	unsigned short nTemp;
	plcstring sIlk5Harf[251];
	plcstring sNumaraliMesaj[251];
	/* VAR_INPUT (digital) */
	plcbit Uyari;
	plcbit EN;
} AlarmSet_typ;

typedef struct HeartBeat
{
	/* VAR_INPUT (analog) */
	plctime TimeoutSuresi;
	/* VAR (analog) */
	signed char zzInternalMemory[10];
	struct TON TON1;
	struct R_TRIG RT1;
	/* VAR_INPUT (digital) */
	plcbit KareDalga;
	/* VAR_OUTPUT (digital) */
	plcbit HeartBeatOK;
} HeartBeat_typ;

typedef struct AnalogScale
{
	/* VAR_INPUT (analog) */
	signed short AnalogAktuelDeger;
	signed short AnalogDeger1;
	signed short Mesafe1_mm_x_10;
	signed short AnalogDeger2;
	signed short Mesafe2_mm_x_10;
	signed short Offset_mm_x_10;
	/* VAR_OUTPUT (analog) */
	signed short Mesafe_mm_x_10;
	unsigned char Mesafe_ByteLow;
	unsigned char Mesafe_ByteHigh;
} AnalogScale_typ;

typedef struct SWAP_INT
{
	/* VAR_INPUT (analog) */
	signed short INT_IN;
	/* VAR_OUTPUT (analog) */
	signed short INT_OUT;
	unsigned char USINT_ByteLow;
	unsigned char USINT_ByteHigh;
} SWAP_INT_typ;

typedef struct SWAP_UINT
{
	/* VAR_INPUT (analog) */
	signed short UINT_IN;
	/* VAR_OUTPUT (analog) */
	signed short UINT_OUT;
	unsigned char USINT_ByteLow;
	unsigned char USINT_ByteHigh;
} SWAP_UINT_typ;

typedef struct MERGE_UINT
{
	/* VAR_INPUT (analog) */
	unsigned char USINT_ByteLow;
	unsigned char USINT_ByteHigh;
	/* VAR_OUTPUT (analog) */
	unsigned short UINT_OUT;
} MERGE_UINT_typ;

typedef struct MOVE_EN_BOOL
{
	/* VAR_INPUT (digital) */
	plcbit IN;
	plcbit EN;
	/* VAR_OUTPUT (digital) */
	plcbit OUT;
} MOVE_EN_BOOL_typ;

typedef struct MOVE_EN_INT
{
	/* VAR_INPUT (analog) */
	signed short IN;
	/* VAR_OUTPUT (analog) */
	signed short OUT;
	/* VAR_INPUT (digital) */
	plcbit EN;
} MOVE_EN_INT_typ;

typedef struct MOVE_EN_USINT
{
	/* VAR_INPUT (analog) */
	unsigned char IN;
	/* VAR_OUTPUT (analog) */
	unsigned char OUT;
	/* VAR_INPUT (digital) */
	plcbit EN;
	/* VAR (digital) */
	plcbit ENOLD;
} MOVE_EN_USINT_typ;

typedef struct MOVE_EN_UINT
{
	/* VAR_INPUT (analog) */
	unsigned short IN;
	/* VAR_OUTPUT (analog) */
	unsigned short OUT;
	/* VAR_INPUT (digital) */
	plcbit EN;
} MOVE_EN_UINT_typ;

typedef struct SPLIT_Byte
{
	/* VAR_INPUT (analog) */
	plcbyte IN_Byte;
	/* VAR_OUTPUT (digital) */
	plcbit Bit1;
	plcbit Bit2;
	plcbit Bit3;
	plcbit Bit4;
	plcbit Bit5;
	plcbit Bit6;
	plcbit Bit7;
	plcbit Bit8;
} SPLIT_Byte_typ;

typedef struct MERGE_Bit8
{
	/* VAR_OUTPUT (analog) */
	plcbyte OUT_Byte;
	/* VAR_INPUT (digital) */
	plcbit Bit1;
	plcbit Bit2;
	plcbit Bit3;
	plcbit Bit4;
	plcbit Bit5;
	plcbit Bit6;
	plcbit Bit7;
	plcbit Bit8;
} MERGE_Bit8_typ;

typedef struct MERGE_Bit16
{
	/* VAR_OUTPUT (analog) */
	plcword OUT_Word;
	/* VAR (analog) */
	plcbyte ByteLow;
	plcbyte ByteHigh;
	unsigned short UintLow;
	plcword WordLow;
	unsigned short UintHigh;
	plcword WordHigh;
	/* VAR_INPUT (digital) */
	plcbit Bit1;
	plcbit Bit2;
	plcbit Bit3;
	plcbit Bit4;
	plcbit Bit5;
	plcbit Bit6;
	plcbit Bit7;
	plcbit Bit8;
	plcbit Bit9;
	plcbit Bit10;
	plcbit Bit11;
	plcbit Bit12;
	plcbit Bit13;
	plcbit Bit14;
	plcbit Bit15;
	plcbit Bit16;
} MERGE_Bit16_typ;

typedef struct SPLIT_Word
{
	/* VAR_INPUT (analog) */
	plcword IN_Word;
	/* VAR (analog) */
	plcword WordTemp;
	plcbyte ByteLow;
	plcbyte ByteHigh;
	/* VAR_OUTPUT (digital) */
	plcbit Bit1;
	plcbit Bit2;
	plcbit Bit3;
	plcbit Bit4;
	plcbit Bit5;
	plcbit Bit6;
	plcbit Bit7;
	plcbit Bit8;
	plcbit Bit9;
	plcbit Bit10;
	plcbit Bit11;
	plcbit Bit12;
	plcbit Bit13;
	plcbit Bit14;
	plcbit Bit15;
	plcbit Bit16;
} SPLIT_Word_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void IskKule_OP(struct IskKule_OP* inst);
_BUR_PUBLIC void Mtr(struct Mtr* inst);
_BUR_PUBLIC void Pstn_5_3(struct Pstn_5_3* inst);
_BUR_PUBLIC void Pstn_5_2(struct Pstn_5_2* inst);
_BUR_PUBLIC void Pstn_5_2_AcanTekBob(struct Pstn_5_2_AcanTekBob* inst);
_BUR_PUBLIC void Pstn_5_2_KapatanTekBob(struct Pstn_5_2_KapatanTekBob* inst);
_BUR_PUBLIC void AlarmSet(struct AlarmSet* inst);
_BUR_PUBLIC void HeartBeat(struct HeartBeat* inst);
_BUR_PUBLIC void AnalogScale(struct AnalogScale* inst);
_BUR_PUBLIC void SWAP_INT(struct SWAP_INT* inst);
_BUR_PUBLIC void SWAP_UINT(struct SWAP_UINT* inst);
_BUR_PUBLIC void MERGE_UINT(struct MERGE_UINT* inst);
_BUR_PUBLIC void MOVE_EN_BOOL(struct MOVE_EN_BOOL* inst);
_BUR_PUBLIC void MOVE_EN_STRING(struct MOVE_EN_STRING* inst);
_BUR_PUBLIC void MOVE_EN_INT(struct MOVE_EN_INT* inst);
_BUR_PUBLIC void MOVE_EN_USINT(struct MOVE_EN_USINT* inst);
_BUR_PUBLIC void MOVE_EN_UINT(struct MOVE_EN_UINT* inst);
_BUR_PUBLIC void SPLIT_Byte(struct SPLIT_Byte* inst);
_BUR_PUBLIC void MERGE_Bit8(struct MERGE_Bit8* inst);
_BUR_PUBLIC void MERGE_Bit16(struct MERGE_Bit16* inst);
_BUR_PUBLIC void SPLIT_Word(struct SPLIT_Word* inst);


#ifdef __cplusplus
};
#endif
#endif /* _ABBTR1_ */

