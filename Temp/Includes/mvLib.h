/* Automation Studio generated header file */
/* Do not edit ! */
/* mvLib  */

#ifndef _MVLIB_
#define _MVLIB_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif



/* Prototyping of functions and function blocks */
_BUR_PUBLIC unsigned long DataProvider(unsigned long value, unsigned long text);


#ifdef __cplusplus
};
#endif
#endif /* _MVLIB_ */

