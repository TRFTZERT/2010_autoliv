/* Automation Studio generated header file */
/* Do not edit ! */
/* RFID_Trp 1.11.0 */

#ifndef _RFID_TRP_
#define _RFID_TRP_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _RFID_Trp_VERSION
#define _RFID_Trp_VERSION 1.11.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG3
		#include "dvframe.h"
		#include "AsUSB.h"
#endif
#ifdef _SG4
		#include "dvframe.h"
		#include "AsUSB.h"
#endif
#ifdef _SGC
		#include "dvframe.h"
		#include "AsUSB.h"
#endif


/* Constants */
#ifdef _REPLACE_CONST
 _WEAK const unsigned short TRANSPONDER_FTDI_VENDOR_ID[3] = {1027U,8137U,2520U};
 _WEAK const unsigned short TRANSPONDER_FTDI_PRODUCT_ID[3] = {24577U,17U,1056U};
 #define TRANSPONDER_FTDI_BCD 1536U
 #define NUM_OF_FRAME 10U
 #define NUM_OF_FRAME_INDEX 9U
 _WEAK const unsigned char ASCII_HEX[36] = {48U,49U,50U,51U,52U,53U,54U,55U,56U,57U,65U,66U,67U,68U,69U,70U,71U,72U,73U,74U,75U,76U,77U,78U,79U,80U,81U,82U,83U,84U,85U,86U,87U,88U,89U,90U};
 #define ASCII_STR "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#else
 _GLOBAL_CONST unsigned short TRANSPONDER_FTDI_VENDOR_ID[3];
 _GLOBAL_CONST unsigned short TRANSPONDER_FTDI_PRODUCT_ID[3];
 _GLOBAL_CONST unsigned short TRANSPONDER_FTDI_BCD;
 _GLOBAL_CONST unsigned char NUM_OF_FRAME;
 _GLOBAL_CONST unsigned char NUM_OF_FRAME_INDEX;
 _GLOBAL_CONST unsigned char ASCII_HEX[36];
 _GLOBAL_CONST plcstring ASCII_STR[81];
#endif




/* Datatypes and datatypes of function blocks */
typedef enum RFIDTrpRWInternalParDataTrpEnum
{	E9010 = 0,
	E9030 = 1,
	ELATEC_TWN4_MULTITECH_NANO
} RFIDTrpRWInternalParDataTrpEnum;

typedef enum RFIDTrpRWInternalStatusEnum
{	IDLE,
	MANAGE_5E9010,
	MANAGE_5E9030,
	USB_GETNODELIST,
	USB_SEARCHDEVICE,
	DVF_DEVICEOPEN,
	RFID_INIT_READER,
	RFID_READ_INVENTORY,
	RFID_READ_SYSINFO,
	RFID_READ_SBLOCK,
	RFID_WRITE_SBLOCK,
	RFID_WRITE_AFI,
	RFID_WRITE_DSFID,
	RFID_RESET_TO_READY,
	RFID_WRITE,
	RFID_READ,
	RFID_ERROR
} RFIDTrpRWInternalStatusEnum;

typedef struct RFIDTrpTagInfoType
{	plcstring UID[18];
	unsigned char NumOfBlocks;
	unsigned char BlockSize;
	plcbit Available;
} RFIDTrpTagInfoType;

typedef struct ConvertInternalType
{	unsigned char LSPart;
	unsigned char MSPart;
	plcstring LSPartLet[2];
	plcstring MSPartLet[2];
	unsigned char i;
} ConvertInternalType;

typedef struct Convert
{
	/* VAR_INPUT (analog) */
	plcstring inHexText[4];
	unsigned char inDecimal;
	/* VAR_OUTPUT (analog) */
	unsigned char outDecimal;
	plcstring outHexText[4];
	plcstring outChar[3];
	/* VAR (analog) */
	struct ConvertInternalType _Internal;
} Convert_typ;

typedef struct RFIDTrpRWInternalFBType
{	struct FRM_read FrameReadFub;
	struct FRM_rbuf FrameReleaseBufferFub;
	struct FRM_write FrameWriteFub;
	struct FRM_xopen FrameXOpenFub;
	struct FRM_close FrameXCloseFub;
	struct UsbNodeGet UsbNodeGetFub;
	struct UsbNodeListGet UsbNodeListGetFub;
	struct Convert ConvertFB;
} RFIDTrpRWInternalFBType;

typedef struct RFIDTrpRWInternalStatusType
{	enum RFIDTrpRWInternalStatusEnum Step;
	unsigned char ReadAnswer;
	unsigned char ReadCount;
	unsigned char BlockCountR;
	unsigned char BlockCountW;
	plcbit Flag;
	plcbit TagInfoFlag;
	unsigned char TagCounter;
} RFIDTrpRWInternalStatusType;

typedef struct RFIDTrpRWInternalParComPVIType
{	unsigned char Cmd;
	unsigned char CmdOld;
	plcstring ReadData[256];
	plcstring WriteData[256];
} RFIDTrpRWInternalParComPVIType;

typedef struct RFIDTrpRWInternalParComType
{	plcstring StringDevice[21];
	unsigned long UsbNodeList[32];
	unsigned char UsbNodeIx;
	struct usbNode_typ UsbDevice;
	struct RFIDTrpRWInternalParComPVIType Pvi;
} RFIDTrpRWInternalParComType;

typedef struct RFIDTrpRWInternalParDataType
{	struct RFIDTrpTagInfoType TagInfo;
	enum RFIDTrpRWInternalParDataTrpEnum TransponderType;
	unsigned long* pReadBuffer;
	plcstring ReadData[81];
	plcstring WriteData[81];
	unsigned char ReadBlock;
	unsigned long EncryptionKey;
} RFIDTrpRWInternalParDataType;

typedef struct RFIDTrpRWInternalParType
{	struct RFIDTrpRWInternalParComType Com;
	struct RFIDTrpRWInternalParDataType Data;
} RFIDTrpRWInternalParType;

typedef struct RFIDTrpRWInternalType
{	struct RFIDTrpRWInternalFBType FB;
	struct RFIDTrpRWInternalStatusType Status;
	struct RFIDTrpRWInternalParType Parameters;
	plcbit ReadTagInfo;
	unsigned char i;
} RFIDTrpRWInternalType;

typedef struct MakeFrameInputType
{	plcstring CompName[5];
	plcstring ColourCode[5][5];
	plcstring Date[5];
	plcstring Volume[5];
	plcstring UID_msp[9];
	plcstring UID_lsp[9];
	plcstring FinalData[91];
} MakeFrameInputType;

typedef struct MakeFrameInternalTempVarType
{	unsigned char LSPart;
	unsigned char MSPart;
	plcstring LSPartLet[2];
	plcstring MSPartLet[2];
} MakeFrameInternalTempVarType;

typedef struct MakeFrameInternalIndexType
{	unsigned char i;
	unsigned char j;
} MakeFrameInternalIndexType;

typedef struct MakeFrameInternalType
{	struct Convert ConvertFB;
	struct MakeFrameInternalTempVarType TempVar;
	struct MakeFrameInternalIndexType Index;
	unsigned char EncryptionKey[8];
} MakeFrameInternalType;

typedef struct RFIDTrpRW
{
	/* VAR_INPUT (analog) */
	enum RFIDTrpRWInternalParDataTrpEnum inTrpType;
	unsigned char inKey[8];
	plcstring inBarCode[10][81];
	/* VAR_OUTPUT (analog) */
	unsigned short outStatusID;
	plcstring outDevice[21];
	struct RFIDTrpTagInfoType outTagInfo;
	plcstring outData[256];
	plcstring outBarCode[10][81];
	/* VAR (analog) */
	struct RFIDTrpRWInternalType _Internal;
	/* VAR_INPUT (digital) */
	plcbit inEnable;
	plcbit inAPC;
	plcbit inRead;
	plcbit inWrite;
	plcbit inClearTag;
	plcbit inReadTagInfo;
	plcbit inResetTag;
	/* VAR_OUTPUT (digital) */
	plcbit outError;
	plcbit outDeviceFound;
	plcbit outDone;
} RFIDTrpRW_typ;

typedef struct MakeFrame
{
	/* VAR_INPUT (analog) */
	struct MakeFrameInputType inCodeInfo;
	unsigned long inKey;
	/* VAR_OUTPUT (analog) */
	plcstring outFrame[10][81];
	unsigned char outKey[8];
	/* VAR (analog) */
	struct MakeFrameInternalType _Internal;
} MakeFrame_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void RFIDTrpRW(struct RFIDTrpRW* inst);
_BUR_PUBLIC void MakeFrame(struct MakeFrame* inst);
_BUR_PUBLIC void Convert(struct Convert* inst);


#ifdef __cplusplus
};
#endif
#endif /* _RFID_TRP_ */

