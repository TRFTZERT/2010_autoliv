/* Automation Studio generated header file */
/* Do not edit ! */
/* ABB_TRFTZ1  */

#ifndef _ABB_TRFTZ1_
#define _ABB_TRFTZ1_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Datatypes and datatypes of function blocks */
#ifdef _BUR_USE_DECLARATION_IN_IEC
typedef struct HMIMode
{	plcbit SystemAuto;
	plcbit ManuelMode;
	plcbit AutoStop;
	plcbit ReserveMod[4];
	unsigned char State;
} HMIMode;
#else
/* Data type HMIMode not declared. Data types with array elements whose starting indexes are not equal to zero cannot be used in ANSI C programs / libraries.*/
#endif

typedef struct hDiag
{	plcbit InitPos;
	plcbit Set;
	plcbit Rst;
	plcbit SetRun;
	plcbit RstRun;
	plcbit Error;
	plcbit InterlockActive;
	plcbit LightBeamActive;
	plcbit SetFault;
	plcbit RstFault;
	plcbit Act;
	unsigned char ErrorCode;
} hDiag;

typedef struct Lighting_FB
{
	/* VAR (analog) */
	signed char zzInternalMemory[10];
	struct TON TON_0;
	struct RS RS_0;
	struct R_TRIG R_TRIG_0;
	struct F_TRIG F_TRIG_1;
	struct F_TRIG F_TRIG_0;
	/* VAR_INPUT (digital) */
	plcbit iCycleOn;
	plcbit iSafetyOk;
	plcbit iSystemAuto;
	plcbit iSystemManual;
	plcbit iManOpen;
	/* VAR_OUTPUT (digital) */
	plcbit qLighting;
	/* VAR (digital) */
	plcbit tAutoLighting;
} Lighting_FB_typ;

typedef struct Conveyor_FB
{
	/* VAR (analog) */
	signed char zzInternalMemory[10];
	/* VAR_INPUT (digital) */
	plcbit iAct;
	plcbit iLB;
	plcbit iSysAuto;
	plcbit iSysMan;
	plcbit iDriveFault;
	plcbit iDriveFuse;
	plcbit iLoadSns;
	plcbit iUnloadSns;
	plcbit iStart;
	plcbit iDirection;
	plcbit iManStart;
	plcbit newParam13;
	plcbit newParam14;
	plcbit newParam15;
	plcbit newParam16;
	plcbit newParam17;
	plcbit newParam18;
	plcbit newParam19;
	plcbit newParam20;
	plcbit newParam21;
	plcbit newParam22;
	plcbit newParam23;
	plcbit newParam24;
	plcbit newParam25;
	plcbit newParam26;
	plcbit newParam27;
	plcbit newParam28;
	plcbit newParam29;
	plcbit newParam30;
	plcbit newParam31;
	/* VAR_OUTPUT (digital) */
	plcbit qStart;
	plcbit qDirection;
} Conveyor_FB_typ;

typedef struct SuctionFB
{
	/* VAR_OUTPUT (analog) */
	signed short qAlarmNo;
	/* VAR (analog) */
	signed char zzInternalMemory[10];
	struct TON FaultTimer;
	/* VAR_INPUT (digital) */
	plcbit iSuction;
	plcbit iSuctionOk;
	plcbit iBlow;
	plcbit iManualSuction;
	plcbit iManualBlow;
	/* VAR_OUTPUT (digital) */
	plcbit qSuction;
	plcbit qBlow;
	plcbit qSuctionOk;
	plcbit qSuctionFault;
	/* VAR (digital) */
	plcbit tSuctionOk;
	plcbit tAutoSuction;
	plcbit tManSuction;
	plcbit tAutoBlow;
	plcbit tManBlow;
} SuctionFB_typ;

typedef struct Piston5_3
{
	/* VAR_INPUT (analog) */
	plcstring iCylinderName[81];
	/* VAR_OUTPUT (analog) */
	plcstring qAlarmMesagge[101];
	signed short qAlarmNo;
	/* VAR (analog) */
	struct TON OpenFaultTimer;
	struct TON CloseFaultTimer;
	plcstring sFaultMessage1[250];
	plcstring sFaultMessage2[250];
	/* VAR_INPUT (digital) */
	plcbit iOpenSignal;
	plcbit iCloseSignal;
	plcbit iAutoOpenReq;
	plcbit iAutoCloseReq;
	plcbit iManOpenReq;
	plcbit iManCloseReq;
	plcbit iAuto;
	plcbit iManual;
	plcbit iSafety;
	plcbit iAir;
	/* VAR_OUTPUT (digital) */
	plcbit qCylinderOpen;
	plcbit qCylinderClose;
	plcbit qOpenAlarm;
	plcbit qCloseAlarm;
	/* VAR (digital) */
	plcbit tAutoOpen;
	plcbit tManOpen;
	plcbit tAutoClose;
	plcbit tManClose;
	plcbit tOpen;
	plcbit tClose;
} Piston5_3_typ;

typedef struct SuctionForFoldingSpace_FB
{
	/* VAR_OUTPUT (analog) */
	signed short qCylndrAlarmNo;
	signed short qVacumAlarmNo;
	/* VAR (analog) */
	signed char zzInternalMemory[10];
	struct SuctionFB Suction_For_Table;
	struct Piston5_3 Cylinder_For_Table;
	/* VAR_INPUT (digital) */
	plcbit iSuctionCupUpSignal;
	plcbit iSuctionCupDownSignal;
	plcbit iSuctionOk;
	plcbit iSuctionCupUp;
	plcbit iSuctionCupDown;
	plcbit iSuction;
	plcbit iBlow;
	plcbit iSystemAuto;
	plcbit iSystemManual;
	plcbit iManSuctionCupUp;
	plcbit iManSuctionCupDown;
	plcbit iManSuction;
	plcbit iManBlow;
	plcbit iEmgOk;
	plcbit iSafetyOk;
	plcbit iAirOk;
	/* VAR_OUTPUT (digital) */
	plcbit qSuctionCupUp;
	plcbit qSuctionCupDown;
	plcbit qSuction;
	plcbit qBlow;
	plcbit qSuctionOk;
	plcbit qSuctionCupUpFault;
	plcbit qSuctionCupDownFault;
	plcbit qSuctionFault;
} SuctionForFoldingSpace_FB_typ;

typedef struct ButtonBox_FB
{
	/* VAR (analog) */
	signed char zzInternalMemory[10];
	struct TON TON1;
	/* VAR_INPUT (digital) */
	plcbit iHucre_GSOK;
	plcbit iHucre_ASOK;
	plcbit iSysOut_AutoON;
	plcbit iSysOut_CycleON;
	plcbit iSysOut_PPMoved;
	plcbit iSGOk;
	plcbit iOP_StartAtMainBtn;
	plcbit iOP_GirisIzinBtn;
	plcbit iOP_ResetBtn;
	plcbit iOP_StopBtn;
	/* VAR_OUTPUT (digital) */
	plcbit qSysIn_PPtoMain;
	plcbit qSysIn_MotorOnStart;
	plcbit qSysIn_ExeErrReset;
	plcbit qSysIn_EmgErrReset;
	plcbit qAlarmReset;
	plcbit qOP_StartLmb;
	plcbit qOP_GirisIzinLmb;
	plcbit qOP_ResetLmb;
	plcbit qGirisIstegiVar;
	plcbit qSysIn_Stop;
	plcbit qOP_StopLmb;
	/* VAR (digital) */
	plcbit b250msTRUE;
	plcbit b250msFALSE;
	plcbit bGirisIzinIstekVar;
	plcbit bOP_GirisIzinSurekiYan;
	plcbit bOP_GirisIzinKesikliYan;
	plcbit bOP_ResetKesikliYan;
	plcbit bOP_ResetSurekliYan;
	plcbit bOP_StartKesikliYan;
	plcbit bOP_StartSurekiYan;
	plcbit bOP_StartAtMainKesikliYan;
	plcbit bOP_StartAtMainSurekiYan;
} ButtonBox_FB_typ;

typedef struct LightTower_FB
{
	/* VAR (analog) */
	signed char zzInternalMemory[13];
	struct TON TON1;
	struct TON TON2;
	/* VAR_INPUT (digital) */
	plcbit Hucre_GSOK;
	plcbit Hucre_ASOK;
	plcbit AlarmVar;
	plcbit UyariVar;
	plcbit SysOut_CycleON;
	plcbit OP_ResetBtn;
	/* VAR_OUTPUT (digital) */
	plcbit Kule_Kirmizi;
	plcbit Kule_Sari;
	plcbit Kule_Yesil;
	plcbit Kule_Buzzer;
	/* VAR (digital) */
	plcbit b250msFALSE;
	plcbit b250msTRUE;
	plcbit bKule_KirmiziSurekliYan;
	plcbit bKule_KirmiziKesikliYan;
	plcbit bKule_SariSurekliYan;
	plcbit bKule_SariKesikliYan;
	plcbit bKule_YesilSurekliYan;
	plcbit bKule_YesilKesikliYan;
	plcbit bKule_BuzzerKesikliYan;
	plcbit bBuzzerSus;
	plcbit bKule_BuzzerSurekliYan;
} LightTower_FB_typ;

typedef struct CameraBlow_FB
{
	/* VAR_INPUT (analog) */
	plctime iWaitTime;
	plctime iBlowTime;
	/* VAR (analog) */
	struct TON TON_BlowWait;
	struct TON TON_BlowOn;
	signed char zzInternalMemory[10];
	/* VAR_INPUT (digital) */
	plcbit iCamScreenEmpty;
	plcbit iRobotWZ;
	plcbit iSystemAuto;
	plcbit iSystemManual;
	plcbit iManualBlow;
	/* VAR_OUTPUT (digital) */
	plcbit qBlow;
	plcbit qTimeOutFault;
	/* VAR (digital) */
	plcbit tBlow;
} CameraBlow_FB_typ;

typedef struct Stacker_FB
{
	/* VAR_OUTPUT (analog) */
	signed short qAlarmNo;
	/* VAR (analog) */
	struct Piston5_3 Valve;
	signed char zzInternalMemory[10];
	struct RS RS_Close;
	struct R_TRIG R_TRIG_0;
	struct RS RS_Open;
	struct F_TRIG F_TRIG_0;
	struct R_TRIG R_TRIG_1;
	struct RS DoorOpened;
	struct RS DoorClosed;
	struct RS bStackerReady;
	struct TON TON1;
	struct R_TRIG R_TRIG_2;
	struct R_TRIG R_TRIG_3;
	/* VAR_INPUT (digital) */
	plcbit iOpenedSignal;
	plcbit iClosedSignal;
	plcbit iEmpty;
	plcbit iWZSignal;
	plcbit iAirOk;
	plcbit iSGOk;
	plcbit iStackerMgntcSns;
	plcbit iSystemAuto;
	plcbit iSystemManual;
	plcbit iManualOpen;
	plcbit iManualClose;
	/* VAR_OUTPUT (digital) */
	plcbit qOpenValve;
	plcbit qCloseValve;
	plcbit qOpenFault;
	plcbit qCloseFault;
	plcbit qStackerReady;
	plcbit qCanOpenGate;
	/* VAR (digital) */
	plcbit tOpen;
	plcbit tClose;
	plcbit tCloseFault1;
	plcbit tCloseFault2;
} Stacker_FB_typ;

#ifdef _BUR_USE_DECLARATION_IN_IEC
typedef struct Mode_FB
{
	/* VAR (analog) */
	struct HMIMode qHMI;
	/* VAR_INPUT (digital) */
	plcbit iSystemAuto;
	plcbit iAutoStop;
	plcbit iEmgAct;
	plcbit iReset;
	plcbit iClockPulse;
	/* VAR_OUTPUT (digital) */
	plcbit qAutoMode;
	plcbit qManuelMode;
	/* VAR (digital) */
	plcbit tAuto;
	plcbit sAuto;
	plcbit iReserveMod[4];
	plcbit qReserveMod[4];
} Mode_FB_typ;
#else
/* Data type Mode_FB not declared. Data types with array elements whose starting indexes are not equal to zero cannot be used in ANSI C programs / libraries.*/
#endif

#ifdef _BUR_USE_DECLARATION_IN_IEC
typedef struct ValveControlFB
{
	/* VAR_INPUT (analog) */
	unsigned char iSystemStep;
	/* VAR_OUTPUT (analog) */
	unsigned char hSnsState[16];
	struct hDiag hDiag;
	unsigned char hAlarm;
	/* VAR (analog) */
	struct TON sTimerTrg;
	plctime sTimerTrg_PT;
	struct TON sTimerS;
	plctime sTimerS_PT;
	struct TON sTimerR;
	plctime sTimerR_PT;
	unsigned char sOld;
	unsigned char sSnsState[16];
	struct hDiag tHMIDiag;
	unsigned char CodeNo[16];
	unsigned char ForNT6;
	unsigned char ForNT13;
	unsigned char ForNT11;
	unsigned char ForNT26;
	/* VAR_INPUT (digital) */
	plcbit iInterlock;
	plcbit iLightBeam;
	plcbit iSet_Sns[16];
	plcbit iRst_Sns[16];
	plcbit iSysAut;
	plcbit iSysManuel;
	plcbit iMan_Set;
	plcbit iAuto_Set;
	plcbit iSep_Set;
	plcbit iMan_Rst;
	plcbit iAuto_Rst;
	plcbit iSep_Rst;
	plcbit iTest_Set;
	plcbit iTest_Rst;
	plcbit iAuto_Mid;
	plcbit iMan_Mid;
	plcbit iBypass;
	plcbit iPartless;
	plcbit iOtherSide;
	plcbit sAct[16];
	/* VAR_OUTPUT (digital) */
	plcbit qValveSet;
	plcbit qValveRst;
	plcbit qValveInitPos;
	plcbit qSetRun;
	plcbit qRstRun;
	plcbit qError;
	plcbit qSetFault;
	plcbit hSFSnsAlarm[16];
	plcbit qRstFault;
	plcbit hRFSnsAlarm[16];
	plcbit htrig;
	/* VAR (digital) */
	plcbit sTimerTrg_IN;
	plcbit sTimerS_IN;
	plcbit sTimerR_IN;
	plcbit sValf_ileride;
	plcbit sValf_Geride;
	plcbit sHataYok;
	plcbit sSetSensOK;
	plcbit sResetSensOK;
	plcbit sSSensorHata;
	plcbit sRSensorHata;
	plcbit sSet;
	plcbit sReset;
	plcbit sTrig;
	plcbit sAutoSetMemory;
	plcbit sAutoSetMemoryWork;
	plcbit sAutoRstMemory;
	plcbit sAutoRstMemoryWork;
	plcbit tFault1;
	plcbit tFault2;
	plcbit tFault3;
	plcbit tProcess;
	plcbit tActSns;
	plcbit SShot[6];
	plcbit SLine1;
	plcbit SLine2;
	plcbit SLine3;
	plcbit SLine4;
	plcbit SLine5;
	plcbit RShot[7];
	plcbit RLine1;
	plcbit RLine2;
	plcbit RLine3;
	plcbit RLine4;
	plcbit RLine5;
	plcbit zzEdge00000;
	plcbit zzEdge00001;
	plcbit zzEdge00002;
} ValveControlFB_typ;
#else
/* Data type ValveControlFB not declared. Data types with array elements whose starting indexes are not equal to zero cannot be used in ANSI C programs / libraries.*/
#endif

typedef struct ALT
{
	/* VAR_INPUT (digital) */
	plcbit In;
	/* VAR_OUTPUT (digital) */
	plcbit Out;
	/* VAR (digital) */
	plcbit Stat;
	plcbit Alt;
} ALT_typ;

typedef struct Transfer
{
	/* VAR_OUTPUT (analog) */
	signed short qStateNo;
	signed short qAlarmNo;
	/* VAR (analog) */
	signed short NState;
	struct TON tPickDownTime;
	struct TON tFoldingWaitTime;
	struct TON AlarmRefreshTimer;
	struct TON TimerStateNo;
	signed short tAlarmNo;
	signed short tOldAlarmNo;
	signed short tOldNState;
	/* VAR_INPUT (digital) */
	plcbit iAirOk;
	plcbit iSafetyOk;
	plcbit iEmgOk;
	plcbit iToolchangerLock;
	plcbit iSystemAuto;
	plcbit iLineerBwdSensor;
	plcbit iLineerFwdSensor;
	plcbit iPickUpSensor;
	plcbit iFoldingBwdSensor;
	plcbit iRotaryUpSensor;
	plcbit iRotaryDownSensor;
	plcbit iVacum1Ok;
	plcbit iR3VacumOk;
	plcbit iVacumOff;
	plcbit iR3PartPicked;
	plcbit iCamOnline;
	plcbit iCell2CamOk;
	plcbit iCell2RWZ;
	plcbit iCell3RWZ;
	/* VAR_OUTPUT (digital) */
	plcbit qLineerBwd;
	plcbit qLineerFwd;
	plcbit qRotaryUp;
	plcbit qRotaryDown;
	plcbit qPickDown;
	plcbit qFoldingFwd;
	plcbit qVacum1;
	plcbit qVacum2;
	plcbit qCamClear;
	plcbit qReadyToR2;
	plcbit qPartReadyToR3;
	plcbit qProductIdle;
	/* VAR (digital) */
	plcbit TEST;
	plcbit tConditions;
	plcbit zzEdge00000;
} Transfer_typ;

typedef struct SM_Cell2
{
	/* VAR_INPUT (analog) */
	unsigned char iPrgNo;
	/* VAR_OUTPUT (analog) */
	signed short qStateNo;
	signed short qAlarmNo;
	/* VAR (analog) */
	struct TON WaitTimer1;
	struct TON WaitTimer2;
	struct TON WaitTimer3;
	struct TON Start1PulseTimer;
	struct TON Start2PulseTimer;
	struct TON Start3PulseTimer;
	struct TON AlarmRefreshTimer;
	struct TON TimerStateNo;
	plctime PulseTime;
	plctime WaitTime;
	signed short NState;
	signed short tAlarmNo;
	signed short tOldAlarmNo;
	signed short tOldNState;
	/* VAR_INPUT (digital) */
	plcbit iStart;
	plcbit iSMClampDownSns;
	plcbit iSMClampUpSns;
	plcbit iSMLockClamp;
	plcbit iSMPressureOk;
	plcbit iSMReadyHome;
	plcbit iSMThreadBreakageError;
	plcbit iSMStopError;
	plcbit iSMVirtualHigh;
	plcbit iSMPrgBit1;
	plcbit iSMPrgBit2;
	plcbit iR2WZ;
	plcbit iSystemAuto;
	plcbit iAirOk;
	plcbit iEmgOk;
	plcbit iSafetyOk;
	/* VAR_OUTPUT (digital) */
	plcbit qBusy;
	plcbit qSMClampWork;
	plcbit qSMStart;
	plcbit qSMVirtual;
	plcbit qSMPrgBit1;
	plcbit qSMPrgBit2;
	plcbit qSMProgNoBit1;
	plcbit qSMProgNoBit2;
	plcbit qSMProgNoBit3;
	plcbit qSMProgNoBit4;
	plcbit qSMRunning;
	plcbit qSMReadyForRobot;
	plcbit qSMCycleOkForRobot;
	/* VAR (digital) */
	plcbit tConditions;
	plcbit Pulse;
	plcbit Pulse2;
} SM_Cell2_typ;

typedef struct SM_Cell3
{
	/* VAR_INPUT (analog) */
	unsigned char iPrgNo;
	/* VAR_OUTPUT (analog) */
	signed short qStateNo;
	signed short qAlarmNo;
	/* VAR (analog) */
	struct TON WaitTimer1;
	struct TON WaitTimer2;
	struct TON Start1PulseTimer;
	struct TON Start2PulseTimer;
	struct TON AlarmRefreshTimer;
	struct TON TimerStateNo;
	plctime WaitTime;
	plctime PulseTime;
	signed short NState;
	signed short tAlarmNo;
	signed short tOldAlarmNo;
	signed short tOldNState;
	/* VAR_INPUT (digital) */
	plcbit iStart1;
	plcbit iStart2;
	plcbit iSMClampDownSns;
	plcbit iSMClampUpSns;
	plcbit iSMLockClamp;
	plcbit iSMPressureOk;
	plcbit iSMReadyHome;
	plcbit iSMThreadBreakageError;
	plcbit iSMStopError;
	plcbit iSMVirtualHigh;
	plcbit iR3WZ;
	plcbit iSystemAuto;
	plcbit iAirOk;
	plcbit iEmgOk;
	plcbit iSafetyOk;
	/* VAR_OUTPUT (digital) */
	plcbit qBusy;
	plcbit qSMStart;
	plcbit qSMVirtual;
	plcbit qSMProgNoBit1;
	plcbit qSMProgNoBit2;
	plcbit qSMProgNoBit3;
	plcbit qSMProgNoBit4;
	plcbit qSMRunning;
	plcbit qSMReady1ForRobot;
	plcbit qSMReady2ForRobot;
	plcbit qSMCycle1OkForRobot;
	plcbit qSMCycle2OkForRobot;
	/* VAR (digital) */
	plcbit tConditions;
	plcbit Pulse;
} SM_Cell3_typ;

typedef struct SM_Cell4
{
	/* VAR_INPUT (analog) */
	unsigned char iPrgNo;
	/* VAR_OUTPUT (analog) */
	signed short qStateNo;
	signed short qAlarmNo;
	/* VAR (analog) */
	struct TON WaitTimer1;
	struct TON Start1PulseTimer;
	struct TON AlarmRefreshTimer;
	struct TON TimerStateNo;
	plctime WaitTime;
	plctime PulseTime;
	signed short NState;
	signed short tAlarmNo;
	signed short tOldAlarmNo;
	signed short tOldNState;
	/* VAR_INPUT (digital) */
	plcbit iStart;
	plcbit iSMClampDownSns;
	plcbit iSMClampUpSns;
	plcbit iSMLockClamp;
	plcbit iSMPressureOk;
	plcbit iSMReadyHome;
	plcbit iSMThreadBreakageError;
	plcbit iSMStopError;
	plcbit iSMVirtualHigh;
	plcbit iR4WZ;
	plcbit iSystemAuto;
	plcbit iAirOk;
	plcbit iEmgOk;
	plcbit iSafetyOk;
	/* VAR_OUTPUT (digital) */
	plcbit qBusy;
	plcbit qSMStart;
	plcbit qSMVirtual;
	plcbit qSMProgNoBit1;
	plcbit qSMProgNoBit2;
	plcbit qSMProgNoBit3;
	plcbit qSMProgNoBit4;
	plcbit qSMRunning;
	plcbit qSMReadyForRobot;
	plcbit qSMCycleOkForRobot;
	/* VAR (digital) */
	plcbit tConditions;
	plcbit Pulse;
} SM_Cell4_typ;

typedef struct CamTrig
{
	/* VAR_OUTPUT (analog) */
	signed short qStateNo;
	signed short qAlarmNo;
	/* VAR (analog) */
	signed short NState;
	signed short tAlarmNo;
	struct TON WaitTimer1;
	struct TON WaitTimer2;
	struct TON AlarmRefreshTimer;
	struct TON TimerStateNo;
	signed short tOldAlarmNo;
	signed short tOldNState;
	/* VAR_INPUT (digital) */
	plcbit iCamOnline;
	plcbit iCamTrigReady;
	plcbit iCamResultReady;
	plcbit iTrigger;
	/* VAR_OUTPUT (digital) */
	plcbit qCamTrig;
	plcbit qCamClear;
	plcbit qLightOn;
	/* VAR (digital) */
	plcbit zzEdge00000;
	plcbit zzEdge00001;
} CamTrig_typ;

typedef struct C1CamResult
{
	/* VAR_INPUT (analog) */
	signed short RefPosX;
	signed short RefPosY;
	signed short RefPos0;
	signed short ReadCamPosX;
	signed short ReadCamPosY;
	signed short ReadCamPos0;
	/* VAR_OUTPUT (analog) */
	signed short qRobotPosX;
	signed short qRobotPosY;
	signed short qRobotRotZ;
	/* VAR (analog) */
	signed short Buffer_XPos;
	signed short Buffer_YPos;
	signed short Buffer_0Pos;
	struct TON WaitTimer;
	/* VAR_INPUT (digital) */
	plcbit iReadyResult;
	plcbit iClearDatas;
	/* VAR_OUTPUT (digital) */
	plcbit qRobotPosXPstveBit;
	plcbit qRobotPosXNgtveBit;
	plcbit qRobotPosYPstveBit;
	plcbit qRobotPosYNgtveBit;
	plcbit qRobotRotZPstveBit;
	plcbit qRobotRotZNgtveBit;
	plcbit qRobotSendValue;
} C1CamResult_typ;

typedef struct Plate_FB
{
	/* VAR_OUTPUT (analog) */
	signed short qStateNo;
	signed short qAlarmNo;
	/* VAR (analog) */
	signed short NState;
	signed short tAlarmNo;
	struct TON AlarmRefreshTimer;
	struct TON TimerStateNo;
	signed short tOldAlarmNo;
	signed short tOldNState;
	/* VAR_INPUT (digital) */
	plcbit iR1Ready;
	plcbit iPlateSns1;
	plcbit iPlateSns2;
	/* VAR_OUTPUT (digital) */
	plcbit qR2Ready;
	/* VAR (digital) */
	plcbit Pulse;
} Plate_FB_typ;

typedef struct ProcesEnd
{
	/* VAR_OUTPUT (analog) */
	signed short qAlarmNo;
	/* VAR (analog) */
	signed short tAlarmNo;
	/* VAR_INPUT (digital) */
	plcbit iLineerFwdSensor;
	plcbit iPickUpSensor;
	plcbit iFoldingBwdSensor;
	plcbit iRotaryUpSensor;
	plcbit iFirstRobotCycleOn;
	plcbit iSecondRobotCycleOn;
	/* VAR_OUTPUT (digital) */
	plcbit qEndProces;
} ProcesEnd_typ;

typedef struct C4CamResult
{
	/* VAR_INPUT (analog) */
	signed short ReadCamPos1_X;
	signed short ReadCamPos1_Y;
	signed short ReadCamPos1_0;
	signed short ReadCamPos2_X;
	signed short ReadCamPos2_Y;
	signed short ReadCamPos2_0;
	/* VAR_OUTPUT (analog) */
	signed short qRobotPos1_X;
	signed short qRobotPos1_Y;
	signed short qRobotRot1_Z;
	signed short qRobotPos2_X;
	signed short qRobotPos2_Y;
	signed short qRobotRot2_Z;
	/* VAR (analog) */
	signed short Buffer_XPos1;
	signed short Buffer_YPos1;
	signed short Buffer_0Pos1;
	signed short Buffer_XPos2;
	signed short Buffer_YPos2;
	signed short Buffer_0Pos2;
	struct TON WaitTimer;
	/* VAR_INPUT (digital) */
	plcbit iReadyResult;
	plcbit iClearDatas;
	/* VAR_OUTPUT (digital) */
	plcbit qRobotPos1_XPstveBit;
	plcbit qRobotPos1_XNgtveBit;
	plcbit qRobotPos1_YPstveBit;
	plcbit qRobotPos1_YNgtveBit;
	plcbit qRobotRot1_ZPstveBit;
	plcbit qRobotRot1_ZNgtveBit;
	plcbit qRobotPos2_XPstveBit;
	plcbit qRobotPos2_XNgtveBit;
	plcbit qRobotPos2_YPstveBit;
	plcbit qRobotPos2_YNgtveBit;
	plcbit qRobotRot2_ZPstveBit;
	plcbit qRobotRot2_ZNgtveBit;
	plcbit qRobotSendValue;
} C4CamResult_typ;

typedef struct GenCamResult
{
	/* VAR_INPUT (analog) */
	signed short iSewingOk;
	signed short iIntegrityOk;
	/* VAR_OUTPUT (analog) */
	signed short qSewingOk;
	signed short qIntegrityOk;
	/* VAR (analog) */
	struct TON WaitTimer;
	/* VAR_INPUT (digital) */
	plcbit iReadyResult;
	plcbit iClearDatas;
	/* VAR_OUTPUT (digital) */
	plcbit qRobotSendValue;
} GenCamResult_typ;

typedef struct C3CamResult
{
	/* VAR_INPUT (analog) */
	signed short ReadCamPos_X;
	signed short ReadCamPos_Y;
	signed short ReadCamPos_0;
	/* VAR_OUTPUT (analog) */
	signed short qRobotPos_X;
	signed short qRobotPos_Y;
	signed short qRobotRot_Z;
	/* VAR (analog) */
	signed short Buffer_XPos;
	signed short Buffer_YPos;
	signed short Buffer_0Pos;
	struct TON WaitTimer;
	/* VAR_INPUT (digital) */
	plcbit iReadyResult;
	plcbit iClearDatas;
	/* VAR_OUTPUT (digital) */
	plcbit qRobotPos_XPstveBit;
	plcbit qRobotPos_XNgtveBit;
	plcbit qRobotPos_YPstveBit;
	plcbit qRobotPos_YNgtveBit;
	plcbit qRobotRot_ZPstveBit;
	plcbit qRobotRot_ZNgtveBit;
	plcbit qRobotSendValue;
} C3CamResult_typ;

typedef struct HotPlugStat
{
	/* VAR (analog) */
	signed short TimerPT_Int_Value;
	struct TON BlinkTimer;
	/* VAR_INPUT (digital) */
	plcbit iButton;
	plcbit iTimer;
	/* VAR_OUTPUT (digital) */
	plcbit qButtonLamp;
	/* VAR (digital) */
	plcbit bBlink;
	plcbit tLamp1;
	plcbit tLamp2;
} HotPlugStat_typ;

typedef struct Compensator_FB
{
	/* VAR_INPUT (analog) */
	plcstring iCompensatorName[81];
	/* VAR_OUTPUT (analog) */
	plcstring qAlarmMesagge[81];
	signed short qAlarmNo;
	/* VAR (analog) */
	struct TON OpenFaultTimer;
	struct TON CloseFaultTimer;
	plcstring sFaultMessage1[250];
	plcstring sFaultMessage2[250];
	/* VAR_INPUT (digital) */
	plcbit iOpenSignal;
	plcbit iCloseSignal;
	plcbit iAutoOpenReq;
	plcbit iAutoCloseReq;
	plcbit iManOpenReq;
	plcbit iManCloseReq;
	plcbit iAuto;
	plcbit iManual;
	plcbit iSafety;
	plcbit iAir;
	/* VAR_OUTPUT (digital) */
	plcbit qCylinderOpen;
	plcbit qCylinderClose;
	plcbit qOpenAlarm;
	plcbit qCloseAlarm;
	/* VAR (digital) */
	plcbit tAutoOpen;
	plcbit tManOpen;
	plcbit tAutoClose;
	plcbit tManClose;
	plcbit tOpen;
	plcbit tClose;
} Compensator_FB_typ;

typedef struct Cell1CamBlowFB
{
	/* VAR_INPUT (analog) */
	plctime iBlowTime;
	/* VAR (analog) */
	struct TON BlowTimer;
	signed short NState;
	/* VAR_INPUT (digital) */
	plcbit iRobotWZ;
	plcbit iManualBlow;
	/* VAR_OUTPUT (digital) */
	plcbit qBlow;
	/* VAR (digital) */
	plcbit tBlow;
} Cell1CamBlowFB_typ;

typedef struct Cell2CamBlowFB
{
	/* VAR_INPUT (analog) */
	plctime iBlowTime;
	/* VAR (analog) */
	struct TON BlowTimer;
	signed short NState;
	/* VAR_INPUT (digital) */
	plcbit iRobotWZ;
	plcbit iManualBlow;
	plcbit iTrnsfrVcm1Ok;
	plcbit iTrnsfrPickUpSns;
	plcbit iTrnsfrLineerBwdSns;
	/* VAR_OUTPUT (digital) */
	plcbit qBlow;
	/* VAR (digital) */
	plcbit tBlow;
} Cell2CamBlowFB_typ;

typedef struct StackerFaz2
{
	/* VAR_OUTPUT (analog) */
	signed short qStateNo;
	signed short qAlarmNo;
	/* VAR (analog) */
	signed short NState;
	struct TON tWaitTimer;
	struct TON AlarmRefreshTimer;
	struct TON TimerStateNo;
	signed short tAlarmNo;
	signed short tOldAlarmNo;
	signed short tOldNState;
	/* VAR_INPUT (digital) */
	plcbit iAirOk;
	plcbit iSafetyOk;
	plcbit iEmgOk;
	plcbit iSystemAuto;
	plcbit iTopStcker_Empty;
	plcbit iBtmStcker_Empty;
	plcbit iTopStackerOpen;
	plcbit iTopStackerClose;
	plcbit iTopStackerLocked;
	plcbit iTopStackerUnLocked;
	plcbit iTopStackerPreSens;
	plcbit iBtmStackerOpen;
	plcbit iBtmStackerClose;
	plcbit iBtmStackerLocked;
	plcbit iBtmStackerUnLocked;
	plcbit iBtmStackerPreSens;
	plcbit iSlidingDoorOpen;
	plcbit iSlidingDoorClose;
	plcbit iRbtTopStckerTaked;
	plcbit iRbtBtmStckerTaked;
	plcbit iWZ_TopStcker;
	plcbit iWZ_BottomStcker;
	/* VAR_OUTPUT (digital) */
	plcbit qTopStackerOpen;
	plcbit qTopStackerClose;
	plcbit qTopStackerLocked;
	plcbit qTopStackerUnLocked;
	plcbit qBtmStackerOpen;
	plcbit qBtmStackerClose;
	plcbit qBtmStackerLocked;
	plcbit qBtmStackerUnLocked;
	plcbit qSlidingDoorOpen;
	plcbit qSlidingDoorClose;
	plcbit qTopStcker_Ready;
	plcbit qBtmStcker_Ready;
	/* VAR (digital) */
	plcbit TEST;
	plcbit tConditions;
	plcbit zzEdge00000;
} StackerFaz2_typ;

typedef struct FlipOverMech
{
	/* VAR_OUTPUT (analog) */
	signed short qStateNo;
	signed short qAlarmNo;
	/* VAR (analog) */
	signed short NState;
	struct TON tWaitTimer;
	struct TON AlarmRefreshTimer;
	struct TON TimerStateNo;
	signed short tAlarmNo;
	signed short tOldAlarmNo;
	signed short tOldNState;
	/* VAR_INPUT (digital) */
	plcbit iAirOk;
	plcbit iSafetyOk;
	plcbit iEmgOk;
	plcbit iSystemAuto;
	plcbit iRobotStart;
	plcbit iFlipOverMechOpened;
	plcbit iFlipOverMechClosed;
	plcbit iFlipOverMechForward;
	plcbit iFlipOverMechBackward;
	/* VAR_OUTPUT (digital) */
	plcbit qFlipOverMechOpen;
	plcbit qFlipOverMechClose;
	plcbit qFlipOverMechForward;
	plcbit qFlipOverMechBackward;
	plcbit qFlippingOverMech_Ready;
	plcbit qFlippingOverMech_CycleOK;
	/* VAR (digital) */
	plcbit TEST;
	plcbit tConditions;
	plcbit zzEdge00000;
} FlipOverMech_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void Lighting_FB(struct Lighting_FB* inst);
_BUR_PUBLIC void Conveyor_FB(struct Conveyor_FB* inst);
_BUR_PUBLIC void SuctionForFoldingSpace_FB(struct SuctionForFoldingSpace_FB* inst);
_BUR_PUBLIC void ButtonBox_FB(struct ButtonBox_FB* inst);
_BUR_PUBLIC void LightTower_FB(struct LightTower_FB* inst);
_BUR_PUBLIC void CameraBlow_FB(struct CameraBlow_FB* inst);
_BUR_PUBLIC void Stacker_FB(struct Stacker_FB* inst);
_BUR_PUBLIC void Mode_FB(struct Mode_FB* inst);
_BUR_PUBLIC void ValveControlFB(struct ValveControlFB* inst);
_BUR_PUBLIC void ALT(struct ALT* inst);
_BUR_PUBLIC void Transfer(struct Transfer* inst);
_BUR_PUBLIC void SM_Cell2(struct SM_Cell2* inst);
_BUR_PUBLIC void SM_Cell3(struct SM_Cell3* inst);
_BUR_PUBLIC void SM_Cell4(struct SM_Cell4* inst);
_BUR_PUBLIC void CamTrig(struct CamTrig* inst);
_BUR_PUBLIC void C1CamResult(struct C1CamResult* inst);
_BUR_PUBLIC void Plate_FB(struct Plate_FB* inst);
_BUR_PUBLIC void ProcesEnd(struct ProcesEnd* inst);
_BUR_PUBLIC void C4CamResult(struct C4CamResult* inst);
_BUR_PUBLIC void GenCamResult(struct GenCamResult* inst);
_BUR_PUBLIC void C3CamResult(struct C3CamResult* inst);
_BUR_PUBLIC void HotPlugStat(struct HotPlugStat* inst);
_BUR_PUBLIC void Piston5_3(struct Piston5_3* inst);
_BUR_PUBLIC void Compensator_FB(struct Compensator_FB* inst);
_BUR_PUBLIC void SuctionFB(struct SuctionFB* inst);
_BUR_PUBLIC void Cell1CamBlowFB(struct Cell1CamBlowFB* inst);
_BUR_PUBLIC void Cell2CamBlowFB(struct Cell2CamBlowFB* inst);
_BUR_PUBLIC void StackerFaz2(struct StackerFaz2* inst);
_BUR_PUBLIC void FlipOverMech(struct FlipOverMech* inst);


#ifdef __cplusplus
};
#endif
#endif /* _ABB_TRFTZ1_ */

