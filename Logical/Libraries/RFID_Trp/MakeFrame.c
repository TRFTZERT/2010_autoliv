/*********************************************************************************
 * Copyright: Bernecker+Rainer
 * Author:    kayaa
 * E-Mail:	  ahmet.kaya@br-automation.com
 * Created:   April 19, 2018/10:47 AM 
 *********************************************************************************/ 

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
	#include "RFID_Trp.h"
#ifdef __cplusplus
	};
#endif

/* This function block takes barcode information and then generates the corresponding frames which are writen on the RFID tag. */
void MakeFrame(struct MakeFrame* inst)
{
	
	/* Partitioning Encryption Key into 8 parts each of them consists of 4 bits. */
	for (inst->_Internal.Index.i = 0; inst->_Internal.Index.i < sizeof(inst->inKey); inst->_Internal.Index.i++)
	{
		USINT power = sizeof(inst->inKey) - inst->_Internal.Index.i - 1;		
		inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i] = ((USINT) (inst->inKey >> 8*power) & 0xF0) >> 4;
		inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i + 1] = (USINT) (inst->inKey >> 8*power) & 0x0F;
	}
	memcpy(&inst->outKey[0], &inst->_Internal.EncryptionKey[0], sizeof(inst->outKey));
		
	
	/* Company Name */
	for (inst->_Internal.Index.i = 0; inst->_Internal.Index.i < 4; inst->_Internal.Index.i++)
	{
		memset(&inst->_Internal.TempVar.MSPart, 0, 1);
		memcpy(&inst->_Internal.TempVar.MSPart, inst->inCodeInfo.CompName + inst->_Internal.Index.i, 1);
			
		inst->_Internal.TempVar.LSPart = inst->_Internal.TempVar.MSPart & 0x0F;
		inst->_Internal.TempVar.LSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i + 1];	// Encryption (XOR)
		if (inst->_Internal.TempVar.LSPart < 10) itoa(inst->_Internal.TempVar.LSPart, inst->_Internal.TempVar.LSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.LSPart == 10) strcpy(inst->_Internal.TempVar.LSPartLet, "A");
			else if (inst->_Internal.TempVar.LSPart == 11) strcpy(inst->_Internal.TempVar.LSPartLet, "B");
			else if (inst->_Internal.TempVar.LSPart == 12) strcpy(inst->_Internal.TempVar.LSPartLet, "C");
			else if (inst->_Internal.TempVar.LSPart == 13) strcpy(inst->_Internal.TempVar.LSPartLet, "D");
			else if (inst->_Internal.TempVar.LSPart == 14) strcpy(inst->_Internal.TempVar.LSPartLet, "E");
			else if (inst->_Internal.TempVar.LSPart == 15) strcpy(inst->_Internal.TempVar.LSPartLet, "F");
		}
			
		inst->_Internal.TempVar.MSPart = (inst->_Internal.TempVar.MSPart & 0xF0) / 16;
		inst->_Internal.TempVar.MSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i];		// Encryption (XOR)
		if (inst->_Internal.TempVar.MSPart < 10) itoa(inst->_Internal.TempVar.MSPart, inst->_Internal.TempVar.MSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.MSPart == 10) strcpy(inst->_Internal.TempVar.MSPartLet, "A");
			else if (inst->_Internal.TempVar.MSPart == 11) strcpy(inst->_Internal.TempVar.MSPartLet, "B");
			else if (inst->_Internal.TempVar.MSPart == 12) strcpy(inst->_Internal.TempVar.MSPartLet, "C");
			else if (inst->_Internal.TempVar.MSPart == 13) strcpy(inst->_Internal.TempVar.MSPartLet, "D");
			else if (inst->_Internal.TempVar.MSPart == 14) strcpy(inst->_Internal.TempVar.MSPartLet, "E");
			else if (inst->_Internal.TempVar.MSPart == 15) strcpy(inst->_Internal.TempVar.MSPartLet, "F");
		}
							
		if (inst->_Internal.Index.i == 0) memset(inst->inCodeInfo.FinalData, 0, strlen(inst->inCodeInfo.FinalData));
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.MSPartLet);
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.LSPartLet);
	}
	
	memset(inst->outFrame[0], 0, sizeof(inst->outFrame[0]));
	strcat(inst->outFrame[0], "010F00030418002100");
	strcat(inst->outFrame[0], inst->inCodeInfo.FinalData);
	strcat(inst->outFrame[0], "0000");
	/*****************************************************************************************************************************/
		
	
	/* Colour Code */
	int length = sizeof(inst->inCodeInfo.ColourCode)/sizeof(inst->inCodeInfo.ColourCode[0]);
	for (inst->_Internal.Index.j = 0; inst->_Internal.Index.j < length; inst->_Internal.Index.j++)
	{
		for (inst->_Internal.Index.i = 0; inst->_Internal.Index.i < 4; inst->_Internal.Index.i++)
		{
			memset(&inst->_Internal.TempVar.MSPart, 0, 1);
			memcpy(&inst->_Internal.TempVar.MSPart, inst->inCodeInfo.ColourCode[inst->_Internal.Index.j] + inst->_Internal.Index.i, 1);
			
			inst->_Internal.TempVar.LSPart = inst->_Internal.TempVar.MSPart & 0x0F;
			inst->_Internal.TempVar.LSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i + 1];	// Encryption (XOR)
			if (inst->_Internal.TempVar.LSPart < 10) itoa(inst->_Internal.TempVar.LSPart, inst->_Internal.TempVar.LSPartLet);
			else
			{
				if 		(inst->_Internal.TempVar.LSPart == 10) strcpy(inst->_Internal.TempVar.LSPartLet, "A");
				else if (inst->_Internal.TempVar.LSPart == 11) strcpy(inst->_Internal.TempVar.LSPartLet, "B");
				else if (inst->_Internal.TempVar.LSPart == 12) strcpy(inst->_Internal.TempVar.LSPartLet, "C");
				else if (inst->_Internal.TempVar.LSPart == 13) strcpy(inst->_Internal.TempVar.LSPartLet, "D");
				else if (inst->_Internal.TempVar.LSPart == 14) strcpy(inst->_Internal.TempVar.LSPartLet, "E");
				else if (inst->_Internal.TempVar.LSPart == 15) strcpy(inst->_Internal.TempVar.LSPartLet, "F");
			}
			
			inst->_Internal.TempVar.MSPart = (inst->_Internal.TempVar.MSPart & 0xF0) / 16;
			inst->_Internal.TempVar.MSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i];		// Encryption (XOR)
			if (inst->_Internal.TempVar.MSPart < 10) itoa(inst->_Internal.TempVar.MSPart, inst->_Internal.TempVar.MSPartLet);
			else
			{
				if 		(inst->_Internal.TempVar.MSPart == 10) strcpy(inst->_Internal.TempVar.MSPartLet, "A");
				else if (inst->_Internal.TempVar.MSPart == 11) strcpy(inst->_Internal.TempVar.MSPartLet, "B");
				else if (inst->_Internal.TempVar.MSPart == 12) strcpy(inst->_Internal.TempVar.MSPartLet, "C");
				else if (inst->_Internal.TempVar.MSPart == 13) strcpy(inst->_Internal.TempVar.MSPartLet, "D");
				else if (inst->_Internal.TempVar.MSPart == 14) strcpy(inst->_Internal.TempVar.MSPartLet, "E");
				else if (inst->_Internal.TempVar.MSPart == 15) strcpy(inst->_Internal.TempVar.MSPartLet, "F");
			}
			
			strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.MSPartLet);
			strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.LSPartLet);
		}
		
		/*************************/
		memset(inst->outFrame[1+inst->_Internal.Index.j], 0, sizeof(inst->outFrame[1+inst->_Internal.Index.j]));
		strcat(inst->outFrame[1+inst->_Internal.Index.j], "010F000304180021");
		
		if 		(inst->_Internal.Index.j == 0) strcat(inst->outFrame[1+inst->_Internal.Index.j], "01");
		else if (inst->_Internal.Index.j == 1) strcat(inst->outFrame[1+inst->_Internal.Index.j], "02");
		else if (inst->_Internal.Index.j == 2) strcat(inst->outFrame[1+inst->_Internal.Index.j], "03");
		else if (inst->_Internal.Index.j == 3) strcat(inst->outFrame[1+inst->_Internal.Index.j], "04");
		else if (inst->_Internal.Index.j == 4) strcat(inst->outFrame[1+inst->_Internal.Index.j], "05");
		
		strcat(inst->outFrame[1+inst->_Internal.Index.j], inst->inCodeInfo.FinalData + 8 + 8*inst->_Internal.Index.j);
		strcat(inst->outFrame[1+inst->_Internal.Index.j], "0000");
		/*************************/
	}
	/*****************************************************************************************************************************/	
	
	
	/* Volume */
	for (inst->_Internal.Index.i = 0; inst->_Internal.Index.i < 4; inst->_Internal.Index.i++)
	{
		memset(&inst->_Internal.TempVar.MSPart, 0, 1);
		memcpy(&inst->_Internal.TempVar.MSPart, inst->inCodeInfo.Volume + inst->_Internal.Index.i, 1);
			
		inst->_Internal.TempVar.LSPart = inst->_Internal.TempVar.MSPart & 0x0F;
		inst->_Internal.TempVar.LSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i + 1];	// Encryption (XOR)
		if (inst->_Internal.TempVar.LSPart < 10) itoa(inst->_Internal.TempVar.LSPart, inst->_Internal.TempVar.LSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.LSPart == 10) strcpy(inst->_Internal.TempVar.LSPartLet, "A");
			else if (inst->_Internal.TempVar.LSPart == 11) strcpy(inst->_Internal.TempVar.LSPartLet, "B");
			else if (inst->_Internal.TempVar.LSPart == 12) strcpy(inst->_Internal.TempVar.LSPartLet, "C");
			else if (inst->_Internal.TempVar.LSPart == 13) strcpy(inst->_Internal.TempVar.LSPartLet, "D");
			else if (inst->_Internal.TempVar.LSPart == 14) strcpy(inst->_Internal.TempVar.LSPartLet, "E");
			else if (inst->_Internal.TempVar.LSPart == 15) strcpy(inst->_Internal.TempVar.LSPartLet, "F");
		}
			
		inst->_Internal.TempVar.MSPart = (inst->_Internal.TempVar.MSPart & 0xF0) / 16;
		inst->_Internal.TempVar.MSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i];		// Encryption (XOR)
		if (inst->_Internal.TempVar.MSPart < 10) itoa(inst->_Internal.TempVar.MSPart, inst->_Internal.TempVar.MSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.MSPart == 10) strcpy(inst->_Internal.TempVar.MSPartLet, "A");
			else if (inst->_Internal.TempVar.MSPart == 11) strcpy(inst->_Internal.TempVar.MSPartLet, "B");
			else if (inst->_Internal.TempVar.MSPart == 12) strcpy(inst->_Internal.TempVar.MSPartLet, "C");
			else if (inst->_Internal.TempVar.MSPart == 13) strcpy(inst->_Internal.TempVar.MSPartLet, "D");
			else if (inst->_Internal.TempVar.MSPart == 14) strcpy(inst->_Internal.TempVar.MSPartLet, "E");
			else if (inst->_Internal.TempVar.MSPart == 15) strcpy(inst->_Internal.TempVar.MSPartLet, "F");
		}
			
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.MSPartLet);
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.LSPartLet);
	}
	
	memset(inst->outFrame[1+inst->_Internal.Index.j], 0, sizeof(inst->outFrame[1+inst->_Internal.Index.j]));
	strcat(inst->outFrame[1+inst->_Internal.Index.j], "010F00030418002106");
	strcat(inst->outFrame[1+inst->_Internal.Index.j], inst->inCodeInfo.FinalData + 8 + 8*inst->_Internal.Index.j);
	strcat(inst->outFrame[1+inst->_Internal.Index.j], "0000");
	/*****************************************************************************************************************************/
	
	
	/* Date */
	for (inst->_Internal.Index.i = 0; inst->_Internal.Index.i < 4; inst->_Internal.Index.i++)
	{
		memset(&inst->_Internal.TempVar.MSPart, 0, 1);
		memcpy(&inst->_Internal.TempVar.MSPart, inst->inCodeInfo.Date + inst->_Internal.Index.i, 1);
			
		inst->_Internal.TempVar.LSPart = inst->_Internal.TempVar.MSPart & 0x0F;
		inst->_Internal.TempVar.LSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i + 1];	// Encryption (XOR)
		if (inst->_Internal.TempVar.LSPart < 10) itoa(inst->_Internal.TempVar.LSPart, inst->_Internal.TempVar.LSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.LSPart == 10) strcpy(inst->_Internal.TempVar.LSPartLet, "A");
			else if (inst->_Internal.TempVar.LSPart == 11) strcpy(inst->_Internal.TempVar.LSPartLet, "B");
			else if (inst->_Internal.TempVar.LSPart == 12) strcpy(inst->_Internal.TempVar.LSPartLet, "C");
			else if (inst->_Internal.TempVar.LSPart == 13) strcpy(inst->_Internal.TempVar.LSPartLet, "D");
			else if (inst->_Internal.TempVar.LSPart == 14) strcpy(inst->_Internal.TempVar.LSPartLet, "E");
			else if (inst->_Internal.TempVar.LSPart == 15) strcpy(inst->_Internal.TempVar.LSPartLet, "F");
		}
			
		inst->_Internal.TempVar.MSPart = (inst->_Internal.TempVar.MSPart & 0xF0) / 16;
		inst->_Internal.TempVar.MSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i];		// Encryption (XOR)
		if (inst->_Internal.TempVar.MSPart < 10) itoa(inst->_Internal.TempVar.MSPart, inst->_Internal.TempVar.MSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.MSPart == 10) strcpy(inst->_Internal.TempVar.MSPartLet, "A");
			else if (inst->_Internal.TempVar.MSPart == 11) strcpy(inst->_Internal.TempVar.MSPartLet, "B");
			else if (inst->_Internal.TempVar.MSPart == 12) strcpy(inst->_Internal.TempVar.MSPartLet, "C");
			else if (inst->_Internal.TempVar.MSPart == 13) strcpy(inst->_Internal.TempVar.MSPartLet, "D");
			else if (inst->_Internal.TempVar.MSPart == 14) strcpy(inst->_Internal.TempVar.MSPartLet, "E");
			else if (inst->_Internal.TempVar.MSPart == 15) strcpy(inst->_Internal.TempVar.MSPartLet, "F");
		}
			
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.MSPartLet);
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.LSPartLet);
	}
	
	memset(inst->outFrame[2+inst->_Internal.Index.j], 0, sizeof(inst->outFrame[2+inst->_Internal.Index.j]));
	strcat(inst->outFrame[2+inst->_Internal.Index.j], "010F00030418002107");
	strcat(inst->outFrame[2+inst->_Internal.Index.j], inst->inCodeInfo.FinalData + 16 + 8*inst->_Internal.Index.j);
	strcat(inst->outFrame[2+inst->_Internal.Index.j], "0000");
	/*****************************************************************************************************************************/
	
	
	/* UID_msp */
	for (inst->_Internal.Index.i = 0; inst->_Internal.Index.i < 4; inst->_Internal.Index.i++)
	{
		memcpy(inst->_Internal.ConvertFB.inHexText, inst->inCodeInfo.UID_msp + 2*inst->_Internal.Index.i, 2);
		Convert(&inst->_Internal.ConvertFB);
		
		inst->_Internal.TempVar.LSPart = inst->_Internal.ConvertFB.outDecimal & 0x0F;
		inst->_Internal.TempVar.LSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i + 1];	// Encryption (XOR)
		if (inst->_Internal.TempVar.LSPart < 10) itoa(inst->_Internal.TempVar.LSPart, inst->_Internal.TempVar.LSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.LSPart == 10) strcpy(inst->_Internal.TempVar.LSPartLet, "A");
			else if (inst->_Internal.TempVar.LSPart == 11) strcpy(inst->_Internal.TempVar.LSPartLet, "B");
			else if (inst->_Internal.TempVar.LSPart == 12) strcpy(inst->_Internal.TempVar.LSPartLet, "C");
			else if (inst->_Internal.TempVar.LSPart == 13) strcpy(inst->_Internal.TempVar.LSPartLet, "D");
			else if (inst->_Internal.TempVar.LSPart == 14) strcpy(inst->_Internal.TempVar.LSPartLet, "E");
			else if (inst->_Internal.TempVar.LSPart == 15) strcpy(inst->_Internal.TempVar.LSPartLet, "F");
		}
			
		inst->_Internal.TempVar.MSPart = (inst->_Internal.ConvertFB.outDecimal & 0xF0) / 16;
		inst->_Internal.TempVar.MSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i];	// Encryption (XOR)
		if (inst->_Internal.TempVar.MSPart < 10) itoa(inst->_Internal.TempVar.MSPart, inst->_Internal.TempVar.MSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.MSPart == 10) strcpy(inst->_Internal.TempVar.MSPartLet, "A");
			else if (inst->_Internal.TempVar.MSPart == 11) strcpy(inst->_Internal.TempVar.MSPartLet, "B");
			else if (inst->_Internal.TempVar.MSPart == 12) strcpy(inst->_Internal.TempVar.MSPartLet, "C");
			else if (inst->_Internal.TempVar.MSPart == 13) strcpy(inst->_Internal.TempVar.MSPartLet, "D");
			else if (inst->_Internal.TempVar.MSPart == 14) strcpy(inst->_Internal.TempVar.MSPartLet, "E");
			else if (inst->_Internal.TempVar.MSPart == 15) strcpy(inst->_Internal.TempVar.MSPartLet, "F");
		}
			
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.MSPartLet);
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.LSPartLet);
	}
	
	memset(inst->outFrame[3+inst->_Internal.Index.j], 0, sizeof(inst->outFrame[3+inst->_Internal.Index.j]));
	strcat(inst->outFrame[3+inst->_Internal.Index.j], "010F00030418002108");
	strcat(inst->outFrame[3+inst->_Internal.Index.j], inst->inCodeInfo.FinalData + 24 + 8*inst->_Internal.Index.j);
	strcat(inst->outFrame[3+inst->_Internal.Index.j], "0000");
	/*****************************************************************************************************************************/
	
	
	/* UID_lsp */
	for (inst->_Internal.Index.i = 0; inst->_Internal.Index.i < 4; inst->_Internal.Index.i++)
	{
		memcpy(inst->_Internal.ConvertFB.inHexText, inst->inCodeInfo.UID_lsp + 2*inst->_Internal.Index.i, 2);
		Convert(&inst->_Internal.ConvertFB);
		
		inst->_Internal.TempVar.LSPart = inst->_Internal.ConvertFB.outDecimal & 0x0F;
		inst->_Internal.TempVar.LSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i + 1];	// Encryption (XOR)
		if (inst->_Internal.TempVar.LSPart < 10) itoa(inst->_Internal.TempVar.LSPart, inst->_Internal.TempVar.LSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.LSPart == 10) strcpy(inst->_Internal.TempVar.LSPartLet, "A");
			else if (inst->_Internal.TempVar.LSPart == 11) strcpy(inst->_Internal.TempVar.LSPartLet, "B");
			else if (inst->_Internal.TempVar.LSPart == 12) strcpy(inst->_Internal.TempVar.LSPartLet, "C");
			else if (inst->_Internal.TempVar.LSPart == 13) strcpy(inst->_Internal.TempVar.LSPartLet, "D");
			else if (inst->_Internal.TempVar.LSPart == 14) strcpy(inst->_Internal.TempVar.LSPartLet, "E");
			else if (inst->_Internal.TempVar.LSPart == 15) strcpy(inst->_Internal.TempVar.LSPartLet, "F");
		}
			
		inst->_Internal.TempVar.MSPart = (inst->_Internal.ConvertFB.outDecimal & 0xF0) / 16;
		inst->_Internal.TempVar.MSPart ^= inst->_Internal.EncryptionKey[2*inst->_Internal.Index.i];	// Encryption (XOR)
		if (inst->_Internal.TempVar.MSPart < 10) itoa(inst->_Internal.TempVar.MSPart, inst->_Internal.TempVar.MSPartLet);
		else
		{
			if 		(inst->_Internal.TempVar.MSPart == 10) strcpy(inst->_Internal.TempVar.MSPartLet, "A");
			else if (inst->_Internal.TempVar.MSPart == 11) strcpy(inst->_Internal.TempVar.MSPartLet, "B");
			else if (inst->_Internal.TempVar.MSPart == 12) strcpy(inst->_Internal.TempVar.MSPartLet, "C");
			else if (inst->_Internal.TempVar.MSPart == 13) strcpy(inst->_Internal.TempVar.MSPartLet, "D");
			else if (inst->_Internal.TempVar.MSPart == 14) strcpy(inst->_Internal.TempVar.MSPartLet, "E");
			else if (inst->_Internal.TempVar.MSPart == 15) strcpy(inst->_Internal.TempVar.MSPartLet, "F");
		}
			
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.MSPartLet);
		strcat(inst->inCodeInfo.FinalData, inst->_Internal.TempVar.LSPartLet);
	}
	
	memset(inst->outFrame[4+inst->_Internal.Index.j], 0, sizeof(inst->outFrame[4+inst->_Internal.Index.j]));
	strcat(inst->outFrame[4+inst->_Internal.Index.j], "010F00030418002109");
	strcat(inst->outFrame[4+inst->_Internal.Index.j], inst->inCodeInfo.FinalData + 32 + 8*inst->_Internal.Index.j);
	strcat(inst->outFrame[4+inst->_Internal.Index.j], "0000");
	/*****************************************************************************************************************************/
}
