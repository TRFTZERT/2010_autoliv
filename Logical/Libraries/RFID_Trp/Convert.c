/*********************************************************************************
 * Copyright: Bernecker+Rainer
 * Author:    kayaa
 * E-Mail:	  ahmet.kaya@br-automation.com
 * Created:   April 18, 2018/4:51 PM 
 *********************************************************************************/ 

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
	#include "RFID_Trp.h"
#ifdef __cplusplus
	};
#endif

/* It used for conversion hex string data to decimal data or conversion of decimal data to hex string data. */
void Convert(struct Convert* inst)
{
	
	/* Conversion from hexadecimal string to decimal integer value. */
	/* inHextext --> outDecimal */
	memset(inst->_Internal.MSPartLet, 0, 1);
	memcpy(inst->_Internal.MSPartLet, inst->inHexText, 1);
	inst->_Internal.MSPart = (USINT) atoi(inst->_Internal.MSPartLet);
	if (inst->_Internal.MSPart == 0)
	{
		if 		(strcmp(inst->_Internal.MSPartLet, "A") == 0) inst->_Internal.MSPart = 10;
		else if (strcmp(inst->_Internal.MSPartLet, "B") == 0) inst->_Internal.MSPart = 11;
		else if (strcmp(inst->_Internal.MSPartLet, "C") == 0) inst->_Internal.MSPart = 12;
		else if (strcmp(inst->_Internal.MSPartLet, "D") == 0) inst->_Internal.MSPart = 13;
		else if (strcmp(inst->_Internal.MSPartLet, "E") == 0) inst->_Internal.MSPart = 14;
		else if (strcmp(inst->_Internal.MSPartLet, "F") == 0) inst->_Internal.MSPart = 15;
	}
	
	memset(inst->_Internal.LSPartLet, 0, 1);
	memcpy(inst->_Internal.LSPartLet, inst->inHexText + 1, 1);
	inst->_Internal.LSPart = (USINT) atoi(inst->_Internal.LSPartLet);
	if (inst->_Internal.LSPart == 0)
	{
		if 		(strcmp(inst->_Internal.LSPartLet, "A") == 0) inst->_Internal.LSPart = 10;
		else if (strcmp(inst->_Internal.LSPartLet, "B") == 0) inst->_Internal.LSPart = 11;
		else if (strcmp(inst->_Internal.LSPartLet, "C") == 0) inst->_Internal.LSPart = 12;
		else if (strcmp(inst->_Internal.LSPartLet, "D") == 0) inst->_Internal.LSPart = 13;
		else if (strcmp(inst->_Internal.LSPartLet, "E") == 0) inst->_Internal.LSPart = 14;
		else if (strcmp(inst->_Internal.LSPartLet, "F") == 0) inst->_Internal.LSPart = 15;
	}
	
	inst->outDecimal = inst->_Internal.MSPart*16 + inst->_Internal.LSPart;
	/*****************************************************************************************************************************/
	
	
	/* Conversion from BYTE to ASCII character. */
	/* outDecimal --> outChar */
	for (inst->_Internal.i = 0; inst->_Internal.i < sizeof(ASCII_HEX); inst->_Internal.i++)
	{
		if (inst->outDecimal == ASCII_HEX[inst->_Internal.i])
		{
			memcpy(inst->outChar, &ASCII_HEX[inst->_Internal.i], 1);
			break;
		}
		
		if (inst->_Internal.i == sizeof(ASCII_HEX) - 1)
		{
			memset(inst->outChar, 0, 1);
		}
	}
	/*****************************************************************************************************************************/
	
	
	/* Conversion from decimal integer value to hexadecimal string. */
	/* inDecimal --> outHexText */
	memset(inst->outHexText, 0, sizeof(inst->outHexText));
	memset(&inst->_Internal.MSPart, 0, 1);
	
	inst->_Internal.MSPart = (inst->inDecimal & 0xF0) / 16;
	if (inst->_Internal.MSPart < 10) itoa(inst->_Internal.MSPart, inst->_Internal.MSPartLet);
	else
	{
		if 		(inst->_Internal.MSPart == 10) strcpy(inst->_Internal.MSPartLet, "A");
		else if (inst->_Internal.MSPart == 11) strcpy(inst->_Internal.MSPartLet, "B");
		else if (inst->_Internal.MSPart == 12) strcpy(inst->_Internal.MSPartLet, "C");
		else if (inst->_Internal.MSPart == 13) strcpy(inst->_Internal.MSPartLet, "D");
		else if (inst->_Internal.MSPart == 14) strcpy(inst->_Internal.MSPartLet, "E");
		else if (inst->_Internal.MSPart == 15) strcpy(inst->_Internal.MSPartLet, "F");
	}
	strcat(inst->outHexText, inst->_Internal.MSPartLet);
	
	inst->_Internal.LSPart = inst->inDecimal & 0x0F;
	if (inst->_Internal.LSPart < 10) itoa(inst->_Internal.LSPart, inst->_Internal.LSPartLet);
	else
	{
		if 		(inst->_Internal.LSPart == 10) strcpy(inst->_Internal.LSPartLet, "A");
		else if (inst->_Internal.LSPart == 11) strcpy(inst->_Internal.LSPartLet, "B");
		else if (inst->_Internal.LSPart == 12) strcpy(inst->_Internal.LSPartLet, "C");
		else if (inst->_Internal.LSPart == 13) strcpy(inst->_Internal.LSPartLet, "D");
		else if (inst->_Internal.LSPart == 14) strcpy(inst->_Internal.LSPartLet, "E");
		else if (inst->_Internal.LSPart == 15) strcpy(inst->_Internal.LSPartLet, "F");
	}
	strcat(inst->outHexText, inst->_Internal.LSPartLet);
	/*****************************************************************************************************************************/
	
}
