/*********************************************************************************
 * Copyright: Bernecker+Rainer
 * Author:    kayaa
 * E-Mail:	  ahmet.kaya@br-automation.com
 * Created:   April 12, 2018/11:45 AM 
 *********************************************************************************/ 

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
	#include "RFID_Trp.h"
#ifdef __cplusplus
	};
#endif

#define	TRUE 1
#define FALSE 0

int pow(int base, int power);

/*
 *	NOTE: Communication based on IEC15693
 *
 *
 *	Initialization sequence:
 *	------------------------
 *	o- USB_GETNODELIST
 *	|
 *	o- USB_SEARCHDEVICE
 *	|
 *	o- DVF_DEVICEOPEN
 *	|
 *	o- RFID_INIT_READER
 *	|
 *	o- RFID_WRITE
 *	|
 *	o- RFID_READ
 *
 *
 *	Command sequence:
 *	------------------------
 *	o- RFID_READ_INVENTORY | RFID_READ_SYSINFO | RFID_READ_SBLOCK | RFID_WRITE_SBLOCK |
 *	   RFID_WRITE_AFI | RFID_WRITE_DSFID
 *	|
 *	o- RFID_WRITE
 *	|
 *	o- RFID_READ
 *
 */

/* Implementation of the function block which provides easy usage of a 13 MHz RFID Transponder. */

void RFIDTrpRW(struct RFIDTrpRW* inst)
{	
	/****************************** FB Enabled ******************************/
	if (inst->inEnable)
	{
		
		/* This part is taken from "13MHzTransp" sample program. */
		/* There are some modifications to improve its suitabiliy of its usage. */
		switch (inst->_Internal.Status.Step)
		{
		
			case IDLE:
				
				if (inst->inAPC)
				{
					if (inst->_Internal.Parameters.Com.Pvi.Cmd == 1)
					{
						inst->outDeviceFound =  TRUE;	// Device is found.
						
						if ((inst->inTrpType == E9010))
						{
							inst->_Internal.Parameters.Data.TransponderType = E9010;
							inst->_Internal.Status.Step = RFID_INIT_READER;
						}
						else if (inst->inTrpType == E9030)
						{
							inst->_Internal.Parameters.Data.TransponderType = E9030;
							inst->_Internal.Status.Step = MANAGE_5E9030;
						}
						else if (inst->inTrpType == ELATEC_TWN4_MULTITECH_NANO)
						{
							inst->_Internal.Parameters.Data.TransponderType = ELATEC_TWN4_MULTITECH_NANO;
							inst->_Internal.Status.Step = MANAGE_5E9010;
						}
					}
				}
				else
				{
					/* Condition of the device */
					inst->outDeviceFound = inst->_Internal.Status.Flag;
				
					/* Initialize the communication between the CPU and RFID Transponder at first. */
					if (!inst->_Internal.Status.Flag)
					{
						inst->_Internal.Status.Flag = TRUE;
						inst->_Internal.Status.Step	= USB_GETNODELIST;
					}
				}
			
				break;
			
			/*==========================================================================*/
			case MANAGE_5E9010:	// MAIN CONTROL OF E9010 TRANSPONDER	
			/*==========================================================================*/	
				
				inst->outDeviceFound = TRUE;	// The device is found.
				
				/* Read the content of the RFID tag. */
				if (inst->inRead)
				{
//					if (inst->_Internal.Status.BlockCountR < inst->outTagInfo.NumOfBlocks)
					if (inst->_Internal.Status.BlockCountR < NUM_OF_FRAME + 2 + 2)
					{
						if (inst->_Internal.Status.BlockCountR == 0)
						{
							memset(inst->outData, 0, sizeof(inst->outData));
							memset(inst->outBarCode, 0, sizeof(inst->outBarCode));
							memset(inst->_Internal.Parameters.Data.ReadData, 0, sizeof(inst->_Internal.Parameters.Data.ReadData));
						}
						else if (inst->_Internal.Status.BlockCountR > 1 - inst->inAPC)
						{
							memcpy((inst->outData + (inst->_Internal.Status.BlockCountR-2+inst->inAPC)*8), inst->_Internal.Parameters.Data.ReadData + 3, 8);
						}
						
						/* Convert the incoming code to a human readable ASCII characters. */
						if ((inst->_Internal.Status.BlockCountR-2+inst->inAPC) < NUM_OF_FRAME)
						{
							for (inst->_Internal.i = (inst->_Internal.Status.BlockCountR-2+inst->inAPC)*4; inst->_Internal.i < strlen(inst->outData)/2; inst->_Internal.i++)
							{
								memcpy(inst->_Internal.FB.ConvertFB.inHexText, inst->outData + 2*inst->_Internal.i, 2);
								Convert(&inst->_Internal.FB.ConvertFB);
							
								/* Decryption Procedure */
								USINT decrypted = inst->_Internal.FB.ConvertFB.outDecimal;
								decrypted ^= inst->inKey[(2*inst->_Internal.i) % 8]*16 + inst->inKey[(2*inst->_Internal.i + 1) % 8];	// Decryption (XOR)
								
								if ((inst->_Internal.Status.BlockCountR-2+inst->inAPC) < NUM_OF_FRAME-2)
								{
									memcpy(inst->outBarCode[inst->_Internal.Status.BlockCountR-2+inst->inAPC] + inst->_Internal.i - (inst->_Internal.Status.BlockCountR-2+inst->inAPC)*4, &decrypted, 1);
								}
								else
								{
									inst->_Internal.FB.ConvertFB.inDecimal = decrypted;
									Convert(&inst->_Internal.FB.ConvertFB);
									strcat(inst->outBarCode[inst->_Internal.Status.BlockCountR-2+inst->inAPC], inst->_Internal.FB.ConvertFB.outHexText);
								}
							}
							
							/* Get The Encryption Key */
							inst->_Internal.Parameters.Data.EncryptionKey = 0;
							for (inst->_Internal.i = 0; inst->_Internal.i < 4; inst->_Internal.i++)
							{
								inst->_Internal.Parameters.Data.EncryptionKey += inst->inKey[(2*inst->_Internal.i) % 8]*16 + inst->inKey[(2*inst->_Internal.i + 1) % 8];
								if (inst->_Internal.i != 3)
								{
									inst->_Internal.Parameters.Data.EncryptionKey *= 256;
								}
							}
							
							/* If the incoming data is the encryption key, then the output will be empty to avoid from showing the encryption key from the user. */
							int tempCode = 0;
							for (inst->_Internal.i = 0; inst->_Internal.i < 8; inst->_Internal.i += 2)
							{
								memcpy(inst->_Internal.FB.ConvertFB.inHexText, inst->outBarCode[inst->_Internal.Status.BlockCountR-2] + inst->_Internal.i, 2);
								Convert(&inst->_Internal.FB.ConvertFB);
								tempCode += inst->_Internal.FB.ConvertFB.outDecimal * pow(256, (4 - inst->_Internal.i/2 - 1));	
							}
							
							if (tempCode == inst->_Internal.Parameters.Data.EncryptionKey)
							{
								memset(inst->outBarCode, 0, sizeof(inst->outBarCode));
							}
						}
						
						inst->_Internal.Parameters.Data.ReadBlock = inst->_Internal.Status.BlockCountR;
						inst->_Internal.Status.Step	= RFID_READ_SBLOCK;
						inst->_Internal.Status.BlockCountR++;
					}
					else
					{						
						inst->outDone = TRUE;
					}
				}
				/* Write some content on the RFID tag. */
				else if (inst->inWrite)
				{
					if (inst->_Internal.Status.BlockCountW < sizeof(inst->inBarCode)/sizeof(inst->inBarCode[0]))
					{
						if (inst->_Internal.Status.BlockCountW == 0)
						{
							memset(inst->_Internal.Parameters.Data.WriteData, 0, sizeof(inst->_Internal.Parameters.Data.WriteData));
						}
						strcpy(inst->_Internal.Parameters.Data.WriteData, inst->inBarCode[inst->_Internal.Status.BlockCountW], sizeof(inst->inBarCode[inst->_Internal.Status.BlockCountW]));
						inst->_Internal.Status.Step	= RFID_WRITE_SBLOCK;
						inst->_Internal.Status.BlockCountW++;
					}
					else
					{
						inst->outDone = TRUE;
					}
				}
				/* Clear the all content of the tag. */
				else if (inst->inClearTag)
				{
					if (inst->_Internal.Status.BlockCountW < NUM_OF_FRAME)
					{
						if (inst->_Internal.Status.BlockCountW == 0)
						{
							memset(inst->_Internal.Parameters.Data.WriteData, 0, sizeof(inst->_Internal.Parameters.Data.WriteData));
						}
						
						strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "010F000304180021");
		
						/* Conversion from decimal to hex string. */
						inst->_Internal.FB.ConvertFB.inDecimal = inst->_Internal.Status.BlockCountW;
						Convert(&inst->_Internal.FB.ConvertFB);
						strcpy((char*)inst->_Internal.Parameters.Data.WriteData + 16, inst->_Internal.FB.ConvertFB.outHexText);
		
						strcpy((char*)inst->_Internal.Parameters.Data.WriteData + 18, "000000000000");
						
						inst->_Internal.Status.Step	= RFID_WRITE_SBLOCK;
						inst->_Internal.Status.BlockCountW++;
					}
					else
					{
						inst->outDone = TRUE;
					}
				}
				/* Get the tag info while there is no read or write operation. */
				else if (inst->inReadTagInfo || inst->_Internal.ReadTagInfo)
				{
					/* Reset outDone output when the rag info is read internally. */
					inst->outDone = !inst->_Internal.ReadTagInfo;
						
					/* Reset at first. */
					inst->_Internal.Parameters.Data.TagInfo.NumOfBlocks = 0;
					inst->_Internal.Parameters.Data.TagInfo.BlockSize = 0;
					memset(inst->_Internal.Parameters.Data.TagInfo.UID, 0, sizeof(inst->_Internal.Parameters.Data.TagInfo.UID));
		
					if (inst->inTrpType == ELATEC_TWN4_MULTITECH_NANO)
					{
						if (memcmp(inst->_Internal.Parameters.Data.ReadData, "0001", 4) == 0)
						{
							/* Set the tag availability flag. */
							inst->_Internal.Parameters.Data.TagInfo.Available = TRUE;
		
							/* Get the UID */
							memcpy(inst->_Internal.Parameters.Data.TagInfo.UID, inst->_Internal.Parameters.Data.ReadData + 10, 8);
		
							/* Set the TagInfo output. */
							memcpy(&inst->outTagInfo, &inst->_Internal.Parameters.Data.TagInfo, sizeof(inst->outTagInfo));
						}
						else
						{
							/* Reset the tag availability flag. */
							inst->_Internal.Parameters.Data.TagInfo.Available = FALSE;
		
							/* Reset the TagInfo output. */
							memcpy(&inst->outTagInfo, &inst->_Internal.Parameters.Data.TagInfo, sizeof(inst->outTagInfo));
						}
		
						/* Always send it to RFID_READ_SYSINFO case. */
						inst->_Internal.Status.Step	= RFID_READ_SYSINFO;
					}
					else
					{
						/* UID */
						for (inst->_Internal.i = 0; inst->_Internal.i < 8; inst->_Internal.i++)
						{
							memcpy(inst->_Internal.Parameters.Data.TagInfo.UID + 2*inst->_Internal.i, inst->_Internal.Parameters.Data.ReadData + 19 - 2*inst->_Internal.i, 2);
						}
									
						/* Number of Data Blocks */
						memcpy(inst->_Internal.FB.ConvertFB.inHexText, inst->_Internal.Parameters.Data.ReadData + 25, 2);
						Convert(&inst->_Internal.FB.ConvertFB);
						inst->_Internal.Parameters.Data.TagInfo.NumOfBlocks = inst->_Internal.FB.ConvertFB.outDecimal;
											
						/* Size of Each Data Block in Bits */
						memcpy(inst->_Internal.FB.ConvertFB.inHexText, inst->_Internal.Parameters.Data.ReadData + 27, 2);
						Convert(&inst->_Internal.FB.ConvertFB);
						inst->_Internal.Parameters.Data.TagInfo.BlockSize = (inst->_Internal.FB.ConvertFB.outDecimal + 1) * 8;
											
						if (inst->_Internal.Parameters.Data.TagInfo.NumOfBlocks == 0)
	//					if (strlen(inst->_Internal.Parameters.Data.TagInfo.UID) < 8)
						{
							/* Try to read any tag 3 times. If there is no tag, reset inst->outTagInfo.Available value. */
							if (inst->_Internal.Status.TagCounter >= 3)
							{
								inst->_Internal.Parameters.Data.TagInfo.Available = FALSE;
								memset(inst->outBarCode, 0, sizeof(inst->outBarCode));
								memset(&inst->outTagInfo, 0, sizeof(inst->outTagInfo));
							}
							else
							{
								inst->_Internal.Status.TagCounter++;
							}
														
							inst->_Internal.Status.Step	= RFID_READ_SYSINFO;
						}
						else if (!inst->_Internal.Status.TagInfoFlag)
						{
							inst->_Internal.Status.TagCounter = 0;
							inst->_Internal.Status.TagInfoFlag = TRUE;
							inst->_Internal.Status.Step	= RFID_READ_SYSINFO;
						}
						else 
						{
							if (!inst->_Internal.ReadTagInfo)
							{
								inst->outDone = TRUE;
							}
							inst->_Internal.ReadTagInfo = FALSE;
							inst->_Internal.Parameters.Data.TagInfo.NumOfBlocks++;
							inst->_Internal.Parameters.Data.TagInfo.Available = TRUE;
							memcpy(&inst->outTagInfo, &inst->_Internal.Parameters.Data.TagInfo, sizeof(inst->outTagInfo));	// Get new info.
						}
										
						if (inst->_Internal.Status.ReadCount == 30)
						{
							inst->_Internal.ReadTagInfo = FALSE;
							inst->_Internal.Status.Step	= RFID_INIT_READER;
						}
					}
				}
				/* Reset the tag. */
				else if (inst->inResetTag)
				{
					if (memcmp(inst->_Internal.Parameters.Data.ReadData, "[00]", 4) == 0)
					{
						inst->outDone = TRUE;
					}
					else
					{
						inst->_Internal.Status.Step	= RFID_RESET_TO_READY;
					}
				}
				else
				{
					inst->outDone = FALSE;
					inst->_Internal.ReadTagInfo = TRUE;
					inst->_Internal.Status.BlockCountR = 0;
					inst->_Internal.Status.BlockCountW = 0;
					inst->_Internal.Status.TagInfoFlag = FALSE;
					memset(inst->_Internal.Parameters.Data.ReadData, 0, sizeof(inst->_Internal.Parameters.Data.ReadData));
				}
				
				break;
			
			/*==========================================================================*/
			case MANAGE_5E9030:	// MAIN CONTROL OF E9030 TRANSPONDER
			/*==========================================================================*/	
				
				inst->outDeviceFound = TRUE;	// The device is found.
				
				/* Read the content of the RFID tag. */
				if (inst->inRead)
				{
					if (inst->_Internal.Status.BlockCountR < NUM_OF_FRAME + 2)
					{
						if (inst->_Internal.Status.BlockCountR == 0)
						{
							memset(inst->outData, 0, sizeof(inst->outData));
							memset(inst->outBarCode, 0, sizeof(inst->outBarCode));
							memset(inst->_Internal.Parameters.Data.ReadData, 0, sizeof(inst->_Internal.Parameters.Data.ReadData));
						}
						else if (inst->_Internal.Status.BlockCountR > 1 - inst->inAPC)
						{
							memcpy((inst->outData + (inst->_Internal.Status.BlockCountR-2)*8), inst->_Internal.Parameters.Data.ReadData + 2, 8);
						}
					
						/* Convert the incoming code to a human readable ASCII characters. */
						if ((inst->_Internal.Status.BlockCountR-2+inst->inAPC) < NUM_OF_FRAME + 2)
						{
							for (inst->_Internal.i = (inst->_Internal.Status.BlockCountR-2+inst->inAPC)*4; inst->_Internal.i < strlen(inst->outData)/2; inst->_Internal.i++)
								{
								memcpy(inst->_Internal.FB.ConvertFB.inHexText, inst->outData + 2*inst->_Internal.i, 2);
								Convert(&inst->_Internal.FB.ConvertFB);
							
								/* Decryption Procedure */
								USINT decrypted = inst->_Internal.FB.ConvertFB.outDecimal;
								decrypted ^= inst->inKey[(2*inst->_Internal.i) % 8]*16 + inst->inKey[(2*inst->_Internal.i + 1) % 8];	// Decryption (XOR)
								
								if ((inst->_Internal.Status.BlockCountR-2+inst->inAPC) < NUM_OF_FRAME-2)
								{
									memcpy(inst->outBarCode[inst->_Internal.Status.BlockCountR-2+inst->inAPC] + inst->_Internal.i - (inst->_Internal.Status.BlockCountR-2+inst->inAPC)*4, &decrypted, 1);
								}
								else
								{
									inst->_Internal.FB.ConvertFB.inDecimal = decrypted;
									Convert(&inst->_Internal.FB.ConvertFB);
									strcat(inst->outBarCode[inst->_Internal.Status.BlockCountR-2+inst->inAPC], inst->_Internal.FB.ConvertFB.outHexText);
								}
							}
						}
						
						inst->_Internal.Parameters.Data.ReadBlock = inst->_Internal.Status.BlockCountR;
						inst->_Internal.Status.Step	= RFID_READ_SBLOCK;
						inst->_Internal.Status.BlockCountR++;
					}
					else
					{						
						inst->outDone = TRUE;
					}
				}
				/* Write some content on the RFID tag. */
				else if (inst->inWrite)
				{
					if (inst->_Internal.Status.BlockCountW < sizeof(inst->inBarCode)/sizeof(inst->inBarCode[0]) + 2)
					{
						memset(inst->_Internal.Parameters.Data.WriteData, 0, sizeof(inst->_Internal.Parameters.Data.WriteData));
						strcpy(inst->_Internal.Parameters.Data.WriteData, "Write,a,ff ff ff ff ff ff,");
						itoa((DINT)inst->_Internal.Status.BlockCountW, inst->_Internal.Parameters.Data.WriteData + strlen(inst->_Internal.Parameters.Data.WriteData));
						strcat(inst->_Internal.Parameters.Data.WriteData, ", ");
						strcpy(inst->_Internal.Parameters.Data.WriteData + strlen(inst->_Internal.Parameters.Data.WriteData), inst->inBarCode[inst->_Internal.Status.BlockCountW] + 18, 8);
						strcat(inst->_Internal.Parameters.Data.WriteData, "\r\n");
						inst->_Internal.Status.Step	= RFID_WRITE_SBLOCK;
						inst->_Internal.Status.BlockCountW++;
					}
					else
					{
						inst->outDone = TRUE;
					}
				}
				/* Clear the all content of the tag. */
				else if (inst->inClearTag)
				{
					if (inst->_Internal.Status.BlockCountW < NUM_OF_FRAME + 2)
					{
						memset(inst->_Internal.Parameters.Data.WriteData, 0, sizeof(inst->_Internal.Parameters.Data.WriteData));
						strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "Write,a,ff ff ff ff ff ff,");
						itoa((DINT)inst->_Internal.Status.BlockCountW, inst->_Internal.Parameters.Data.WriteData + strlen(inst->_Internal.Parameters.Data.WriteData));
						strcat(inst->_Internal.Parameters.Data.WriteData, ",000000000000\r\n");
						inst->_Internal.Status.Step	= RFID_WRITE_SBLOCK;
						inst->_Internal.Status.BlockCountW++;
					}
					else
					{
						inst->outDone = TRUE;
					}
				}
				/* Reset the tag. */
				else if (inst->inResetTag)
				{
					/*	if (memcmp(inst->_Internal.Parameters.Data.ReadData, "[00]", 4) == 0)
						{
							inst->outDone = TRUE;
						}
						else
						{
							inst->_Internal.Status.Step	= RFID_RESET_TO_READY;
						}*/
					inst->outDone = TRUE;
				}
				else
				{
					inst->outDone = FALSE;
					inst->_Internal.Status.BlockCountR = 0;
					inst->_Internal.Status.BlockCountW = 0;
					inst->_Internal.Status.TagInfoFlag = FALSE;
					inst->_Internal.Status.Step	= RFID_READ;
				}	
				
				/* Availabity Condition of an RFID Tag */
				if (memcmp(inst->_Internal.Parameters.Data.ReadData, "PiccSelect", 10) == 0)
				{
					inst->_Internal.Parameters.Data.TagInfo.Available = TRUE;
					inst->_Internal.Parameters.Data.TagInfo.NumOfBlocks = 28;
					inst->_Internal.Parameters.Data.TagInfo.BlockSize = 32;
				
					/* UID */
					for (inst->_Internal.i = 0; inst->_Internal.i < 8; inst->_Internal.i++)
						{
						memcpy(inst->_Internal.Parameters.Data.TagInfo.UID + 2*inst->_Internal.i, inst->_Internal.Parameters.Data.ReadData + 26 - 2*inst->_Internal.i, 2);
					}
				
					memcpy(&inst->outTagInfo, &inst->_Internal.Parameters.Data.TagInfo, sizeof(inst->outTagInfo));
				}
				else if (memcmp(inst->_Internal.Parameters.Data.ReadData, "PiccRemove", 10) == 0)
				{
					inst->_Internal.Parameters.Data.TagInfo.Available = FALSE;
					inst->_Internal.Parameters.Data.TagInfo.NumOfBlocks = 0;
					inst->_Internal.Parameters.Data.TagInfo.BlockSize = 0;
					memset(inst->_Internal.Parameters.Data.TagInfo.UID, 0, sizeof(inst->_Internal.Parameters.Data.TagInfo.UID));
					memcpy(&inst->outTagInfo, &inst->_Internal.Parameters.Data.TagInfo, sizeof(inst->outTagInfo));
				}
			
				break;
			
		
			case USB_GETNODELIST:
				
				inst->_Internal.FB.UsbNodeListGetFub.enable = TRUE;
				inst->_Internal.FB.UsbNodeListGetFub.pBuffer = (UDINT)&inst->_Internal.Parameters.Com.UsbNodeList;
				inst->_Internal.FB.UsbNodeListGetFub.bufferSize = sizeof(inst->_Internal.Parameters.Com.UsbNodeList);
				inst->_Internal.FB.UsbNodeListGetFub.filterInterfaceClass = 0;
				inst->_Internal.FB.UsbNodeListGetFub.filterInterfaceSubClass = 0;
				UsbNodeListGet(&inst->_Internal.FB.UsbNodeListGetFub);
			
				if (inst->_Internal.FB.UsbNodeListGetFub.status == ERR_OK && inst->_Internal.FB.UsbNodeListGetFub.listNodes)
				{
					/* USB Device Attach or detach */
					inst->_Internal.Status.Step = USB_SEARCHDEVICE;
					inst->_Internal.Parameters.Com.UsbNodeIx = 0;
//					usbAttachDetachCount = inst->_Internal.FB.UsbNodeListGetFub.attachDetachCount;
				}
				else if (inst->_Internal.FB.UsbNodeListGetFub.status == asusbERR_USB_NOTFOUND || inst->_Internal.FB.UsbNodeListGetFub.status == ERR_FUB_BUSY)
				{
					// Do nothing.
				}
				else
				{
					inst->_Internal.Status.Step = RFID_ERROR;
				}
				
				/* Status Info */
				inst->outStatusID = inst->_Internal.FB.UsbNodeListGetFub.status;
				
				break;
	
	
			case USB_SEARCHDEVICE:
				
//				sizeUsbNode = sizeof(inst->_Internal.Parameters.Com.UsbDevice);
				inst->_Internal.FB.UsbNodeGetFub.enable = TRUE;
				inst->_Internal.FB.UsbNodeGetFub.nodeId = inst->_Internal.Parameters.Com.UsbNodeList[inst->_Internal.Parameters.Com.UsbNodeIx];
				inst->_Internal.FB.UsbNodeGetFub.pBuffer = (UDINT)&inst->_Internal.Parameters.Com.UsbDevice;
				inst->_Internal.FB.UsbNodeGetFub.bufferSize = sizeof(inst->_Internal.Parameters.Com.UsbDevice);
				UsbNodeGet(&inst->_Internal.FB.UsbNodeGetFub);
			
				if (inst->_Internal.FB.UsbNodeGetFub.status == ERR_OK )
				{
					/* USB FTDI Transponder ? */
					for (inst->_Internal.i = 0; inst->_Internal.i < 3; inst->_Internal.i++)
					{
						if (inst->_Internal.Parameters.Com.UsbDevice.vendorId == TRANSPONDER_FTDI_VENDOR_ID[inst->_Internal.i]
							&& inst->_Internal.Parameters.Com.UsbDevice.productId == TRANSPONDER_FTDI_PRODUCT_ID[inst->_Internal.i])
						{
							/* USB FTDI Transponder found */
							strcpy((char*)inst->_Internal.Parameters.Com.StringDevice, inst->_Internal.Parameters.Com.UsbDevice.ifName);
							strcpy(inst->outDevice, inst->_Internal.Parameters.Com.StringDevice);
							//usbNodeId = inst->_Internal.Parameters.Com.UsbNodeList[inst->_Internal.Parameters.Com.UsbNodeIx];
							inst->_Internal.Parameters.Data.TransponderType = inst->_Internal.i;
							inst->_Internal.Status.Step = DVF_DEVICEOPEN;
						}
					}
					
					if (inst->_Internal.Status.Step != DVF_DEVICEOPEN)
					{
						inst->_Internal.Parameters.Com.UsbNodeIx++;
						if (inst->_Internal.Parameters.Com.UsbNodeIx >= inst->_Internal.FB.UsbNodeListGetFub.allNodes)
							/* USB Device not found */
							inst->_Internal.Status.Step = USB_GETNODELIST;
					}
				}
				else if (inst->_Internal.FB.UsbNodeGetFub.status == ERR_FUB_BUSY)
				{
					// Do nothing.
				}
				else if (inst->_Internal.FB.UsbNodeGetFub.status == asusbERR_USB_NOTFOUND)
				{
					/* USB Device not found */
					inst->_Internal.Status.Step = USB_GETNODELIST;
				}
				else
				{
					inst->_Internal.Status.Step = RFID_ERROR;
				}
				
				/* Status Info */
				inst->outStatusID = inst->_Internal.FB.UsbNodeGetFub.status;
				
				break;
	
	
			case DVF_DEVICEOPEN:
				
				/* initialize open structure */
				inst->_Internal.FB.FrameXOpenFub.device = (UDINT) inst->_Internal.Parameters.Com.StringDevice;
				inst->_Internal.FB.FrameXOpenFub.mode = (UDINT) 0;
				inst->_Internal.FB.FrameXOpenFub.config = (UDINT) 0;
				inst->_Internal.FB.FrameXOpenFub.enable = TRUE;
				FRM_xopen(&inst->_Internal.FB.FrameXOpenFub); /* open an interface */
			
				if (inst->_Internal.FB.FrameXOpenFub.status == frmERR_OK)
				{
					if (inst->_Internal.Parameters.Data.TransponderType == E9010)
					{
						inst->_Internal.Status.Step = RFID_INIT_READER;
					}
					else if (inst->_Internal.Parameters.Data.TransponderType == E9030)
					{
						inst->_Internal.Status.Step = MANAGE_5E9030;
					}
					else if (inst->_Internal.Parameters.Data.TransponderType == ELATEC_TWN4_MULTITECH_NANO)
					{
						inst->_Internal.Status.Step = MANAGE_5E9010;
					}
				}
				else if (inst->_Internal.FB.FrameXOpenFub.status == ERR_FUB_BUSY)
				{
					// Do nothing.
				}
				else
				{
					inst->_Internal.Status.Step = RFID_ERROR;
				}
				
				/* Status Info */
				inst->outStatusID = inst->_Internal.FB.FrameXOpenFub.status;
				
				break;
	
	
			case RFID_INIT_READER:
			/*
			 *   Register Write:
	         *   5V operation, RF output active, full power; ISO 15693, low bit rate, 6.62 kbps, one subcarrier, 1 out of 4
			 *
	         *   10  00  21  01  00
	         *   |   |   |   |   |
	         *   |   |   |   |   Value Register 0x01
	         *   |   |   |   ISO Control Register (0x01)
	         *   |   |   Value Register 0x00
	         *   |   Chip Status Control Register (0x00)
	         *   Register Write Cmd
			 */
				strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "010C00030410002101000000"); /* Set Protocol, 1 out of 4, full power, low bit rate */
				inst->_Internal.Status.ReadAnswer = 1; /* 1 answer */
				inst->_Internal.Status.Step = RFID_WRITE;
				break;
	
	
			case RFID_READ_INVENTORY:
			/*
	         *   Inventory
	         *   14  04  01  00
	         *   |   |   |   |
	         *   |   |   |   Mask Length
	         *   |   |   Anticollision Cmd
	         *   |   Flags (Inventory flag = 1, HDR = 0)
	         *   Inventory request
			 */
				if (inst->_Internal.Parameters.Data.TransponderType == E9010)
				{
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "010B000304140401000000");
				}
				else if (inst->_Internal.Parameters.Data.TransponderType == E9030)
				{
					
				}
				inst->_Internal.Status.ReadAnswer = 16; /* 16 slot answer */
				inst->_Internal.Status.Step = RFID_WRITE;
				break;
	
	
			case RFID_READ_SYSINFO:
			/*
	         *   Read System Info
	         *   18  00  2B
	         *   |   |   |
	         *   |   |   Get System Info Cmd
	         *   |   Flags (Option = 0, HDR = 0)
	         *   Request Cmd
			 */
				if (inst->_Internal.Parameters.Data.TransponderType == E9010)
				{
	 				strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "010A00030418002B0000");
				}
				else if (inst->_Internal.Parameters.Data.TransponderType == E9030)
				{
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "show_sn\r\n");
				}
				else if (inst->_Internal.Parameters.Data.TransponderType == ELATEC_TWN4_MULTITECH_NANO)
				{
						strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "050020\r\n");
				}
				inst->_Internal.Status.ReadAnswer = 1; /* 1 answer */
				inst->_Internal.Status.Step = RFID_WRITE;
				break;
		
		
			case RFID_READ_SBLOCK:
			/*
	         *   Read Single Block
	         *   18  00  20
	         *   |   |   |
	         *   |   |   Read Single Block Cmd
	         *   |   Flags (Option = 0, HDR = 0)
	         *   Request Cmd
	         *
	         *   Option = 1: Return block security status 
			 */			
				if (inst->_Internal.Parameters.Data.TransponderType == E9010)
				{
					/* Conversion from decimal to hex string. */
					inst->_Internal.FB.ConvertFB.inDecimal = inst->_Internal.Parameters.Data.ReadBlock;
					Convert(&inst->_Internal.FB.ConvertFB);
					
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "010B000304180020");
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData + 16, inst->_Internal.FB.ConvertFB.outHexText);
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData + 18, "0000");
				}
				else if (inst->_Internal.Parameters.Data.TransponderType == E9030)
				{
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "Read,a,ff ff ff ff ff ff,");
					itoa((DINT)inst->_Internal.Parameters.Data.ReadBlock, inst->_Internal.Parameters.Data.WriteData + strlen(inst->_Internal.Parameters.Data.WriteData));
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData + strlen(inst->_Internal.Parameters.Data.WriteData), "\r\n");
				}
				
				inst->_Internal.Status.ReadAnswer = 1; /* 1 answer */
				inst->_Internal.Status.Step = RFID_WRITE;
				break;
		
		
			case RFID_WRITE_SBLOCK:
			/*
	         *   Write Single Block
	         *   18  00  21
	         *   |   |   |
	         *   |   |   Write Single Block Cmd
	         *   |   Flags (Option = 0, HDR = 0)
	         *   Request Cmd
			 */
				inst->_Internal.Status.ReadAnswer = 1; /* 1 answer */
				inst->_Internal.Status.Step = RFID_WRITE;
				break;


			case RFID_WRITE_AFI:
			/*
	         *   Write AFI
	         *   18  00  30
	         *   |   |   |
	         *   |   |   Get System Info Cmd
	         *   |   Flags (Option = 0, HDR = 0)
	         *   Request Cmd
			 */
				strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "010B000304180027300000"); /* write AFI (0x30 = Identification) */
				inst->_Internal.Status.ReadAnswer = 1; /* 1 answer */
				inst->_Internal.Status.Step = RFID_WRITE;
				break;


			case RFID_WRITE_DSFID:
			/*
	         *   Write DSFID
	         *   18  00  29
	         *   |   |   |
	         *   |   |   Write DSFID Cmd
	         *   |   Flags (Option = 0, HDR = 0)
	         *   Request Cmd
			 */
				strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "010B000304180029180000"); /* write DSFID 0x18 */
				inst->_Internal.Status.ReadAnswer = 1; /* 1 answer */
				inst->_Internal.Status.Step = RFID_WRITE;
				break;

			
			case RFID_RESET_TO_READY:
				
				if (inst->_Internal.Parameters.Data.TransponderType == E9010)
				{
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "010A0003041800260000");
				}
				else if (inst->_Internal.Parameters.Data.TransponderType == E9030)
				{
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData, "Reset_To_Ready,a,");
					strcpy((char*)inst->_Internal.Parameters.Data.WriteData + 17, "Reset_To_Ready,a,ff ff ff ff ff ff\r\n");
				}
				
				inst->_Internal.Status.ReadAnswer = 1; /* 1 answer */
				inst->_Internal.Status.Step = RFID_WRITE;
				break;
			

			case RFID_WRITE:

				/* To be able to use RFID transponder connected to APC USB ports, it is necessarry to use RFID Gateway program. */
				/* Therefore, the communication is established over the RFID Gateway program instead of AsUSB library function blocks. */
				if (inst->inAPC)
				{
					if (inst->_Internal.Parameters.Com.Pvi.CmdOld == 1)
					{
						memset(inst->_Internal.Parameters.Com.Pvi.WriteData, 0, sizeof(inst->_Internal.Parameters.Com.Pvi.WriteData));
						strcpy(inst->_Internal.Parameters.Com.Pvi.WriteData, inst->_Internal.Parameters.Data.WriteData);
						inst->_Internal.Parameters.Com.Pvi.Cmd = 10;
						inst->_Internal.Parameters.Com.Pvi.CmdOld = 10;
					}
					else
					{
						if (inst->_Internal.Parameters.Com.Pvi.Cmd == 1)
						{
							inst->_Internal.Parameters.Com.Pvi.CmdOld = 1;
							inst->_Internal.Status.Step = RFID_READ;
						}
					}
				}
				else
				{
					/* initialize write structure */
					inst->_Internal.FB.FrameWriteFub.ident = inst->_Internal.FB.FrameXOpenFub.ident;
					inst->_Internal.FB.FrameWriteFub.buffer = (UDINT)&inst->_Internal.Parameters.Data.WriteData;
					inst->_Internal.FB.FrameWriteFub.buflng = strlen((char*)&inst->_Internal.Parameters.Data.WriteData);
					inst->_Internal.FB.FrameWriteFub.enable = TRUE;
					FRM_write(&inst->_Internal.FB.FrameWriteFub); /* write data to interface */
			
					if (inst->_Internal.FB.FrameWriteFub.status == frmERR_OK) /* check status */
					{
						inst->_Internal.Status.Step = RFID_READ; 
						inst->_Internal.Status.ReadCount = 0;
					}
					else if (inst->_Internal.FB.FrameWriteFub.status == ERR_FUB_BUSY)
					{
						// Do nothing.
					}
						else
						{
							inst->_Internal.Status.Step = RFID_ERROR;
						}
				
					/* Status Info */
					inst->outStatusID = inst->_Internal.FB.FrameWriteFub.status;
				}
				
				break;


			case RFID_READ:
				
				if (inst->inAPC)
				{
					memset(inst->_Internal.Parameters.Data.ReadData, 0, sizeof(inst->_Internal.Parameters.Data.ReadData));
					memcpy(inst->_Internal.Parameters.Data.ReadData, inst->_Internal.Parameters.Com.Pvi.ReadData, strlen(inst->_Internal.Parameters.Com.Pvi.ReadData));
					
					inst->_Internal.Status.ReadCount++;
					/* read cycle finished */
//					if (inst->_Internal.Status.ReadCount == inst->_Internal.Status.ReadAnswer)
//					{
//						if (inst->_Internal.Parameters.Data.TransponderType == E9010)
//						{
//							inst->_Internal.Status.Step = IDLE;
//						}
//						else if (inst->_Internal.Parameters.Data.TransponderType == E9030)
//						{
//							inst->_Internal.Status.Step = RFID_READ;
//						}
//					}
				}
				else
				{
					/* initialize read structure */
					inst->_Internal.FB.FrameReadFub.enable = TRUE;
					inst->_Internal.FB.FrameReadFub.ident = inst->_Internal.FB.FrameXOpenFub.ident;
					FRM_read(&inst->_Internal.FB.FrameReadFub); /* read data form reader */
					
					inst->_Internal.Parameters.Data.pReadBuffer = (UDINT*) inst->_Internal.FB.FrameReadFub.buffer; /* get adress of read buffer */
				
					if (inst->_Internal.FB.FrameReadFub.status == frmERR_OK) /* check status */
					{
						memset(inst->_Internal.Parameters.Data.ReadData, 0, sizeof(inst->_Internal.Parameters.Data.ReadData));
						memcpy(inst->_Internal.Parameters.Data.ReadData, (char*)inst->_Internal.Parameters.Data.pReadBuffer, inst->_Internal.FB.FrameReadFub.buflng); 	/* copy read data into array */
						
						/* initialize release buffer structure */
						inst->_Internal.FB.FrameReleaseBufferFub.enable = TRUE;
						inst->_Internal.FB.FrameReleaseBufferFub.ident = inst->_Internal.FB.FrameXOpenFub.ident;
						inst->_Internal.FB.FrameReleaseBufferFub.buffer = (UDINT) inst->_Internal.Parameters.Data.pReadBuffer;
						inst->_Internal.FB.FrameReleaseBufferFub.buflng = inst->_Internal.FB.FrameReadFub.buflng;
						FRM_rbuf(&inst->_Internal.FB.FrameReleaseBufferFub); /* release read buffer */
						
						inst->_Internal.Status.ReadCount++;
						/* read cycle finished */
//						if (inst->_Internal.Status.ReadCount == inst->_Internal.Status.ReadAnswer)
//						{
//							if (inst->_Internal.Parameters.Data.TransponderType == E9010)
//							{
//								inst->_Internal.Status.Step = IDLE;
//							}
//							else if (inst->_Internal.Parameters.Data.TransponderType == E9030)
//							{
//								inst->_Internal.Status.Step = RFID_READ;
//							}
//						}
					}
					else if (inst->_Internal.FB.FrameReadFub.status == ERR_FUB_BUSY)
					{
						// Do nothing.
					}
					else if (inst->_Internal.FB.FrameReadFub.status == frmERR_NOINPUT && inst->_Internal.Parameters.Data.TransponderType == E9030)
					{
						/* Read again. */
						inst->_Internal.FB.FrameReadFub.enable = FALSE;
						FRM_read(&inst->_Internal.FB.FrameReadFub);
					}
					else
					{
						inst->_Internal.Status.Step = RFID_ERROR;
					}
				}
				
				/* Status Info */
				inst->outStatusID = inst->_Internal.FB.FrameReadFub.status;
				if (inst->_Internal.FB.FrameReadFub.status == frmERR_FUB_ENABLE_FALSE)
				{
					inst->outStatusID = 0;
				}
				
				/* Go to management case of the corresponding RFID transponder. */
				if ((inst->_Internal.Parameters.Data.TransponderType == E9010) || (inst->_Internal.Parameters.Data.TransponderType == ELATEC_TWN4_MULTITECH_NANO))
				{
					inst->_Internal.Status.Step = MANAGE_5E9010;
				}
				else if (inst->_Internal.Parameters.Data.TransponderType == E9030)
				{
					inst->_Internal.Status.Step = MANAGE_5E9030;
				}
				
				break;
			
			
			case RFID_ERROR:
				
				inst->outError = TRUE;
				
				break;

		} /* switch (inst->_Internal.Status.Step) */
		
	}
	
	/****************************** FB Disabled ******************************/
	else
	{
		/* Reset inputs. */
		inst->inRead = FALSE;
		inst->inWrite = FALSE;
		
		/* Reset outputs. */
		inst->outError = FALSE;
		inst->outDone = FALSE;
		inst->outStatusID = 0;
		inst->outDeviceFound = FALSE;
		memset(inst->outDevice, 0, sizeof(inst->outDevice));
		memset(&inst->outTagInfo, 0, sizeof(inst->outTagInfo));
		memset(inst->outData, 0, sizeof(inst->outData));
		memset(inst->outBarCode, 0, sizeof(inst->outBarCode));
		
		/* Disable all functions which are used internally. */
		inst->_Internal.FB.FrameReadFub.enable = FALSE;
		inst->_Internal.FB.FrameReleaseBufferFub.enable = FALSE;
		inst->_Internal.FB.FrameWriteFub.enable = FALSE;
		inst->_Internal.FB.FrameXOpenFub.enable = FALSE;
		inst->_Internal.FB.UsbNodeGetFub.enable = FALSE;
		inst->_Internal.FB.UsbNodeListGetFub.enable = FALSE;
		
		/* Set the default step. */
		inst->_Internal.Status.Step = IDLE;
		
		/* Close the transponder and deallocate the memory used by transponder. */
		inst->_Internal.FB.FrameXCloseFub.enable = TRUE;
		inst->_Internal.FB.FrameXCloseFub.ident = inst->_Internal.FB.FrameXOpenFub.ident;
		if ((inst->_Internal.FB.FrameXCloseFub.status != 0 || inst->_Internal.Status.Flag) && inst->_Internal.FB.FrameXCloseFub.ident != 0)
		{
			FRM_close(&inst->_Internal.FB.FrameXCloseFub);	// Call FB
		}
				
		/* Call all functions used internally for once to disable them. */
		if (inst->_Internal.Status.Flag)
		{
			FRM_read(&inst->_Internal.FB.FrameReadFub);
			FRM_rbuf(&inst->_Internal.FB.FrameReleaseBufferFub);
			FRM_write(&inst->_Internal.FB.FrameWriteFub);
			FRM_xopen(&inst->_Internal.FB.FrameXOpenFub);
			UsbNodeGet(&inst->_Internal.FB.UsbNodeGetFub);
			UsbNodeListGet(&inst->_Internal.FB.UsbNodeListGetFub);
			
			inst->_Internal.Status.Flag = FALSE;
		}
	}
}

int pow(int base, int power)
{
	int i, result = 1;
	
	if (power == 0)
	{
		return 1;
	}
	
	for (i = 0; i < power; i++)
	{
		result *= base;
	}
	
	return result;
}