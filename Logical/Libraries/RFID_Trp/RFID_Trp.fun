
FUNCTION_BLOCK RFIDTrpRW (*Implementation of the function block which provides easy usage of a 13 MHz RFID Transponder.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		inEnable : BOOL;
		inAPC : BOOL;
		inTrpType : RFIDTrpRWInternalParDataTrpEnum;
		inRead : BOOL;
		inWrite : BOOL;
		inClearTag : BOOL;
		inReadTagInfo : BOOL;
		inResetTag : BOOL;
		inKey : ARRAY[0..7] OF USINT;
		inBarCode : ARRAY[0..NUM_OF_FRAME_INDEX] OF STRING[80];
	END_VAR
	VAR_OUTPUT
		outError : BOOL;
		outDeviceFound : BOOL;
		outStatusID : UINT;
		outDone : BOOL;
		outDevice : STRING[20];
		outTagInfo : RFIDTrpTagInfoType;
		outData : STRING[255];
		outBarCode : ARRAY[0..NUM_OF_FRAME_INDEX] OF STRING[80];
	END_VAR
	VAR
		_Internal : RFIDTrpRWInternalType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK MakeFrame (*This function block takes barcode information and then generates the corresponding frames which are writen on the RFID tag.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		inCodeInfo : MakeFrameInputType;
		inKey : UDINT;
	END_VAR
	VAR_OUTPUT
		outFrame : ARRAY[0..NUM_OF_FRAME_INDEX] OF STRING[80];
		outKey : ARRAY[0..7] OF USINT;
	END_VAR
	VAR
		_Internal : MakeFrameInternalType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK Convert (*It used for conversion hex string data to decimal data or conversion of decimal data to hex string data.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		inHexText : STRING[3];
		inDecimal : USINT;
	END_VAR
	VAR_OUTPUT
		outDecimal : USINT;
		outHexText : STRING[3];
		outChar : STRING[2];
	END_VAR
	VAR
		_Internal : ConvertInternalType;
	END_VAR
END_FUNCTION_BLOCK
