(********************************************************** RFIDTrpRW Function Block Types **********************************************************)
(*=====================================================*)
(*Function Block Data Types*)
(*=====================================================*)

TYPE
	RFIDTrpTagInfoType : 	STRUCT 
		UID : STRING[17];
		NumOfBlocks : USINT;
		BlockSize : USINT;
		Available : BOOL;
	END_STRUCT;
END_TYPE

(**)
(*=====================================================*)
(*Internal Data Types*)
(*=====================================================*)

TYPE
	RFIDTrpRWInternalType : 	STRUCT 
		FB : RFIDTrpRWInternalFBType;
		Status : RFIDTrpRWInternalStatusType;
		Parameters : RFIDTrpRWInternalParType;
		ReadTagInfo : BOOL;
		i : USINT;
	END_STRUCT;
	RFIDTrpRWInternalFBType : 	STRUCT 
		FrameReadFub : FRM_read;
		FrameReleaseBufferFub : FRM_rbuf;
		FrameWriteFub : FRM_write;
		FrameXOpenFub : FRM_xopen;
		FrameXCloseFub : FRM_close;
		UsbNodeGetFub : UsbNodeGet;
		UsbNodeListGetFub : UsbNodeListGet;
		ConvertFB : Convert;
	END_STRUCT;
	RFIDTrpRWInternalStatusType : 	STRUCT 
		Step : RFIDTrpRWInternalStatusEnum;
		ReadAnswer : USINT;
		ReadCount : USINT;
		BlockCountR : USINT;
		BlockCountW : USINT;
		Flag : BOOL;
		TagInfoFlag : BOOL;
		TagCounter : USINT;
	END_STRUCT;
	RFIDTrpRWInternalParType : 	STRUCT 
		Com : RFIDTrpRWInternalParComType;
		Data : RFIDTrpRWInternalParDataType;
	END_STRUCT;
	RFIDTrpRWInternalParComType : 	STRUCT 
		StringDevice : STRING[20];
		UsbNodeList : ARRAY[0..31]OF UDINT;
		UsbNodeIx : USINT;
		UsbDevice : usbNode_typ;
		Pvi : RFIDTrpRWInternalParComPVIType;
	END_STRUCT;
	RFIDTrpRWInternalParComPVIType : 	STRUCT 
		Cmd : USINT;
		CmdOld : USINT;
		ReadData : STRING[255];
		WriteData : STRING[255];
	END_STRUCT;
	RFIDTrpRWInternalParDataType : 	STRUCT 
		TagInfo : RFIDTrpTagInfoType;
		TransponderType : RFIDTrpRWInternalParDataTrpEnum;
		pReadBuffer : REFERENCE TO UDINT;
		ReadData : STRING[80];
		WriteData : STRING[80];
		ReadBlock : USINT;
		EncryptionKey : UDINT;
	END_STRUCT;
	RFIDTrpRWInternalParDataTrpEnum : 
		(
		E9010 := 0,
		E9030 := 1,
		ELATEC_TWN4_MULTITECH_NANO
		);
	RFIDTrpRWInternalStatusEnum : 
		(
		IDLE,
		MANAGE_5E9010,
		MANAGE_5E9030,
		USB_GETNODELIST,
		USB_SEARCHDEVICE,
		DVF_DEVICEOPEN,
		RFID_INIT_READER,
		RFID_READ_INVENTORY,
		RFID_READ_SYSINFO,
		RFID_READ_SBLOCK,
		RFID_WRITE_SBLOCK,
		RFID_WRITE_AFI,
		RFID_WRITE_DSFID,
		RFID_RESET_TO_READY,
		RFID_WRITE,
		RFID_READ,
		RFID_ERROR
		);
END_TYPE

(**)
(**)
(********************************************************** MakeFrame Function Block Types **********************************************************)
(*=====================================================*)
(*Function Block Data Types*)
(*=====================================================*)

TYPE
	MakeFrameInputType : 	STRUCT 
		CompName : STRING[4];
		ColourCode : ARRAY[0..4]OF STRING[4];
		Date : STRING[4];
		Volume : STRING[4];
		UID_msp : STRING[8];
		UID_lsp : STRING[8];
		FinalData : STRING[90];
	END_STRUCT;
END_TYPE

(**)
(*=====================================================*)
(*Internal Data Types*)
(*=====================================================*)

TYPE
	MakeFrameInternalType : 	STRUCT 
		ConvertFB : Convert;
		TempVar : MakeFrameInternalTempVarType;
		Index : MakeFrameInternalIndexType;
		EncryptionKey : ARRAY[0..7]OF USINT;
	END_STRUCT;
	MakeFrameInternalTempVarType : 	STRUCT 
		LSPart : USINT;
		MSPart : USINT;
		LSPartLet : STRING[1];
		MSPartLet : STRING[1];
	END_STRUCT;
	MakeFrameInternalIndexType : 	STRUCT 
		i : USINT;
		j : USINT;
	END_STRUCT;
END_TYPE

(**)
(**)
(********************************************************** Convert Function Block Types **********************************************************)
(*=====================================================*)
(*Function Block Data Types*)
(*=====================================================*)
(**)
(*=====================================================*)
(*Internal Data Types*)
(*=====================================================*)

TYPE
	ConvertInternalType : 	STRUCT 
		LSPart : USINT;
		MSPart : USINT;
		LSPartLet : STRING[1];
		MSPartLet : STRING[1];
		i : USINT;
	END_STRUCT;
END_TYPE
