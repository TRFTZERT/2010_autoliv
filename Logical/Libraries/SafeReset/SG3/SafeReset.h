/* Automation Studio generated header file */
/* Do not edit ! */
/* SafeReset  */

#ifndef _SAFERESET_
#define _SAFERESET_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Datatypes and datatypes of function blocks */
typedef struct SafetyFBType
{	struct safeRemoteControl_V2 RemoteControl;
} SafetyFBType;

typedef struct SafetyParType
{	struct RemoteControlCmdTypeV1 RemoteControlPar;
} SafetyParType;

typedef struct SafetyType
{	struct SafetyFBType FB;
	struct SafetyParType Par;
} SafetyType;

typedef struct SafetyReset
{
	/* VAR_INPUT (analog) */
	plcstring Password[81];
	/* VAR_OUTPUT (analog) */
	unsigned short StatusID;
	/* VAR (analog) */
	struct SafetyType Loc;
	/* VAR_INPUT (digital) */
	plcbit Reset;
	/* VAR_OUTPUT (digital) */
	plcbit Done;
	plcbit Error;
} SafetyReset_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void SafetyReset(struct SafetyReset* inst);


#ifdef __cplusplus
};
#endif
#endif /* _SAFERESET_ */

