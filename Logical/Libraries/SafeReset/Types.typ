
TYPE
	SafetyType : 	STRUCT 
		FB : SafetyFBType;
		Par : SafetyParType;
	END_STRUCT;
	SafetyFBType : 	STRUCT 
		RemoteControl : safeRemoteControl_V2;
	END_STRUCT;
	SafetyParType : 	STRUCT 
		RemoteControlPar : RemoteControlCmdTypeV1;
	END_STRUCT;
END_TYPE
