
TYPE
	HMIMode : 	STRUCT 
		SystemAuto : BOOL;
		ManuelMode : BOOL;
		AutoStop : BOOL;
		ReserveMod : ARRAY[1..4]OF BOOL;
		State : USINT;
	END_STRUCT;
	hDiag : 	STRUCT 
		InitPos : BOOL;
		Set : BOOL;
		Rst : BOOL;
		SetRun : BOOL;
		RstRun : BOOL;
		Error : BOOL;
		InterlockActive : BOOL;
		LightBeamActive : BOOL;
		SetFault : BOOL;
		RstFault : BOOL;
		Act : BOOL;
		ErrorCode : USINT;
	END_STRUCT;
END_TYPE
