
{REDUND_ERROR} FUNCTION_BLOCK ALT (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		In : BOOL;
	END_VAR
	VAR_OUTPUT
		Out : BOOL;
	END_VAR
	VAR
		Stat : BOOL;
		Alt : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK ValveControlFB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=pneumatic-2-way-solenoid-valve-icon-vectorr.png,$CATICON=pneumatic-2-way-solenoid-valve-icon-vectorr.png*)
	VAR_INPUT
		iInterlock : BOOL;
		iLightBeam : BOOL;
		iSet_Sns : ARRAY[1..16] OF BOOL;
		iRst_Sns : ARRAY[1..16] OF BOOL;
		iSysAut : BOOL;
		iSysManuel : BOOL;
		iMan_Set : BOOL;
		iAuto_Set : BOOL;
		iSep_Set : BOOL;
		iMan_Rst : BOOL;
		iAuto_Rst : BOOL;
		iSep_Rst : BOOL;
		iTest_Set : BOOL;
		iTest_Rst : BOOL;
		iAuto_Mid : BOOL;
		iMan_Mid : BOOL;
		iBypass : BOOL;
		iPartless : BOOL;
		iOtherSide : BOOL;
		iSystemStep : USINT;
		sAct : ARRAY[1..16] OF BOOL;
	END_VAR
	VAR_OUTPUT
		qValveSet : BOOL;
		qValveRst : BOOL;
		qValveInitPos : BOOL;
		qSetRun : BOOL;
		qRstRun : BOOL;
		qError : BOOL;
		qSetFault : BOOL;
		hSFSnsAlarm : ARRAY[1..16] OF BOOL;
		qRstFault : BOOL;
		hRFSnsAlarm : ARRAY[1..16] OF BOOL;
		hSnsState : ARRAY[1..16] OF USINT;
		hDiag : hDiag;
		hAlarm : USINT;
		htrig : BOOL;
	END_VAR
	VAR
		sTimerTrg : TON;
		sTimerTrg_IN : BOOL;
		sTimerTrg_PT : TIME;
		sTimerS : TON;
		sTimerS_IN : BOOL;
		sTimerS_PT : TIME;
		sTimerR : TON;
		sTimerR_IN : BOOL;
		sTimerR_PT : TIME;
		sValf_ileride : BOOL;
		sValf_Geride : BOOL;
		sHataYok : BOOL;
		sSetSensOK : BOOL;
		sResetSensOK : BOOL;
		sSSensorHata : BOOL;
		sRSensorHata : BOOL;
	END_VAR
	VAR RETAIN
		sSet : BOOL;
		sReset : BOOL;
	END_VAR
	VAR
		sTrig : BOOL;
	END_VAR
	VAR RETAIN
		sAutoSetMemory : BOOL;
	END_VAR
	VAR
		sAutoSetMemoryWork : BOOL;
	END_VAR
	VAR RETAIN
		sAutoRstMemory : BOOL;
	END_VAR
	VAR
		sAutoRstMemoryWork : BOOL;
		sOld : USINT;
	END_VAR
	VAR RETAIN
		sSnsState : ARRAY[1..16] OF USINT;
	END_VAR
	VAR
		tFault1 : BOOL;
		tFault2 : BOOL;
		tFault3 : BOOL;
		tProcess : BOOL;
		tActSns : BOOL;
		tHMIDiag : hDiag;
	END_VAR
	VAR CONSTANT
		CodeNo : ARRAY[1..16] OF USINT := [11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26];
	END_VAR
	VAR
		ForNT6 : USINT;
		ForNT13 : USINT;
		ForNT11 : USINT;
		ForNT26 : USINT;
		SShot : ARRAY[0..5] OF BOOL;
		SLine1 : BOOL;
		SLine2 : BOOL;
		SLine3 : BOOL;
		SLine4 : BOOL;
		SLine5 : BOOL;
		RShot : ARRAY[0..6] OF BOOL;
		RLine1 : BOOL;
		RLine2 : BOOL;
		RLine3 : BOOL;
		RLine4 : BOOL;
		RLine5 : BOOL;
		zzEdge00000 : BOOL;
		zzEdge00001 : BOOL;
		zzEdge00002 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Mode_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iAutoStop : {REDUND_UNREPLICABLE} BOOL;
		iEmgAct : {REDUND_UNREPLICABLE} BOOL;
		iReset : {REDUND_UNREPLICABLE} BOOL;
		iClockPulse : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qAutoMode : {REDUND_UNREPLICABLE} BOOL;
		qManuelMode : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR
		qHMI : HMIMode;
		tAuto : BOOL;
		sAuto : BOOL;
		iReserveMod : ARRAY[1..4] OF BOOL;
		qReserveMod : ARRAY[1..4] OF BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK VACUMCNTRL (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iVacumOnOff : {REDUND_UNREPLICABLE} BOOL;
		iVacumOk : BOOL;
		iBlowOnOff : BOOL;
	END_VAR
	VAR_OUTPUT
		qVacum : BOOL;
		qBlow : BOOL;
		qVacumOk : BOOL;
		qVacumFault : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} {REDUND_UNREPLICABLE} FUNCTION_BLOCK OutputMultiplexer (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		IN1 : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		OUT1 : {REDUND_UNREPLICABLE} BOOL;
		OUT2 : {REDUND_UNREPLICABLE} BOOL;
		OUT3 : {REDUND_UNREPLICABLE} BOOL;
		OUT4 : {REDUND_UNREPLICABLE} BOOL;
		OUT5 : {REDUND_UNREPLICABLE} BOOL;
		OUT6 : {REDUND_UNREPLICABLE} BOOL;
		OUT7 : {REDUND_UNREPLICABLE} BOOL;
		OUT8 : {REDUND_UNREPLICABLE} BOOL;
		OUT9 : {REDUND_UNREPLICABLE} BOOL;
		OUT10 : {REDUND_UNREPLICABLE} BOOL;
		OUT11 : {REDUND_UNREPLICABLE} BOOL;
		OUT12 : {REDUND_UNREPLICABLE} BOOL;
		OUT13 : {REDUND_UNREPLICABLE} BOOL;
		OUT14 : {REDUND_UNREPLICABLE} BOOL;
		OUT15 : {REDUND_UNREPLICABLE} BOOL;
		OUT16 : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Stacker_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iOpenedSignal : {REDUND_UNREPLICABLE} BOOL;
		iClosedSignal : {REDUND_UNREPLICABLE} BOOL;
		iEmpty : BOOL;
		iWZSignal : BOOL;
		iAirOk : BOOL;
		iSGOk : BOOL;
		iStackerMgntcSns : BOOL;
		iSafetyOk : BOOL;
		iSystemAuto : BOOL;
		iSystemManual : BOOL;
		iManualOpen : {REDUND_UNREPLICABLE} BOOL;
		iManualClose : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qOpenValve : BOOL;
		qCloseValve : BOOL;
		qOpenFault : BOOL;
		qCloseFault : BOOL;
		qStackerReady : BOOL;
		qCanOpenGate : BOOL;
	END_VAR
	VAR
		Valve : Pstn_5_3;
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		RS_Close : RS;
		R_TRIG_0 : R_TRIG;
		RS_Open : RS;
		tOpen : BOOL;
		tClose : BOOL;
		F_TRIG_0 : F_TRIG;
		R_TRIG_1 : R_TRIG;
		DoorOpened : RS;
		DoorClosed : RS;
		bStackerReady : RS;
		tCloseFault1 : BOOL;
		tCloseFault2 : BOOL;
		TON1 : TON;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CameraBlow_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iCamScreenEmpty : {REDUND_UNREPLICABLE} BOOL;
		iRobotWZ : {REDUND_UNREPLICABLE} BOOL;
		iSystemAuto : BOOL;
		iSystemManual : BOOL;
		iManualBlow : BOOL;
		iWaitTime : {REDUND_UNREPLICABLE} TIME;
		iBlowTime : {REDUND_UNREPLICABLE} TIME;
	END_VAR
	VAR_OUTPUT
		qBlow : BOOL;
		qTimeOutFault : BOOL;
	END_VAR
	VAR
		tBlow : BOOL;
		TON_BlowWait : TON;
		TON_BlowOn : TON;
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK LightTower_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Hucre_GSOK : BOOL;
		Hucre_ASOK : BOOL;
		AlarmVar : BOOL;
		UyariVar : BOOL;
		SysOut_CycleON : BOOL;
		OP_ResetBtn : BOOL;
	END_VAR
	VAR_OUTPUT
		Kule_Kirmizi : BOOL;
		Kule_Sari : BOOL;
		Kule_Yesil : BOOL;
		Kule_Buzzer : BOOL;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..12] OF SINT; (*Internal memory*)
		b250msFALSE : BOOL;
		b250msTRUE : BOOL;
		TON1 : TON;
		bKule_KirmiziSurekliYan : BOOL;
		bKule_KirmiziKesikliYan : BOOL;
		bKule_SariSurekliYan : BOOL;
		bKule_SariKesikliYan : BOOL;
		bKule_YesilSurekliYan : BOOL;
		bKule_YesilKesikliYan : BOOL;
		TON2 : TON;
		bKule_BuzzerKesikliYan : BOOL;
		bBuzzerSus : BOOL;
		bKule_BuzzerSurekliYan : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK ButtonBox_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iHucre_GSOK : BOOL;
		iHucre_ASOK : BOOL;
		iSysOut_AutoON : BOOL;
		iSysOut_CycleON : BOOL;
		iSysOut_PPMoved : BOOL;
		iOP_StartAtMainBtn : BOOL;
		iOP_GirisIzinBtn : BOOL;
		iOP_ResetBtn : BOOL;
		iOP_StopBtn : BOOL;
	END_VAR
	VAR_OUTPUT
		qSysIn_PPtoMain : BOOL;
		qSysIn_MotorOnStart : BOOL;
		qSysIn_ExeErrReset : BOOL;
		qSysIn_EmgErrReset : BOOL;
		qOP_StartLmb : BOOL;
		qOP_GirisIzinLmb : BOOL;
		qOP_ResetLmb : BOOL;
		qGirisIstegiVar : BOOL;
		qSysIn_Stop : BOOL;
		qOP_StopLmb : BOOL;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		TON1 : TON;
		b250msTRUE : BOOL;
		b250msFALSE : BOOL;
		bGirisIzinIstekVar : BOOL;
		bOP_GirisIzinSurekiYan : BOOL;
		bOP_GirisIzinKesikliYan : BOOL;
		bOP_ResetKesikliYan : BOOL;
		bOP_ResetSurekliYan : BOOL;
		bOP_StartKesikliYan : BOOL;
		bOP_StartSurekiYan : BOOL;
		bOP_StartAtMainKesikliYan : BOOL;
		bOP_StartAtMainSurekiYan : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Suction_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSuction : {REDUND_UNREPLICABLE} BOOL;
		iSuctionOk : {REDUND_UNREPLICABLE} BOOL;
		iBlow : BOOL;
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iSystemManual : {REDUND_UNREPLICABLE} BOOL;
		iManualSuction : BOOL;
		iManualBlow : BOOL;
	END_VAR
	VAR_OUTPUT
		qSuction : BOOL;
		qBlow : BOOL;
		qSuctionOk : BOOL;
		qSuctionFault : BOOL;
	END_VAR
	VAR
		tSuction : BOOL;
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		tBlow : BOOL;
		StabilityTime : TON;
		FaultTime : TON;
		tSuctionOk : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Compensator_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iCloseSignal : BOOL;
		iOpenSignal : BOOL;
		iAutoClose : {REDUND_UNREPLICABLE} BOOL;
		iAutoOpen : {REDUND_UNREPLICABLE} BOOL;
		iManualClose : {REDUND_UNREPLICABLE} BOOL;
		iManualOpen : {REDUND_UNREPLICABLE} BOOL;
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iSystemManual : BOOL;
		iAirOk : BOOL;
	END_VAR
	VAR_OUTPUT
		qClose : BOOL;
		qOpen : BOOL;
		qOpenFault : BOOL;
		qCloseFault : BOOL;
	END_VAR
	VAR CONSTANT
		AlwaysTRUE : BOOL := TRUE;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		Compensator : Pstn_5_3;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SuctionForFoldingSpace_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSuctionCupUpSignal : {REDUND_UNREPLICABLE} BOOL;
		iSuctionCupDownSignal : {REDUND_UNREPLICABLE} BOOL;
		iSuctionOk : {REDUND_UNREPLICABLE} BOOL;
		iSuctionCupUp : BOOL;
		iSuctionCupDown : BOOL;
		iSuction : BOOL;
		iBlow : BOOL;
		iSystemAuto : BOOL;
		iSystemManual : BOOL;
		iManSuctionCupUp : BOOL;
		iManSuctionCupDown : BOOL;
		iManSuction : BOOL;
		iManBlow : {REDUND_UNREPLICABLE} BOOL;
		iSafetyOk : BOOL;
		iAirOk : BOOL;
	END_VAR
	VAR_OUTPUT
		qSuctionCupUp : BOOL;
		qSuctionCupDown : BOOL;
		qSuction : BOOL;
		qBlow : BOOL;
		qSuctionOk : BOOL;
		qSuctionCupUpFault : BOOL;
		qSuctionCupDownFault : BOOL;
		qSuctionFault : BOOL;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		Suction_FB_For_Table : Suction_FB;
		Pstn_5_3_For_Table : Pstn_5_3;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Conveyor_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iAct : {REDUND_UNREPLICABLE} BOOL;
		iLB : {REDUND_UNREPLICABLE} BOOL;
		iSysAuto : {REDUND_UNREPLICABLE} BOOL;
		iSysMan : {REDUND_UNREPLICABLE} BOOL;
		iDriveFault : {REDUND_UNREPLICABLE} BOOL;
		iDriveFuse : {REDUND_UNREPLICABLE} BOOL;
		iLoadSns : {REDUND_UNREPLICABLE} BOOL;
		iUnloadSns : {REDUND_UNREPLICABLE} BOOL;
		iStart : {REDUND_UNREPLICABLE} BOOL;
		iDirection : {REDUND_UNREPLICABLE} BOOL;
		iManStart : {REDUND_UNREPLICABLE} BOOL;
		newParam13 : {REDUND_UNREPLICABLE} BOOL;
		newParam14 : {REDUND_UNREPLICABLE} BOOL;
		newParam15 : {REDUND_UNREPLICABLE} BOOL;
		newParam16 : {REDUND_UNREPLICABLE} BOOL;
		newParam17 : {REDUND_UNREPLICABLE} BOOL;
		newParam18 : {REDUND_UNREPLICABLE} BOOL;
		newParam19 : {REDUND_UNREPLICABLE} BOOL;
		newParam20 : {REDUND_UNREPLICABLE} BOOL;
		newParam21 : {REDUND_UNREPLICABLE} BOOL;
		newParam22 : {REDUND_UNREPLICABLE} BOOL;
		newParam23 : {REDUND_UNREPLICABLE} BOOL;
		newParam24 : {REDUND_UNREPLICABLE} BOOL;
		newParam25 : {REDUND_UNREPLICABLE} BOOL;
		newParam26 : {REDUND_UNREPLICABLE} BOOL;
		newParam27 : {REDUND_UNREPLICABLE} BOOL;
		newParam28 : {REDUND_UNREPLICABLE} BOOL;
		newParam29 : {REDUND_UNREPLICABLE} BOOL;
		newParam30 : {REDUND_UNREPLICABLE} BOOL;
		newParam31 : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qStart : BOOL;
		qDirection : BOOL;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SW_Cell2_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSWClampAutoClose : {REDUND_UNREPLICABLE} BOOL;
		iSWClampManClose : BOOL;
		iSWStop : BOOL;
		iSWRealClampCloseSignal : {REDUND_UNREPLICABLE} BOOL;
		iSWRealClampOpenSignal : {REDUND_UNREPLICABLE} BOOL;
		iSWVirtualClampCloseSignal : BOOL;
		iSWPrgSignal1 : {REDUND_UNREPLICABLE} BOOL;
		iSWPrgSignal2 : {REDUND_UNREPLICABLE} BOOL;
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iSystemManual : {REDUND_UNREPLICABLE} BOOL;
		iAirOk : {REDUND_UNREPLICABLE} BOOL;
		iSafetyOk : BOOL;
		iSWThreadBreakageError : {REDUND_UNREPLICABLE} BOOL;
		iSWStopError : {REDUND_UNREPLICABLE} BOOL;
		iSWHome : BOOL;
		iWZSignal : BOOL;
		iRobotSuctionOk : BOOL;
		iPrgNoFromRobot : USINT;
	END_VAR
	VAR_OUTPUT
		qSWStart : BOOL;
		qSWStop : BOOL;
		qSWRealClampClose : BOOL;
		qSWVirtualClampClose : BOOL;
		qSWRealClampCloseFault : BOOL;
		qSWRealClampOpenFault : BOOL;
		qSWVirtualClampCloseFault : BOOL;
		qSWPrgSignal1 : BOOL;
		qSWPrgSignal2 : BOOL;
		qRobotPrgSignal1 : BOOL;
		qRobotPrgSignal2 : BOOL;
		qPrgNoBit1 : BOOL;
		qPrgNoBit2 : BOOL;
		qPrgNoBit3 : BOOL;
		qPrgNoBit4 : BOOL;
		qAlarmNo : USINT;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..54] OF SINT; (*Internal memory*)
		tSWStart : BOOL;
		tClampClose : BOOL;
		tStepNo : USINT;
		MOVE_EN_USINT_0 : MOVE_EN_USINT;
		SPLIT_Byte_0 : SPLIT_Byte;
		TON_0 : TON;
		TON_1 : TON;
		TON_2 : TON;
		TP_0 : TP;
		TP_1 : TP;
		tConditions : BOOL;
		tPulseTime : TIME;
		tSWRealClampClose : BOOL;
		tSWVirtualClampClose : BOOL;
		tSWVirtualClampCloseSignal : BOOL;
		MOVE_EN_USINT_10 : MOVE_EN_USINT;
		MOVE_EN_USINT_20 : MOVE_EN_USINT;
		MOVE_EN_USINT_30 : MOVE_EN_USINT;
		MOVE_EN_USINT_40 : MOVE_EN_USINT;
		MOVE_EN_USINT_50 : MOVE_EN_USINT;
		MOVE_EN_USINT_60 : MOVE_EN_USINT;
		MOVE_EN_USINT_70 : MOVE_EN_USINT;
		MOVE_EN_USINT_80 : MOVE_EN_USINT;
		MOVE_EN_USINT_90 : MOVE_EN_USINT;
		MOVE_EN_USINT_100 : MOVE_EN_USINT;
		oldStepNo : USINT;
		TON_Error : TON;
		TimeError : BOOL;
		SW_Cell2_Error_0 : SW_Cell2_Error;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SW_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSWClampAutoClose : {REDUND_UNREPLICABLE} BOOL;
		iSWClampManClose : BOOL;
		iSWStop : BOOL;
		iSWRealClampCloseSignal : BOOL;
		iSWRealClampOpenSignal : BOOL;
		iSWVirtualClampCloseSignal : BOOL;
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iSystemManual : {REDUND_UNREPLICABLE} BOOL;
		iAirOk : {REDUND_UNREPLICABLE} BOOL;
		iSafetyOk : BOOL;
		iSWThreadBreakageError : {REDUND_UNREPLICABLE} BOOL;
		iSWStopError : {REDUND_UNREPLICABLE} BOOL;
		iSWHome : BOOL;
		iWZSignal : BOOL;
		iRobotSuctionOk : BOOL;
		iPrgNoFromRobot : USINT;
	END_VAR
	VAR_OUTPUT
		qSWStart : BOOL;
		qSWStop : BOOL;
		qSWRealClampClose : BOOL;
		qSWVirtualClampClose : BOOL;
		qSWRealClampCloseFault : BOOL;
		qSWRealClampOpenFault : BOOL;
		qSWVirtualClampCloseFault : BOOL;
		qRobotSWHome : BOOL;
		qRobotSWClampOpen : BOOL;
		qPrgNoBit1 : BOOL;
		qPrgNoBit2 : BOOL;
		qPrgNoBit3 : BOOL;
		qPrgNoBit4 : BOOL;
		qAlarmNo : USINT;
	END_VAR
	VAR
		tSWStart : BOOL;
		tStepNo : USINT;
		SPLIT_Byte_0 : SPLIT_Byte;
		TON_0 : TON;
		TON_1 : TON;
		TON_2 : TON;
		TP_0 : TP;
		TP_1 : TP;
		tConditions : BOOL;
		zzInternalMemory : ARRAY[0..39] OF SINT; (*Internal memory*)
		tPulseTime : TIME;
		tSWRealClampClose : BOOL;
		tSWVirtualClampClose : BOOL;
		tSWVirtualClampCloseSignal : BOOL;
		MOVE_EN_USINT_10 : MOVE_EN_USINT;
		MOVE_EN_USINT_20 : MOVE_EN_USINT;
		MOVE_EN_USINT_30 : MOVE_EN_USINT;
		MOVE_EN_USINT_40 : MOVE_EN_USINT;
		MOVE_EN_USINT_50 : MOVE_EN_USINT;
		MOVE_EN_USINT_60 : MOVE_EN_USINT;
		MOVE_EN_USINT_70 : MOVE_EN_USINT;
		MOVE_EN_USINT_80 : MOVE_EN_USINT;
		MOVE_EN_USINT_0 : MOVE_EN_USINT;
		oldStepNo : USINT;
		TON_Error : TON;
		TimeError : BOOL;
		SW_Error_0 : SW_Error;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Transfer_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$CATICON=Icon_MpClamp.png*)
	VAR_INPUT
		iMecLinearBwdSns : {REDUND_UNREPLICABLE} BOOL;
		iMecLinearFwdSns : {REDUND_UNREPLICABLE} BOOL;
		iMecTurnC2DownSns : {REDUND_UNREPLICABLE} BOOL;
		iMecTurnC2UpSns : {REDUND_UNREPLICABLE} BOOL;
		iMecTurnC3DownSns : {REDUND_UNREPLICABLE} BOOL;
		iMecTurnC3UpSns : {REDUND_UNREPLICABLE} BOOL;
		iMecPickDownSns : {REDUND_UNREPLICABLE} BOOL;
		iMecPickUpSns : {REDUND_UNREPLICABLE} BOOL;
		iMecFoldingOpenSns : {REDUND_UNREPLICABLE} BOOL;
		iMecFoldingCloseSns : {REDUND_UNREPLICABLE} BOOL;
		iMecSuctionOk : {REDUND_UNREPLICABLE} BOOL;
		iToolchangerLock : BOOL;
		iR2WZ : BOOL;
		iR3WZ : BOOL;
		iCell2CamOk : BOOL;
		iSystemAuto : BOOL;
		iSystemManual : {REDUND_UNREPLICABLE} BOOL;
		iManMecLinearBwd : BOOL;
		iManMecLinearFwd : BOOL;
		iManMecTurnBwd : BOOL;
		iManMecTurnFwd : BOOL;
		iManPickValve : BOOL;
		iManFoldingValve : BOOL;
		iManSuction1 : BOOL;
		iManSuction2 : BOOL;
		iAirOk : BOOL;
		iSafetyOk : BOOL;
	END_VAR
	VAR_OUTPUT
		qMecLinearBwd : BOOL;
		qMecLinearFwd : BOOL;
		qMecTurnBwd : BOOL;
		qMecTurnFwd : BOOL;
		qMecPickValve : BOOL;
		qMecFoldingValve : BOOL;
		qSuction1 : BOOL;
		qSuction2 : BOOL;
		qBusy : BOOL;
		qEvacuationFromMecForR3 : BOOL;
		qMecLinearBwdFault : BOOL;
		qMecLinearFwdFault : BOOL;
		qMecTurnBwdFault : BOOL;
		qMecTurnFwdFault : BOOL;
		qMecPickDownFault : BOOL;
		qMecPickUpFault : BOOL;
		qMecFoldingCloseFault : BOOL;
		qMecFoldingOpenFault : BOOL;
		qSuction1Fault : BOOL;
		qAlarmNo : USINT;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..59] OF SINT; (*Internal memory*)
		tConditions : BOOL;
		tStepNo : USINT;
		oldStepNo : USINT;
		tBusy : BOOL;
		tMecLinearBwd : BOOL;
		tMecLinearFwd : BOOL;
		tMecTurnBwd : BOOL;
		tMecTurnFwd : BOOL;
		tMecPickValve : BOOL;
		tSuction1 : BOOL;
		tSuction2 : BOOL;
		tMecFoldingValve : BOOL;
		tMecSuctionOk : BOOL;
		TimeError : BOOL;
		MecTurn_Pstn_5_2 : Pstn_5_2;
		MecLinear_Pstn_5_2 : Pstn_5_2;
		TON_Error : TON;
		TON_0 : TON;
		TON_1 : TON;
		TON_2 : TON;
		TON_3 : TON;
		TON_4 : TON;
		MOVE_EN_USINT_10 : MOVE_EN_USINT;
		MOVE_EN_USINT_20 : MOVE_EN_USINT;
		MOVE_EN_USINT_30 : MOVE_EN_USINT;
		MOVE_EN_USINT_40 : MOVE_EN_USINT;
		MOVE_EN_USINT_50 : MOVE_EN_USINT;
		MOVE_EN_USINT_60 : MOVE_EN_USINT;
		MOVE_EN_USINT_70 : MOVE_EN_USINT;
		MOVE_EN_USINT_80 : MOVE_EN_USINT;
		MOVE_EN_USINT_90 : MOVE_EN_USINT;
		MOVE_EN_USINT_100 : MOVE_EN_USINT;
		MOVE_EN_USINT_110 : MOVE_EN_USINT;
		MOVE_EN_USINT_120 : MOVE_EN_USINT;
		MOVE_EN_USINT_0 : MOVE_EN_USINT;
		Suction_FB_0 : Suction_FB;
		Suction_FB_1 : Suction_FB;
		Transfer_Error_0 : Transfer_Error;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SW_Error (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		SWClampAutoClose : BOOL;
		SWRealClampOpenSignal : BOOL;
		SWRealClampCloseSignal : BOOL;
		SWVirtualClampCloseSignal : BOOL;
		SystemAuto : {REDUND_UNREPLICABLE} BOOL;
		AirOk : {REDUND_UNREPLICABLE} BOOL;
		SWThreadBreakageError : {REDUND_UNREPLICABLE} BOOL;
		SWStopError : {REDUND_UNREPLICABLE} BOOL;
		SafetyOk : BOOL;
		SWHome : BOOL;
		WZSignal : BOOL;
		RobotSuctionOk : BOOL;
		DelayTime : BOOL;
		StepNo : USINT;
	END_VAR
	VAR_OUTPUT
		AlarmNo : USINT;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SW_Cell2_Error (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=Icon_MpAlarm.png*)
	VAR_INPUT
		SWClampAutoClose : {REDUND_UNREPLICABLE} BOOL;
		SWRealClampCloseSignal : {REDUND_UNREPLICABLE} BOOL;
		SWRealClampOpenSignal : {REDUND_UNREPLICABLE} BOOL;
		SWVirtualClampCloseSignal : BOOL;
		SWPrgSignal1 : {REDUND_UNREPLICABLE} BOOL;
		SWPrgSignal2 : {REDUND_UNREPLICABLE} BOOL;
		SystemAuto : {REDUND_UNREPLICABLE} BOOL;
		AirOk : {REDUND_UNREPLICABLE} BOOL;
		SWThreadBreakageError : {REDUND_UNREPLICABLE} BOOL;
		SWStopError : {REDUND_UNREPLICABLE} BOOL;
		SafetyOk : BOOL;
		SWHome : BOOL;
		WZSignal : BOOL;
		DelayTime : BOOL;
		StepNo : USINT;
	END_VAR
	VAR_OUTPUT
		AlarmNo : USINT;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Transfer_Error (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=Icon_MpAlarm.png,$CATICON=Icon_MpAlarm.png*)
	VAR_INPUT
		MecLinearBwdSns : {REDUND_UNREPLICABLE} BOOL;
		MecLinearFwdSns : {REDUND_UNREPLICABLE} BOOL;
		MecTurnC2DownSns : {REDUND_UNREPLICABLE} BOOL;
		MecTurnC2UpSns : {REDUND_UNREPLICABLE} BOOL;
		MecTurnC3DownSns : {REDUND_UNREPLICABLE} BOOL;
		MecTurnC3UpSns : {REDUND_UNREPLICABLE} BOOL;
		MecPickDownSns : {REDUND_UNREPLICABLE} BOOL;
		MecPickUpSns : {REDUND_UNREPLICABLE} BOOL;
		MecFoldingOpenSns : {REDUND_UNREPLICABLE} BOOL;
		MecFoldingCloseSns : {REDUND_UNREPLICABLE} BOOL;
		MecSuctionOk : {REDUND_UNREPLICABLE} BOOL;
		ToolchangerLock : BOOL;
		R2WZ : BOOL;
		R3WZ : BOOL;
		SystemAuto : BOOL;
		AirOk : BOOL;
		SafetyOk : BOOL;
		DelayTime : BOOL;
		StepNo : USINT;
	END_VAR
	VAR_OUTPUT
		AlarmNo : USINT;
	END_VAR
END_FUNCTION_BLOCK
