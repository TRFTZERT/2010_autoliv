
(* TODO: Add your comment here *)
(* --------------------HATA KODLARI------------------
	1=MANUEL VE OTOMATIK BILGILERININ AYNI ANDA GELMESI ARIZASI
	2=MANUELDE ILERI VE GERI KONUTLARININ AYNI ANDA GELMESI ARIZASI
	3=ILERIDE VE GERIDE SENS�RLERININ AYNI ANDA GELMESI ARIZASI
	4= INTERLOCK AKTIF
	5= lSIK BARIYERI KONTROLU AKTIF
	6= GENEL SENSOR HATASI (ALARMA BAKINIZ)
	11= V1 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	12= V2 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	13= V3 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	14= V4 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	15= V5 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	16= V6 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	17= V7 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	18= V8 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	19= V9 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	20= V10 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	21= V11 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	22= V12 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	23= V13 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	24= V14 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	25= V15 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	26= V16 ILERI VE GERI SENS�RLERIN AYNI ANDA G�RMESI ARIZASI
	
	(2001 CANEL VALVE CONTROL)

*)
FUNCTION_BLOCK ValveControlFB
	//INIT
	//-----------------------------------------------------------------------------------------
	sTimerS(IN := sTimerS_IN,
		PT := sTimerS_PT);
	
	sTimerR(IN := sTimerR_IN,
		PT := sTimerR_PT);

	sTimerTrg(IN := sTimerTrg_IN,
		PT := sTimerTrg_PT);
	
	
	// NETWORK 1
	//-----------------------------------------------------------------------------------------
	IF ((iSystemStep <= 21) AND (iSystemStep >= 90))
		OR iBypass
		THEN
		tProcess := TRUE;
	ELSE
		tProcess := FALSE;
	END_IF;
	
	// NETWORK 2 Otomatik set hafizasi
	//-----------------------------------------------------------------------------------------
	IF iSysAut AND iAuto_Set
		THEN
		sAutoSetMemory := TRUE;
	END_IF;
	
	IF (iAuto_Rst
		OR iAuto_Mid
		OR iSep_Rst)
		AND 
		iSysAut
		THEN
		sAutoSetMemory := FALSE;
	END_IF;
	
	IF sAutoSetMemory AND iLightBeam
		THEN
		sAutoSetMemoryWork := TRUE;
	ELSE
		sAutoSetMemoryWork := FALSE;
	END_IF;
	
	// NETWORK 3 Otomatik reset hafizasi
	//-----------------------------------------------------------------------------------------	
	IF iSysAut AND iAuto_Rst
		THEN
		sAutoRstMemory := TRUE;
	END_IF;
	
	IF (iAuto_Set
		OR iAuto_Mid
		OR iSep_Set
		OR iPartless)
		AND
		iSysAut
		THEN
		sAutoRstMemory := FALSE;
	END_IF;
	
	IF sAutoRstMemory AND iLightBeam
		THEN
		sAutoRstMemoryWork := TRUE;
	ELSE
		sAutoRstMemoryWork := FALSE;
	END_IF;	
	
	// NETWORK 4	
	//-----------------------------------------------------------------------------------------	
	IF iSysAut AND iSysManuel
		THEN
		tFault1 := TRUE;
	ELSE
		tFault1 := FALSE;
	END_IF;
	
	// NETWORK 5  manuel veya otomatikte ileri ve geri bilgileri ayni anda gelior 	
	//-----------------------------------------------------------------------------------------	
	IF (iSysAut OR iSysManuel)
		AND
		(iAuto_Set
		OR iMan_Set
		OR iSep_Set)
		AND
		(iAuto_Rst
		OR iMan_Rst
		OR iSep_Rst)
		AND NOT
		tFault1
		THEN
		tHMIDiag.ErrorCode := 2;
		tFault2 := TRUE;
	ELSE
		tFault2 := FALSE;
	END_IF;	
	
	
	// NETWORK 6  ileri vegeri sens�r inputlari ayni anda g�r�yor
	//-----------------------------------------------------------------------------------------
	ForNT6:= 1;
	FOR ForNT6 := 1 TO 16 DO
		IF NOT tFault2 
			AND NOT	sAct[ForNT6]
			AND iSet_Sns[ForNT6]
			AND iRst_Sns[ForNT6]
			THEN
			tHMIDiag.ErrorCode := CodeNo[ForNT6];
		END_IF;
	END_FOR;
	
	IF (tHMIDiag.ErrorCode >= 11) AND (tHMIDiag.ErrorCode <= 26)
		THEN
		tFault3 := TRUE;
	ELSE
		tFault3 := FALSE;
	END_IF;

	// NETWORK 7	olusan arizalari resetleme
	//-----------------------------------------------------------------------------------------	
	IF NOT tFault1
		AND NOT tFault2
		AND NOT tFault3
		AND NOT sSSensorHata
		THEN
		tHMIDiag.ErrorCode := 0;
		sHataYok := TRUE;
	ELSE
		sHataYok := FALSE;
	END_IF;
	
	// NETWORK 8	Interlock aktif bilgisi
	//-----------------------------------------------------------------------------------------	
	IF (sSet
		OR
		sReset)
		AND NOT
		iInterlock
		THEN
		tHMIDiag.ErrorCode := 4;
		tHMIDiag.InterlockActive := TRUE;
	ELSE
		tHMIDiag.InterlockActive := FALSE;
	END_IF;
	
	// NETWORK 9	Light beam Aktif bilgisi
	//-----------------------------------------------------------------------------------------	
	IF (sSet AND sSetSensOK)
		OR
		(sReset AND sResetSensOK)
		AND NOT
		iLightBeam
		THEN
		tHMIDiag.ErrorCode := 5;
		tHMIDiag.LightBeamActive := TRUE;
	ELSE
		tHMIDiag.LightBeamActive := FALSE;
	END_IF;	

	// NETWORK 10	Gruplarin set sens�r kontrol�	
	//-----------------------------------------------------------------------------------------	
	IF (iSet_Sns[1] OR sAct[1])
		AND
		(iSet_Sns[2] OR sAct[2])
		AND
		(iSet_Sns[3] OR sAct[3])
		AND
		(iSet_Sns[4] OR sAct[4])
		AND
		(iSet_Sns[5] OR sAct[5])
		AND
		(iSet_Sns[6] OR sAct[6])
		AND
		(iSet_Sns[7] OR sAct[7])
		AND
		(iSet_Sns[8] OR sAct[8])
		AND
		(iSet_Sns[9] OR sAct[9])
		AND
		(iSet_Sns[10] OR sAct[10])
		AND
		(iSet_Sns[11] OR sAct[11])
		AND
		(iSet_Sns[12] OR sAct[12])
		AND
		(iSet_Sns[13] OR sAct[13])
		AND
		(iSet_Sns[14] OR sAct[14])
		AND
		(iSet_Sns[15] OR sAct[15])
		AND
		(iSet_Sns[16] OR sAct[16])
		THEN
		sSetSensOK := TRUE;
	ELSE
		sSetSensOK := FALSE;
	END_IF;
	
	// NETWORK 11	Gruplarin set sens�r alarmlari	
	//-----------------------------------------------------------------------------------------		
	IF sValf_ileride AND sSSensorHata THEN
		tHMIDiag.SetFault := TRUE;
		qSetFault := TRUE;
	ELSE
		tHMIDiag.SetFault := FALSE;
		qSetFault := FALSE;
	END_IF;
	
	// Sensor hatasi
	ForNT11 := 1;
	FOR ForNT11 := 1 TO 16 DO
		IF sValf_ileride
			AND sSSensorHata
			AND NOT sAct[ForNT11]
			AND NOT iSet_Sns[ForNT11]
			THEN
			hSFSnsAlarm[ForNT11] := TRUE;
		ELSE
			hSFSnsAlarm[ForNT11] := FALSE;
		END_IF;
	END_FOR;

	// NETWORK 12	Gruplarin reset sens�r kontrol�	
	//-----------------------------------------------------------------------------------------	
	IF (iRst_Sns[1] OR sAct[1])
		AND
		(iRst_Sns[2] OR sAct[2])
		AND
		(iRst_Sns[3] OR sAct[3])
		AND
		(iRst_Sns[4] OR sAct[4])
		AND
		(iRst_Sns[5] OR sAct[5])
		AND
		(iRst_Sns[6] OR sAct[6])
		AND
		(iRst_Sns[7] OR sAct[7])
		AND
		(iRst_Sns[8] OR sAct[8])
		AND
		(iRst_Sns[9] OR sAct[9])
		AND
		(iRst_Sns[10] OR sAct[10])
		AND
		(iRst_Sns[11] OR sAct[11])
		AND
		(iRst_Sns[12] OR sAct[12])
		AND
		(iRst_Sns[13] OR sAct[13])
		AND
		(iRst_Sns[14] OR sAct[14])
		AND
		(iRst_Sns[15] OR sAct[15])
		AND
		(iRst_Sns[16] OR sAct[16])
		THEN
			sResetSensOK := TRUE;
		ELSE
			sResetSensOK := FALSE;
		END_IF;
	
	// NETWORK 13	Gruplarin reset sens�r alarmlari	
	//-----------------------------------------------------------------------------------------		
	IF sValf_Geride AND sRSensorHata THEN
		tHMIDiag.RstFault := TRUE;
		qRstFault := TRUE;
	ELSE
		tHMIDiag.RstFault := FALSE;
		qRstFault := FALSE;
	END_IF;
	
	// Sensor  hatasi
	ForNT13 := 1;
	FOR ForNT13 := 1 TO 16 DO
		IF sValf_Geride
			AND sRSensorHata
		 	AND NOT sAct[ForNT13]
			AND NOT iRst_Sns[ForNT13]
			THEN
			hRFSnsAlarm[ForNT13] := TRUE;
		ELSE
			hRFSnsAlarm[ForNT13] := FALSE;
		END_IF;
	END_FOR;
	
	// NETWORK 14-15-16-17	
	//-----------------------------------------------------------------------------------------	


	// NETWORK 18  ileri hareket
	//-----------------------------------------------------------------------------------------		
	// sSET sinyali 1 (TRUE) yapmak i�in
	//SLine1
	IF iSysAut AND iAuto_Set THEN
		SLine1 := TRUE;
	ELSE
		SLine1 := FALSE;
	END_IF;
	//SLine2
	IF iSysAut AND iSep_Set THEN
		SLine2 := TRUE;
	ELSE
		SLine2 := FALSE;
	END_IF;
	//SLine3
	IF iSysAut AND sAutoSetMemoryWork AND iOtherSide THEN
		SLine3 := TRUE;
	ELSE
		SLine3 := FALSE;
	END_IF;	
	
	IF (SLine1 AND NOT SShot[0])
		OR
		(SLine2 AND NOT SShot[1])
		OR 
		(SLine3 AND NOT SShot[2])
		OR
		(iSysManuel AND iMan_Set)
		OR
		iTest_Set
		OR
		iPartless
		THEN
		sSet := TRUE;
		END_IF;
		SShot[0] := SLine1;
		SShot[1] := SLine2;
		SShot[2] := SLine3;
	
	// sSET sinyali 0 (FALSE) yapmak i�in
	//SLine4
	IF iSysManuel AND iMan_Mid THEN
		SLine4 := TRUE;
	ELSE
		SLine4 := FALSE;
	END_IF;
	//SLine5
	IF iSysAut AND iAuto_Mid THEN
		SLine5 := TRUE;
	ELSE
		SLine5 := FALSE;
	END_IF;	

	IF (sReset AND NOT SShot[3])
		OR
		(SLine4 AND NOT SShot[4])
		OR 
		(SLine5 AND NOT SShot[5])
		THEN
		sSet := FALSE;
	END_IF;
	SShot[3] := sReset;
	SShot[4] := SLine4;
	SShot[5] := SLine5;
	
	IF (iLightBeam OR tProcess)
		AND sSet
		AND iInterlock
		THEN
		qValveSet := TRUE;
		sValf_ileride := TRUE;
		tHMIDiag.Set := TRUE;
	ELSE
		qValveSet := FALSE;
		sValf_ileride := FALSE;
		tHMIDiag.Set := FALSE;
	END_IF;

	// NETWORK 19  Geri Hareket
	//-----------------------------------------------------------------------------------------		
	// sReset sinyali 1 (TRUE) yapmak i�in
	//RLine1
	IF iSysAut AND iAuto_Rst THEN
		RLine1 := TRUE;
	ELSE
		RLine1 := FALSE;
	END_IF;
	//RLine2
	IF iSysAut AND iSep_Rst THEN
		RLine2 := TRUE;
	ELSE
		RLine2 := FALSE;
	END_IF;
	//RLine3
	IF iSysAut AND sAutoRstMemoryWork AND iOtherSide THEN
		RLine3 := TRUE;
	ELSE
		RLine3 := FALSE;
	END_IF;	

	IF (RLine1 AND NOT RShot[0])
		OR
		(RLine2 AND NOT RShot[1])
		OR 
		(RLine3 AND NOT RShot[2])
		OR
		(iSysManuel AND iMan_Rst)
		OR
		iTest_Rst
		THEN
		sReset := TRUE;
	END_IF;
	RShot[0] := RLine1;
	RShot[1] := RLine2;
	RShot[2] := RLine3;

	// sReset sinyali 0 (FALSE) yapmak i�in
	//RLine4
	IF iSysManuel AND iMan_Mid THEN
		RLine4 := TRUE;
	ELSE
		RLine4 := FALSE;
	END_IF;
	//SLine5
	IF iSysAut AND iAuto_Mid THEN
		RLine5 := TRUE;
	ELSE
		RLine5 := FALSE;
	END_IF;	
	
	IF (sSet AND NOT RShot[3])
		OR
		(iPartless AND NOT RShot[6])
		OR
		(RLine4 AND NOT RShot[4])
		OR 
		(RLine5 AND NOT RShot[5])
		THEN
		sReset := FALSE;
	END_IF;
	RShot[3] := sSet;
	RShot[4] := RLine4;
	RShot[5] := RLine5;
	RShot[6] := iPartless;

	IF (iLightBeam OR tProcess)
		AND sReset
		AND iInterlock
		THEN
		qValveRst := TRUE;
		sValf_Geride := TRUE;
		tHMIDiag.Rst := TRUE;
	ELSE
		qValveRst := FALSE;
		sValf_Geride := FALSE;
		tHMIDiag.Rst := FALSE;
	END_IF;

	// NETWORK 20  calisma zamani hatasi  burada kullaniliyor
	//-----------------------------------------------------------------------------------------	
	IF sValf_ileride AND NOT sSetSensOK THEN
		sTimerS_PT	:= T#1s;
		sTimerS_IN	:= TRUE;
	ELSE
		sTimerS_IN	:= FALSE;
	END_IF;

	IF sValf_Geride AND NOT sResetSensOK THEN
		sTimerR_PT	:= T#1s;
		sTimerR_IN	:= TRUE;
	ELSE
		sTimerR_IN	:= FALSE;
	END_IF;

	IF sTimerS.Q THEN
		sSSensorHata 		:= TRUE;
		tHMIDiag.ErrorCode	:= 6;
	ELSE
		sSSensorHata 		:= FALSE;
	END_IF;
	
	IF sTimerR.Q THEN
		sRSensorHata 		:= TRUE;
		tHMIDiag.ErrorCode	:= 6;
	ELSE
		sRSensorHata 		:= FALSE;
	END_IF;

	// NETWORK 21-22  qSetRun qRstRun sinyallerinini olusmasi 
	//-----------------------------------------------------------------------------------------	
	IF sValf_ileride AND sSetSensOK THEN
		qSetRun			:= TRUE;
		tHMIDiag.SetRun	:= TRUE;
	ELSE
		qSetRun			:= FALSE;
		tHMIDiag.SetRun	:= FALSE;
	END_IF;
	
	IF sValf_Geride AND sResetSensOK THEN
		qRstRun			:= TRUE;
		tHMIDiag.RstRun	:= TRUE;
	ELSE
		qRstRun			:= FALSE;
		tHMIDiag.RstRun	:= FALSE;
	END_IF;

	// NETWORK 23  Ariza �ikisi 
	//-----------------------------------------------------------------------------------------	
	IF tFault1
		OR tFault2
		OR tFault3
		OR sSSensorHata
		OR sRSensorHata
		THEN
		qError			:= TRUE;
		tHMIDiag.Error	:= TRUE;
	ELSE
		qError			:= FALSE;
		tHMIDiag.Error	:= FALSE;
	END_IF;

	// NETWORK 24  Valve Init pos 
	//-----------------------------------------------------------------------------------------	
	IF NOT sValf_ileride AND NOT sValf_Geride THEN
		qValveInitPos		:= TRUE;
		tHMIDiag.InitPos	:= TRUE;
	ELSE
		qValveInitPos		:= FALSE;
		tHMIDiag.InitPos	:= FALSE;
	END_IF;
	
	// NETWORK 25  Aktivasyon bilgisi
	//-----------------------------------------------------------------------------------------	
	IF NOT sAct[1]
		OR NOT sAct[2]
		OR NOT sAct[3]
		OR NOT sAct[4]
		OR NOT sAct[5]
		OR NOT sAct[6]
		OR NOT sAct[7]
		OR NOT sAct[8]
		OR NOT sAct[9]
		OR NOT sAct[10]
		OR NOT sAct[11]
		OR NOT sAct[12]
		OR NOT sAct[13]
		OR NOT sAct[14]
		OR NOT sAct[15]
		OR NOT sAct[16]
		THEN
		tActSns			:= TRUE;
		tHMIDiag.Act 	:= TRUE;
	ELSE
		tActSns			:= FALSE;
		tHMIDiag.Act 	:= FALSE;
	END_IF;

	// NETWORK 26  Klemp sens�r durumlari panele yollanir
	//-----------------------------------------------------------------------------------------
	IF tActSns THEN
		ForNT26 := 1;
		FOR ForNT26 := 1 TO 16 DO
			IF NOT sAct[ForNT26]
				THEN
				// Valve mid the set sensor is TRUE 
				IF qValveInitPos AND iSet_Sns[ForNT26]
					THEN
					sSnsState[ForNT26] := 1;   //image  "setok"
                
					// Valve mid the reset sensor is TRUE 
				ELSIF qValveInitPos AND iRst_Sns[ForNT26]
					THEN
					sSnsState[ForNT26] := 3;   //image  "resetok"
            
					// Valve set but wait the sensor 
				ELSIF sValf_ileride AND NOT iSet_Sns[ForNT26]
					THEN
					sSnsState[ForNT26] := 2;   //image  "setnok"
                
					// Valve set the sensor is TRUE
				ELSIF sValf_ileride AND iSet_Sns[ForNT26]
					THEN
					sSnsState[ForNT26] := 1;   //image  "setok"
                
					// Valve reset but wait the sensor    
				ELSIF sValf_Geride AND NOT iRst_Sns[ForNT26]
					THEN
					sSnsState[ForNT26] := 4;   //image  "resetnok"
                
					// Valve reset the sensor is TRUE
				ELSIF sValf_Geride AND iRst_Sns[ForNT26]
					THEN
					sSnsState[ForNT26] := 3;   //image  "resetok"
                
				ELSE
					sSnsState[ForNT26] := 0;   //image  "dubleempty"
				END_IF;
			END_IF;
		END_FOR;
	END_IF;

	// NETWORK 27  Ileride alarm
	//-----------------------------------------------------------------------------------------

	IF sValf_Geride THEN
		IF hSFSnsAlarm[1] THEN
			hAlarm := 1;
		ELSIF hSFSnsAlarm[2] THEN
			hAlarm := 2;
		ELSIF hSFSnsAlarm[3] THEN
			hAlarm := 3;
		ELSIF hSFSnsAlarm[4] THEN
			hAlarm := 4;
		ELSIF hSFSnsAlarm[5] THEN
			hAlarm := 5;
		ELSIF hSFSnsAlarm[6] THEN
			hAlarm := 6;
		ELSIF hSFSnsAlarm[7] THEN
			hAlarm := 7;
		ELSIF hSFSnsAlarm[8] THEN
			hAlarm := 8;
		ELSIF  hSFSnsAlarm[9] THEN
			hAlarm := 9;
		ELSIF  hSFSnsAlarm[10] THEN
			hAlarm := 10;
		ELSIF  hSFSnsAlarm[11] THEN
			hAlarm := 11;
		ELSIF  hSFSnsAlarm[12] THEN
			hAlarm := 12;
		ELSIF  hSFSnsAlarm[13] THEN
			hAlarm := 13;
		ELSIF  hSFSnsAlarm[14] THEN
			hAlarm := 14;
		ELSIF  hSFSnsAlarm[15] THEN
			hAlarm := 15;
		ELSIF  hSFSnsAlarm[16] THEN
			hAlarm := 16;
		END_IF;
	END_IF;

	// NETWORK 28  Geride alarm
	//-----------------------------------------------------------------------------------------

	IF sValf_Geride THEN
		IF hRFSnsAlarm[1] THEN
			hAlarm := 21;
		ELSIF hRFSnsAlarm[2] THEN
			hAlarm := 22;
		ELSIF hRFSnsAlarm[3] THEN
			hAlarm := 23;
		ELSIF hRFSnsAlarm[4] THEN
			hAlarm := 24;
		ELSIF hRFSnsAlarm[5] THEN
			hAlarm := 25;
		ELSIF hRFSnsAlarm[6] THEN
			hAlarm := 26;
		ELSIF hRFSnsAlarm[7] THEN
			hAlarm := 27;
		ELSIF hRFSnsAlarm[8] THEN
			hAlarm := 28;
		ELSIF hRFSnsAlarm[9] THEN
			hAlarm := 29;
		ELSIF hRFSnsAlarm[10] THEN
			hAlarm := 30;
		ELSIF hRFSnsAlarm[11] THEN
			hAlarm := 31;
		ELSIF hRFSnsAlarm[12] THEN
			hAlarm := 32;
		ELSIF hRFSnsAlarm[13] THEN
			hAlarm := 33;
		ELSIF hRFSnsAlarm[14] THEN
			hAlarm := 34;
		ELSIF hRFSnsAlarm[15] THEN
			hAlarm := 35;
		ELSIF hRFSnsAlarm[16] THEN
			hAlarm := 36;
		END_IF;
	END_IF;

	// NETWORK 29  Alarm  trigger olusturulur
	//-----------------------------------------------------------------------------------------
	IF hAlarm <> sOld THEN
		sTrig := FALSE;
	ELSE
		sTrig := TRUE;
	END_IF;
	sOld := hAlarm;

	// NETWORK 30
	//-----------------------------------------------------------------------------------------
	IF (tHMIDiag.SetFault OR tHMIDiag.RstFault)
		AND sTrig
		THEN
		sTimerTrg_PT := T#100ms;
		sTimerTrg_IN := TRUE;
	ELSE
		sTimerTrg_IN := FALSE;
	END_IF;

	IF sTimerTrg.Q THEN
		htrig := TRUE;
	ELSE
		htrig := FALSE;
	END_IF;

	// NETWORK 31
	//-----------------------------------------------------------------------------------------
	hDiag := tHMIDiag;
	hSnsState := sSnsState;

	

	
END_FUNCTION_BLOCK
