
(* TODO: Add your comment here *)
FUNCTION_BLOCK VACUMCNTRL	
	IF iVacumOnOff THEN
		qVacum:=TRUE;
	ELSE;
		qVacum:=FALSE;
	END_IF;
	IF iBlowOnOff THEN
		qBlow :=TRUE;
	ELSE;
		qBlow :=FALSE;
	END_IF;
	IF iVacumOnOff AND NOT iVacumOk THEN
		qVacumOk:=FALSE;		
		qVacumFault:=TRUE;
	ELSE;
    	qVacumFault:=FALSE;
		qVacumOk:=FALSE;
	END_IF;
END_FUNCTION_BLOCK
