
(* TODO: Add your comment here *)
FUNCTION_BLOCK SW_Error
	
	IF NOT AirOk THEN
		AlarmNo := 251;
	ELSIF NOT SystemAuto THEN
		AlarmNo := 252;
	ELSIF NOT SWThreadBreakageError THEN
		AlarmNo := 253;
	ELSIF NOT SWStopError THEN
		AlarmNo := 254;
	ELSIF NOT SafetyOk THEN
		AlarmNo := 255;
	ELSE
		AlarmNo := 0;
	END_IF;
	
	// Adim 0 hatalari
	IF StepNo = 0 AND DelayTime AND SWClampAutoClose THEN
		IF NOT SWHome THEN
			AlarmNo := 1;
		ELSIF NOT WZSignal THEN
			AlarmNo := 2;
		ELSIF NOT SWRealClampOpenSignal THEN
			AlarmNo := 3;
		ELSIF SWVirtualClampCloseSignal THEN
			AlarmNo := 4;
		END_IF;
	END_IF;

	// Adim 10 hatalari
	IF StepNo = 10 AND DelayTime AND SWClampAutoClose THEN
		IF NOT SWHome THEN
			AlarmNo := 11;
		ELSIF WZSignal THEN
			AlarmNo := 12;
		ELSIF NOT SWRealClampCloseSignal THEN
			AlarmNo := 13;
		ELSIF NOT SWVirtualClampCloseSignal THEN
			AlarmNo := 14;
		END_IF;
	END_IF;

	// Adim 20 hatalari
	IF StepNo = 20 AND DelayTime AND SWClampAutoClose THEN
		IF SWHome THEN
			AlarmNo := 21;
		ELSIF WZSignal THEN
			AlarmNo := 22;
		ELSIF NOT SWRealClampCloseSignal THEN
			AlarmNo := 23;
		ELSIF NOT SWVirtualClampCloseSignal THEN
			AlarmNo := 24;
		END_IF;
	END_IF;

	// Adim 30 hatalari
	IF StepNo = 30 AND DelayTime AND SWClampAutoClose THEN
		IF NOT SWHome THEN
			AlarmNo := 31;
		ELSIF WZSignal THEN
			AlarmNo := 32;
		ELSIF NOT SWRealClampCloseSignal THEN
			AlarmNo := 33;
		ELSIF NOT SWVirtualClampCloseSignal THEN
			AlarmNo := 34;
		END_IF;
	END_IF;

	// Adim 40 hatalari
	IF StepNo = 40 AND DelayTime AND SWClampAutoClose THEN
		IF NOT SWHome THEN
			AlarmNo := 41;
		ELSIF NOT WZSignal THEN
			AlarmNo := 42;
		ELSIF NOT SWRealClampCloseSignal THEN
			AlarmNo := 43;
		ELSIF NOT SWVirtualClampCloseSignal THEN
			AlarmNo := 44;
		ELSIF NOT RobotSuctionOk THEN
			AlarmNo := 45;
		END_IF;
	END_IF;

	// Adim 50 hatalari
	IF StepNo = 50 AND DelayTime AND NOT SWClampAutoClose THEN
		IF NOT SWHome THEN
			AlarmNo := 51;
		ELSIF NOT WZSignal THEN
			AlarmNo := 52;
		ELSIF NOT SWRealClampOpenSignal THEN
			AlarmNo := 53;
		ELSIF SWVirtualClampCloseSignal THEN
			AlarmNo := 54;
		ELSIF NOT RobotSuctionOk THEN
			AlarmNo := 55;
		END_IF;
	END_IF;

	// Adim 60 hatalari
	IF StepNo = 60 AND DelayTime AND NOT SWClampAutoClose THEN
		IF NOT SWHome THEN
			AlarmNo := 61;
		ELSIF WZSignal THEN
			AlarmNo := 62;
		ELSIF NOT SWRealClampOpenSignal THEN
			AlarmNo := 63;
		ELSIF SWVirtualClampCloseSignal THEN
			AlarmNo := 64;
		END_IF;
	END_IF;
END_FUNCTION_BLOCK
		