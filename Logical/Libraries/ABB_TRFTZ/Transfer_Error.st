
(* TODO: Add your comment here *)
FUNCTION_BLOCK Transfer_Error
	
	IF NOT AirOk THEN
		AlarmNo := 251;
	ELSIF NOT SystemAuto THEN
		AlarmNo := 252;
	ELSIF NOT ToolchangerLock THEN
		AlarmNo := 253;
	ELSIF NOT SafetyOk THEN
		AlarmNo := 254;
	ELSE
		AlarmNo := 0;
	END_IF;
	
	// Adim 0 hatalari
	IF StepNo = 0 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 1;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 2;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 3;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 4;
		ELSIF NOT MecTurnC3DownSns THEN // IN8
			AlarmNo := 5;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 6;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 7;
		//ELSIF TRUE THEN // IN11
		//	AlarmNo := 8;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 9;
		END_IF;
	END_IF;
	
	// Adim 10 hatalari
	IF StepNo = 10 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 11;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 12;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 13;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 14;
		ELSIF NOT MecTurnC3UpSns THEN // IN8
			AlarmNo := 15;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 16;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 17;
		//ELSIF TRUE THEN // IN11
		//	AlarmNo := 18;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 19;
		END_IF;
	END_IF;	
	
	// Adim 20 hatalari
	IF StepNo = 20 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 21;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 22;
		ELSIF NOT MecLinearFwdSns THEN // IN6
			AlarmNo := 23;
		ELSIF NOT MecTurnC2UpSns THEN // IN7
			AlarmNo := 24;
		//ELSIF TRUE THEN // IN8
		//	AlarmNo := 25;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 26;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 27;
		//ELSIF TRUE THEN // IN11
		//	AlarmNo := 28;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 29;
		END_IF;
	END_IF;		
	
	// Adim 30 hatalari
	IF StepNo = 30 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 31;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 32;
		ELSIF NOT MecLinearFwdSns THEN // IN6
			AlarmNo := 33;
		ELSIF NOT MecTurnC2UpSns THEN // IN7
			AlarmNo := 34;
		//ELSIF TRUE THEN // IN8
		//	AlarmNo := 35;
		ELSIF NOT MecPickDownSns THEN // IN9
			AlarmNo := 36;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 37;
		//ELSIF TRUE THEN // IN11
		//	AlarmNo := 38;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 39;
		END_IF;
	END_IF;	

	// Adim 40 hatalari
	IF StepNo = 40 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 41;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 42;
		ELSIF NOT MecLinearFwdSns THEN // IN6
			AlarmNo := 43;
		ELSIF NOT MecTurnC2UpSns THEN // IN7
			AlarmNo := 44;
		//ELSIF TRUE THEN // IN8
		//	AlarmNo := 45;
		ELSIF NOT MecPickDownSns THEN // IN9
			AlarmNo := 46;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 47;
		ELSIF NOT MecSuctionOk THEN // IN11
			AlarmNo := 48;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 49;
		END_IF;
	END_IF;	

	// Adim 50 hatalari
	IF StepNo = 50 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 51;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 52;
		ELSIF NOT MecLinearFwdSns THEN // IN6
			AlarmNo := 53;
		ELSIF NOT MecTurnC2UpSns THEN // IN7
			AlarmNo := 54;
		//ELSIF TRUE THEN // IN8
		//	AlarmNo := 55;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 56;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 57;
		ELSIF NOT MecSuctionOk THEN // IN11
			AlarmNo := 58;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 59;
		END_IF;
	END_IF;	

	// Adim 60 hatalari
	IF StepNo = 60 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 61;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 62;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 63;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 64;
		ELSIF NOT MecTurnC3UpSns THEN // IN8
			AlarmNo := 65;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 66;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 67;
		ELSIF NOT MecSuctionOk THEN // IN11
			AlarmNo := 68;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 69;
		END_IF;
	END_IF;	

	// Adim 70 hatalari
	IF StepNo = 70 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 71;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 72;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 73;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 74;
		ELSIF NOT MecTurnC3DownSns THEN // IN8
			AlarmNo := 75;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 76;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 77;
		ELSIF NOT MecSuctionOk THEN // IN11
			AlarmNo := 78;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 79;
		END_IF;
	END_IF;	

	// Adim 80 hatalari
	IF StepNo = 80 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 81;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 82;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 83;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 84;
		ELSIF NOT MecTurnC3DownSns THEN // IN8
			AlarmNo := 85;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 86;
		ELSIF NOT MecFoldingCloseSns THEN // IN10
			AlarmNo := 87;
		ELSIF NOT MecSuctionOk THEN // IN11
			AlarmNo := 88;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 89;
		END_IF;
	END_IF;	

	// Adim 90 hatalari
	IF StepNo = 90 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 91;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 92;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 93;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 94;
		ELSIF NOT MecTurnC3DownSns THEN // IN8
			AlarmNo := 95;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 96;
		ELSIF NOT MecFoldingCloseSns THEN // IN10
			AlarmNo := 97;
		ELSIF NOT MecSuctionOk THEN // IN11
			AlarmNo := 98;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 99;
		END_IF;
	END_IF;	

	// Adim 100 hatalari
	IF StepNo = 100 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 101;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 102;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 103;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 104;
		ELSIF NOT MecTurnC3DownSns THEN // IN8
			AlarmNo := 105;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 106;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 107;
		ELSIF NOT MecSuctionOk THEN // IN11
			AlarmNo := 108;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 109;
		END_IF;
	END_IF;	

	// Adim 110 hatalari
	IF StepNo = 110 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 111;
		ELSIF NOT R3WZ THEN // IN5
			AlarmNo := 112;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 113;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 114;
		ELSIF NOT MecTurnC3DownSns THEN // IN8
			AlarmNo := 115;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 116;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 117;
		ELSIF NOT MecSuctionOk THEN // IN11
			AlarmNo := 118;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 119;
		END_IF;
	END_IF;	

	// Adim 120 hatalari
	IF StepNo = 120 AND DelayTime THEN
		IF R2WZ THEN // IN4
			AlarmNo := 121;
		ELSIF R3WZ THEN // IN5
			AlarmNo := 122;
		ELSIF NOT MecLinearBwdSns THEN // IN6
			AlarmNo := 123;
		//ELSIF TRUE THEN // IN7
		//	AlarmNo := 124;
		ELSIF NOT MecTurnC3DownSns THEN // IN8
			AlarmNo := 125;
		ELSIF NOT MecPickUpSns THEN // IN9
			AlarmNo := 126;
		ELSIF NOT MecFoldingOpenSns THEN // IN10
			AlarmNo := 127;
		//ELSIF TRUE THEN // IN11
		//	AlarmNo := 128;
		//ELSIF TRUE THEN // IN12
		//	AlarmNo := 129;
		END_IF;
	END_IF;

END_FUNCTION_BLOCK