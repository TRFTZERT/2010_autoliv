
(* TODO: Add your comment here *)
FUNCTION_BLOCK MERGE_Bit16
	ByteLow.0:=Bit1;
	ByteLow.1:=Bit2;
	ByteLow.2:=Bit3;
	ByteLow.3:=Bit4;
	ByteLow.4:=Bit5;
	ByteLow.5:=Bit6;
	ByteLow.6:=Bit7;
	ByteLow.7:=Bit8;	
	ByteHigh.0:=Bit9;
	ByteHigh.1:=Bit10;
	ByteHigh.2:=Bit11;
	ByteHigh.3:=Bit12;
	ByteHigh.4:=Bit13;
	ByteHigh.5:=Bit14;
	ByteHigh.6:=Bit15;
	ByteHigh.7:=Bit16;
	
	WordHigh:=ByteHigh;
	WordHigh:=SHL(WordHigh,8);
	UintHigh:=WORD_TO_UINT(WordHigh);
	WordLow:=ByteLow;
	UintLow:=WORD_TO_UINT(WordLow);
	OUT_Word:=(WordHigh OR WordLow);
END_FUNCTION_BLOCK
