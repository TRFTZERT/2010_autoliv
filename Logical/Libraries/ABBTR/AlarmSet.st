
(* TODO: Add your comment here *)
FUNCTION_BLOCK AlarmSet
	AlarmListesi300LOC:=AlarmListesi300IN;
	IF LEN(Mesaj)>1 AND EN=TRUE THEN
		IF MesajNo>0 AND MesajNo<=300 THEN
			IF Uyari=FALSE THEN
				nTemp:=INT_TO_UINT(MesajNo);
				sTemp:=UINT_TO_STRING(nTemp);
				IF MesajNo<10 THEN
					sNumaraliMesaj:=CONCAT('ALARM00',sTemp);
				ELSIF MesajNo<100 THEN
					sNumaraliMesaj:=CONCAT('ALARM0',sTemp);
				ELSE
					sNumaraliMesaj:=CONCAT('ALARM',sTemp);
				END_IF;
				sNumaraliMesaj:=CONCAT(sNumaraliMesaj,' ');
				sNumaraliMesaj:=CONCAT(sNumaraliMesaj,Mesaj);
			ELSE
				nTemp:=INT_TO_UINT(MesajNo);
				sTemp:=UINT_TO_STRING(nTemp);
				IF MesajNo<10 THEN
					sNumaraliMesaj:=CONCAT('UYARI00',sTemp);
				ELSIF MesajNo<100 THEN
					sNumaraliMesaj:=CONCAT('UYARI0',sTemp);
				ELSE
					sNumaraliMesaj:=CONCAT('UYARI',sTemp);
				END_IF;
				sNumaraliMesaj:=CONCAT(sNumaraliMesaj,' ');
				sNumaraliMesaj:=CONCAT(sNumaraliMesaj,Mesaj);
			END_IF;
			AlarmListesi300LOC[MesajNo]:=sNumaraliMesaj;
		END_IF;
	ELSE
		AlarmListesi300LOC[MesajNo]:=' ';
	END_IF;			
	AlarmListesi300:=AlarmListesi300LOC;
END_FUNCTION_BLOCK
