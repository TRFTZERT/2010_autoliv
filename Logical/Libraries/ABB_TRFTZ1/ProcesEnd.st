
(* TODO: Add your comment here *)
FUNCTION_BLOCK ProcesEnd
	
	IF iFirstRobotCycleOn = FALSE
		AND iSecondRobotCycleOn = FALSE
		AND iLineerFwdSensor
		AND iPickUpSensor
		AND iFoldingBwdSensor
		AND iRotaryUpSensor
		THEN
		qEndProces := TRUE;
	ELSE
		qEndProces := FALSE;
	END_IF;
			
	// Generate Alarm
		
		IF iFirstRobotCycleOn = FALSE
			OR iSecondRobotCycleOn = FALSE
			THEN
			IF 	iLineerFwdSensor = FALSE
				THEN
				tAlarmNo := 201;
			ELSIF iPickUpSensor = FALSE
				THEN
				tAlarmNo := 202;
			ELSIF iFoldingBwdSensor = FALSE
				THEN
				tAlarmNo := 203;
			ELSIF iRotaryUpSensor = FALSE
				THEN
				tAlarmNo := 204;
			END_IF;
		ELSIF iFirstRobotCycleOn
			AND iSecondRobotCycleOn
			OR (iLineerFwdSensor AND iPickUpSensor AND iFoldingBwdSensor AND iRotaryUpSensor)
			THEN
			tAlarmNo := 0;
		END_IF;

		qAlarmNo := tAlarmNo;
	
END_FUNCTION_BLOCK
