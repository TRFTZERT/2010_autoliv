
(* TODO: Add your comment here *)
FUNCTION_BLOCK SM_Cell2

	PulseTime := t#250ms;
	WaitTime := t#250ms;
	AlarmRefreshTimer.PT := T#4S;
	TimerStateNo.PT := T#4S;
	
	qSMProgNoBit1 := iPrgNo.0;
	qSMProgNoBit2 := iPrgNo.1;
	qSMProgNoBit3 := iPrgNo.2;
	qSMProgNoBit4 := iPrgNo.3;
	
	
	IF iAirOk
		AND iSafetyOk
		AND iEmgOk
		AND iSMLockClamp
		AND iSystemAuto
		AND NOT iSMThreadBreakageError
		AND NOT iSMStopError
		THEN
		tConditions := TRUE;
	ELSE
		tConditions := FALSE;
	END_IF;
	
	IF tConditions = FALSE
		THEN
		NState := 100;
	END_IF;
	
	IF NState = 10
		AND iSMClampUpSns
		AND iSMReadyHome
		THEN
		qSMReadyForRobot := TRUE;
	ELSE
		qSMReadyForRobot := FALSE;
	END_IF;

	
	
	CASE NState OF
		
		0:
			IF tConditions
				AND iR2WZ
				AND iSMReadyHome
				THEN
				NState := 10;
			END_IF;
			
			//	Generate Alarm No
			IF iSMReadyHome = FALSE
				THEN
				tAlarmNo := 1;
			ELSIF iR2WZ = FALSE
				THEN
				tAlarmNo := 2;
			END_IF;
			
			
		10:		// INITIAL POSITION
			
			
			qSMClampWork:=TRUE;
			qSMPrgBit1:=FALSE;
			qSMPrgBit2:=FALSE;
			qSMStart:=FALSE;
			qSMVirtual:=FALSE;
			qSMRunning := FALSE;
			qBusy := FALSE;
			
			IF tConditions
				AND iSMClampUpSns
				AND iR2WZ
				AND iStart
				THEN
				NState := 20;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions 
				THEN
				IF iSMClampUpSns = FALSE
					THEN
					tAlarmNo := 10;
				ELSIF iStart = FALSE
					THEN
					tAlarmNo := 11;
				ELSIF iR2WZ = FALSE
					THEN
					tAlarmNo := 12;
				END_IF;
			END_IF;
			
		
		20:		// Virtual Work
			
			
			qSMCycleOkForRobot := FALSE;
			qSMVirtual := TRUE;
			qSMRunning := TRUE;
			qBusy := TRUE;
			
			IF iSMVirtualHigh
				THEN
				qSMVirtual := FALSE;
				NState := 30;
			END_IF;
			
			//	Generate Alarm No
			IF iSMVirtualHigh = FALSE
				THEN
				tAlarmNo := 20;
			END_IF;
			
			
		30:		// Cyle Start
			
			
			WaitTimer1.PT := WaitTime;
			WaitTimer1.IN := TRUE;
			
			IF WaitTimer1.Q
				THEN
				Start1PulseTimer.PT := PulseTime;
				Start1PulseTimer.IN := TRUE;
				qSMStart := TRUE;
			END_IF;
			
			IF Start1PulseTimer.Q
				THEN
				Start1PulseTimer.IN := FALSE;
				WaitTimer1.IN := FALSE;
				qSMStart := FALSE;
			END_IF;
			
			IF tConditions
				AND iR2WZ
				AND iSMPrgBit1
				THEN
				NState := 40;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iSMPrgBit1 =FALSE
					THEN
					tAlarmNo := 30;
				ELSIF iR2WZ = FALSE
					THEN
					tAlarmNo := 31;
				END_IF;
			END_IF;
			
			
		40:		//CLAMP WORK
		
			
			qSMClampWork:=FALSE;
			
			IF tConditions
				AND iR2WZ
				AND iSMClampDownSns
				AND iSMPressureOk
				THEN
				qSMPrgBit1 := TRUE;
				NState := 50;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iSMClampDownSns = FALSE
					THEN
					tAlarmNo := 40;
				ELSIF iR2WZ = FALSE
					THEN
					tAlarmNo := 41;
				ELSIF iSMPressureOk = FALSE
					THEN
					tAlarmNo := 42;
				END_IF;
			END_IF;
			
			
		50:		// START SEWING
			
			
			WaitTimer2.PT := WaitTime;
			WaitTimer2.IN := TRUE;
			
			IF WaitTimer2.Q
				THEN
				Start2PulseTimer.PT := PulseTime;
				Start2PulseTimer.IN := TRUE;
				qSMStart := TRUE;
			END_IF;
			
			IF Start2PulseTimer.Q
				THEN
				Start2PulseTimer.IN := FALSE;
				WaitTimer2.IN := FALSE;
				qSMStart := FALSE;
			END_IF;
			
			IF tConditions
				AND iR2WZ
				AND iSMPrgBit2
				THEN
				NState := 60;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iSMPrgBit2 = FALSE
				THEN
					tAlarmNo := 50;
				ELSIF iR2WZ = FALSE
					THEN
					tAlarmNo := 51;
				END_IF;
			END_IF;
			
			
		60:		// END SEWING CLAMP OPEN
			
			
			qSMPrgBit1 := FALSE;
			qSMClampWork := TRUE;
			
			IF tConditions
				AND iR2WZ
				AND iSMClampUpSns
				THEN
				qSMPrgBit2 := TRUE;
				NState := 70;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iSMClampUpSns = FALSE
				THEN
					tAlarmNo := 60;
				ELSIF iR2WZ = FALSE
					THEN
					tAlarmNo := 61;
				END_IF;
			END_IF;
					
					
		70:
			
			
			WaitTimer3.PT := WaitTime;
			WaitTimer3.IN := TRUE;
			
			IF WaitTimer3.Q
				THEN
				Start3PulseTimer.PT := PulseTime;
				Start3PulseTimer.IN := TRUE;
				qSMStart := TRUE;
			END_IF;
			
			IF Start3PulseTimer.Q
				THEN
				Start3PulseTimer.IN := FALSE;
				WaitTimer3.IN := FALSE;
				qSMStart := FALSE;
			END_IF;
			
			IF tConditions
				AND iR2WZ
				AND iSMReadyHome
				THEN
				qSMPrgBit2 := FALSE;
				qSMCycleOkForRobot := TRUE;
				NState := 80;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions THEN
				IF iSMReadyHome = FALSE
					THEN
					tAlarmNo := 70;
				ELSIF iR2WZ = FALSE
					THEN
					tAlarmNo := 71;
				END_IF;
			END_IF;
			
			
		80:
			
			//
			NState := 90;
			//
			
		90:
			
			//
			NState := 100;
			//
			
		100:
			
			//
			NState := 10;
			//
		

	END_CASE;
	
	
	


	
	//	Generate Alarm No
	IF iAirOk = FALSE THEN
		tAlarmNo := 200;
	ELSIF iSafetyOk = FALSE THEN
		tAlarmNo := 201;
	ELSIF iEmgOk= FALSE  THEN
		tAlarmNo := 202;
	ELSIF iSMLockClamp = FALSE THEN
		tAlarmNo := 203;
	ELSIF iSystemAuto = FALSE THEN
		tAlarmNo := 205;
	ELSIF iSMThreadBreakageError THEN
		tAlarmNo := 206;
	ELSIF iSMStopError THEN
		tAlarmNo := 207;
	END_IF;

	
	
	IF tAlarmNo <> tOldAlarmNo
		THEN
		AlarmRefreshTimer.IN := FALSE;
		qAlarmNo := 0;
	END_IF;
	
	IF tAlarmNo = tOldAlarmNo
		AND TimerStateNo.Q
		THEN
		AlarmRefreshTimer.IN := TRUE;
	END_IF;

	
	IF NState <> tOldNState
		THEN
		TimerStateNo.IN := FALSE;
	END_IF;

	IF NState = tOldNState 
		THEN
		TimerStateNo.IN := TRUE;
	END_IF;
	
	IF AlarmRefreshTimer.Q AND NOT Pulse
		THEN
		qAlarmNo := tAlarmNo;
	END_IF;
	Pulse := AlarmRefreshTimer.Q;
	
	
	tOldAlarmNo := tAlarmNo;
	tOldNState := NState;
	qStateNo := NState;

	
	Start1PulseTimer ();
	Start2PulseTimer ();
	Start3PulseTimer ();
	WaitTimer1 ();
	WaitTimer2 ();
	WaitTimer3 ();
	AlarmRefreshTimer ();
	TimerStateNo ();
	
END_FUNCTION_BLOCK
