
(* TODO: Add your comment here *)
FUNCTION_BLOCK GenCamResult
	
	WaitTimer.PT := T#250ms;
	
	IF iReadyResult
		THEN
		qSewingOk		 := iSewingOk;
		qIntegrityOk	 := iIntegrityOk;
		WaitTimer.IN := TRUE;
	END_IF;
	
	qRobotSendValue := WaitTimer.Q;
	
	IF iClearDatas
		THEN
		qSewingOk		 := 0;
		qIntegrityOk	 := 0;
		WaitTimer.IN := FALSE;
	END_IF;
	
	WaitTimer();
	
END_FUNCTION_BLOCK
