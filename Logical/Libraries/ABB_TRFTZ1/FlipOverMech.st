
(* TODO: Add your comment here *)
FUNCTION_BLOCK FlipOverMech
	
	AlarmRefreshTimer.PT := T#4S;
	TimerStateNo.PT := T#4S;
	tWaitTimer.PT := T#1S;
		
	IF iAirOk
		AND iSafetyOk
		AND iEmgOk
		AND iSystemAuto
		THEN
		tConditions := TRUE;
	ELSE
		tConditions := FALSE;
	END_IF;
		
	IF tConditions = FALSE
		THEN
	END_IF;
		
	CASE NState OF
				
		0:  // FlipOverMech initioal position
		
			IF tConditions
				AND iRobotStart
				AND iFlipOverMechOpened
				AND iFlipOverMechBackward
				THEN
				qFlippingOverMech_Ready := FALSE;
				qFlippingOverMech_CycleOK := FALSE;
				NState := 10;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iRobotStart = TRUE
					THEN
					IF iFlipOverMechOpened = FALSE
						THEN
						tAlarmNo := 1;
					END_IF;
					IF iFlipOverMechBackward = FALSE
						THEN
						tAlarmNo := 2;
					END_IF;
				END_IF;
			END_IF;
								
		10:  // FlipOverMech for Start position 1
		
			qFlipOverMechOpen := FALSE;
			qFlipOverMechClose := TRUE;
			
			IF tConditions
				AND iFlipOverMechClosed
				THEN
				tWaitTimer.IN := TRUE;				
			ELSE
				tWaitTimer.IN := FALSE;
			END_IF;
		
			IF tWaitTimer.Q
				THEN
				NState := 20;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iFlipOverMechClosed = FALSE
					THEN
					tAlarmNo := 3;
				END_IF;
			END_IF;		
			
		
		20:  // FlipOverMech for Start position 2
		
			tWaitTimer.IN := FALSE;
			qFlipOverMechBackward := FALSE;
			qFlipOverMechForward := TRUE;		
		
			IF tConditions
				AND iFlipOverMechForward
				THEN
				NState := 30;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iFlipOverMechForward = FALSE
					THEN
					tAlarmNo := 4;
				END_IF;
			END_IF;
				
		30:  // FlipOverMech for Start position 3
			
			qFlipOverMechClose := FALSE;
			qFlipOverMechOpen := TRUE;
		
			IF tConditions
				AND iFlipOverMechOpened
				THEN
				tWaitTimer.IN := TRUE;
				
				IF tWaitTimer.Q
					THEN
					NState := 40;
				END_IF;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iFlipOverMechOpened = FALSE
					THEN
					tAlarmNo := 5;
				END_IF;
			END_IF;
			
		40:  // FlipOverMech for Start position 3
			
			qFlipOverMechForward := FALSE;
			qFlipOverMechBackward := TRUE;
		
			IF tConditions
				AND iFlipOverMechBackward
				THEN
				NState := 50;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iFlipOverMechBackward = FALSE
					THEN
					tAlarmNo := 6;
				END_IF;
			END_IF;
								
		50:  // FlipOverMech for Start position 4
			
			qFlippingOverMech_Ready := TRUE;
			qFlippingOverMech_CycleOK := TRUE;
		
			IF tConditions
				THEN
				NState := 60;
			END_IF;
		
		60:  // FlipOverMech for Start position 5
		
			IF tConditions
				THEN
				NState := 0;
			END_IF;
	END_CASE;
				
	//	Generate Alarm No
	
	IF iAirOk = FALSE
		THEN
		tAlarmNo := 200;
	ELSIF iSafetyOk = FALSE
		THEN
		tAlarmNo := 201;
	ELSIF iEmgOk= FALSE
		THEN
		tAlarmNo := 202;
	ELSIF iSystemAuto = FALSE
		THEN
		tAlarmNo := 205;
	END_IF;	
		
	IF tAlarmNo <> tOldAlarmNo
		THEN
		AlarmRefreshTimer.IN := FALSE;
		qAlarmNo := 0;
	END_IF;
		
	IF tAlarmNo = tOldAlarmNo
		AND TimerStateNo.Q
		THEN
		AlarmRefreshTimer.IN := TRUE;
	END_IF;
				
	IF NState <> tOldNState
		THEN
		TimerStateNo.IN := FALSE;
	END_IF;
		
	IF NState = tOldNState 
		THEN
		TimerStateNo.IN := TRUE;
	END_IF;
		
	IF (EDGEPOS (AlarmRefreshTimer.Q))
		THEN
		qAlarmNo := tAlarmNo;
	END_IF;
		
	tOldAlarmNo := tAlarmNo;
	tOldNState := NState;
	qStateNo := NState;
	
	tWaitTimer ();
	AlarmRefreshTimer ();
	TimerStateNo ();
END_FUNCTION_BLOCK
