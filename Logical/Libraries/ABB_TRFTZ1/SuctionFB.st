
(* TODO: Add your comment here *)
FUNCTION_BLOCK SuctionFB
	
	FaultTimer.PT := T#3s;
	
	//====================================================================
		(*Suction Work*)
	
		IF iSuction
			AND iManualSuction = FALSE
			THEN
			tAutoSuction := TRUE;
		ELSE
			tAutoSuction := FALSE;
		END_IF;
	
		IF iManualSuction
			AND iSuction = FALSE
			THEN
			tManSuction := TRUE;
		ELSE
			tManSuction := FALSE;
		END_IF;
	
	//====================================================================
	
	
	//====================================================================
		(*Blow Work*)
	
		IF iBlow
			AND iManualBlow = FALSE
			THEN
			tAutoBlow := TRUE;
		ELSE
			tAutoBlow := FALSE;
		END_IF;
	
		IF iManualBlow
			AND iBlow = FALSE
			THEN
			tManBlow := TRUE;
		ELSE
			tManBlow := FALSE;
		END_IF;
	
	//====================================================================
	
	
	//====================================================================
		(*Suction OK*)
	
		IF qSuction
			AND iSuctionOk = FALSE
			THEN
			FaultTimer.IN := TRUE;
		ELSE
			FaultTimer.IN := FALSE;
		END_IF;
	
	//====================================================================
	
	
	//====================================================================
		(*Outputs*)
	
		qSuction := tAutoSuction OR tManSuction;
		qBlow	 := tAutoBlow OR tManBlow;
		
		qSuctionFault := FaultTimer.Q;
		qSuctionOk := iSuctionOk;

	//====================================================================
	
	
	//====================================================================
		(*AlarmNo*)
	
		IF iSuction
			AND iManualSuction
			THEN
			qAlarmNo := 230;
		ELSIF iBlow
			AND iManualBlow
			THEN
			qAlarmNo := 231;
		ELSIF FaultTimer.Q
			THEN
			qAlarmNo := 232;
		ELSIF (iSuction OR iManualSuction)
			AND (iBlow OR iManualBlow)
			THEN
			qAlarmNo := 233;
		ELSE
			qAlarmNo := 0;
		END_IF;
	
	//====================================================================
	
	
	FaultTimer();
	
END_FUNCTION_BLOCK
