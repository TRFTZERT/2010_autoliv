
(* TODO: Add your comment here *)
FUNCTION_BLOCK SM_Cell3
	
	PulseTime := t#250ms;
	WaitTime := t#250ms;
	WaitTimer1.PT := WaitTime;
	AlarmRefreshTimer.PT := T#4S;
	TimerStateNo.PT := T#4S;
	
	qSMProgNoBit1 := iPrgNo.0;
	qSMProgNoBit2 := iPrgNo.1;
	qSMProgNoBit3 := iPrgNo.2;
	qSMProgNoBit4 := iPrgNo.3;
	
	
	IF iAirOk
		AND iSafetyOk
		AND iEmgOk
		AND iSMLockClamp
		AND iSystemAuto
		AND NOT iSMThreadBreakageError
		AND NOT iSMStopError
		THEN
		tConditions := TRUE;
	ELSE
		tConditions := FALSE;
	END_IF;
	
	IF tConditions = FALSE
		THEN
		NState := 0;
	END_IF;
	
	IF NState = 20
		AND iSMReadyHome
		AND iSMClampUpSns
		THEN
		qSMReady1ForRobot := TRUE;
	ELSE
		qSMReady1ForRobot := FALSE;
	END_IF;
	
	IF NState = 60
		AND iSMReadyHome
		AND iSMClampUpSns
		THEN
		qSMReady2ForRobot := TRUE;
	ELSE
		qSMReady2ForRobot := FALSE;
	END_IF;
	
	
	CASE NState OF
		
		0:
			
			
			IF tConditions
				AND iR3WZ
				AND iSMReadyHome
				THEN
				NState := 10;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 1;
				ELSIF iSMReadyHome = FALSE
					THEN
					tAlarmNo := 2;
				END_IF;
			END_IF;
			
			
		10:		// INITIAL POSITION
			
			
			qSMCycle1OkForRobot := FALSE;
			qSMCycle2OkForRobot := FALSE;
			qSMStart:=FALSE;
			qSMVirtual:=FALSE;
			qSMRunning := FALSE;
			qBusy := FALSE;
			
			IF tConditions
				AND iR3WZ
				AND iSMClampUpSns
				THEN
				NState := 20;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 10;
				ELSIF iSMClampUpSns = FALSE
					THEN
					tAlarmNo := 11;
				END_IF;
			END_IF;
		
			
		20:		// Cycle1 Start 1
			
			
			IF tConditions
				AND iR3WZ
				AND iSMClampDownSns
				AND iSMPressureOk
				AND iStart1
				THEN
				NState := 30;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 20;
				ELSIF iSMClampDownSns = FALSE
					THEN
					tAlarmNo := 21;
				ELSIF iSMPressureOk = FALSE
					THEN
					tAlarmNo := 22;
				ELSIF iStart1 = FALSE
					THEN
					tAlarmNo := 23;
				END_IF;
			END_IF;
			
			
		30:		// Virtual Work
			
			
			qSMVirtual := TRUE;
			qSMRunning := TRUE;
			qBusy := TRUE;
			
			IF iSMVirtualHigh
				THEN
				qSMVirtual := FALSE;
				NState := 40;
			END_IF;
			
			//	Generate Alarm No
			IF iSMVirtualHigh = FALSE
				THEN
				tAlarmNo := 30;
			END_IF;
			
			
		40:		// SM Start 1
			
			
			WaitTimer1.IN := TRUE;
			
			IF WaitTimer1.Q
				THEN
				Start1PulseTimer.PT := PulseTime;
				Start1PulseTimer.IN := TRUE;
				qSMStart := TRUE;
			END_IF;
			
			IF Start1PulseTimer.Q
				THEN
				Start1PulseTimer.IN := FALSE;
				WaitTimer1.IN := FALSE;
				qSMStart := FALSE;
			END_IF;
			
			IF tConditions
				AND iR3WZ
				AND iSMReadyHome = FALSE
				THEN
				Start1PulseTimer.IN := FALSE;
				WaitTimer1.IN := FALSE;
				qSMStart := FALSE;
				NState := 50;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 40;
				ELSIF iSMReadyHome
					THEN
					tAlarmNo := 41;
				END_IF;
			END_IF;
			
		
		50:		// SM Finish 1
			
			
			IF tConditions
				AND iR3WZ
				AND iSMReadyHome
				THEN
				qSMRunning := FALSE;
				qSMCycle1OkForRobot := TRUE;
				NState := 60;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 50;
				ELSIF iSMReadyHome = FALSE
					THEN
					tAlarmNo := 51;
				END_IF;
			END_IF;
			
			
		60:		// Cycle Start 2
			
			
			IF tConditions
				AND iR3WZ
				AND iSMClampDownSns
				AND iSMPressureOk
				AND iStart2
				THEN
				qSMCycle1OkForRobot := FALSE;
				NState := 70;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 60;
				ELSIF iSMClampDownSns = FALSE
					THEN
					tAlarmNo := 61;
				ELSIF iSMPressureOk = FALSE
					THEN
					tAlarmNo := 62;
				ELSIF iStart2 = FALSE
					THEN
					tAlarmNo := 63;
				END_IF;
			END_IF;
			
			
		70:		// Virtual Work
			
			
			qSMVirtual := TRUE;
			qSMRunning := TRUE;
			
			IF iSMVirtualHigh
				THEN
				qSMVirtual := FALSE;
				NState := 80;
			END_IF;
			
			//	Generate Alarm No
			IF iSMVirtualHigh = FALSE
				THEN
				tAlarmNo := 70;
			END_IF;
			
			
		80:		// SM Start 2
			
			
			WaitTimer2.PT := WaitTime;
			WaitTimer2.IN := TRUE;
			
			IF WaitTimer2.Q
				THEN
				Start2PulseTimer.PT := PulseTime;
				Start2PulseTimer.IN := TRUE;
				qSMStart := TRUE;
			END_IF;
			
			IF Start2PulseTimer.Q
				THEN
				Start2PulseTimer.IN := FALSE;
				WaitTimer2.IN := FALSE;
				qSMStart := FALSE;
			END_IF;
			
			IF tConditions
				AND iR3WZ
				AND iSMReadyHome = FALSE
				THEN
				Start2PulseTimer.IN := FALSE;
				WaitTimer2.IN := FALSE;
				qSMStart := FALSE;
				NState := 90;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 80;
				ELSIF iSMReadyHome
					THEN
					tAlarmNo := 81;
				END_IF;
			END_IF;
			
			
		90:		// SM Finish 2
			
			
			IF tConditions
				AND iR3WZ
				AND iSMReadyHome
				THEN
				qSMRunning := FALSE;
				qSMCycle2OkForRobot := TRUE;
				NState := 100;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 90;
				ELSIF iSMReadyHome = FALSE
					THEN
					tAlarmNo := 91;
				END_IF;
			END_IF;
			
			
		100:
			
			
			IF tConditions
				AND iR3WZ
				AND iSMReadyHome
				AND iSMClampUpSns
				THEN
				qSMCycle2OkForRobot := FALSE;
				NState := 110;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3WZ = FALSE
					THEN
					tAlarmNo := 100;
				ELSIF iSMReadyHome = FALSE
					THEN
					tAlarmNo := 101;
				ELSIF iSMClampUpSns = FALSE
					THEN
					tAlarmNo := 102;
				END_IF;
			END_IF;
			
			
		110:
			
			//
			NState := 10;
		//
		
	END_CASE;
	
	
	
	
	//	Generate Alarm No
	IF iAirOk = FALSE THEN
		tAlarmNo := 200;
	ELSIF iSafetyOk = FALSE THEN
		tAlarmNo := 201;
	ELSIF iEmgOk= FALSE  THEN
		tAlarmNo := 202;
	ELSIF iSMLockClamp = FALSE THEN
		tAlarmNo := 203;
	ELSIF iSystemAuto = FALSE THEN
		tAlarmNo := 205;
	ELSIF iSMThreadBreakageError THEN
		tAlarmNo := 206;
	ELSIF iSMStopError THEN
		tAlarmNo := 207;
	END_IF;

	
	
	IF tAlarmNo <> tOldAlarmNo
		THEN
		AlarmRefreshTimer.IN := FALSE;
		qAlarmNo := 0;
	END_IF;
	
	IF tAlarmNo = tOldAlarmNo
		AND TimerStateNo.Q
		THEN
		AlarmRefreshTimer.IN := TRUE;
	END_IF;

	
	IF NState <> tOldNState
		THEN
		TimerStateNo.IN := FALSE;
	END_IF;

	IF NState = tOldNState 
		THEN
		TimerStateNo.IN := TRUE;
	END_IF;
	
	IF AlarmRefreshTimer.Q AND NOT Pulse
		THEN
		qAlarmNo := tAlarmNo;
	END_IF;
	Pulse := AlarmRefreshTimer.Q;

	
	tOldAlarmNo := tAlarmNo;
	tOldNState := NState;
	qStateNo := NState;
	

	
	
	Start1PulseTimer ();
	Start2PulseTimer ();
	WaitTimer1 ();
	WaitTimer2 ();
	AlarmRefreshTimer ();
	TimerStateNo ();
	
END_FUNCTION_BLOCK
