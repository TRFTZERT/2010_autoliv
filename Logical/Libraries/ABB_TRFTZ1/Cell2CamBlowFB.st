
(* TODO: Add your comment here *)
FUNCTION_BLOCK Cell2CamBlowFB
	
	BlowTimer.PT := iBlowTime;
	
	CASE NState OF
		
			
		0:
			
			IF iTrnsfrLineerBwdSns
				AND iTrnsfrVcm1Ok
				AND iRobotWZ
				THEN
				NState := 1;
			END_IF;
			
		1:
			
			IF iTrnsfrLineerBwdSns = FALSE
				AND iTrnsfrVcm1Ok
				AND iTrnsfrPickUpSns
				THEN
				NState := 2;
			END_IF;
			
		2:
		
			BlowTimer.IN := TRUE;
			tBlow		 := TRUE;
			NState := 0;
			
		
	END_CASE;
	
	
	IF BlowTimer.Q
		THEN
		tBlow		 := FALSE;
		BlowTimer.IN := FALSE;
	END_IF;
	
	
	qBlow := tBlow OR iManualBlow;
	
	
	BlowTimer ();
	
END_FUNCTION_BLOCK
