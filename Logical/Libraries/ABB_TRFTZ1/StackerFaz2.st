
(* TODO: Add your comment here *)
FUNCTION_BLOCK StackerFaz2
	
	AlarmRefreshTimer.PT := T#4S;
	TimerStateNo.PT := T#4S;
	tWaitTimer.PT := T#1S;
		
	IF iAirOk
		AND iSafetyOk
		AND iEmgOk
		AND iSystemAuto
		THEN
		tConditions := TRUE;
	ELSE
		tConditions := FALSE;
	END_IF;
		
	IF tConditions = FALSE
		THEN
	END_IF;
		
	CASE NState OF
				
		0:  // Top Stacker initioal position
		
			IF tConditions
				AND iTopStcker_Empty
				AND iTopStackerLocked
				AND iTopStackerClose
				AND iTopStackerPreSens
				AND iBtmStackerPreSens
				AND iSlidingDoorClose
				AND iWZ_TopStcker
				AND iWZ_BottomStcker
				THEN
				NState := 10;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iTopStcker_Empty = TRUE 
					THEN
					IF iTopStackerLocked = FALSE
						THEN
						tAlarmNo := 1;
					END_IF;
					IF iTopStackerClose = FALSE
						THEN
						tAlarmNo := 2;
					END_IF;
					IF iTopStackerPreSens = FALSE
						THEN
						tAlarmNo := 3;
					END_IF;
					IF iBtmStackerPreSens = FALSE
						THEN
						tAlarmNo := 3;
					END_IF;
					IF iSlidingDoorClose = FALSE
						THEN
						tAlarmNo := 4;
					END_IF;
					IF iWZ_TopStcker = FALSE
						THEN
						tAlarmNo := 5;
					END_IF;
					IF iWZ_BottomStcker = FALSE
						THEN
						tAlarmNo := 6;
					END_IF;
				END_IF;
			END_IF;
			
			// Bottom Stacker initioal position
			
			IF tConditions
				AND iBtmStcker_Empty
				AND iBtmStackerLocked
				AND iBtmStackerClose
				AND iBtmStackerPreSens
				AND iTopStackerPreSens
				AND iSlidingDoorClose
				AND iWZ_TopStcker
				AND iWZ_BottomStcker
				THEN
				NState := 60;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iBtmStcker_Empty = TRUE
					THEN
					IF iBtmStackerLocked = FALSE
						THEN
						tAlarmNo := 7;
					END_IF;
					IF iBtmStackerClose = FALSE
						THEN
						tAlarmNo := 8;
					END_IF;
					IF iBtmStackerPreSens = FALSE
						THEN
						tAlarmNo := 9;
					END_IF;
					IF iTopStackerPreSens = FALSE
						THEN
						tAlarmNo := 9;
					END_IF;
					IF iSlidingDoorClose = FALSE
						THEN
						tAlarmNo := 10;
					END_IF;
					IF iWZ_TopStcker = FALSE
						THEN
						tAlarmNo := 11;
					END_IF;
					IF iWZ_BottomStcker = FALSE
						THEN
						tAlarmNo := 12;
					END_IF;
				END_IF;
			END_IF;
								
		10:  // Bottom Stacker for Loading Start Position 1
		
			qSlidingDoorClose := FALSE;
			qSlidingDoorOpen := TRUE;
			
			IF tConditions
				AND iSlidingDoorOpen
				THEN
				tWaitTimer.IN := TRUE;				
			ELSE
				tWaitTimer.IN := FALSE;
			END_IF;
		
			IF tWaitTimer.Q
				THEN
				NState := 20;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iSlidingDoorOpen = FALSE
					THEN
					tAlarmNo := 13;
				END_IF;
			END_IF;		
			
		
		20:  // Bottom Stacker for Loading Start Position 2
		
			tWaitTimer.IN := FALSE;
			qTopStackerClose := FALSE;
			qTopStackerOpen := TRUE;
			qBtmStackerOpen := FALSE;
			qBtmStackerClose := TRUE;		
		
			IF tConditions
				AND iTopStackerOpen
				AND iBtmStackerClose
				AND iBtmStackerPreSens
				THEN
				NState := 30;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iTopStackerOpen = FALSE
					THEN
					tAlarmNo := 14;
				END_IF;
				IF iBtmStackerClose = FALSE
					THEN
					tAlarmNo := 15;
				END_IF;
				IF iBtmStackerPreSens = FALSE
					THEN
					tAlarmNo := 16;
				END_IF;
				
			END_IF;
				
		30:  // Bottom Stacker for Loading Start Position 3
			
			qSlidingDoorOpen := FALSE;
			qSlidingDoorClose := TRUE;
			qBtmStackerUnLocked := FALSE;
			qBtmStackerLocked := TRUE; 
			qTopStackerLocked := FALSE; 
			qTopStackerUnLocked := TRUE;
		
			IF tConditions
				AND iSlidingDoorClose
				AND iBtmStackerLocked
				AND iTopStackerUnLocked
				THEN
				NState := 40;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iSlidingDoorClose = FALSE
					THEN
					tAlarmNo := 17;
				END_IF;
				IF iBtmStackerLocked = FALSE
					THEN
					tAlarmNo := 18;
				END_IF;
				IF iTopStackerUnLocked = FALSE
					THEN
					tAlarmNo := 19;
				END_IF;
			END_IF;
											
		40:  // Bottom Stacker for Loading Start Position 4
			
			qBtmStcker_Ready := TRUE;
		
			IF tConditions
				AND iRbtBtmStckerTaked
				THEN
				qBtmStcker_Ready := FALSE;
				NState := 50;
			END_IF;			
								
		50:  // Bottom Stacker for Loading Start Position 5
		
			IF tConditions
				THEN
				NState := 0;
			END_IF;
								
		60:  // Top Stacker for Loading Start Position 1
		
			qSlidingDoorClose := FALSE;
			qSlidingDoorOpen := TRUE;
			
			IF tConditions
				AND iSlidingDoorOpen
				THEN
				tWaitTimer.IN := TRUE;				
			ELSE
				tWaitTimer.IN := FALSE;
			END_IF;
		
			IF tWaitTimer.Q
				THEN
				NState := 70;
			END_IF;
		
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iSlidingDoorOpen = FALSE
					THEN
					tAlarmNo := 20;
				END_IF;
			END_IF;			
		
		70:  // Top Stacker for Loading Start Position 2
			
			tWaitTimer.IN := FALSE;
			qBtmStackerClose := FALSE;
			qBtmStackerOpen := TRUE;
			qTopStackerOpen := FALSE;
			qTopStackerClose := TRUE;		
		
			IF tConditions
				AND iBtmStackerOpen
				AND iTopStackerClose
				AND iTopStackerPreSens
				THEN
				NState := 80;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iBtmStackerOpen = FALSE
					THEN
					tAlarmNo := 21;
				END_IF;
				IF iTopStackerClose = FALSE
					THEN
					tAlarmNo := 22;
				END_IF;
				IF iTopStackerPreSens = FALSE
					THEN
					tAlarmNo := 23;
				END_IF;
				
			END_IF;
				
		80:  // Top Stacker for Loading Start Position 3
			
			qSlidingDoorOpen := FALSE;
			qSlidingDoorClose := TRUE;
			qTopStackerUnLocked := FALSE;
			qTopStackerLocked := TRUE; 
			qBtmStackerLocked := FALSE; 
			qBtmStackerUnLocked := TRUE;
		
			IF tConditions
				AND iSlidingDoorClose
				AND iTopStackerLocked
				AND iBtmStackerUnLocked
				THEN
				NState := 90;
			END_IF;
			
			//	Generate Alarm No
		
			IF tConditions
				THEN
				IF iSlidingDoorClose = FALSE
					THEN
					tAlarmNo := 24;
				END_IF;
				IF iTopStackerLocked = FALSE
					THEN
					tAlarmNo := 25;
				END_IF;
				IF iBtmStackerUnLocked = FALSE
					THEN
					tAlarmNo := 26;
				END_IF;
			END_IF;
											
		90:  // Top Stacker for Loading Start Position 4
			
			qTopStcker_Ready := TRUE;
		
			IF tConditions
				AND iRbtTopStckerTaked
				THEN
				qTopStcker_Ready := FALSE;
				NState := 100;
			END_IF;			
								
		100:  // Top Stacker for Loading Start Position 5
		
			IF tConditions
				THEN
				NState := 0;
			END_IF;
	END_CASE;
				
	//	Generate Alarm No
	
	IF iAirOk = FALSE
		THEN
		tAlarmNo := 200;
	ELSIF iSafetyOk = FALSE
		THEN
		tAlarmNo := 201;
	ELSIF iEmgOk= FALSE
		THEN
		tAlarmNo := 202;
	ELSIF iSystemAuto = FALSE
		THEN
		tAlarmNo := 205;
	END_IF;	
		
	IF tAlarmNo <> tOldAlarmNo
		THEN
		AlarmRefreshTimer.IN := FALSE;
		qAlarmNo := 0;
	END_IF;
		
	IF tAlarmNo = tOldAlarmNo
		AND TimerStateNo.Q
		THEN
		AlarmRefreshTimer.IN := TRUE;
	END_IF;
				
	IF NState <> tOldNState
		THEN
		TimerStateNo.IN := FALSE;
	END_IF;
		
	IF NState = tOldNState 
		THEN
		TimerStateNo.IN := TRUE;
	END_IF;
		
	IF (EDGEPOS (AlarmRefreshTimer.Q))
		THEN
		qAlarmNo := tAlarmNo;
	END_IF;
		
	tOldAlarmNo := tAlarmNo;
	tOldNState := NState;
	qStateNo := NState;
	
	tWaitTimer ();
	AlarmRefreshTimer ();
	TimerStateNo ();
END_FUNCTION_BLOCK
