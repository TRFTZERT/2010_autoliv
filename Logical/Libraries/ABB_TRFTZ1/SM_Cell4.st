
(* TODO: Add your comment here *)
FUNCTION_BLOCK SM_Cell4
	
	
	PulseTime := t#250ms;
	WaitTime := t#250ms;
	AlarmRefreshTimer.PT := T#4S;
	TimerStateNo.PT := T#4S;
	
	qSMProgNoBit1 := iPrgNo.0;
	qSMProgNoBit2 := iPrgNo.1;
	qSMProgNoBit3 := iPrgNo.2;
	qSMProgNoBit4 := iPrgNo.3;
	
	
	IF iAirOk
		AND iSafetyOk
		AND iEmgOk
		AND iSMLockClamp
		AND iSystemAuto
		AND NOT iSMThreadBreakageError
		AND NOT iSMStopError
		THEN
		tConditions := TRUE;
	ELSE
		tConditions := FALSE;
	END_IF;
	
	IF tConditions = FALSE
		THEN
		NState := 0;
	END_IF;
	
	IF NState = 20
		AND iSMReadyHome
		AND iSMClampUpSns
		THEN
		qSMReadyForRobot := TRUE;
	ELSE
		qSMReadyForRobot := FALSE;
	END_IF;
	
	
	
	CASE NState OF
		
		0:
			
			IF tConditions
				AND iR4WZ
				AND iSMReadyHome
				THEN
				NState := 10;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR4WZ = FALSE
					THEN
					tAlarmNo := 1;
				ELSIF iSMReadyHome = FALSE
					THEN
					tAlarmNo := 2;
				END_IF;
			END_IF;
			
			
		10:		// INITIAL POSITION
			
			
			qSMCycleOkForRobot:=FALSE;
			qSMStart:=FALSE;
			qSMVirtual:=FALSE;
			qSMRunning := FALSE;
			qBusy := FALSE;
			
			IF tConditions
				AND iR4WZ
				AND iSMClampUpSns
				THEN
				NState := 20;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR4WZ = FALSE
					THEN
					tAlarmNo := 10;
				ELSIF iSMClampUpSns = FALSE
					THEN
					tAlarmNo := 11;
				END_IF;
			END_IF;
			
		
		20:		// Cycle1 Start
			
			
			IF tConditions
				AND iR4WZ
				AND iSMClampDownSns
				AND iSMPressureOk
				AND iStart
				THEN
				NState := 30;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR4WZ = FALSE
					THEN
					tAlarmNo := 20;
				ELSIF iSMClampDownSns = FALSE
					THEN
					tAlarmNo := 21;
				ELSIF iSMPressureOk = FALSE
					THEN
					tAlarmNo := 22;
				ELSIF iStart = FALSE
					THEN
					tAlarmNo := 23;
				END_IF;
			END_IF;
			
				
		30:		// Virtual Work
			
			
			qSMVirtual := TRUE;
			qSMRunning := TRUE;
			qBusy := TRUE;
			
			IF iSMVirtualHigh
				THEN
				qSMVirtual := FALSE;
				NState := 40;
			END_IF;
			
			//	Generate Alarm No
			IF iSMVirtualHigh = FALSE
				THEN
				tAlarmNo := 30;
			END_IF;
			
			
		40:		// SM Start
			
			
			WaitTimer1.PT := WaitTime;
			WaitTimer1.IN := TRUE;
			
			IF WaitTimer1.Q
				THEN
				Start1PulseTimer.PT := PulseTime;
				Start1PulseTimer.IN := TRUE;
				qSMStart := TRUE;
			END_IF;
			
			IF Start1PulseTimer.Q
				THEN
				Start1PulseTimer.IN := FALSE;
				WaitTimer1.IN := FALSE;
				qSMStart := FALSE;
			END_IF;
			
			IF tConditions
				AND iR4WZ
				AND iSMReadyHome = FALSE
				THEN
				NState := 50;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR4WZ = FALSE
					THEN
					tAlarmNo := 40;
				ELSIF iSMReadyHome
					THEN
					tAlarmNo := 41;
				END_IF;
			END_IF;
			
		
		50:		// SM Finish 1
			
			
			IF tConditions
				AND iR4WZ
				AND iSMReadyHome
				THEN
				qSMCycleOkForRobot := TRUE;
				qSMRunning := FALSE;
				NState := 60;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR4WZ = FALSE
					THEN
					tAlarmNo := 50;
				ELSIF iSMReadyHome = FALSE
					THEN
					tAlarmNo := 51;
				END_IF;
			END_IF;
			
			
		60:
			
			IF tConditions
				AND iR4WZ
				AND iSMReadyHome
				AND iSMClampUpSns
				THEN
				qSMCycleOkForRobot := FALSE;
				NState := 70;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR4WZ = FALSE
					THEN
					tAlarmNo := 60;
				ELSIF iSMReadyHome = FALSE
					THEN
					tAlarmNo := 61;
				ELSIF iSMClampUpSns = FALSE
					THEN
					tAlarmNo := 62;
				END_IF;
			END_IF;
			
			
		70:
			
			//
			NState := 80;
			//
			
		80:
			
			//
			NState := 90;
			//
			
		90:
			
			//
			NState := 100;
			//
			
		100:
			
			//
			NState := 110;
			//
			
		110:
			
			//
			NState := 10;
		//
		
	END_CASE;
	
	
	//	Generate Alarm No
	IF iAirOk = FALSE THEN
		tAlarmNo := 200;
	ELSIF iSafetyOk = FALSE THEN
		tAlarmNo := 201;
	ELSIF iEmgOk= FALSE  THEN
		tAlarmNo := 202;
	ELSIF iSMLockClamp = FALSE THEN
		tAlarmNo := 203;
	ELSIF iSystemAuto = FALSE THEN
		tAlarmNo := 205;
	ELSIF iSMThreadBreakageError THEN
		tAlarmNo := 206;
	ELSIF iSMStopError THEN
		tAlarmNo := 207;
	END_IF;
	
	
	IF tAlarmNo <> tOldAlarmNo
		THEN
		AlarmRefreshTimer.IN := FALSE;
		qAlarmNo := 0;
	END_IF;
	
	IF tAlarmNo = tOldAlarmNo
		AND TimerStateNo.Q
		THEN
		AlarmRefreshTimer.IN := TRUE;
	END_IF;

	
	IF NState <> tOldNState
		THEN
		TimerStateNo.IN := FALSE;
	END_IF;

	IF NState = tOldNState 
		THEN
		TimerStateNo.IN := TRUE;
	END_IF;
	
	IF AlarmRefreshTimer.Q AND NOT Pulse
		THEN
		qAlarmNo := tAlarmNo;
	END_IF;
	Pulse := AlarmRefreshTimer.Q;

	
	tOldAlarmNo := tAlarmNo;
	tOldNState := NState;
	qStateNo := NState;
	
	Start1PulseTimer ();
	WaitTimer1 ();
	AlarmRefreshTimer ();
	TimerStateNo ();
	
	
END_FUNCTION_BLOCK
