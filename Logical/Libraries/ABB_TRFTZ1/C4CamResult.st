
(* TODO: Add your comment here *)
FUNCTION_BLOCK C4CamResult
	
	WaitTimer.PT := T#250ms;
	
	IF iReadyResult
		THEN
		Buffer_XPos1 := ReadCamPos1_X;//- RefPosX;
		IF Buffer_XPos1 < 0 THEN
			qRobotPos1_X := Buffer_XPos1 * -1;
			qRobotPos1_XNgtveBit := TRUE;
		ELSE
			qRobotPos1_X := Buffer_XPos1;
			qRobotPos1_XPstveBit := TRUE;
		END_IF;
		
		Buffer_YPos1 := ReadCamPos1_Y;// - RefPosY;
		IF Buffer_YPos1 < 0 THEN
			qRobotPos1_Y := Buffer_YPos1 * -1;
			qRobotPos1_YNgtveBit := TRUE;
		ELSE
			qRobotPos1_Y := Buffer_YPos1;
			qRobotPos1_YPstveBit := TRUE;
		END_IF;
		
		Buffer_0Pos1 := ReadCamPos1_0;// - RefPos0;
		IF Buffer_0Pos1 < 0 THEN
			qRobotRot1_Z := Buffer_0Pos1 * -1;
			qRobotRot1_ZNgtveBit := TRUE;
		ELSE
			qRobotRot1_Z := Buffer_0Pos1;
			qRobotRot1_ZPstveBit := TRUE;
		END_IF;
		
		Buffer_XPos2 := ReadCamPos2_X;//- RefPosX;
		IF Buffer_XPos2 < 0 THEN
			qRobotPos2_X := Buffer_XPos2 * -1;
			qRobotPos2_XNgtveBit := TRUE;
		ELSE
			qRobotPos2_X := Buffer_XPos2;
			qRobotPos2_XPstveBit := TRUE;
		END_IF;
		
		Buffer_YPos2 := ReadCamPos2_Y;// - RefPosY;
		IF Buffer_YPos2 < 0 THEN
			qRobotPos2_Y := Buffer_YPos2 * -1;
			qRobotPos2_YNgtveBit := TRUE;
		ELSE
			qRobotPos2_Y := Buffer_YPos2;
			qRobotPos2_YPstveBit := TRUE;
		END_IF;
		
		Buffer_0Pos2 := ReadCamPos2_0;// - RefPos0;
		IF Buffer_0Pos2 < 0 THEN
			qRobotRot2_Z := Buffer_0Pos2 * -1;
			qRobotRot2_ZNgtveBit := TRUE;
		ELSE
			qRobotRot2_Z := Buffer_0Pos2;
			qRobotRot2_ZPstveBit := TRUE;
		END_IF;
		
		WaitTimer.IN := TRUE;
		
	END_IF;
	
	qRobotSendValue := WaitTimer.Q;
	
	IF iClearDatas
		THEN
		qRobotPos1_X := 0;
		qRobotPos1_Y := 0;
		qRobotRot1_Z := 0;
		qRobotPos1_XNgtveBit := FALSE;
		qRobotPos1_XPstveBit := FALSE;
		qRobotPos1_YNgtveBit := FALSE;
		qRobotPos1_YPstveBit := FALSE;
		qRobotRot1_ZNgtveBit := FALSE;
		qRobotRot1_ZPstveBit := FALSE;
		qRobotPos2_X := 0;
		qRobotPos2_Y := 0;
		qRobotRot2_Z := 0;
		qRobotPos2_XNgtveBit := FALSE;
		qRobotPos2_XPstveBit := FALSE;
		qRobotPos2_YNgtveBit := FALSE;
		qRobotPos2_YPstveBit := FALSE;
		qRobotRot2_ZNgtveBit := FALSE;
		qRobotRot2_ZPstveBit := FALSE;
		WaitTimer.IN := FALSE;
	END_IF;
	
	WaitTimer();
		
END_FUNCTION_BLOCK
