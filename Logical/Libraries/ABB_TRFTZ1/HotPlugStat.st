
(* TODO: Add your comment here *)
FUNCTION_BLOCK HotPlugStat
	
	BlinkTimer.PT := T#1000ms;
	BlinkTimer.IN := NOT BlinkTimer.Q;
	
	TimerPT_Int_Value := TIME_TO_INT(BlinkTimer.ET);
	
	IF TimerPT_Int_Value >= 500
		THEN
		bBlink := TRUE;
	ELSE
		bBlink := FALSE;
	END_IF;
	
	IF iButton
		AND iTimer = FALSE
		AND bBlink
		THEN
		tLamp1 := TRUE;
	ELSE
		tLamp1 := FALSE;
	END_IF;
	
	IF tLamp1 =FALSE
		AND iTimer
		THEN
		tLamp2 := TRUE;
	ELSE
		tLamp2 := FALSE;
	END_IF;
	
	qButtonLamp := tLamp1 OR tLamp2;
		
	
	BlinkTimer();
		
END_FUNCTION_BLOCK
