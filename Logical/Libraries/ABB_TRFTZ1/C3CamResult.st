
(* TODO: Add your comment here *)
FUNCTION_BLOCK C3CamResult
		
	WaitTimer.PT := T#250ms;
	
	IF iReadyResult
		THEN
		Buffer_XPos := ReadCamPos_X;//- RefPosX;
		IF Buffer_XPos < 0 THEN
			qRobotPos_X := Buffer_XPos * -1;
			qRobotPos_XNgtveBit := TRUE;
		ELSE
			qRobotPos_X := Buffer_XPos;
			qRobotPos_XPstveBit := TRUE;
		END_IF;
		
		Buffer_YPos := ReadCamPos_Y;// - RefPosY;
		IF Buffer_YPos < 0 THEN
			qRobotPos_Y := Buffer_YPos * -1;
			qRobotPos_YNgtveBit := TRUE;
		ELSE
			qRobotPos_Y := Buffer_YPos;
			qRobotPos_YPstveBit := TRUE;
		END_IF;
		
		Buffer_0Pos := ReadCamPos_0;// - RefPos0;
		IF Buffer_0Pos < 0 THEN
			qRobotRot_Z := Buffer_0Pos * -1;
			qRobotRot_ZNgtveBit := TRUE;
		ELSE
			qRobotRot_Z := Buffer_0Pos;
			qRobotRot_ZPstveBit := TRUE;
		END_IF;

		
		WaitTimer.IN := TRUE;
		
	END_IF;
	
	qRobotSendValue := WaitTimer.Q;
	
	IF iClearDatas
		THEN
		qRobotPos_X := 0;
		qRobotPos_Y := 0;
		qRobotRot_Z := 0;
		qRobotPos_XNgtveBit := FALSE;
		qRobotPos_XPstveBit := FALSE;
		qRobotPos_YNgtveBit := FALSE;
		qRobotPos_YPstveBit := FALSE;
		qRobotRot_ZNgtveBit := FALSE;
		qRobotRot_ZPstveBit := FALSE;
		WaitTimer.IN := FALSE;
	END_IF;
	
	WaitTimer();
		
END_FUNCTION_BLOCK
