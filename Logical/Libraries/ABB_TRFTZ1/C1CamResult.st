
(* TODO: Add your comment here *)
FUNCTION_BLOCK C1CamResult
	
	WaitTimer.PT := T#250ms;
	
	IF iReadyResult
		THEN
		Buffer_XPos := ReadCamPosX - RefPosX;
		IF Buffer_XPos < 0 THEN
			qRobotPosX := Buffer_XPos * -1;
			qRobotPosXNgtveBit := TRUE;
		ELSE
			qRobotPosX := Buffer_XPos;
			qRobotPosXPstveBit := TRUE;
		END_IF;
		
		Buffer_YPos := ReadCamPosY - RefPosY;
		IF Buffer_YPos < 0 THEN
			qRobotPosY := Buffer_YPos * -1;
			qRobotPosYNgtveBit := TRUE;
		ELSE
			qRobotPosY := Buffer_YPos;
			qRobotPosYPstveBit := TRUE;
		END_IF;
		
		Buffer_0Pos := ReadCamPos0 - RefPos0;
		IF Buffer_0Pos < 0 THEN
			qRobotRotZ := Buffer_0Pos * -1;
			qRobotRotZNgtveBit := TRUE;
		ELSE
			qRobotRotZ := Buffer_0Pos;
			qRobotRotZPstveBit := TRUE;
		END_IF;
		
		WaitTimer.IN := TRUE;
		
	END_IF;
	
	qRobotSendValue := WaitTimer.Q;
	
	IF iClearDatas
		THEN
		qRobotPosX := 0;
		qRobotPosY := 0;
		qRobotRotZ := 0;
		qRobotPosXNgtveBit := FALSE;
		qRobotPosXPstveBit := FALSE;
		qRobotPosYNgtveBit := FALSE;
		qRobotPosYPstveBit := FALSE;
		qRobotRotZNgtveBit := FALSE;
		qRobotRotZPstveBit := FALSE;
		WaitTimer.IN := FALSE;
	END_IF;
	
	WaitTimer();
		
		
END_FUNCTION_BLOCK
