
(* TODO: Add your comment here *)
FUNCTION_BLOCK CamTrig
	

	WaitTimer1.PT := T#500ms;
	WaitTimer2.PT := T#500ms;
	AlarmRefreshTimer.PT := T#3S;
	TimerStateNo.PT := T#3S;
	
	CASE NState OF
		
		0:	// Wait online From Camera
			
			
			IF iCamOnline
				THEN
				NState := 10;
			END_IF;
			
			//	Generate Alarm No
			IF iCamOnline = FALSE
				THEN
				tAlarmNo := 11;
			END_IF;
			
			
		10:	// Wait Trigger From Robot 
			
			
			IF iCamOnline
				AND iTrigger
				THEN
				qCamClear := TRUE;
				WaitTimer1.IN := TRUE;
				NState := 20;
			END_IF;
		
			//	Generate Alarm No
			IF iCamOnline = FALSE
				THEN
				tAlarmNo := 21;
			ELSIF iTrigger = FALSE
				THEN
				tAlarmNo := 22;
			END_IF;
			
			
		20:	// Clear Datas
			

			qCamClear := TRUE;
			
			IF WaitTimer1.Q
				THEN
				qCamClear := FALSE;
				NState := 30;
			END_IF;
			
			
		30:	// Wait Trigger Ready From Camera
			
			
			WaitTimer1.IN := FALSE;
			
			IF iCamOnline
				AND iCamTrigReady
				THEN
				qLightOn := TRUE;
				WaitTimer2.IN := TRUE;
				NState := 40;
			END_IF;
			
			//	Generate Alarm No
			IF iCamOnline = FALSE
				THEN
				tAlarmNo := 31;
			ELSIF iCamTrigReady = FALSE
				THEN
				tAlarmNo := 32;
			END_IF;	
			
			
		40:	// Send to Camera Trigger
			
			
			IF WaitTimer2.Q
				AND iCamOnline
				THEN
				qCamTrig := TRUE;
				NState := 50;
			END_IF;
			
			
			//	Generate Alarm No
			IF iCamOnline = FALSE
				THEN
				tAlarmNo := 41;
			END_IF;
			
			
		50:	// Wait Result Ready From Camera

			
			WaitTimer2.IN := FALSE;
			
			IF iCamOnline
				AND iCamResultReady
				THEN
				qCamTrig := FALSE;
				qLightOn := FALSE;
				NState := 60;
			END_IF;
			
			//	Generate Alarm No
			IF iCamOnline = FALSE
				THEN
				tAlarmNo := 51;
			ELSIF iCamResultReady = FALSE
				THEN
				tAlarmNo := 52;
			END_IF;

			
		60:	
			

			//
			NState := 70;
			//
			
			
		70:	
			
			
			//
			NState := 80;
			//
			

		80:
			
			
			//
			NState := 90;
			//
		
			
		90:
			
			
			//
			NState := 0;
			//
		
		
	END_CASE;
	
	IF (EDGENEG (iCamOnline))
		THEN
		qCamClear := FALSE;
		qCamTrig := FALSE;
		qLightOn := FALSE;
		WaitTimer1.IN := FALSE;
		NState := 0;
	END_IF;
	
	
	IF tAlarmNo <> tOldAlarmNo
		THEN
		AlarmRefreshTimer.IN := FALSE;
		qAlarmNo := 0;
	END_IF;
	
	IF tAlarmNo = tOldAlarmNo
		AND TimerStateNo.Q
		THEN
		AlarmRefreshTimer.IN := TRUE;
	END_IF;

	
	IF NState <> tOldNState
		THEN
		TimerStateNo.IN := FALSE;
	END_IF;

	IF NState = tOldNState 
		THEN
		TimerStateNo.IN := TRUE;
	END_IF;
	
	IF (EDGEPOS (AlarmRefreshTimer.Q))
		THEN
		qAlarmNo := tAlarmNo;
	END_IF;
	
	
	tOldAlarmNo := tAlarmNo;
	tOldNState := NState;
	qStateNo := NState;
	
		
	WaitTimer1( );
	WaitTimer2( );			
	AlarmRefreshTimer ();
	TimerStateNo ();
		
END_FUNCTION_BLOCK
