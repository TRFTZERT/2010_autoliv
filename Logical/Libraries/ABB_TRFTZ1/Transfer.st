
(* TODO: Add your comment here *)
FUNCTION_BLOCK Transfer
	
	AlarmRefreshTimer.PT := T#4S;
	TimerStateNo.PT := T#4S;
	
	IF iAirOk
		AND iSafetyOk
		AND iEmgOk
		AND iToolchangerLock
		AND iCamOnline
		AND iSystemAuto
		THEN
		tConditions := TRUE;
	ELSE
		tConditions := FALSE;
	END_IF;
	
	IF tConditions = FALSE
		THEN
		NState := 0;
	END_IF;
	
	CASE NState OF
		
		
		0:
			
			
			IF tConditions
				AND iLineerFwdSensor
				THEN
				NState := 1;
			END_IF;
				
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iLineerFwdSensor = FALSE
					THEN
					tAlarmNo := 1;
				END_IF;
			END_IF;
			
			
		1:	// Folding cylinder initioal position
			
			
			qFoldingFwd := FALSE;
			
			IF tConditions
				AND iLineerFwdSensor
				AND iFoldingBwdSensor
				THEN
				NState := 2;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iLineerFwdSensor = FALSE
					THEN
					tAlarmNo := 2;
				ELSIF iFoldingBwdSensor = FALSE
					THEN
					tAlarmNo := 3;
				END_IF;
			END_IF;
			
			
		2:	// Pick cylinder initioal position
			
			
			qPickDown := FALSE;
			
			IF tConditions
				AND iLineerFwdSensor
				AND iFoldingBwdSensor
				AND iPickUpSensor
				THEN
				NState := 3;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iLineerFwdSensor = FALSE
					THEN
					tAlarmNo := 4;
				ELSIF iFoldingBwdSensor = FALSE
					THEN
					tAlarmNo := 5;
				ELSIF iPickUpSensor = FALSE
					THEN
					tAlarmNo := 6;
				END_IF;
			END_IF;
			
			
		3:	// Rotary cylinder initioal position
			
			
			qRotaryDown := FALSE;
			qRotaryUp := TRUE;
			
			IF tConditions
				AND iLineerFwdSensor
				AND iFoldingBwdSensor
				AND iPickUpSensor
				AND iRotaryUpSensor
				THEN
				NState := 10;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iLineerFwdSensor = FALSE
					THEN
					tAlarmNo := 7;
				ELSIF iFoldingBwdSensor = FALSE
					THEN
					tAlarmNo := 8;
				ELSIF iPickUpSensor = FALSE
					THEN
					tAlarmNo := 9;
				ELSIF iRotaryUpSensor = FALSE
					THEN
					tAlarmNo := 10;
				END_IF;
			END_IF;
			
			
		10: // INITIAL POSITION
			
			
			qPickDown := FALSE;
			qFoldingFwd := FALSE;
			qRotaryDown := FALSE;
			qRotaryUp := TRUE;
			
			IF tConditions
				AND iCell2RWZ
				THEN
				qReadyToR2 := TRUE;
			END_IF;

			IF tConditions 
				AND iCell2RWZ
				AND iCell2CamOk
				THEN
				NState := 20;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iCell2CamOk = FALSE
					THEN
					tAlarmNo := 12;
				ELSIF iCell2RWZ = FALSE
					THEN
					tAlarmNo := 13;
				END_IF;
			END_IF;
			
			
		20: // LINEER CYLINDER MOVE TO CELL2
		
			
			qReadyToR2 := FALSE;
			qLineerFwd := FALSE;
			qCamClear := TRUE;
			qLineerBwd := TRUE;
			
			IF tConditions
				AND iLineerBwdSensor
				THEN
				NState := 30;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iLineerBwdSensor = FALSE
					THEN
					tAlarmNo := 20;
				END_IF;
			END_IF;
			
			
		30: // TAKE PART
			
			
			qCamClear := FALSE;
			qPickDown := TRUE;
			qVacum1 := TRUE;
			qVacum2 := TRUE;
			tPickDownTime.PT := T#1S ;
			tPickDownTime.IN := TRUE;
			
			IF tConditions
				AND iVacum1Ok
				AND tPickDownTime.Q 
				THEN
				NState := 40;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 30;
				END_IF;
			END_IF;
			
			
		40: //Picker Move to above
			
			
			tPickDownTime.IN := FALSE;
			qPickDown := FALSE;
			
			IF tConditions
				AND iPickUpSensor
				AND iVacum1Ok
				THEN
				NState := 50;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iPickUpSensor = FALSE
					THEN
					tAlarmNo := 40;
				ELSIF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 41;
				END_IF;
			END_IF;
			
				
		50: // Lineer cylinder Move to Forward
			
			
			IF tConditions
				AND iCell3RWZ
				AND iVacum1Ok
				THEN
				qLineerFwd := TRUE;
				qLineerBwd := FALSE;
				NState := 55;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iCell3RWZ = FALSE
					THEN
					tAlarmNo := 50;
				ELSIF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 51;
				END_IF;
			END_IF;
			
			
		55:
			
		
			IF tConditions
				AND iLineerFwdSensor
				AND iVacum1Ok
				THEN
				NState := 60;
			END_IF;
		
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iLineerFwdSensor = FALSE
					THEN
					tAlarmNo := 55;
				ELSIF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 56;
				END_IF;
			END_IF;
			
			
		60:	//Rotary cylinder turn to down
			
			
			IF tConditions
				AND iLineerFwdSensor
				AND iVacum1Ok
				THEN
				qRotaryDown := TRUE;
				qRotaryUp := FALSE;
				NState := 65;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iLineerFwdSensor = FALSE
					THEN
					tAlarmNo := 60;
				ELSIF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 61;
				END_IF;
			END_IF;
			
			
		65:
			
			
			IF tConditions
				AND iRotaryDownSensor
				AND iVacum1Ok
				THEN
				NState := 70;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iRotaryDownSensor = FALSE
					THEN
					tAlarmNo := 65;
				ELSIF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 66;
				END_IF;
			END_IF;
			
			
		70:	// Folding
			
			
			IF tConditions
				THEN
				qVacum2 := FALSE;
				qFoldingFwd := TRUE;
				tFoldingWaitTime.PT := T#2S;
				tFoldingWaitTime.IN := TRUE;
			END_IF;
			
			IF tFoldingWaitTime.Q 
				AND iFoldingBwdSensor = FALSE
				AND iVacum1Ok
				THEN
				qFoldingFwd := FALSE;
				NState := 80;
			END_IF;
			
			//	Generate Alarm No
			IF tFoldingWaitTime.Q
				THEN
				IF iFoldingBwdSensor = FALSE
					THEN
					tAlarmNo := 70;
				ELSIF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 71;
				END_IF;
			END_IF;
			
			
		80:	// Part ready to Robot
			
			tFoldingWaitTime.IN := FALSE;
			
			IF tConditions
				AND iFoldingBwdSensor
				AND iVacum1Ok
				THEN
				qPartReadyToR3 := TRUE;
				//qVacum1 := FALSE;
				NState := 90;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iFoldingBwdSensor = FALSE
					THEN
					tAlarmNo := 80;
				ELSIF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 81;
				END_IF;
			END_IF;
			
			
			
		90:	// Robot takes part
			
			
			IF tConditions
				AND iCell3RWZ = FALSE
				AND iR3VacumOk
				AND iVacum1Ok
				AND iVacumOff
				THEN
				qPartReadyToR3 := FALSE;
				qVacum1 := FALSE;
				NState := 91;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3VacumOk = FALSE
					THEN
					tAlarmNo := 90;
				ELSIF iCell3RWZ
					THEN
					tAlarmNo := 91;
				ELSIF iVacum1Ok = FALSE
					THEN
					tAlarmNo := 92;
				ELSIF iVacumOff = FALSE
					THEN
					tAlarmNo := 93;
				END_IF;
			END_IF;
			
		
		91:	// Product idle
			
			
			IF tConditions
				AND iCell3RWZ = FALSE
				AND iR3VacumOk
				AND iVacum1Ok = FALSE
				AND iVacumOff
				THEN
				qProductIdle := TRUE;
				NState := 100;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iR3VacumOk = FALSE
					THEN
					tAlarmNo := 94;
				ELSIF iCell3RWZ
					THEN
					tAlarmNo := 95;
				ELSIF iVacum1Ok
					THEN
					tAlarmNo := 96;
				ELSIF iVacumOff = FALSE
					THEN
					tAlarmNo := 97;
				END_IF;
			END_IF;
			
			
		100:	//Move to initial position
			
			
			IF tConditions
				AND iCell3RWZ
				AND iR3PartPicked
				THEN
				qProductIdle := FALSE;
				qRotaryDown := FALSE;
				qRotaryUp := TRUE;
				NState := 110;
			END_IF;
			
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iCell3RWZ = FALSE
					THEN
					tAlarmNo := 100;
				ELSIF iR3PartPicked = FALSE
					THEN
					tAlarmNo := 101;
				END_IF;
			END_IF;
			
			
		110:
			
			
			IF tConditions
				AND iRotaryUpSensor
				THEN
				NState :=10;
			END_IF;
		
			//	Generate Alarm No
			IF tConditions
				THEN
				IF iRotaryUpSensor = FALSE
					THEN
					tAlarmNo := 110;
				END_IF;
			END_IF;
		
	END_CASE;
	
	
	//	Generate Alarm No
	IF iAirOk = FALSE
		THEN
		tAlarmNo := 200;
	ELSIF iSafetyOk = FALSE
		THEN
		tAlarmNo := 201;
	ELSIF iEmgOk= FALSE
		THEN
		tAlarmNo := 202;
	ELSIF iCamOnline = FALSE
		THEN
		tAlarmNo := 203;
	ELSIF iToolchangerLock = FALSE
		THEN
		tAlarmNo := 204;
	ELSIF iSystemAuto = FALSE
		THEN
		tAlarmNo := 205;
	END_IF;

	
	
	IF tAlarmNo <> tOldAlarmNo
		THEN
		AlarmRefreshTimer.IN := FALSE;
		qAlarmNo := 0;
	END_IF;
	
	IF tAlarmNo = tOldAlarmNo
		AND TimerStateNo.Q
		THEN
		AlarmRefreshTimer.IN := TRUE;
	END_IF;

	
	IF NState <> tOldNState
		THEN
		TimerStateNo.IN := FALSE;
	END_IF;

	IF NState = tOldNState 
		THEN
		TimerStateNo.IN := TRUE;
	END_IF;
	
	IF (EDGEPOS (AlarmRefreshTimer.Q))
		THEN
		qAlarmNo := tAlarmNo;
	END_IF;
	
	
	tOldAlarmNo := tAlarmNo;
	tOldNState := NState;
	qStateNo := NState;
	
	
	tPickDownTime ();
	tFoldingWaitTime ();
	AlarmRefreshTimer ();
	TimerStateNo ();
	
END_FUNCTION_BLOCK
