
{REDUND_ERROR} FUNCTION_BLOCK Lighting_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iCycleOn : {REDUND_UNREPLICABLE} BOOL;
		iSafetyOk : {REDUND_UNREPLICABLE} BOOL;
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iSystemManual : BOOL;
		iManOpen : BOOL;
	END_VAR
	VAR_OUTPUT
		qLighting : BOOL;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		tAutoLighting : BOOL;
		TON_0 : TON;
		RS_0 : RS;
		R_TRIG_0 : R_TRIG;
		F_TRIG_1 : F_TRIG;
		F_TRIG_0 : F_TRIG;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Conveyor_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iAct : {REDUND_UNREPLICABLE} BOOL;
		iLB : {REDUND_UNREPLICABLE} BOOL;
		iSysAuto : {REDUND_UNREPLICABLE} BOOL;
		iSysMan : {REDUND_UNREPLICABLE} BOOL;
		iDriveFault : {REDUND_UNREPLICABLE} BOOL;
		iDriveFuse : {REDUND_UNREPLICABLE} BOOL;
		iLoadSns : {REDUND_UNREPLICABLE} BOOL;
		iUnloadSns : {REDUND_UNREPLICABLE} BOOL;
		iStart : {REDUND_UNREPLICABLE} BOOL;
		iDirection : {REDUND_UNREPLICABLE} BOOL;
		iManStart : {REDUND_UNREPLICABLE} BOOL;
		newParam13 : {REDUND_UNREPLICABLE} BOOL;
		newParam14 : {REDUND_UNREPLICABLE} BOOL;
		newParam15 : {REDUND_UNREPLICABLE} BOOL;
		newParam16 : {REDUND_UNREPLICABLE} BOOL;
		newParam17 : {REDUND_UNREPLICABLE} BOOL;
		newParam18 : {REDUND_UNREPLICABLE} BOOL;
		newParam19 : {REDUND_UNREPLICABLE} BOOL;
		newParam20 : {REDUND_UNREPLICABLE} BOOL;
		newParam21 : {REDUND_UNREPLICABLE} BOOL;
		newParam22 : {REDUND_UNREPLICABLE} BOOL;
		newParam23 : {REDUND_UNREPLICABLE} BOOL;
		newParam24 : {REDUND_UNREPLICABLE} BOOL;
		newParam25 : {REDUND_UNREPLICABLE} BOOL;
		newParam26 : {REDUND_UNREPLICABLE} BOOL;
		newParam27 : {REDUND_UNREPLICABLE} BOOL;
		newParam28 : {REDUND_UNREPLICABLE} BOOL;
		newParam29 : {REDUND_UNREPLICABLE} BOOL;
		newParam30 : {REDUND_UNREPLICABLE} BOOL;
		newParam31 : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qStart : BOOL;
		qDirection : BOOL;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SuctionForFoldingSpace_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSuctionCupUpSignal : {REDUND_UNREPLICABLE} BOOL;
		iSuctionCupDownSignal : {REDUND_UNREPLICABLE} BOOL;
		iSuctionOk : {REDUND_UNREPLICABLE} BOOL;
		iSuctionCupUp : BOOL;
		iSuctionCupDown : BOOL;
		iSuction : BOOL;
		iBlow : BOOL;
		iSystemAuto : BOOL;
		iSystemManual : BOOL;
		iManSuctionCupUp : BOOL;
		iManSuctionCupDown : BOOL;
		iManSuction : BOOL;
		iManBlow : {REDUND_UNREPLICABLE} BOOL;
		iEmgOk : BOOL;
		iSafetyOk : BOOL;
		iAirOk : BOOL;
	END_VAR
	VAR_OUTPUT
		qSuctionCupUp : BOOL;
		qSuctionCupDown : BOOL;
		qSuction : BOOL;
		qBlow : BOOL;
		qSuctionOk : BOOL;
		qSuctionCupUpFault : BOOL;
		qSuctionCupDownFault : BOOL;
		qSuctionFault : BOOL;
		qCylndrAlarmNo : INT;
		qVacumAlarmNo : INT;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		Suction_For_Table : SuctionFB;
		Cylinder_For_Table : Piston5_3;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK ButtonBox_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iHucre_GSOK : BOOL;
		iHucre_ASOK : BOOL;
		iSysOut_AutoON : BOOL;
		iSysOut_CycleON : BOOL;
		iSysOut_PPMoved : BOOL;
		iSGOk : BOOL;
		iOP_StartAtMainBtn : BOOL;
		iOP_GirisIzinBtn : BOOL;
		iOP_ResetBtn : BOOL;
		iOP_StopBtn : BOOL;
	END_VAR
	VAR_OUTPUT
		qSysIn_PPtoMain : BOOL;
		qSysIn_MotorOnStart : BOOL;
		qSysIn_ExeErrReset : BOOL;
		qSysIn_EmgErrReset : BOOL;
		qAlarmReset : BOOL;
		qOP_StartLmb : BOOL;
		qOP_GirisIzinLmb : BOOL;
		qOP_ResetLmb : BOOL;
		qGirisIstegiVar : BOOL;
		qSysIn_Stop : BOOL;
		qOP_StopLmb : BOOL;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		TON1 : TON;
		b250msTRUE : BOOL;
		b250msFALSE : BOOL;
		bGirisIzinIstekVar : BOOL;
		bOP_GirisIzinSurekiYan : BOOL;
		bOP_GirisIzinKesikliYan : BOOL;
		bOP_ResetKesikliYan : BOOL;
		bOP_ResetSurekliYan : BOOL;
		bOP_StartKesikliYan : BOOL;
		bOP_StartSurekiYan : BOOL;
		bOP_StartAtMainKesikliYan : BOOL;
		bOP_StartAtMainSurekiYan : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK LightTower_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Hucre_GSOK : BOOL;
		Hucre_ASOK : BOOL;
		AlarmVar : BOOL;
		UyariVar : BOOL;
		SysOut_CycleON : BOOL;
		OP_ResetBtn : BOOL;
	END_VAR
	VAR_OUTPUT
		Kule_Kirmizi : BOOL;
		Kule_Sari : BOOL;
		Kule_Yesil : BOOL;
		Kule_Buzzer : BOOL;
	END_VAR
	VAR
		zzInternalMemory : ARRAY[0..12] OF SINT; (*Internal memory*)
		b250msFALSE : BOOL;
		b250msTRUE : BOOL;
		TON1 : TON;
		bKule_KirmiziSurekliYan : BOOL;
		bKule_KirmiziKesikliYan : BOOL;
		bKule_SariSurekliYan : BOOL;
		bKule_SariKesikliYan : BOOL;
		bKule_YesilSurekliYan : BOOL;
		bKule_YesilKesikliYan : BOOL;
		TON2 : TON;
		bKule_BuzzerKesikliYan : BOOL;
		bBuzzerSus : BOOL;
		bKule_BuzzerSurekliYan : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CameraBlow_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iCamScreenEmpty : {REDUND_UNREPLICABLE} BOOL;
		iRobotWZ : {REDUND_UNREPLICABLE} BOOL;
		iSystemAuto : BOOL;
		iSystemManual : BOOL;
		iManualBlow : BOOL;
		iWaitTime : {REDUND_UNREPLICABLE} TIME;
		iBlowTime : {REDUND_UNREPLICABLE} TIME;
	END_VAR
	VAR_OUTPUT
		qBlow : BOOL;
		qTimeOutFault : BOOL;
	END_VAR
	VAR
		tBlow : BOOL;
		TON_BlowWait : TON;
		TON_BlowOn : TON;
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Stacker_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iOpenedSignal : {REDUND_UNREPLICABLE} BOOL;
		iClosedSignal : {REDUND_UNREPLICABLE} BOOL;
		iEmpty : BOOL;
		iWZSignal : BOOL;
		iAirOk : BOOL;
		iSGOk : BOOL;
		iStackerMgntcSns : BOOL;
		iSystemAuto : BOOL;
		iSystemManual : BOOL;
		iManualOpen : {REDUND_UNREPLICABLE} BOOL;
		iManualClose : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qOpenValve : BOOL;
		qCloseValve : BOOL;
		qOpenFault : BOOL;
		qCloseFault : BOOL;
		qStackerReady : BOOL;
		qCanOpenGate : BOOL;
		qAlarmNo : INT;
	END_VAR
	VAR
		Valve : Piston5_3;
		zzInternalMemory : ARRAY[0..9] OF SINT; (*Internal memory*)
		RS_Close : RS;
		R_TRIG_0 : R_TRIG;
		RS_Open : RS;
		tOpen : BOOL;
		tClose : BOOL;
		F_TRIG_0 : F_TRIG;
		R_TRIG_1 : R_TRIG;
		DoorOpened : RS;
		DoorClosed : RS;
		bStackerReady : RS;
		tCloseFault1 : BOOL;
		tCloseFault2 : BOOL;
		TON1 : TON;
		R_TRIG_2 : R_TRIG;
		R_TRIG_3 : R_TRIG;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Mode_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iAutoStop : {REDUND_UNREPLICABLE} BOOL;
		iEmgAct : {REDUND_UNREPLICABLE} BOOL;
		iReset : {REDUND_UNREPLICABLE} BOOL;
		iClockPulse : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qAutoMode : {REDUND_UNREPLICABLE} BOOL;
		qManuelMode : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR
		qHMI : HMIMode;
		tAuto : BOOL;
		sAuto : BOOL;
		iReserveMod : ARRAY[1..4] OF BOOL;
		qReserveMod : ARRAY[1..4] OF BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK ValveControlFB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=pneumatic-2-way-solenoid-valve-icon-vectorr.png,$CATICON=pneumatic-2-way-solenoid-valve-icon-vectorr.png*)
	VAR_INPUT
		iInterlock : BOOL;
		iLightBeam : BOOL;
		iSet_Sns : ARRAY[1..16] OF BOOL;
		iRst_Sns : ARRAY[1..16] OF BOOL;
		iSysAut : BOOL;
		iSysManuel : BOOL;
		iMan_Set : BOOL;
		iAuto_Set : BOOL;
		iSep_Set : BOOL;
		iMan_Rst : BOOL;
		iAuto_Rst : BOOL;
		iSep_Rst : BOOL;
		iTest_Set : BOOL;
		iTest_Rst : BOOL;
		iAuto_Mid : BOOL;
		iMan_Mid : BOOL;
		iBypass : BOOL;
		iPartless : BOOL;
		iOtherSide : BOOL;
		iSystemStep : USINT;
		sAct : ARRAY[1..16] OF BOOL;
	END_VAR
	VAR_OUTPUT
		qValveSet : BOOL;
		qValveRst : BOOL;
		qValveInitPos : BOOL;
		qSetRun : BOOL;
		qRstRun : BOOL;
		qError : BOOL;
		qSetFault : BOOL;
		hSFSnsAlarm : ARRAY[1..16] OF BOOL;
		qRstFault : BOOL;
		hRFSnsAlarm : ARRAY[1..16] OF BOOL;
		hSnsState : ARRAY[1..16] OF USINT;
		hDiag : hDiag;
		hAlarm : USINT;
		htrig : BOOL;
	END_VAR
	VAR
		sTimerTrg : TON;
		sTimerTrg_IN : BOOL;
		sTimerTrg_PT : TIME;
		sTimerS : TON;
		sTimerS_IN : BOOL;
		sTimerS_PT : TIME;
		sTimerR : TON;
		sTimerR_IN : BOOL;
		sTimerR_PT : TIME;
		sValf_ileride : BOOL;
		sValf_Geride : BOOL;
		sHataYok : BOOL;
		sSetSensOK : BOOL;
		sResetSensOK : BOOL;
		sSSensorHata : BOOL;
		sRSensorHata : BOOL;
	END_VAR
	VAR RETAIN
		sSet : BOOL;
		sReset : BOOL;
	END_VAR
	VAR
		sTrig : BOOL;
	END_VAR
	VAR RETAIN
		sAutoSetMemory : BOOL;
	END_VAR
	VAR
		sAutoSetMemoryWork : BOOL;
	END_VAR
	VAR RETAIN
		sAutoRstMemory : BOOL;
	END_VAR
	VAR
		sAutoRstMemoryWork : BOOL;
		sOld : USINT;
	END_VAR
	VAR RETAIN
		sSnsState : ARRAY[1..16] OF USINT;
	END_VAR
	VAR
		tFault1 : BOOL;
		tFault2 : BOOL;
		tFault3 : BOOL;
		tProcess : BOOL;
		tActSns : BOOL;
		tHMIDiag : hDiag;
	END_VAR
	VAR CONSTANT
		CodeNo : ARRAY[1..16] OF USINT := [11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26];
	END_VAR
	VAR
		ForNT6 : USINT;
		ForNT13 : USINT;
		ForNT11 : USINT;
		ForNT26 : USINT;
		SShot : ARRAY[0..5] OF BOOL;
		SLine1 : BOOL;
		SLine2 : BOOL;
		SLine3 : BOOL;
		SLine4 : BOOL;
		SLine5 : BOOL;
		RShot : ARRAY[0..6] OF BOOL;
		RLine1 : BOOL;
		RLine2 : BOOL;
		RLine3 : BOOL;
		RLine4 : BOOL;
		RLine5 : BOOL;
		zzEdge00000 : BOOL;
		zzEdge00001 : BOOL;
		zzEdge00002 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK ALT (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		In : BOOL;
	END_VAR
	VAR_OUTPUT
		Out : BOOL;
	END_VAR
	VAR
		Stat : BOOL;
		Alt : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Transfer (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iAirOk : BOOL;
		iSafetyOk : BOOL;
		iEmgOk : BOOL;
		iToolchangerLock : BOOL;
		iSystemAuto : BOOL;
		iLineerBwdSensor : {REDUND_UNREPLICABLE} BOOL;
		iLineerFwdSensor : BOOL;
		iPickUpSensor : BOOL;
		iFoldingBwdSensor : BOOL;
		iRotaryUpSensor : BOOL;
		iRotaryDownSensor : BOOL;
		iVacum1Ok : BOOL;
		iR3VacumOk : BOOL;
		iVacumOff : BOOL;
		iR3PartPicked : BOOL;
		iCamOnline : BOOL;
		iCell2CamOk : BOOL;
		iCell2RWZ : BOOL;
		iCell3RWZ : BOOL;
	END_VAR
	VAR_OUTPUT
		qLineerBwd : BOOL;
		qLineerFwd : BOOL;
		qRotaryUp : BOOL;
		qRotaryDown : BOOL;
		qPickDown : BOOL;
		qFoldingFwd : BOOL;
		qVacum1 : BOOL;
		qVacum2 : BOOL;
		qCamClear : BOOL;
		qReadyToR2 : BOOL;
		qPartReadyToR3 : BOOL;
		qProductIdle : BOOL;
		qStateNo : INT;
		qAlarmNo : INT;
	END_VAR
	VAR
		NState : INT;
		TEST : BOOL;
		tConditions : BOOL;
		tPickDownTime : TON;
		tFoldingWaitTime : TON;
		AlarmRefreshTimer : TON;
		TimerStateNo : TON;
		tAlarmNo : INT;
		tOldAlarmNo : INT;
		tOldNState : INT;
		zzEdge00000 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SM_Cell2 (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iStart : {REDUND_UNREPLICABLE} BOOL;
		iSMClampDownSns : {REDUND_UNREPLICABLE} BOOL;
		iSMClampUpSns : {REDUND_UNREPLICABLE} BOOL;
		iSMLockClamp : BOOL;
		iSMPressureOk : BOOL;
		iSMReadyHome : BOOL;
		iSMThreadBreakageError : {REDUND_UNREPLICABLE} BOOL;
		iSMStopError : {REDUND_UNREPLICABLE} BOOL;
		iSMVirtualHigh : {REDUND_UNREPLICABLE} BOOL;
		iSMPrgBit1 : {REDUND_UNREPLICABLE} BOOL;
		iSMPrgBit2 : {REDUND_UNREPLICABLE} BOOL;
		iR2WZ : BOOL;
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iAirOk : {REDUND_UNREPLICABLE} BOOL;
		iEmgOk : {REDUND_UNREPLICABLE} BOOL;
		iSafetyOk : {REDUND_UNREPLICABLE} BOOL;
		iPrgNo : USINT;
	END_VAR
	VAR_OUTPUT
		qBusy : BOOL;
		qSMClampWork : BOOL;
		qSMStart : BOOL;
		qSMVirtual : BOOL;
		qSMPrgBit1 : BOOL;
		qSMPrgBit2 : BOOL;
		qSMProgNoBit1 : BOOL;
		qSMProgNoBit2 : BOOL;
		qSMProgNoBit3 : BOOL;
		qSMProgNoBit4 : BOOL;
		qSMRunning : BOOL;
		qSMReadyForRobot : BOOL;
		qSMCycleOkForRobot : BOOL;
		qStateNo : INT;
		qAlarmNo : INT;
	END_VAR
	VAR
		WaitTimer1 : TON;
		WaitTimer2 : TON;
		WaitTimer3 : TON;
		Start1PulseTimer : TON;
		Start2PulseTimer : TON;
		Start3PulseTimer : TON;
		AlarmRefreshTimer : TON;
		TimerStateNo : TON;
		PulseTime : TIME;
		WaitTime : TIME;
		NState : INT;
		tConditions : BOOL;
		tAlarmNo : INT;
		tOldAlarmNo : INT;
		tOldNState : INT;
		Pulse : BOOL;
		Pulse2 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SM_Cell3 (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iStart1 : BOOL;
		iStart2 : BOOL;
		iSMClampDownSns : {REDUND_UNREPLICABLE} BOOL;
		iSMClampUpSns : {REDUND_UNREPLICABLE} BOOL;
		iSMLockClamp : BOOL;
		iSMPressureOk : BOOL;
		iSMReadyHome : BOOL;
		iSMThreadBreakageError : {REDUND_UNREPLICABLE} BOOL;
		iSMStopError : {REDUND_UNREPLICABLE} BOOL;
		iSMVirtualHigh : {REDUND_UNREPLICABLE} BOOL;
		iR3WZ : BOOL;
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iAirOk : {REDUND_UNREPLICABLE} BOOL;
		iEmgOk : {REDUND_UNREPLICABLE} BOOL;
		iSafetyOk : {REDUND_UNREPLICABLE} BOOL;
		iPrgNo : USINT;
	END_VAR
	VAR_OUTPUT
		qBusy : BOOL;
		qSMStart : BOOL;
		qSMVirtual : BOOL;
		qSMProgNoBit1 : BOOL;
		qSMProgNoBit2 : BOOL;
		qSMProgNoBit3 : BOOL;
		qSMProgNoBit4 : BOOL;
		qSMRunning : BOOL;
		qSMReady1ForRobot : BOOL;
		qSMReady2ForRobot : BOOL;
		qSMCycle1OkForRobot : BOOL;
		qSMCycle2OkForRobot : BOOL;
		qStateNo : INT;
		qAlarmNo : INT;
	END_VAR
	VAR
		WaitTimer1 : TON;
		WaitTimer2 : TON;
		Start1PulseTimer : TON;
		Start2PulseTimer : TON;
		AlarmRefreshTimer : TON;
		TimerStateNo : TON;
		WaitTime : TIME;
		PulseTime : TIME;
		NState : INT;
		tConditions : BOOL;
		tAlarmNo : INT;
		tOldAlarmNo : INT;
		tOldNState : INT;
		Pulse : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SM_Cell4 (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iStart : {REDUND_UNREPLICABLE} BOOL;
		iSMClampDownSns : {REDUND_UNREPLICABLE} BOOL;
		iSMClampUpSns : {REDUND_UNREPLICABLE} BOOL;
		iSMLockClamp : {REDUND_UNREPLICABLE} BOOL;
		iSMPressureOk : {REDUND_UNREPLICABLE} BOOL;
		iSMReadyHome : {REDUND_UNREPLICABLE} BOOL;
		iSMThreadBreakageError : {REDUND_UNREPLICABLE} BOOL;
		iSMStopError : {REDUND_UNREPLICABLE} BOOL;
		iSMVirtualHigh : {REDUND_UNREPLICABLE} BOOL;
		iR4WZ : {REDUND_UNREPLICABLE} BOOL;
		iSystemAuto : {REDUND_UNREPLICABLE} BOOL;
		iAirOk : {REDUND_UNREPLICABLE} BOOL;
		iEmgOk : {REDUND_UNREPLICABLE} BOOL;
		iSafetyOk : {REDUND_UNREPLICABLE} BOOL;
		iPrgNo : {REDUND_UNREPLICABLE} USINT;
	END_VAR
	VAR_OUTPUT
		qBusy : {REDUND_UNREPLICABLE} BOOL;
		qSMStart : {REDUND_UNREPLICABLE} BOOL;
		qSMVirtual : {REDUND_UNREPLICABLE} BOOL;
		qSMProgNoBit1 : {REDUND_UNREPLICABLE} BOOL;
		qSMProgNoBit2 : {REDUND_UNREPLICABLE} BOOL;
		qSMProgNoBit3 : {REDUND_UNREPLICABLE} BOOL;
		qSMProgNoBit4 : {REDUND_UNREPLICABLE} BOOL;
		qSMRunning : {REDUND_UNREPLICABLE} BOOL;
		qSMReadyForRobot : {REDUND_UNREPLICABLE} BOOL;
		qSMCycleOkForRobot : {REDUND_UNREPLICABLE} BOOL;
		qStateNo : {REDUND_UNREPLICABLE} INT;
		qAlarmNo : INT;
	END_VAR
	VAR
		WaitTimer1 : {REDUND_UNREPLICABLE} TON;
		Start1PulseTimer : {REDUND_UNREPLICABLE} TON;
		AlarmRefreshTimer : {REDUND_UNREPLICABLE} TON;
		TimerStateNo : {REDUND_UNREPLICABLE} TON;
		WaitTime : {REDUND_UNREPLICABLE} TIME;
		PulseTime : {REDUND_UNREPLICABLE} TIME;
		NState : {REDUND_UNREPLICABLE} INT;
		tConditions : {REDUND_UNREPLICABLE} BOOL;
		tAlarmNo : {REDUND_UNREPLICABLE} INT;
		tOldAlarmNo : {REDUND_UNREPLICABLE} INT;
		tOldNState : {REDUND_UNREPLICABLE} INT;
		Pulse : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK CamTrig (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iCamOnline : BOOL;
		iCamTrigReady : {REDUND_UNREPLICABLE} BOOL;
		iCamResultReady : {REDUND_UNREPLICABLE} BOOL;
		iTrigger : BOOL;
	END_VAR
	VAR_OUTPUT
		qCamTrig : BOOL;
		qCamClear : BOOL;
		qLightOn : BOOL;
		qStateNo : INT;
		qAlarmNo : INT;
	END_VAR
	VAR
		NState : INT;
		tAlarmNo : INT;
		WaitTimer1 : TON;
		WaitTimer2 : TON;
		AlarmRefreshTimer : TON;
		TimerStateNo : TON;
		tOldAlarmNo : INT;
		tOldNState : INT;
		zzEdge00000 : BOOL;
		zzEdge00001 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK C1CamResult (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		RefPosX : {REDUND_UNREPLICABLE} INT;
		RefPosY : {REDUND_UNREPLICABLE} INT;
		RefPos0 : {REDUND_UNREPLICABLE} INT;
		ReadCamPosX : {REDUND_UNREPLICABLE} INT;
		ReadCamPosY : {REDUND_UNREPLICABLE} INT;
		ReadCamPos0 : {REDUND_UNREPLICABLE} INT;
		iReadyResult : {REDUND_UNREPLICABLE} BOOL;
		iClearDatas : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qRobotPosX : {REDUND_UNREPLICABLE} INT;
		qRobotPosY : {REDUND_UNREPLICABLE} INT;
		qRobotRotZ : {REDUND_UNREPLICABLE} INT;
		qRobotPosXPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPosXNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPosYPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPosYNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotRotZPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotRotZNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotSendValue : BOOL;
	END_VAR
	VAR
		Buffer_XPos : {REDUND_UNREPLICABLE} INT;
		Buffer_YPos : {REDUND_UNREPLICABLE} INT;
		Buffer_0Pos : {REDUND_UNREPLICABLE} INT;
		WaitTimer : TON;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Plate_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iR1Ready : {REDUND_UNREPLICABLE} BOOL;
		iPlateSns1 : BOOL;
		iPlateSns2 : BOOL;
	END_VAR
	VAR_OUTPUT
		qR2Ready : {REDUND_UNREPLICABLE} BOOL;
		qStateNo : INT;
		qAlarmNo : INT;
	END_VAR
	VAR
		NState : INT;
		tAlarmNo : INT;
		AlarmRefreshTimer : TON;
		TimerStateNo : TON;
		tOldAlarmNo : INT;
		Pulse : BOOL;
		tOldNState : INT;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK ProcesEnd (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iLineerFwdSensor : {REDUND_UNREPLICABLE} BOOL;
		iPickUpSensor : BOOL;
		iFoldingBwdSensor : BOOL;
		iRotaryUpSensor : BOOL;
		iFirstRobotCycleOn : BOOL;
		iSecondRobotCycleOn : BOOL;
	END_VAR
	VAR_OUTPUT
		qEndProces : BOOL;
		qAlarmNo : INT;
	END_VAR
	VAR
		tAlarmNo : INT;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK C4CamResult (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		ReadCamPos1_X : {REDUND_UNREPLICABLE} INT;
		ReadCamPos1_Y : {REDUND_UNREPLICABLE} INT;
		ReadCamPos1_0 : {REDUND_UNREPLICABLE} INT;
		ReadCamPos2_X : {REDUND_UNREPLICABLE} INT;
		ReadCamPos2_Y : {REDUND_UNREPLICABLE} INT;
		ReadCamPos2_0 : {REDUND_UNREPLICABLE} INT;
		iReadyResult : {REDUND_UNREPLICABLE} BOOL;
		iClearDatas : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qRobotPos1_X : {REDUND_UNREPLICABLE} INT;
		qRobotPos1_Y : {REDUND_UNREPLICABLE} INT;
		qRobotRot1_Z : {REDUND_UNREPLICABLE} INT;
		qRobotPos2_X : {REDUND_UNREPLICABLE} INT;
		qRobotPos2_Y : {REDUND_UNREPLICABLE} INT;
		qRobotRot2_Z : {REDUND_UNREPLICABLE} INT;
		qRobotPos1_XPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos1_XNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos1_YPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos1_YNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotRot1_ZPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotRot1_ZNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos2_XPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos2_XNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos2_YPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos2_YNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotRot2_ZPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotRot2_ZNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotSendValue : BOOL;
	END_VAR
	VAR
		Buffer_XPos1 : {REDUND_UNREPLICABLE} INT;
		Buffer_YPos1 : {REDUND_UNREPLICABLE} INT;
		Buffer_0Pos1 : {REDUND_UNREPLICABLE} INT;
		Buffer_XPos2 : {REDUND_UNREPLICABLE} INT;
		Buffer_YPos2 : {REDUND_UNREPLICABLE} INT;
		Buffer_0Pos2 : {REDUND_UNREPLICABLE} INT;
		WaitTimer : TON;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK GenCamResult (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSewingOk : {REDUND_UNREPLICABLE} INT;
		iIntegrityOk : {REDUND_UNREPLICABLE} INT;
		iReadyResult : {REDUND_UNREPLICABLE} BOOL;
		iClearDatas : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qSewingOk : {REDUND_UNREPLICABLE} INT;
		qIntegrityOk : {REDUND_UNREPLICABLE} INT;
		qRobotSendValue : BOOL;
	END_VAR
	VAR
		WaitTimer : TON;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} {REDUND_UNREPLICABLE} FUNCTION_BLOCK C3CamResult (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		ReadCamPos_X : {REDUND_UNREPLICABLE} INT;
		ReadCamPos_Y : {REDUND_UNREPLICABLE} INT;
		ReadCamPos_0 : {REDUND_UNREPLICABLE} INT;
		iReadyResult : {REDUND_UNREPLICABLE} BOOL;
		iClearDatas : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qRobotPos_X : {REDUND_UNREPLICABLE} INT;
		qRobotPos_Y : {REDUND_UNREPLICABLE} INT;
		qRobotRot_Z : {REDUND_UNREPLICABLE} INT;
		qRobotPos_XPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos_XNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos_YPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotPos_YNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotRot_ZPstveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotRot_ZNgtveBit : {REDUND_UNREPLICABLE} BOOL;
		qRobotSendValue : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR
		Buffer_XPos : {REDUND_UNREPLICABLE} INT;
		Buffer_YPos : {REDUND_UNREPLICABLE} INT;
		Buffer_0Pos : {REDUND_UNREPLICABLE} INT;
		WaitTimer : {REDUND_UNREPLICABLE} TON;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK HotPlugStat (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iButton : {REDUND_UNREPLICABLE} BOOL;
		iTimer : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qButtonLamp : BOOL;
	END_VAR
	VAR
		TimerPT_Int_Value : INT;
		BlinkTimer : TON;
		bBlink : BOOL;
		tLamp1 : BOOL;
		tLamp2 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Piston5_3 (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iOpenSignal : BOOL;
		iCloseSignal : BOOL;
		iAutoOpenReq : {REDUND_UNREPLICABLE} BOOL;
		iAutoCloseReq : {REDUND_UNREPLICABLE} BOOL;
		iManOpenReq : BOOL;
		iManCloseReq : BOOL;
		iAuto : {REDUND_UNREPLICABLE} BOOL;
		iManual : {REDUND_UNREPLICABLE} BOOL;
		iSafety : {REDUND_UNREPLICABLE} BOOL;
		iAir : {REDUND_UNREPLICABLE} BOOL;
		iCylinderName : {REDUND_UNREPLICABLE} STRING[80];
	END_VAR
	VAR_OUTPUT
		qCylinderOpen : BOOL;
		qCylinderClose : BOOL;
		qOpenAlarm : BOOL;
		qCloseAlarm : BOOL;
		qAlarmMesagge : STRING[100];
		qAlarmNo : INT;
	END_VAR
	VAR
		OpenFaultTimer : TON;
		CloseFaultTimer : TON;
		tAutoOpen : BOOL;
		tManOpen : BOOL;
		tAutoClose : BOOL;
		tManClose : BOOL;
		tOpen : BOOL;
		tClose : BOOL;
		sFaultMessage1 : STRING[249];
		sFaultMessage2 : STRING[249];
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} {REDUND_UNREPLICABLE} FUNCTION_BLOCK Compensator_FB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iOpenSignal : {REDUND_UNREPLICABLE} BOOL;
		iCloseSignal : {REDUND_UNREPLICABLE} BOOL;
		iAutoOpenReq : {REDUND_UNREPLICABLE} BOOL;
		iAutoCloseReq : {REDUND_UNREPLICABLE} BOOL;
		iManOpenReq : {REDUND_UNREPLICABLE} BOOL;
		iManCloseReq : {REDUND_UNREPLICABLE} BOOL;
		iAuto : {REDUND_UNREPLICABLE} BOOL;
		iManual : {REDUND_UNREPLICABLE} BOOL;
		iSafety : {REDUND_UNREPLICABLE} BOOL;
		iAir : {REDUND_UNREPLICABLE} BOOL;
		iCompensatorName : {REDUND_UNREPLICABLE} STRING[80];
	END_VAR
	VAR_OUTPUT
		qCylinderOpen : {REDUND_UNREPLICABLE} BOOL;
		qCylinderClose : {REDUND_UNREPLICABLE} BOOL;
		qOpenAlarm : {REDUND_UNREPLICABLE} BOOL;
		qCloseAlarm : {REDUND_UNREPLICABLE} BOOL;
		qAlarmMesagge : {REDUND_UNREPLICABLE} STRING[80];
		qAlarmNo : {REDUND_UNREPLICABLE} INT;
	END_VAR
	VAR
		OpenFaultTimer : {REDUND_UNREPLICABLE} TON;
		CloseFaultTimer : {REDUND_UNREPLICABLE} TON;
		tAutoOpen : {REDUND_UNREPLICABLE} BOOL;
		tManOpen : {REDUND_UNREPLICABLE} BOOL;
		tAutoClose : {REDUND_UNREPLICABLE} BOOL;
		tManClose : {REDUND_UNREPLICABLE} BOOL;
		tOpen : {REDUND_UNREPLICABLE} BOOL;
		tClose : {REDUND_UNREPLICABLE} BOOL;
		sFaultMessage1 : {REDUND_UNREPLICABLE} STRING[249];
		sFaultMessage2 : {REDUND_UNREPLICABLE} STRING[249];
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK SuctionFB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iSuction : {REDUND_UNREPLICABLE} BOOL;
		iSuctionOk : {REDUND_UNREPLICABLE} BOOL;
		iBlow : {REDUND_UNREPLICABLE} BOOL;
		iManualSuction : {REDUND_UNREPLICABLE} BOOL;
		iManualBlow : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		qSuction : {REDUND_UNREPLICABLE} BOOL;
		qBlow : {REDUND_UNREPLICABLE} BOOL;
		qSuctionOk : {REDUND_UNREPLICABLE} BOOL;
		qSuctionFault : {REDUND_UNREPLICABLE} BOOL;
		qAlarmNo : INT;
	END_VAR
	VAR
		zzInternalMemory : {REDUND_UNREPLICABLE} ARRAY[0..9] OF SINT; (*Internal memory*)
		FaultTimer : {REDUND_UNREPLICABLE} TON;
		tSuctionOk : {REDUND_UNREPLICABLE} BOOL;
		tAutoSuction : {REDUND_UNREPLICABLE} BOOL;
		tManSuction : {REDUND_UNREPLICABLE} BOOL;
		tAutoBlow : {REDUND_UNREPLICABLE} BOOL;
		tManBlow : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Cell1CamBlowFB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iRobotWZ : BOOL;
		iManualBlow : BOOL;
		iBlowTime : TIME;
	END_VAR
	VAR_OUTPUT
		qBlow : BOOL;
	END_VAR
	VAR
		BlowTimer : TON;
		NState : INT;
		tBlow : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK Cell2CamBlowFB (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iRobotWZ : BOOL;
		iManualBlow : BOOL;
		iTrnsfrVcm1Ok : BOOL;
		iTrnsfrPickUpSns : BOOL;
		iTrnsfrLineerBwdSns : BOOL;
		iBlowTime : TIME;
	END_VAR
	VAR_OUTPUT
		qBlow : BOOL;
	END_VAR
	VAR
		BlowTimer : TON;
		NState : INT;
		tBlow : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} {REDUND_UNREPLICABLE} FUNCTION_BLOCK StackerFaz2 (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iAirOk : BOOL;
		iSafetyOk : BOOL;
		iEmgOk : BOOL;
		iSystemAuto : BOOL;
		iTopStcker_Empty : BOOL;
		iBtmStcker_Empty : BOOL;
		iTopStackerOpen : {REDUND_UNREPLICABLE} BOOL;
		iTopStackerClose : BOOL;
		iTopStackerLocked : BOOL;
		iTopStackerUnLocked : BOOL;
		iTopStackerPreSens : BOOL;
		iBtmStackerOpen : {REDUND_UNREPLICABLE} BOOL;
		iBtmStackerClose : BOOL;
		iBtmStackerLocked : BOOL;
		iBtmStackerUnLocked : BOOL;
		iBtmStackerPreSens : BOOL;
		iSlidingDoorOpen : BOOL;
		iSlidingDoorClose : BOOL;
		iRbtTopStckerTaked : BOOL;
		iRbtBtmStckerTaked : BOOL;
		iWZ_TopStcker : BOOL;
		iWZ_BottomStcker : BOOL;
	END_VAR
	VAR_OUTPUT
		qTopStackerOpen : BOOL;
		qTopStackerClose : BOOL;
		qTopStackerLocked : BOOL;
		qTopStackerUnLocked : BOOL;
		qBtmStackerOpen : BOOL;
		qBtmStackerClose : BOOL;
		qBtmStackerLocked : BOOL;
		qBtmStackerUnLocked : BOOL;
		qSlidingDoorOpen : BOOL;
		qSlidingDoorClose : BOOL;
		qTopStcker_Ready : BOOL;
		qBtmStcker_Ready : BOOL;
		qStateNo : INT;
		qAlarmNo : INT;
	END_VAR
	VAR
		NState : INT;
		TEST : BOOL;
		tConditions : BOOL;
		tWaitTimer : TON;
		AlarmRefreshTimer : TON;
		TimerStateNo : TON;
		tAlarmNo : INT;
		tOldAlarmNo : INT;
		tOldNState : INT;
		zzEdge00000 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} {REDUND_UNREPLICABLE} FUNCTION_BLOCK FlipOverMech (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		iAirOk : BOOL;
		iSafetyOk : BOOL;
		iEmgOk : BOOL;
		iSystemAuto : BOOL;
		iRobotStart : BOOL;
		iFlipOverMechOpened : BOOL;
		iFlipOverMechClosed : {REDUND_UNREPLICABLE} BOOL;
		iFlipOverMechForward : BOOL;
		iFlipOverMechBackward : BOOL;
	END_VAR
	VAR_OUTPUT
		qFlipOverMechOpen : BOOL;
		qFlipOverMechClose : BOOL;
		qFlipOverMechForward : BOOL;
		qFlipOverMechBackward : BOOL;
		qFlippingOverMech_Ready : BOOL;
		qFlippingOverMech_CycleOK : BOOL;
		qStateNo : INT;
		qAlarmNo : INT;
	END_VAR
	VAR
		NState : INT;
		TEST : BOOL;
		tConditions : BOOL;
		tWaitTimer : TON;
		AlarmRefreshTimer : TON;
		TimerStateNo : TON;
		tAlarmNo : INT;
		tOldAlarmNo : INT;
		tOldNState : INT;
		zzEdge00000 : BOOL;
	END_VAR
END_FUNCTION_BLOCK
