
(* TODO: Add your comment here *)
FUNCTION_BLOCK Plate_FB
	
	AlarmRefreshTimer.PT := T#4S;
	TimerStateNo.PT := T#4S;
	
	CASE NState OF
		
		0:
			
			IF iR1Ready
				THEN
				NState := 10;
			END_IF;
			
			//	Generate Alarm No
			IF iR1Ready = FALSE
				THEN
				tAlarmNo := 1;
			END_IF;
			
			
		10:
			
			IF iPlateSns1
				AND iPlateSns2
				THEN
				qR2Ready := TRUE;
				NState := 20;
			END_IF;
			
			//	Generate Alarm No
			IF iPlateSns1 = FALSE
				THEN
				tAlarmNo := 10;
			ELSIF iPlateSns2 = FALSE
				THEN
				tAlarmNo := 11;
			END_IF;
			
			
		20:
			
			IF iPlateSns1 = FALSE
				AND iPlateSns2 = FALSE
				THEN
				qR2Ready := FALSE;
				NState := 30;
			END_IF;
		
			//	Generate Alarm No
			IF iPlateSns1 = TRUE
				THEN
				tAlarmNo := 20;
			ELSIF iPlateSns2 = TRUE
				THEN
				tAlarmNo := 21;
			END_IF;
		
		30:
		
			//
			NState := 0;
			//
		
		
	END_CASE;
	

	
	IF tAlarmNo <> tOldAlarmNo
		THEN
		AlarmRefreshTimer.IN := FALSE;
		qAlarmNo := 0;
	END_IF;
	
	IF tAlarmNo = tOldAlarmNo
		AND TimerStateNo.Q
		THEN
		AlarmRefreshTimer.IN := TRUE;
	END_IF;

	
	IF NState <> tOldNState
		THEN
		TimerStateNo.IN := FALSE;
	END_IF;

	IF NState = tOldNState 
		THEN
		TimerStateNo.IN := TRUE;
	END_IF;
	
	IF AlarmRefreshTimer.Q AND NOT Pulse
		THEN
		qAlarmNo := tAlarmNo;
	END_IF;
	Pulse := AlarmRefreshTimer.Q;
	
	
	tOldAlarmNo := tAlarmNo;
	tOldNState := NState;
	qStateNo := NState;
	
	AlarmRefreshTimer ();
	TimerStateNo ();
	
END_FUNCTION_BLOCK
