
(* TODO: Add your comment here *)
FUNCTION_BLOCK Mode_FB
	
	
	// Y�KLEME MODU OLABILIR
	IF iReserveMod[1]
		THEN
		qReserveMod[1] := TRUE;
		qHMI.ReserveMod[1] := TRUE;
	ELSE
		iReserveMod[1] := FALSE;
		qHMI.ReserveMod[1] := FALSE;
	END_IF;
	
	
	// BOSALTMA MODU OLABILIR
	IF iReserveMod[2]
		THEN
		qReserveMod[2] := TRUE;
		qHMI.ReserveMod[2] := TRUE;
	ELSE
		iReserveMod[2] := FALSE;
		qHMI.ReserveMod[2] := FALSE;
	END_IF;
	
	
	// Reserve[3]
	IF iReserveMod[3]
		THEN
		qReserveMod[3] := TRUE;
		qHMI.ReserveMod[3] := TRUE;
	ELSE
		iReserveMod[3] := FALSE;
		qHMI.ReserveMod[3] := FALSE;
	END_IF;
	
	
	// Reserve[4]
	IF iReserveMod[4]
		THEN
		qReserveMod[4] := TRUE;
		qHMI.ReserveMod[4] := TRUE;
	ELSE
		iReserveMod[4] := FALSE;
		qHMI.ReserveMod[4] := FALSE;
	END_IF;
	
	
	// MANUAL MOD
	IF iEmgAct
		AND iSystemAuto = FALSE
		THEN
		qManuelMode := TRUE;
		qHMI.ManuelMode := TRUE;
	ELSE
		qManuelMode := FALSE;
		qHMI.ManuelMode := FALSE;
	END_IF;
	
	
	// OTOMATIK MOD
	IF iEmgAct
		AND iAutoStop
		AND iSystemAuto
		THEN
		tAuto := TRUE;
	ELSE
		tAuto := FALSE;
	END_IF;
	
	IF tAuto AND
		(iReset AND NOT sAuto)
		OR
		sAuto
		THEN
		sAuto := TRUE;
	ELSE
		sAuto := FALSE;
	END_IF;
	
	IF sAuto
		THEN
		qAutoMode := TRUE;
		qHMI.SystemAuto := TRUE;
	ELSE
		qAutoMode := FALSE;
		qHMI.SystemAuto := FALSE;
	END_IF;
	
	
	// DURUMLAR
	IF NOT iEmgAct
		THEN
		qHMI.State := 5;
	ELSE
		IF iSystemAuto
			AND iAutoStop = FALSE
			THEN
			qHMI.State := 3;
		END_IF;
		
		IF iSystemAuto
			AND iAutoStop
			AND iReserveMod[1] = FALSE
			THEN
			qHMI.State := 4;
		END_IF;
		
		IF iSystemAuto
			AND iAutoStop
			AND iReserveMod[2] = FALSE
			AND iClockPulse = FALSE
			THEN
			qHMI.State := 7;
		END_IF;
		
		IF iSystemAuto
			AND iAutoStop
			AND iReserveMod[2] = FALSE
			AND iClockPulse
			THEN
			qHMI.State := 1;
		END_IF;
		
		IF iSystemAuto
			AND iAutoStop
			AND iReserveMod[1] = FALSE
			AND iReserveMod[2] = FALSE
			AND sAuto
			THEN
			qHMI.State := 1;
		END_IF;
		
		IF tAuto
			AND sAuto = FALSE
			THEN
			qHMI.State := 8;
		END_IF;
		
		IF qManuelMode
			THEN
			qHMI.State := 2;
		END_IF;
	END_IF;
	
END_FUNCTION_BLOCK
