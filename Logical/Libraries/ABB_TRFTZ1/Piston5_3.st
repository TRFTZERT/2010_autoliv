
(* TODO: Add your comment here *)
FUNCTION_BLOCK Piston5_3
	
	OpenFaultTimer.PT := T#7s;
	CloseFaultTimer.PT := T#7s;
	
	//====================================================================
		(*Automatic Open*)
	
		IF iAutoOpenReq
			AND iAutoCloseReq = FALSE
			AND iAuto
			AND iManual = FALSE
			AND iSafety
			AND iAir
			THEN
			tAutoOpen := TRUE;
		ELSE
			tAutoOpen := FALSE;
		END_IF;

	//====================================================================
	
	
	//====================================================================
		(*Manual Open*)
	
		IF iManOpenReq
			AND iManCloseReq = FALSE
			AND iAuto = FALSE
			AND iManual
			AND iSafety
			AND iAir
			THEN
			tManOpen := TRUE;
		ELSE
			tManOpen := FALSE;
		END_IF;
	
	//====================================================================
	
	
	//====================================================================
		(*Automatic Close*)
	
		IF iAutoCloseReq
			AND iAutoOpenReq = FALSE
			AND iAuto
			AND iManual = FALSE
			AND iSafety
			AND iAir
			THEN
			tAutoClose := TRUE;
		ELSE
			tAutoClose := FALSE;
		END_IF;
	
	//====================================================================
	
	
	//====================================================================
		(*Manual Close*)
	
		IF iManCloseReq
			AND iManOpenReq = FALSE
			AND iAuto = FALSE
			AND iManual
			AND iSafety
			AND iAir
			THEN
			tManClose := TRUE;
		ELSE
			tManClose := FALSE;
		END_IF;
	
	//====================================================================
	
	
	//====================================================================
		(*Outputs*)
	
		tOpen	 := tAutoOpen OR tManOpen;
		tClose	 := tAutoClose OR tManClose;
		qOpenAlarm	 := OpenFaultTimer.Q;
		qCloseAlarm	 := CloseFaultTimer.Q;	
		qCylinderOpen := tOpen;
		qCylinderClose := tClose;
	
	//====================================================================
	
	
	//====================================================================
		(*Alarms*)
	
		IF tOpen
			AND (iCloseSignal OR iOpenSignal = FALSE)
			THEN
			OpenFaultTimer.IN := TRUE;
		ELSE
			OpenFaultTimer.IN := FALSE;
		END_IF;
		
		IF tClose
			AND (iOpenSignal OR iCloseSignal = FALSE)
			THEN
			CloseFaultTimer.IN := TRUE;
		ELSE
			CloseFaultTimer.IN := FALSE;
		END_IF;
	
		(*Alarm Message*)
	
		sFaultMessage1 := CONCAT (iCylinderName , ' did not open or opened and open position sensor did not output.');
		sFaultMessage2 := CONCAT (iCylinderName , ' did not close or closed and close position sensor did not output.');
		
		IF qOpenAlarm
			THEN
			qAlarmMesagge := sFaultMessage1;
		END_IF;
		
		IF qCloseAlarm
			THEN
			qAlarmMesagge := sFaultMessage2;
		END_IF;
		
		IF qOpenAlarm = FALSE
			AND qCloseAlarm = FALSE
			THEN
			qAlarmMesagge := ' ';
		END_IF;
	
	//====================================================================
	
	
	//====================================================================
		(*AlarmNo*)
	
		IF iAir = FALSE
			THEN 
			qAlarmNo := 200;
		ELSIF iSafety = FALSE
			THEN
			qAlarmNo := 201;
		ELSIF iAuto AND iManual
			THEN
			qAlarmNo := 204;
		ELSIF iOpenSignal AND iOpenSignal
			THEN
			qAlarmNo := 205;
		ELSIF iAutoOpenReq AND iAutoCloseReq
			THEN
			qAlarmNo := 206;
		ELSIF iManOpenReq AND iManCloseReq
			THEN
			qAlarmNo := 206;
		ELSIF qOpenAlarm
			THEN
			qAlarmNo := 207;
		ELSIF qCloseAlarm
			THEN
			qAlarmNo := 208;
		END_IF;
	
	//====================================================================
	
	OpenFaultTimer();
	CloseFaultTimer();
	
END_FUNCTION_BLOCK
