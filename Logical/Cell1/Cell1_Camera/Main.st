
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC

	
	//================================================================================================================
	(*               CAMERA STATUS BITS                   *)
	(*
		Status Bit.0  --> Online
		Status Bit.1  --> Toggle
		Status Bit.2  --> Not Used
		Status Bit.3  --> Busy
		Status Bit.4  --> QualitySubmitErr
		Status Bit.5  --> ImageErr
		Status Bit.6  --> TriggerReady
		Status Bit.7  --> ExposureComplate
		Status Bit.8  --> Not Used
		Status Bit.9  --> Not Used
		Status Bit.10 --> Pass
		Status Bit.11 --> Fail
		Status Bit.12 --> ResultReady
		Status Bit.13 --> LightingNok
		Status Bit.14 --> TurnLightOn
		Status Bit.15 --> Not Used
	*)
	//================================================================================================================
	
		SendWaitTime := T#250ms;
		PulseTimer.PT := T#500ms;
		Part1WaitSendTimer.PT := SendWaitTime;
		Part2WaitSendTimer.PT := SendWaitTime;
		Part3WaitSendTimer.PT := SendWaitTime;
		Part4WaitSendTimer.PT := SendWaitTime;
	
	//================================================================================================================
	(*              Cell 1 Camera Verification            *)

		PC1_C1_Verifi_ProdID := 0;
		CamTrig_C1_Verifi(
			iCamOnline					 := PC1_C1_Verifi_Sts.0,
			iCamTrigReady				 := PC1_C1_Verifi_Sts.6,
			iCamResultReady				 := PC1_C1_Verifi_Sts.12,
			iTrigger					 := Local_C1_Verifi_Trigger); 	// Tetik sinyali konulacak ge�ici olarak Local_C1_Verifi_Trigger konulmustur
	
		PC1_C1_Verifi_Trigger			 := CamTrig_C1_Verifi.qCamTrig;
		PC1_C1_Verifi_PLCClear			 := CamTrig_C1_Verifi.qCamClear;
		LocalDO_C1_Verifi_CamLightB_Open := CamTrig_C1_Verifi.qLightOn;
		C1_Verifi_StatusNo				 := CamTrig_C1_Verifi.qStateNo;
		C1_Verifi_AlarmNo				 := CamTrig_C1_Verifi.qAlarmNo;
	
	//================================================================================================================
	
	
	
	//================================================================================================================
	(*                Cell 1 Camera Part 1                *)

	PC1_C1_Part1_ProdID := 0;
	Part1WaitSendTimer.PT := SendWaitTime;
		CamTrig_C1_Part1(
			iCamOnline					 := PC1_C1_Part1_Sts.0,
			iCamTrigReady				 := PC1_C1_Part1_Sts.6,
			iCamResultReady				 := PC1_C1_Part1_Sts.12,
			iTrigger					 := R1_diCam_Check1);
		
		PC1_C1_Part1_Trigger			 := CamTrig_C1_Part1.qCamTrig;
		PC1_C1_Part1_PLCClear			 := CamTrig_C1_Part1.qCamClear;
		LocalDO_C1_Part1_CamLightB_Open	 := CamTrig_C1_Part1.qLightOn;
		C1_Part1_StatusNo				 := CamTrig_C1_Part1.qStateNo;
		C1_Part1_AlarmNo				 := CamTrig_C1_Part1.qAlarmNo;
	
	
		memcpy(ADR(C1_Part1_XPos),ADR(PC1_C1_Part1_X[0]),SIZEOF(PC1_C1_Part1_X[0]));
		memcpy(ADR(C1_Part1_YPos),ADR(PC1_C1_Part1_Y[0]),SIZEOF(PC1_C1_Part1_Y[0]));
		memcpy(ADR(C1_Part1_0Pos),ADR(PC1_C1_Part1_0[0]),SIZEOF(PC1_C1_Part1_0[0]));

	
		C1CamResult_Part1(
			RefPosX		 := 2881, // Eski deger : 
			RefPosY		 := -346, // Eski deger : 
			RefPos0		 := 0,    // Eski deger : 
			ReadCamPosX	 := C1_Part1_XPos,
			ReadCamPosY	 := C1_Part1_YPos,
			ReadCamPos0	 := C1_Part1_0Pos,
			iReadyResult := PC1_C1_Part1_Sts.12,	// Waiting ResultReady
			iClearDatas	 := R1_diCam_Check1);
	
		IF (EDGEPOS (C1CamResult_Part1.qRobotSendValue))
			THEN
			R1_goCam_PosX 			:= C1CamResult_Part1.qRobotPosX;
			R1_doCam_PosXPstvBit 	:= C1CamResult_Part1.qRobotPosXPstveBit;
			R1_doCam_PosXNgtvBit 	:= C1CamResult_Part1.qRobotPosXNgtveBit;
			
			R1_goCam_PosY 			:= C1CamResult_Part1.qRobotPosY;
			R1_doCam_PosYPstvBit 	:= C1CamResult_Part1.qRobotPosYPstveBit;
			R1_doCam_PosYNgtvBit 	:= C1CamResult_Part1.qRobotPosYNgtveBit;
			
			R1_goCam_RotZ 			:= C1CamResult_Part1.qRobotRotZ;
			R1_doCam_RotZPstvBit 	:= C1CamResult_Part1.qRobotRotZPstveBit;
			R1_doCam_RotZNgtvBit 	:= C1CamResult_Part1.qRobotRotZNgtveBit;
		
			Part1WaitSendTimer.IN := TRUE;
		
		END_IF;
		
		R1_doCam_OK1 := PC1_C1_Part1_Sts.10; (*Camera Pass Bit*)
		//R1_doCam_Part1Fail := PC1_C1_Part1_Sts.11 (*Camera Fail Bit*)
	
		IF Part1WaitSendTimer.Q
			THEN
			R1_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	
	//================================================================================================================	
	
	
	
	//================================================================================================================
	(*                Cell 1 Camera Part 2                *)

		PC1_C1_Part2_ProdID := 0;
		Part2WaitSendTimer.PT := SendWaitTime;
		CamTrig_C1_Part2(
			iCamOnline					 := PC1_C1_Part2_Sts.0,
			iCamTrigReady				 := PC1_C1_Part2_Sts.6,
			iCamResultReady				 := PC1_C1_Part2_Sts.12,
			iTrigger					 := R1_diCam_Check2);
			
		PC1_C1_Part2_Trigger			 := CamTrig_C1_Part2.qCamTrig;
		PC1_C1_Part2_PLCClear			 := CamTrig_C1_Part2.qCamClear;
		LocalDO_C1_Part2_CamLightB_Open	 := CamTrig_C1_Part2.qLightOn;
		C1_Part2_StatusNo				 := CamTrig_C1_Part2.qStateNo;
		C1_Part2_AlarmNo				 := CamTrig_C1_Part2.qAlarmNo;
	
	
		memcpy(ADR(C1_Part2_XPos),ADR(PC1_C1_Part2_X[0]),SIZEOF(PC1_C1_Part2_X[0]));
		memcpy(ADR(C1_Part2_YPos),ADR(PC1_C1_Part2_Y[0]),SIZEOF(PC1_C1_Part2_Y[0]));
		memcpy(ADR(C1_Part2_0Pos),ADR(PC1_C1_Part2_0[0]),SIZEOF(PC1_C1_Part2_0[0]));
	
	
		C1CamResult_Part2(
			RefPosX		 := 766,  // Eski deger : 
			RefPosY		 := -612, // Eski deger : 
			RefPos0		 := 0,    // Eski deger : 
			ReadCamPosX	 := C1_Part2_XPos,
			ReadCamPosY	 := C1_Part2_YPos,
			ReadCamPos0	 := C1_Part2_0Pos,
			iReadyResult := PC1_C1_Part2_Sts.12, 	// Waiting ResultReady
			iClearDatas	 := R1_diCam_Check2);
		
		IF (EDGEPOS (C1CamResult_Part2.qRobotSendValue))
			THEN
			R1_goCam_PosX := C1CamResult_Part2.qRobotPosX;
			R1_doCam_PosXPstvBit := C1CamResult_Part2.qRobotPosXPstveBit;
			R1_doCam_PosXNgtvBit := C1CamResult_Part2.qRobotPosXNgtveBit;
			
			R1_goCam_PosY := C1CamResult_Part2.qRobotPosY;
			R1_doCam_PosYPstvBit := C1CamResult_Part2.qRobotPosYPstveBit;
			R1_doCam_PosYNgtvBit := C1CamResult_Part2.qRobotPosYNgtveBit;
				
			R1_goCam_RotZ := C1CamResult_Part2.qRobotRotZ;
			R1_doCam_RotZPstvBit := C1CamResult_Part2.qRobotRotZPstveBit;
			R1_doCam_RotZNgtvBit := C1CamResult_Part2.qRobotRotZNgtveBit;
		
			Part2WaitSendTimer.IN := TRUE;

		END_IF;
	
		R1_doCam_OK2 := PC1_C1_Part2_Sts.10; (*Camera Pass Bit*)
		//R1_doCam_Part2Fail := PC1_C1_Part2_Sts.11 (*Camera Fail Bit*)
	
		IF Part2WaitSendTimer.Q
			THEN
			R1_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	//================================================================================================================
	
	
	
	//================================================================================================================
	(*                Cell 1 Camera Part 3                *)
	
		PC1_C1_Part3_ProdID := 0;
		Part3WaitSendTimer.PT := SendWaitTime;
		CamTrig_C1_Part3(
			iCamOnline					 := PC1_C1_Part3_Sts.0,
			iCamTrigReady				 := PC1_C1_Part3_Sts.6,
			iCamResultReady				 := PC1_C1_Part3_Sts.12,
			iTrigger					 := R1_diCam_Check3);
			
		PC1_C1_Part3_Trigger			 := CamTrig_C1_Part3.qCamTrig;
		PC1_C1_Part3_PLCClear			 := CamTrig_C1_Part3.qCamClear;
		LocalDO_C1_Part3_CamLightB_Open	 := CamTrig_C1_Part3.qLightOn;
		C1_Part3_StatusNo				 := CamTrig_C1_Part3.qStateNo;
		C1_Part3_AlarmNo				 := CamTrig_C1_Part3.qAlarmNo;
		
		
		memcpy(ADR(C1_Part3_XPos),ADR(PC1_C1_Part3_X[0]),SIZEOF(PC1_C1_Part3_X[0]));
		memcpy(ADR(C1_Part3_YPos),ADR(PC1_C1_Part3_Y[0]),SIZEOF(PC1_C1_Part3_Y[0]));
		memcpy(ADR(C1_Part3_0Pos),ADR(PC1_C1_Part3_0[0]),SIZEOF(PC1_C1_Part3_0[0]));
	
	
		C1CamResult_Part3(
			RefPosX		 := 765,  // Eski deger : 
			RefPosY		 := -617, // Eski deger : 
			RefPos0		 := 0,    // Eski deger : 
			ReadCamPosX	 := C1_Part3_XPos,
			ReadCamPosY	 := C1_Part3_YPos,
			ReadCamPos0	 := C1_Part3_0Pos,
			iReadyResult := PC1_C1_Part3_Sts.12, 	// Waiting ResultReady
			iClearDatas	 := R1_diCam_Check3);
		
		IF (EDGEPOS (C1CamResult_Part3.qRobotSendValue))
			THEN
			R1_goCam_PosX := C1CamResult_Part3.qRobotPosX;
			R1_doCam_PosXPstvBit := C1CamResult_Part3.qRobotPosXPstveBit;
			R1_doCam_PosXNgtvBit := C1CamResult_Part3.qRobotPosXNgtveBit;
			
			R1_goCam_PosY := C1CamResult_Part3.qRobotPosY;
			R1_doCam_PosYPstvBit := C1CamResult_Part3.qRobotPosYPstveBit;
			R1_doCam_PosYNgtvBit := C1CamResult_Part3.qRobotPosYNgtveBit;
			
			R1_goCam_RotZ := C1CamResult_Part3.qRobotRotZ;
			R1_doCam_RotZPstvBit := C1CamResult_Part3.qRobotRotZPstveBit;
			R1_doCam_RotZNgtvBit := C1CamResult_Part3.qRobotRotZNgtveBit;
			
			Part3WaitSendTimer.IN := TRUE;
		
		END_IF;
	
		R1_doCam_OK3 := PC1_C1_Part3_Sts.10; (*Camera Pass Bit*)
		//R1_doCam_Part3Fail := PC1_C1_Part3_Sts.11 (*Camera Fail Bit*)
	
		IF Part3WaitSendTimer.Q
			THEN
			R1_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	//================================================================================================================
	
	
	
	//================================================================================================================		
	(*                Cell 1 Camera Part 4                *)
	
		PC1_C1_Part4_ProdID := 0;
		Part4WaitSendTimer.PT := SendWaitTime;
		CamTrig_C1_Part4(
			iCamOnline					 := PC1_C1_Part4_Sts.0,
			iCamTrigReady				 := PC1_C1_Part4_Sts.6,
			iCamResultReady				 := PC1_C1_Part4_Sts.12,
			iTrigger					 := R1_diCam_Check4);
			
		PC1_C1_Part4_Trigger			 := CamTrig_C1_Part4.qCamTrig;
		PC1_C1_Part4_PLCClear			 := CamTrig_C1_Part4.qCamClear;
		LocalDO_C1_Part4_CamLightB_Open	 := CamTrig_C1_Part4.qLightOn;
		C1_Part4_StatusNo				 := CamTrig_C1_Part4.qStateNo;
		C1_Part4_AlarmNo				 := CamTrig_C1_Part4.qAlarmNo;
	
	
		memcpy(ADR(C1_Part4_XPos),ADR(PC1_C1_Part4_X[0]),SIZEOF(PC1_C1_Part4_X[0]));
		memcpy(ADR(C1_Part4_YPos),ADR(PC1_C1_Part4_Y[0]),SIZEOF(PC1_C1_Part4_Y[0]));
		memcpy(ADR(C1_Part4_0Pos),ADR(PC1_C1_Part4_0[0]),SIZEOF(PC1_C1_Part4_0[0]));
	
	
		C1CamResult_Part4(
			RefPosX		 := 766,  // Eski deger : 
			RefPosY		 := -643, // Eski deger : 
			RefPos0		 := 0,    // Eski deger : 
			ReadCamPosX	 := C1_Part4_XPos,
			ReadCamPosY	 := C1_Part4_YPos,
			ReadCamPos0	 := C1_Part4_0Pos,
			iReadyResult := PC1_C1_Part4_Sts.12, 	// Waiting ResultReady
			iClearDatas	 := R1_diCam_Check4);
		
		IF (EDGEPOS (C1CamResult_Part4.qRobotSendValue))
			THEN
			R1_goCam_PosX := C1CamResult_Part4.qRobotPosX;
			R1_doCam_PosXPstvBit := C1CamResult_Part4.qRobotPosXPstveBit;
			R1_doCam_PosXNgtvBit := C1CamResult_Part4.qRobotPosXNgtveBit;
			
			R1_goCam_PosY := C1CamResult_Part4.qRobotPosY;
			R1_doCam_PosYPstvBit := C1CamResult_Part4.qRobotPosYPstveBit;
			R1_doCam_PosYNgtvBit := C1CamResult_Part4.qRobotPosYNgtveBit;
			
			R1_goCam_RotZ := C1CamResult_Part4.qRobotRotZ;
			R1_doCam_RotZPstvBit := C1CamResult_Part4.qRobotRotZPstveBit;
			R1_doCam_RotZNgtvBit := C1CamResult_Part4.qRobotRotZNgtveBit;
			
			Part4WaitSendTimer.IN := TRUE;

		END_IF;
	
		R1_doCam_OK4 := PC1_C1_Part4_Sts.10; (*Camera Pass Bit*)
		//R1_doCam_Part4Fail := PC1_C1_Part4_Sts.11 (*Camera Fail Bit*)
	
		IF Part4WaitSendTimer.Q
			THEN
			R1_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	//================================================================================================================		

	
	IF PulseTimer.Q
		THEN
		R1_doPartValueSended := FALSE;
		PulseTimer.IN := FALSE;
		Part1WaitSendTimer.IN := FALSE;
		Part2WaitSendTimer.IN := FALSE;
		Part3WaitSendTimer.IN := FALSE;
		Part4WaitSendTimer.IN := FALSE;
	END_IF;
	
	
	doCell1_CamLightBottom_Open := LocalDO_C1_Verifi_CamLightB_Open
								OR LocalDO_C1_Part1_CamLightB_Open
								OR LocalDO_C1_Part2_CamLightB_Open
								OR LocalDO_C1_Part3_CamLightB_Open
								OR LocalDO_C1_Part4_CamLightB_Open
								OR Man_C1_CamLightOn;	
	
	Part1WaitSendTimer();
	Part2WaitSendTimer();
	Part3WaitSendTimer();
	Part4WaitSendTimer();
	PulseTimer();
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

