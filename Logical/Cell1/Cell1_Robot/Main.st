
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	
	//======================================================================================================================================
	
		aiVoltageValue := aiCell1_Grp_ThckSens / 327;
		
		Local_ZeroValue := aiCell1_Grp_ThckSens - 1807;
		Local_ThicknesValue_mm := Local_ZeroValue / 516;
		
		IF (EDGEPOS (Local_Calibration_Button))
			THEN
			Local_DifferenceValue := Local_ThicknesValue_mm;
		END_IF;
		
		Local_CalibratetdValue := Local_ThicknesValue_mm - Local_DifferenceValue;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================
	
		IF TRUE
			//AND PC1_C1_Verifi_Sts.0
			AND PC1_C1_Part1_Sts.0
			AND PC1_C1_Part2_Sts.0
			AND PC1_C1_Part3_Sts.0
			AND PC1_C1_Part4_Sts.0
			THEN
			R1_doCam_Running := TRUE;
			R1_doCam_Error := FALSE;
		ELSE
			R1_doCam_Running := FALSE;
			R1_doCam_Error := TRUE;
		END_IF;

	//======================================================================================================================================
	
	
	
	//======================================================================================================================================
	
		IF diCell1_LOP_EntryRqst
			OR diCell1_ROP_EntryRqst
			OR diCell2_LOP_EntryRqst
			OR diCell2_ROP_EntryRqst
			THEN
			R1_doOP_EntryRqst := TRUE;
		END_IF;
		
		IF Cell1_2_ProcesEnd = FALSE
			THEN
			R1_doOP_EntryRqst := FALSE;
		END_IF;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================	
	(*            Send to R1              *)
	
		R1_doLPlate_PreSens		 := diCell1_LPlate_PreSens;
		R1_doRPlate_PreSens		 := diCell1_RPlate_PreSens;
		R1_doLPlate_PartPreSens	 := diCell1_LPlate_PartPreSens;
		R1_doRPlate_PartPreSens	 := diCell1_RPlate_PartPreSens;
		R1_doGrpCmpnstr_Opened	 := diCell1_GrpCmpnstr_Opened;
		R1_doGrpCmpnstr_Closed	 := diCell1_GrpCmpnstr_Closed;
		R1_goRecipe				 := RecipeNo;
		R1_doAuto 				 := R1_diAutoON;
		R1_doTool1_PreSens		 := diCell1_Tool1_PreSens;
		R1_doTool2_PreSens		 := diCell1_Tool2_PreSens;
		R1_goGrp_ThckSens		 := Local_CalibratetdValue;
		
		
		Plate_FB_L(
			iR1Ready 			:= R1_diLPlate_Ready,
			iPlateSns1 			:= diCell1_LPlate_PreSens,
			iPlateSns2 			:= diCell1_LPlate_PartPreSens);
		
		R2_doLPlate_Ready 		:= Plate_FB_L.qR2Ready;
		Local_PlateL_Status 	:= Plate_FB_L.qStateNo;
		Local_PlateL_AlarmNo 	:= Plate_FB_L.qAlarmNo;
		
		
		Plate_FB_R(
			iR1Ready 			:= R1_diRPlate_Ready,
			iPlateSns1 			:= diCell1_RPlate_PreSens,
			iPlateSns2 			:= diCell1_RPlate_PartPreSens);
		
		R2_doRPlate_Ready 		:= Plate_FB_R.qR2Ready;
		Local_PlateR_Status 	:= Plate_FB_R.qStateNo;
		Local_PlateR_AlarmNo 	:= Plate_FB_R.qAlarmNo;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

