	
PROGRAM _INIT
	
	TrpFB.inEnable		:= TRUE;
	TrpFB.inTrpType		:= ELATEC_TWN4_MULTITECH_NANO;
	FirstRecipeLoad		:= TRUE;			// When start machine, first recipe load.
	
	//MpUserX 
	FB_MpUserXLogin.Enable   := TRUE;
	FB_MpUserXLogin.MpLink   := ADR(gUserXLogin);
	FB_MpUserXLogin.Password := ADR(Password);
	FB_MpUserXLogin.UserName := ADR(UserName);
	
	//MpUserXloginUI for visulalization
	FB_MpUserXLoginUI.Enable    := TRUE;
	FB_MpUserXLoginUI.MpLink    := ADR(gUserXLogin);
	FB_MpUserXLoginUI.UIConnect := ADR(UIConnect);
	
	FB_MpUserXManagerUI.MpLink  := ADR(gUserXLogin);
	FB_MpUserXManagerUI.Enable  := TRUE;
	FB_MpUserXManagerUI.UIConnect := ADR(ManagerUIConnect);
	
	FB_MpRecipe.MpLink := ADR(gRecipeXml);
	FB_MpRecipe.Enable := TRUE;
	FB_MpRecipe.DeviceName := ADR('mappDir');
	FB_MpRecipe.FileName   := ADR('UserData');
	
	FB_MpRecipeRegPar.MpLink := ADR(gRecipeXml);
	FB_MpRecipeRegPar.Enable := TRUE;
	FB_MpRecipeRegPar.PVName := ADR('UserMngmnt:UserRecipe');
	
	UserRecipe;
	temp_Username;
	temp_Password;
END_PROGRAM
	
PROGRAM _CYCLIC
	IF FirstRecipeLoad THEN
		FB_MpRecipe.Load			:= TRUE;
		IF FB_MpRecipe.CommandDone THEN
			FB_MpRecipe.Load		:= FALSE;
			FirstRecipeLoad			:= FALSE;
		END_IF;
	END_IF;
	

	IF UIConnect.CurrentUser = "" THEN
		CurrentUserHMI := 'Anonymous';
	ELSE
		CurrentUserHMI := WSTRING_TO_STRING(UIConnect.CurrentUser);	
	END_IF;	
	
	
	memset(ADR(dataProvider_Users), 0, SIZEOF(dataProvider_Users)); // HMI
	(* HMI *)
	FOR i := 0 TO 19 DO
		
		tempStr	:= WSTRING_TO_STRING(ManagerUIConnect.User.List.UserNames[i]);
		strcpy(ADR(dataProvider_Users[i]), DataProvider(i, ADR(tempStr)));
		
	END_FOR;
	
	memset(ADR(dataProvider_UserRole), 0, SIZEOF(dataProvider_UserRole));
	FOR i:=0 TO 9 DO
		tempStr_UserRole := WSTRING_TO_STRING(ManagerUIConnect.Role.List.Names[i]);
		strcpy(ADR(dataProvider_UserRole[i]),DataProvider(i,ADR(tempStr_UserRole)));
	END_FOR;
	

	// Admin Check
	
	AdminLoggedIn :=MpUserXHasRole(gUserXLogin,"Administrator");
	
	
	IF  TrpFB.outTagInfo.UID <> '' AND (NOT AssignTagIDActive)  THEN
		FOR z:=1 TO MAX_USER_NUMBER DO
			IF  UserRecipe[z].UID = TrpFB.outTagInfo.UID THEN
				SelectedIndexUser := z;
				EXIT;
			END_IF;
		END_FOR;
		IF SelectedIndexUser=0 THEN
			TagIdVisuInfo := 'This tag is not assigned.';
		ELSIF SelectedIndexUser <100 AND SelectedIndexUser>0 THEN
			IF NOT AdminLoggedIn THEN
				UserName	:=	UserRecipe[SelectedIndexUser].Username;
				Password	:=  UserRecipe[SelectedIndexUser].Password ;
				FB_MpUserXLogin.Login := TRUE;
			END_IF;
		END_IF;
	END_IF;		
					

	//login automatically
	IF	FB_MpUserXLogin.CommandDone THEN
		FB_MpUserXLogin.Login := FALSE;
		Password := "";
		UserName := "";	
	END_IF;
	  
	IF AdminLoggedIn THEN
		CreateUserButonIsActive := TRUE;
		AssignTagIDButtonActive := TRUE;
		FOR z:=1 TO MAX_USER_NUMBER DO
			IF  UserRecipe[z].UID = TrpFB.outTagInfo.UID AND SelectedIndexUser>0 AND NOT(TrpFB.outTagInfo.UID = '') THEN
				TagIdVisuInfo := 'This ID have another user.';
				AssignTagIDButtonActive	:= FALSE;
				EXIT;
			END_IF;
			IF UserRecipe[z].UID <> TrpFB.outTagInfo.UID AND NOT(TrpFB.outTagInfo.UID = '') THEN
	  			TagIdVisuInfo := 'This ID is OK.';
			END_IF;
		END_FOR;
		
		IF ManagerUIConnect.User.Remove THEN
			FOR z:=1 TO MAX_USER_NUMBER DO
				IF ManagerUIConnect.User.Info.UserName =  UserRecipe[z].Username THEN
					UserRecipe[z].Username 	:= "";
					UserRecipe[z].Password	:= "";
					UserRecipe[z].UID		:= '';
					FB_MpRecipe.Save		:= TRUE;
					IF FB_MpRecipe.CommandDone THEN
						FB_MpRecipe.Save		:= FALSE;
					END_IF;
				END_IF;
			END_FOR;
		END_IF;
		
		
		
		IF CreateNewUserActive AND NOT AssignTagIDActive THEN
			UserManagementStep := STATE_NEW_USER;
			
		ELSIF (NOT CreateNewUserActive) AND AssignTagIDActive THEN
			
			ProcessDone			:= FALSE;	
			UserManagementStep := STATE_RFID_ASSIGN_PROCESS;
		END_IF;
		
		CASE UserManagementStep OF

			STATE_NEW_USER:
				
				CreateNewUser;
				
			STATE_RFID_ASSIGN_PROCESS:
				
				
				
				CASE RFIDStep OF
					STATE_RFID_READ:
						IF TrpFB.outTagInfo.UID <> '' THEN
							temp_UID := TrpFB.outTagInfo.UID;
							IF NOT ProcessDone THEN
								RFIDStep := STATE_RFID_CHECK_USER;
							END_IF;
						END_IF;
					STATE_RFID_CHECK_USER:
						temp_Username:= ManagerUIConnect.User.Info.UserName;
						IF ManagerUIConnect.User.Info.UserName = "" THEN
							//error
						END_IF;
						//find user
						FOR z:=0 TO MAX_USER_NUMBER DO
							IF temp_Username = UserRecipe[z].Username THEN
								SelectedUserUID := z;
								RFIDStep := STATE_RFID_ASSING_CONFIRM;
								EXIT;
							END_IF;
						END_FOR;
						
						
					STATE_RFID_ASSING_CONFIRM :
						IF ConfirmButton THEN
							UserRecipe[SelectedUserUID].UID := temp_UID ;
							FB_MpRecipe.Save				:= TRUE;
							IF FB_MpRecipe.CommandDone THEN
								FB_MpRecipe.Save			:= FALSE;
								RFIDStep := STATE_PROCESS_END;
							END_IF;
						END_IF;
						
					STATE_PROCESS_END:
						FB_MpRecipe.Load						:= TRUE;
						IF FB_MpRecipe.CommandDone THEN
							FB_MpRecipe.Load					:= FALSE;
							ProcessDone							:= TRUE;
							TagIdVisuInfo 						:= 'TagID assigned succesfully';
							AssignTagIDActive 					:= FALSE;
							RFIDStep							:= STATE_RFID_READ;
						END_IF;
				END_CASE;

		END_CASE;  			
	END_IF;
		
	FB_MpRecipeUI();
	FB_MpRecipeRegPar();
	FB_MpRecipe();
	FB_MpUserXManagerUI();
	TrpFB();
	FB_MpUserXLogin();
	FB_MpUserXLoginUI();
	END_PROGRAM
	
PROGRAM _EXIT
	FB_MpUserXLogin.Enable     := FALSE;
	FB_MpUserXLogin();
	FB_MpUserXLoginUI.Enable   := FALSE;	
	FB_MpUserXLoginUI();
	FB_MpUserXManagerUI.Enable := FALSE;
	FB_MpUserXManagerUI();
	FB_MpRecipeRegPar.Enable   := FALSE;
	FB_MpRecipeRegPar();
	FB_MpRecipe.Enable   := FALSE;
	FB_MpRecipe();
	END_PROGRAM
	
