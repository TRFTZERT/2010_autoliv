
ACTION CreateNewUser: 
	CASE UserCreationStep OF
		STATE_CHECK_STATUS:
			IF ManagerUIConnect.Status = mpUSERX_UI_STATUS_WAIT_DLG THEN	
				UserCreationStep := STATE_CHECK_PASSWORD;	
			END_IF
//		STATE_CHECK_USER_NAME:
//			IF ManagerUIConnect.User.Create.Dialog.UserNameOk = 1 THEN
//				UserCreationStep := STATE_CHECK_PASSWORD;
//			END_IF;
		STATE_CHECK_PASSWORD:
			IF ManagerUIConnect.User.Create.Dialog.ConfirmPasswordOk = 1 AND ManagerUIConnect.User.Create.Dialog.NewPasswordOk = 1 THEN
				temp_Password    := ManagerUIConnect.User.Create.Dialog.NewPassword;
				temp_Username    := ManagerUIConnect.User.Create.Dialog.UserName;
				IF ManagerUIConnect.User.Create.Dialog.Confirm THEN
					UserCreationStep := STATE_CONFIRM_USER;
				ELSIF ManagerUIConnect.User.Create.Dialog.Cancel THEN
					UserCreationStep := STATE_CANCEL_USER;
				END_IF; 
			END_IF;
		STATE_CONFIRM_USER:
			IF ManagerUIConnect.Status= mpUSERX_UI_STATUS_IDLE THEN
				ManagerUIConnect.User.Create.Dialog.UserName := "";
				ManagerUIConnect.User.Create.Dialog.ConfirmPasswordOk := 0;
				ManagerUIConnect.User.Create.Dialog.NewPasswordOk 	  := 0;
				ManagerUIConnect.User.Create.Dialog.UserNameOk        := 0;
				UserCreationStep := STATE_CHECK_EMPTY_SLOT;
			END_IF
					
		STATE_CHECK_EMPTY_SLOT:
			FOR z := 1 TO MAX_USER_NUMBER DO
				IF (UserRecipe[z].Password ="") AND (UserRecipe[z].Username = "") AND (UserRecipe[z].UID = '')  THEN
					SelectedIndex := z;
					EXIT;
				END_IF;
			END_FOR;	
			UserRecipe[SelectedIndex].Username := temp_Username;
			UserRecipe[SelectedIndex].Password := temp_Password;
			UserCreationStep    			   := STATE_SAVE_USER;
						
		STATE_SAVE_USER:
			FB_MpRecipe.Save := TRUE;
			IF FB_MpRecipe.CommandDone THEN
				FB_MpRecipe.Save := FALSE;
				temp_Username    := "";
				temp_Password    := "";
				ManagerUIConnect.User.Create.Dialog.UserName := "";
				ManagerUIConnect.User.Create.Dialog.ConfirmPasswordOk := 0;
				ManagerUIConnect.User.Create.Dialog.NewPasswordOk 	  := 0;
				ManagerUIConnect.User.Create.Dialog.UserNameOk        := 0;
				UserCreationStep := STATE_LOAD_USER_DATA;
			END_IF
		STATE_LOAD_USER_DATA:
			FB_MpRecipe.Load := TRUE;
			IF FB_MpRecipe.CommandDone THEN
				FB_MpRecipe.Load := FALSE;
				CreateNewUserActive := FALSE;
				UserCreationStep := STATE_CHECK_STATUS;
			END_IF;
		STATE_CANCEL_USER:
			ManagerUIConnect.User.Create.Dialog.UserName := "";
			ManagerUIConnect.User.Create.Dialog.ConfirmPasswordOk := 0;
			ManagerUIConnect.User.Create.Dialog.NewPasswordOk 	  := 0;
			ManagerUIConnect.User.Create.Dialog.UserNameOk        := 0;
			temp_Username         := "";
			temp_Password         := "";
			UserCreationStep 	  := STATE_CHECK_STATUS;
//			CreateNewUserIsActive := FALSE;
	END_CASE;
	
END_ACTION
