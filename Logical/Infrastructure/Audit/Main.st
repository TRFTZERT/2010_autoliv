

PROGRAM _CYCLIC
	MpAuditTrail_0(
	MpLink := ADR(gAuditTrail),
	Enable := TRUE, DeviceName := ADR('mappDir'));
	MpAuditTrailUI_0(
	MpLink := ADR(gAuditTrail),
	Enable := TRUE, 
	UIConnect := ADR(MpAuditTrailUIConnect));
	IF (MpAuditTrail_0.Export) AND (MpAuditTrail_0.CommandDone) THEN
		MpAuditTrail_0.Export := FALSE;
	END_IF
	IF MpAuditTrail_0.ArchiveAvailable THEN
		MpAuditTrail_0.ExportArchive := TRUE;
	ELSE
		MpAuditTrail_0.ExportArchive := FALSE;
	END_IF
	FilterConfiguration;
END_PROGRAM

PROGRAM _EXIT
	MpAuditTrail_0(Enable := FALSE);
	MpAuditTrailUI_0(Enable := FALSE);
END_PROGRAM

