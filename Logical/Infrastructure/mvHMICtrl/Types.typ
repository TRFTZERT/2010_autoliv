
TYPE
	RecipeType : 	STRUCT 
		ExpectedBarcode : ARRAY[1..12]OF INT;
		CurrentBarcod : ARRAY[1..12]OF INT;
		Result : ARRAY[1..12]OF BOOL;
		SelectedIndex : ARRAY[1..12]OF INT;
	END_STRUCT;
	Cell_Main_Type : 	STRUCT 
		DoorStatus : Cell_Door_type;
		VisiblityStatus : Cell_Visiblity_Type;
		Emergency : Cell_Emergency_Type;
		MachineStatusLight : Cell_MachineStatusType;
		ImageLstTimer : Cell_ImageListTimer_Type;
		Index : Cell_Index_Type;
	END_STRUCT;
	Cell_DialogBoxVisiblity_Type : 	STRUCT 
		BackDoor : BOOL;
		FrontDoor : BOOL;
		SideDoor : BOOL;
		Emergency_Green : BOOL;
		Emergency_LambImageList : BOOL;
		MainCell : BOOL;
		EmptyMachineLamb : BOOL;
		MachineStatLamb_ImageList : BOOL;
	END_STRUCT;
	Cell_Door_type : 	STRUCT 
		ClosedFront : BOOL;
		ClosedBack : BOOL;
		ClosedSide : BOOL;
	END_STRUCT;
	Cell_Visiblity_Type : 	STRUCT 
		BackDoor : BOOL;
		FrontDoor : BOOL;
		SideDoor : BOOL;
		Emergency_Green : BOOL;
		Emergency_LambImageList : BOOL;
		MainCell : BOOL;
		EmptyMachineLamb : BOOL;
		MachineStatLamb_ImageList : BOOL;
		DialogBox : Cell_DialogBoxVisiblity_Type;
	END_STRUCT;
	Cell_MachineStatusType : 	STRUCT 
		Yellow : BOOL;
		Red : BOOL;
		Green : BOOL;
	END_STRUCT;
	Cell_ImageListTimer_Type : 	STRUCT 
		MachineStatLight_TON : TON;
		Emergency_TON : TON;
	END_STRUCT;
	Cell_Index_Type : 	STRUCT 
		EmergencyIndex : USINT;
		MachineStatLambIndex : USINT;
	END_STRUCT;
	Cell_Emergency_Type : 	STRUCT 
		ButtonPressed : BOOL;
	END_STRUCT;
	Cell_Enum : 
		(
		NOT_SELECTED := 0,
		CELL_SELECTED
		);
END_TYPE
