
PROGRAM _INIT
	bModuleOKCell1_Card01;
	ExampleAlarm;
	ProductTypeExample[1] := 'Type 1';
	ProductTypeExample[2] := 'Type 2';
	BadPRoduct[1] := 12;
	BadPRoduct[2] := 11;
	GoodProduct[1] := 112;
	GoodProduct[2] := 114;
	TotalProduct[1]:= GoodProduct[1]+BadPRoduct[1];
	TotalProduct[2]:= GoodProduct[2]+BadPRoduct[2];
	(*Target[1] := 400;*)
	Efficiency[1] := 98;
	CellRecipe;
	Cell1RecipeCode;
END_PROGRAM




PROGRAM _CYCLIC

	FOR i:=1 TO 12 DO
		IF CellRecipe[1].CurrentBarcod[i] =CellRecipe[1].ExpectedBarcode[i] THEN
			CellRecipe[1].Result[i] := TRUE;
		ELSE
			CellRecipe[1].Result[i] := FALSE;
		END_IF;
	  CellRecipe[1].SelectedIndex[i]:=CellRecipe[1].Result[i];

	END_FOR;
	
	
	
	
	
	
	
	GetTimeInfo(enable := TRUE, pDTStructure := ADR(TimeInfo));
	Weekday            := TimeInfo.wday;
	
	
	IF NOT oneCellSelected  THEN
		CellStep := NOT_SELECTED;
	ELSE
		CellStep := CELL_SELECTED;
	END_IF;
	CASE  CellStep OF
		NOT_SELECTED:
		FOR i:=1 TO 4 DO
				Cell[i].VisiblityStatus.MainCell         := SHOW;
				Cell[i].VisiblityStatus.EmptyMachineLamb := SHOW;
				//the door is not closed, show image on HMI.
				IF NOT Cell[i].DoorStatus.ClosedBack  THEN
					Cell[i].VisiblityStatus.BackDoor:= SHOW;
				ELSE
					Cell[i].VisiblityStatus.BackDoor := HIDE;
				END_IF
				//only Cell1 has side door
				IF NOT Cell[1].DoorStatus.ClosedSide THEN
					Cell[1].VisiblityStatus.SideDoor := SHOW;
				ELSE
					Cell[1].VisiblityStatus.SideDoor := HIDE;
				END_IF	
				IF NOT Cell[i].DoorStatus.ClosedFront THEN
					Cell[i].VisiblityStatus.FrontDoor := SHOW;
				ELSE
					Cell[i].VisiblityStatus.FrontDoor := HIDE;
				END_IF;
				
				IF Cell[i].Emergency.ButtonPressed THEN
					Cell[i].VisiblityStatus.Emergency_Green         := HIDE;
					Cell[i].VisiblityStatus.Emergency_LambImageList := SHOW;
	
				ELSIF NOT Cell[i].Emergency.ButtonPressed THEN
					Cell[i].VisiblityStatus.Emergency_LambImageList := HIDE;
					Cell[i].VisiblityStatus.Emergency_Green         := SHOW;
					
				END_IF;
				
				IF Cell[i].MachineStatusLight.Green THEN
					Cell[i].VisiblityStatus.MachineStatLamb_ImageList := SHOW;
					Cell[i].Index.MachineStatLambIndex                := 2;	
				ELSIF Cell[i].MachineStatusLight.Red THEN
					Cell[i].VisiblityStatus.MachineStatLamb_ImageList := SHOW;
					Cell[i].Index.MachineStatLambIndex := 0;
				ELSIF  Cell[i].MachineStatusLight.Yellow THEN
					Cell[i].VisiblityStatus.MachineStatLamb_ImageList := SHOW;
					Cell[i].Index.MachineStatLambIndex := 1;
				END_IF;
				
			END_FOR;
			i:=1;
		CELL_SELECTED:
			FOR i:=1 TO 4 DO
	  			Cell[i].VisiblityStatus.BackDoor                 := HIDE;
				Cell[i].VisiblityStatus.FrontDoor                := HIDE;
				Cell[1].VisiblityStatus.SideDoor                 := HIDE;
				Cell[i].VisiblityStatus.Emergency_Green          := HIDE;
				Cell[i].VisiblityStatus.Emergency_LambImageList  := HIDE;
				Cell[i].VisiblityStatus.MainCell			     := HIDE;
				Cell[i].VisiblityStatus.MachineStatLamb_ImageList:= HIDE;
				Cell[i].VisiblityStatus.EmptyMachineLamb         := HIDE;
				
			END_FOR;
	END_CASE;
	
	
END_PROGRAM
 
  

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

