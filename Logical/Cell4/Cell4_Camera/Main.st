
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	
	//================================================================================================================
	(*               CAMERA STATUS BITS                   *)
	(*
		Status Bit.0  --> Online
		Status Bit.1  --> Toggle
		Status Bit.2  --> Not Used
		Status Bit.3  --> Busy
		Status Bit.4  --> QualitySubmitErr
		Status Bit.5  --> ImageErr
		Status Bit.6  --> TriggerReady
		Status Bit.7  --> ExposureComplate
		Status Bit.8  --> Not Used
		Status Bit.9  --> Not Used
		Status Bit.10 --> Pass
		Status Bit.11 --> Fail
		Status Bit.12 --> ResultReady
		Status Bit.13 --> LightingNok
		Status Bit.14 --> TurnLightOn
		Status Bit.15 --> Not Used
	*)
	//================================================================================================================
	

	SendWaitTime := T#250ms;
	PulseTimer.PT := T#500ms;
	PosTopWaitSendTimer.PT		 := SendWaitTime;
	Sew1TopWaitSendTimer.PT		 := SendWaitTime;
	Sew2TopWaitSendTimer.PT		 := SendWaitTime;
	SewBottomWaitSendTimer.PT	 := SendWaitTime;
	
	//================================================================================================================
	(*                Cell 4 Camera Top View  Position / Integrity Inspection               *)

		PC1_C4_TView_Pos_ProdID := 0;
		PosTopWaitSendTimer.PT := SendWaitTime;
		CamTrig_C4_TopPos(
			iCamOnline					 := PC1_C4_TView_Pos_Sts.0,
			iCamTrigReady				 := PC1_C4_TView_Pos_Sts.6,
			iCamResultReady				 := PC1_C4_TView_Pos_Sts.12,
			iTrigger					 := R4_diCam_Check1);
			
		PC1_C4_TView_Pos_Trigger		 := CamTrig_C4_TopPos.qCamTrig;
		PC1_C4_TView_Pos_PLCClear		 := CamTrig_C4_TopPos.qCamClear;
		LocalDO_C4_CamLightTPos_Open	 := CamTrig_C4_TopPos.qLightOn;
		C4_TView_Pos_StatusNo			 := CamTrig_C4_TopPos.qStateNo;
		C4_TView_Pos_AlarmNo			 := CamTrig_C4_TopPos.qAlarmNo;
	
	
		memcpy(ADR(C4_TopView_XPos1),ADR(PC1_C4_TView_Pos1_X[0]),SIZEOF(PC1_C4_TView_Pos1_X[0]));
		memcpy(ADR(C4_TopView_YPos1),ADR(PC1_C4_TView_Pos1_Y[0]),SIZEOF(PC1_C4_TView_Pos1_Y[0]));
		memcpy(ADR(C4_TopView_0Pos1),ADR(PC1_C4_TView_Pos1_0[0]),SIZEOF(PC1_C4_TView_Pos1_0[0]));
		memcpy(ADR(C4_TopView_XPos2),ADR(PC1_C4_TView_Pos2_X[0]),SIZEOF(PC1_C4_TView_Pos2_X[0]));
		memcpy(ADR(C4_TopView_YPos2),ADR(PC1_C4_TView_Pos2_Y[0]),SIZEOF(PC1_C4_TView_Pos2_Y[0]));
		memcpy(ADR(C4_TopView_0Pos2),ADR(PC1_C4_TView_Pos2_0[0]),SIZEOF(PC1_C4_TView_Pos2_0[0]));
	
		C4CamResult_TPos(
			ReadCamPos1_X	 := C4_TopView_XPos1,
			ReadCamPos1_Y	 := C4_TopView_YPos1,
			ReadCamPos1_0	 := C4_TopView_0Pos1,
			ReadCamPos2_X	 := C4_TopView_XPos2,
			ReadCamPos2_Y	 := C4_TopView_YPos2,
			ReadCamPos2_0	 := C4_TopView_0Pos2,
			iReadyResult	 := PC1_C4_TView_Pos_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R4_diCam_Check1);
	
		IF (EDGEPOS (C4CamResult_TPos.qRobotSendValue))
			THEN
			R4_goCamTop_Edge1PosX 			:= C4CamResult_TPos.qRobotPos1_X;
			R4_doCamTop_Edge1PosXPstvBit 	:= C4CamResult_TPos.qRobotPos1_XPstveBit;
			R4_doCamTop_Edge1PosXNgtvBit 	:= C4CamResult_TPos.qRobotPos1_XNgtveBit;
				
			R4_goCamTop_Edge1PosY 			:= C4CamResult_TPos.qRobotPos1_Y;
			R4_doCamTop_Edge1PosYPstvBit 	:= C4CamResult_TPos.qRobotPos1_YPstveBit;
			R4_doCamTop_Edge1PosYNgtvBit 	:= C4CamResult_TPos.qRobotPos1_YNgtveBit;
				
			R4_goCamTop_Edge1RotZ 			:= C4CamResult_TPos.qRobotRot1_Z;
			R4_doCamTop_Edge1RotZPstvBit 	:= C4CamResult_TPos.qRobotRot1_ZPstveBit;
			R4_doCamTop_Edge1RotZNgtvBit 	:= C4CamResult_TPos.qRobotRot1_ZNgtveBit;
			
			R4_goCamTop_Edge2PosX 			:= C4CamResult_TPos.qRobotPos2_X;
			R4_doCamTop_Edge2PosXPstvBit 	:= C4CamResult_TPos.qRobotPos2_XPstveBit;
			R4_doCamTop_Edge2PosXNgtvBit 	:= C4CamResult_TPos.qRobotPos2_XNgtveBit;
					
			R4_goCamTop_Edge2PosY 			:= C4CamResult_TPos.qRobotPos2_Y;
			R4_doCamTop_Edge2PosYPstvBit 	:= C4CamResult_TPos.qRobotPos2_YPstveBit;
			R4_doCamTop_Edge2PosYNgtvBit 	:= C4CamResult_TPos.qRobotPos2_YNgtveBit;
					
			R4_goCamTop_Edge2RotZ 			:= C4CamResult_TPos.qRobotRot2_Z;
			R4_doCamTop_Edge2RotZPstvBit 	:= C4CamResult_TPos.qRobotRot2_ZPstveBit;
			R4_doCamTop_Edge2RotZNgtvBit 	:= C4CamResult_TPos.qRobotRot2_ZNgtveBit;
		
			PosTopWaitSendTimer.IN := TRUE;
			
		END_IF;
		
		R4_doCam_OK1 := PC1_C4_TView_Pos_Sts.10; (*Camera Pass Bit*)
		//R4_doCam_NOK1 := PC1_C1_Part1_Sts.11 (*Camera Fail Bit*)
	
		IF PosTopWaitSendTimer.Q
			THEN
			R4_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	
	//================================================================================================================	
	
	
	
	//================================================================================================================
	(*                Cell 4 Camera Top View  Sewing 1 Inspection               *)
	
		PC1_C4_TView_Sew1_ProdID := 0;
		Sew1TopWaitSendTimer.PT := SendWaitTime;
		CamTrig_C4_TopSew1(
			iCamOnline					 := PC1_C4_TView_Sew1_Sts.0,
			iCamTrigReady				 := PC1_C4_TView_Sew1_Sts.6,
			iCamResultReady				 := PC1_C4_TView_Sew1_Sts.12,
			iTrigger					 := R4_diCam_Check2);
				
		PC1_C4_TView_Sew1_Trigger		 := CamTrig_C4_TopSew1.qCamTrig;
		PC1_C4_TView_Sew1_PLCClear		 := CamTrig_C4_TopSew1.qCamClear;
		LocalDO_C4_CamLightTSew1_Open	 := CamTrig_C4_TopSew1.qLightOn;
		C4_TView_Sew1_StatusNo			 := CamTrig_C4_TopSew1.qStateNo;
		C4_TView_Sew1_AlarmNo			 := CamTrig_C4_TopSew1.qAlarmNo;

	
		C4CamResult_TSew1(
			iSewingOk		 := PC1_C4_TView_Sew1_SewingOK,
			iIntegrityOk	 := 0,
			iReadyResult	 := PC1_C4_TView_Sew1_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R4_diCam_Check2);
		
		IF (EDGEPOS (C4CamResult_TSew1.qRobotSendValue))
			THEN
			Local_C4TSew1_SewingOk 			:= C4CamResult_TSew1.qSewingOk;
			
			Sew1TopWaitSendTimer.IN := TRUE;
				
		END_IF;
	
		R4_doCam_OK2 := PC1_C4_TView_Sew1_Sts.10; (*Camera Pass Bit*)
		//R4_doCam_NOK2 := PC1_C4_TView_Sew2_Sts.11 (*Camera Fail Bit*)
	
		IF Sew1TopWaitSendTimer.Q
			THEN
			R4_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	//================================================================================================================
	
	

	//================================================================================================================
	(*                Cell 4 Camera Top View  Sewing 2 Inspection               *)
	
		PC1_C4_TView_Sew2_ProdID := 0;
		Sew2TopWaitSendTimer.PT := SendWaitTime;
		CamTrig_C4_TopSew2(
			iCamOnline					 := PC1_C4_TView_Sew2_Sts.0,
			iCamTrigReady				 := PC1_C4_TView_Sew2_Sts.6,
			iCamResultReady				 := PC1_C4_TView_Sew2_Sts.12,
			iTrigger					 := R4_diCam_Check3);
					
		PC1_C4_TView_Sew2_Trigger		 := CamTrig_C4_TopSew2.qCamTrig;
		PC1_C4_TView_Sew2_PLCClear		 := CamTrig_C4_TopSew2.qCamClear;
		LocalDO_C4_CamLightTSew2_Open	 := CamTrig_C4_TopSew2.qLightOn;
		C4_TView_Sew2_StatusNo			 := CamTrig_C4_TopSew2.qStateNo;
		C4_TView_Sew2_AlarmNo			 := CamTrig_C4_TopSew2.qAlarmNo;
	
		
		C4CamResult_TSew2(
			iSewingOk		 := PC1_C4_TView_Sew2_SewingOK,
			iIntegrityOk	 := 0,
			iReadyResult	 := PC1_C4_TView_Sew2_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R4_diCam_Check2);
			
		IF (EDGEPOS (C4CamResult_TSew2.qRobotSendValue))
			THEN
			Local_C4TSew2_SewingOk 			:= C4CamResult_TSew2.qSewingOk;
				
			Sew2TopWaitSendTimer.IN := TRUE;
					
		END_IF;
		
		R4_doCam_OK3 := PC1_C4_TView_Sew2_Sts.10; (*Camera Pass Bit*)
		//R4_doCam_NOK3 := PC1_C4_TView_Sew2_Sts.11 (*Camera Fail Bit*)
	
		IF Sew2TopWaitSendTimer.Q
			THEN
			R4_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	//================================================================================================================
	
	
	
	
	//================================================================================================================
	(*                Cell 4 Camera Bottom View  Sewing Inspection               *)
	
		PC1_C4_BView_Sew_ProdID := 0;
		SewBottomWaitSendTimer.PT := SendWaitTime;
		CamTrig_C4_BottomSew(
			iCamOnline					 := PC1_C4_BView_Sew_Sts.0,
			iCamTrigReady				 := PC1_C4_BView_Sew_Sts.6,
			iCamResultReady				 := PC1_C4_BView_Sew_Sts.12,
			iTrigger					 := R4_diCam_Check4);
					
		PC1_C4_BView_Sew_Trigger		 := CamTrig_C4_BottomSew.qCamTrig;
		PC1_C4_BView_Sew_PLCClear		 := CamTrig_C4_BottomSew.qCamClear;
		LocalDO_C4_CamLightBSew_Open	 := CamTrig_C4_BottomSew.qLightOn;
		C4_BView_Sew_StatusNo			 := CamTrig_C4_BottomSew.qStateNo;
		C4_BView_Sew_AlarmNo			 := CamTrig_C4_BottomSew.qAlarmNo;
	
		
		C4CamResult_BSew(
			iSewingOk		 := PC1_C4_BView_Sew_SewingOK,
			iIntegrityOk	 := 0,
			iReadyResult	 := PC1_C4_BView_Sew_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R4_diCam_Check4);
			
		IF (EDGEPOS (C4CamResult_BSew.qRobotSendValue))
			THEN
			Local_C4BSew_SewingOk 			:= C4CamResult_BSew.qSewingOk;
				
			SewBottomWaitSendTimer.IN := TRUE;
					
		END_IF;	

		R4_doCam_OK4 := PC1_C4_BView_Sew_Sts.10; (*Camera Pass Bit*)
		//R4_doCam_NOK4 := PC1_C4_BView_Sew_Sts.11 (*Camera Fail Bit*)
	
		IF SewBottomWaitSendTimer.Q
			THEN
			R4_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	//================================================================================================================
	
	
	
	//================================================================================================================		

	

	
	//================================================================================================================		

	IF PulseTimer.Q
		THEN
		R4_doPartValueSended := FALSE;
		PosTopWaitSendTimer.IN := FALSE;
		Sew1TopWaitSendTimer.IN := FALSE;
		Sew2TopWaitSendTimer.IN := FALSE;
		SewBottomWaitSendTimer.IN := FALSE;
		PulseTimer.IN := FALSE;
	END_IF;	

	
	
	doCell4_CamLightTop_Open	 := LocalDO_C4_CamLightTPos_Open
								 OR LocalDO_C4_CamLightTSew1_Open
								 OR LocalDO_C4_CamLightTSew2_Open
								 OR Man_C4_TCamLightOn;	
	
	doCell4_CamLightBottom_Open	 := LocalDO_C4_CamLightBSew_Open
								 OR Man_C4_BCamLightOn;	
	
	
	PulseTimer();
	PosTopWaitSendTimer();
	Sew1TopWaitSendTimer();
	Sew2TopWaitSendTimer();
	SewBottomWaitSendTimer();
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

