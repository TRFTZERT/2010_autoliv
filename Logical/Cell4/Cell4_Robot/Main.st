
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC

	//======================================================================================================================================
	
		IF TRUE
			AND PC1_C4_TView_Pos_Sts.0
			AND PC1_C4_TView_Sew1_Sts.0
			AND PC1_C4_TView_Sew2_Sts.0
			AND PC1_C4_BView_Sew_Sts.0
			THEN
			R4_doCam_Running := TRUE;
			R4_doCam_Error := FALSE;
		ELSE
			R4_doCam_Running := FALSE;
			R4_doCam_Error := TRUE;
		END_IF;
	
	//======================================================================================================================================



	//======================================================================================================================================
	
		IF diCell3_LOP_EntryRqst
			OR diCell3_ROP_EntryRqst
			OR diCell4_LOP_EntryRqst
			OR diCell4_ROP_EntryRqst
			THEN
			R4_doOP_EntryRqst := TRUE;
		END_IF;
				
		IF Cell3_4_ProcesEnd = FALSE
			THEN
			R4_doOP_EntryRqst := FALSE;
		END_IF;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================	
	(*            Send to R4              *)
	
		R4_doTable_PartPreSens		 := diCell3_Table_PartPreSens;
		R4_doGrpCmpnstr_Opened		 := diCell4_GrpCmpnstr_Opened;
		R4_doGrpCmpnstr_Closed		 := diCell4_GrpCmpnstr_Closed;
		R4_doVcmCylndr_Opened		 := diCell4_VcmCylndr_Opened;
		R4_doVcmCylndr_Closed		 := diCell4_VcmCylndr_Closed;
		R4_goRecipe					 := RecipeNo;
		R4_doAuto					 := R4_diAutoON;
		R4_doSM_ClmpOpened			 := diCell4_SM_ClampOpened;
		R4_doSM_ClmpClosed			 := diCell4_SM_ClampClosed;
		R4_doCell3_TableClamp_Opened := diCell3_TableClamp_Opened;
		R4_doCell3_TableClamp_Closed := diCell3_TableClamp_Closed;
		R4_doCell4_TableClamp_Opened := diCell4_TableClamp_Opened;
		R4_doCell4_TableClamp_Closed := diCell4_TableClamp_Closed;
		doCell4_PartBlw				 := R4_diPartAirBlow;
		R4_doTool1_PreSens			 := diCell4_Tool1_PreSens;
		R4_doTool2_PreSens			 := diCell4_Tool2_PreSens;

	//======================================================================================================================================
	
	
	
	//======================================================================================================================================
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

