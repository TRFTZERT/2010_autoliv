
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	
	
	qAutoMode 			:= diCell1_HMIAutoMode;
	qEmgReset			:= diCell1_HMIEmgReset;
	
	//================================================================================================================	
	(*		Cell1 from PLC send to SafePLC		*)
	
		Cell1Merge.Bit1 	:= R1_diWZ_Home;
		Cell1Merge.Bit2 	:= R1_diAutoON;
		Cell1Merge.Bit3 	:= R1_diManual;
		Cell1Merge.Bit4 	:= diCell1_IntPreReset;
		Cell1Merge.Bit5 	:= diCell1_LOP_EntryRqst;
		Cell1Merge.Bit6 	:= diCell1_LOP_Reset;
		Cell1Merge.Bit7 	:= diCell1_LGate_Closed;
		Cell1Merge.Bit8 	:= diCell1_ROP_EntryRqst;
		Cell1Merge.Bit9 	:= diCell1_ROP_Reset;
		Cell1Merge.Bit10 	:= diCell1_RGate_Closed;
		Cell1Merge.Bit11 	:= diCell1_MainGate_Closed;
		Cell1Merge.Bit12 	:= Cell1_2_ProcesEnd;
		Cell1_MergeWord 	:= Cell1Merge.OUT_Word;
	
		Cell1_MergeInt := WORD_TO_INT(Cell1_MergeWord);
	
	//================================================================================================================
	
	
	//================================================================================================================
	(*		Cell2 from PLC send to SafePLC		*)
	
		Cell2Merge.Bit1 	:= R2_diWZ_Home;
		Cell2Merge.Bit2 	:= R2_diAutoON;
		Cell2Merge.Bit3 	:= R2_diManual;
		Cell2Merge.Bit4 	:= diCell2_IntPreReset;
		Cell2Merge.Bit5 	:= diCell2_LOP_EntryRqst;
		Cell2Merge.Bit6 	:= diCell2_LOP_Reset;
		Cell2Merge.Bit7 	:= diCell2_LGate_Closed;
		Cell2Merge.Bit8 	:= diCell2_ROP_EntryRqst;
		Cell2Merge.Bit9 	:= diCell2_ROP_Reset;
		Cell2Merge.Bit10	:= diCell2_RGate_Closed;
		Cell2Merge.Bit11	:= diCell2_MtenanceGate_Closed;
		Cell2Merge.Bit12 	:= Cell1_2_ProcesEnd;
		Cell2_MergeWord 	:= Cell2Merge.OUT_Word;
		
		Cell2_MergeInt := WORD_TO_INT(Cell2_MergeWord);

	//================================================================================================================
	
	
	//================================================================================================================
	(*		Cell3 from PLC send to SafePLC		*)	
	
		Cell3Merge.Bit1		 := R3_diWZ_Home;
		Cell3Merge.Bit2 	 := R3_diAutoON;
		Cell3Merge.Bit3		 := R3_diManual;
		Cell3Merge.Bit4		 := diCell3_IntPreReset;
		Cell3Merge.Bit5		 := diCell3_LOP_EntryRqst;
		Cell3Merge.Bit6		 := diCell3_LOP_Reset;
		Cell3Merge.Bit7		 := diCell3_LGate_Closed;
		Cell3Merge.Bit8		 := diCell3_ROP_EntryRqst;
		Cell3Merge.Bit9 	 := diCell3_ROP_Reset;
		Cell3Merge.Bit10	 := diCell3_RGate_Closed;
		Cell3Merge.Bit11	 := Cell3_4_ProcesEnd;
		Cell3_MergeWord		 := Cell3Merge.OUT_Word;
		
		Cell3_MergeInt := WORD_TO_INT(Cell3_MergeWord);
	
	//================================================================================================================
	
	
	//================================================================================================================
	(*		Cell4 from PLC send to SafePLC		*)	
	
		Cell4Merge.Bit1		 := R4_diWZ_Home;
		Cell4Merge.Bit2		 := R4_diAutoON;
		Cell4Merge.Bit3		 := R4_diManual;
		Cell4Merge.Bit4		 := diCell4_IntPreReset;
		Cell4Merge.Bit5		 := diCell4_LOP_EntryRqst;
		Cell4Merge.Bit6		 := diCell4_LOP_Reset;
		Cell4Merge.Bit7		 := diCell4_LGate_Closed;
		Cell4Merge.Bit8		 := diCell4_ROP_EntryRqst;
		Cell4Merge.Bit9		 := diCell4_ROP_Reset;
		Cell4Merge.Bit10	 := diCell4_RGate_Closed;
		Cell4Merge.Bit11	 := Cell3_4_ProcesEnd;
		Cell4_MergeWord 	 := Cell4Merge.OUT_Word;
		
		Cell4_MergeInt := WORD_TO_INT(Cell4_MergeWord);

	//================================================================================================================	
	

	//================================================================================================================
	(*		Receive HotPlug Staus From SafePLC		*)	
	
	
		HotPlugTCell1		 := HotPlugTimerStat_Int.0;
		HotPlugTCell2		 := HotPlugTimerStat_Int.1;
		HotPlugTCell3		 := HotPlugTimerStat_Int.2;
		HotPlugTCell4		 := HotPlugTimerStat_Int.3;
		

	//================================================================================================================	
	
	
	
	//================================================================================================================	
	
		IF EMGOk = FALSE
			THEN
			EmgFault := TRUE;
		ELSE
			EmgFault := FALSE;
		END_IF;
		
	
		IF Cell1_LSGOk = FALSE
			THEN
			Cell1_LSG_Fault := TRUE;
		ELSE
			Cell1_LSG_Fault := FALSE;
		END_IF;
		
	
		IF Cell1_RSGOk = FALSE
			THEN
			Cell1_RSG_Fault := TRUE;
		ELSE
			Cell1_RSG_Fault := FALSE;
		END_IF;
		
	
		IF Cell1_MainSGOk = FALSE
			THEN
			Cell1_MainSG_Fault := TRUE;
		ELSE
			Cell1_MainSG_Fault := FALSE;
		END_IF;	
		
		
		IF Cell2_LSGOk = FALSE
			THEN
			Cell2_LSG_Fault := TRUE;
		ELSE
			Cell2_LSG_Fault := FALSE;
		END_IF;
			
		
		IF Cell2_RSGOk = FALSE
			THEN
			Cell2_RSG_Fault := TRUE;
		ELSE
			Cell2_RSG_Fault := FALSE;
		END_IF;	
	
	
		IF Cell3_LSGOk = FALSE
			THEN
			Cell3_LSG_Fault := TRUE;
		ELSE
			Cell3_LSG_Fault := FALSE;
		END_IF;
				
			
		IF Cell3_RSGOk = FALSE
			THEN
			Cell3_RSG_Fault := TRUE;
		ELSE
			Cell3_RSG_Fault := FALSE;
		END_IF;	
	
	
		IF Cell4_LSGOk = FALSE
			THEN
			Cell4_LSG_Fault := TRUE;
		ELSE
			Cell4_LSG_Fault := FALSE;
		END_IF;
					
				
		IF Cell4_RSGOk = FALSE
			THEN
			Cell4_RSG_Fault := TRUE;
		ELSE
			Cell4_RSG_Fault := FALSE;
		END_IF;		
	
	
		IF Cell2_MtenanceSGOk = FALSE
			THEN
			Cell2_MaintenanceSG_Fault := TRUE;
		ELSE
			Cell2_MaintenanceSG_Fault := FALSE;
		END_IF;		
	
	
		IF Cell1_ASOk = FALSE
			THEN
			Cell1_ASOk_Fault := TRUE;
		ELSE
			Cell1_ASOk_Fault := FALSE;
		END_IF;	
	
	
		IF Cell2_ASOk = FALSE
			THEN
			Cell2_ASOk_Fault := TRUE;
		ELSE
			Cell2_ASOk_Fault := FALSE;
		END_IF;		
	
	
		IF Cell3_ASOk = FALSE
			THEN
			Cell3_ASOk_Fault := TRUE;
		ELSE
			Cell3_ASOk_Fault := FALSE;
		END_IF;		
	
	
		IF Cell4_ASOk = FALSE
			THEN
			Cell4_ASOk_Fault := TRUE;
		ELSE
			Cell4_ASOk_Fault := FALSE;
		END_IF;		
	

		IF Cell1_2AirPreparation = FALSE
			THEN
			Cell1_2_AirPreparation_Fault := TRUE;
		ELSE
			Cell1_2_AirPreparation_Fault := FALSE;
		END_IF;	
	
	
		IF Cell3_4AirPreparation = FALSE
			THEN
			Cell3_4_AirPreparation_Fault := TRUE;
		ELSE
			Cell3_4_AirPreparation_Fault := FALSE;
		END_IF;		
	
	//================================================================================================================	
	
	
	//================================================================================================================	
	
		ProcesEnd_Cell1_2(
			iLineerFwdSensor 	:= diCell2_MecLineer_FwdSns ,
			iPickUpSensor 		:= diCPX_PickUpSensor ,
			iFoldingBwdSensor 	:= diCPX_FoldingBwd ,
			iRotaryUpSensor 	:= diCPX_MecTurnUp ,
			iFirstRobotCycleOn	:= R1_diCycleON ,
			iSecondRobotCycleOn := R2_diCycleON);
	
			Cell1_2_ProcesEnd			:= ProcesEnd_Cell1_2.qEndProces;
			Cell1_2_ProcesEnd_AlarmNo 	:= ProcesEnd_Cell1_2.qAlarmNo;
										   
		
		ProcesEnd_Cell3_4(
			iLineerFwdSensor 	:= diCell2_MecLineer_FwdSns ,
			iPickUpSensor 		:= diCPX_PickUpSensor ,
			iFoldingBwdSensor 	:= diCPX_FoldingBwd ,
			iRotaryUpSensor 	:= diCPX_MecTurnUp ,
			iFirstRobotCycleOn	:= R3_diCycleON ,
			iSecondRobotCycleOn := R4_diCycleON);;
		
			Cell3_4_ProcesEnd			:= ProcesEnd_Cell3_4.qEndProces;
			Cell3_4_ProcesEnd_AlarmNo 	:= ProcesEnd_Cell3_4.qAlarmNo;
	
	//================================================================================================================	
	
	
	
	//================================================================================================================
	(*		HotPlug Lamp		*)	
	
		HotPlugStat_Cell1(
				iButton			 := siCell1_HotPlug_Ch1_2,
				iTimer			 := HotPlugTCell1);
		doCell1_HotPlugBtnLmp	 := HotPlugStat_Cell1.qButtonLamp;
		

		HotPlugStat_Cell2(
			iButton			 := siCell2_HotPlug_Ch1_2,
			iTimer			 := HotPlugTCell2);
		doCell2_HotPlugBtnLmp	 := HotPlugStat_Cell2.qButtonLamp;
		
		
		HotPlugStat_Cell3(
			iButton			 := siCell3_HotPlug_Ch1_2,
			iTimer			 := HotPlugTCell3);
		doCell3_HotPlugBtnLmp	 := HotPlugStat_Cell3.qButtonLamp;
		
		
		HotPlugStat_Cell4(
			iButton			 := siCell4_HotPlug_Ch1_2,
			iTimer			 := HotPlugTCell4);
		doCell4_HotPlugBtnLmp	 := HotPlugStat_Cell4.qButtonLamp;
	
	
	//================================================================================================================	
	
	
	
	//================================================================================================================		
	
	
	(*FB Call*)
	Cell1Merge();
	Cell2Merge();
	Cell3Merge();
	Cell4Merge();

	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

