
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	//======================================================================================================================================
	
		IF TRUE
			AND PC1_C3_TView_Pos1_Sts.0
			AND PC1_C3_TView_Sewing_Sts.0
			AND PC1_C3_TView_Pos2_3_Sts.0
			AND PC1_C3_BView_Sewing_Sts.0
			THEN
			R3_doCam_Running := TRUE;
			R3_doCam_Error := FALSE;
		ELSE
			R3_doCam_Running := FALSE;
			R3_doCam_Error := TRUE;
		END_IF;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================
	
		IF diCell3_LOP_EntryRqst
			OR diCell3_ROP_EntryRqst
			OR diCell4_LOP_EntryRqst
			OR diCell4_ROP_EntryRqst
			THEN
			R3_doOP_EntryRqst := TRUE;
		END_IF;
			
		IF Cell3_4_ProcesEnd = FALSE
			THEN
			R3_doOP_EntryRqst := FALSE;
		END_IF;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================	
	(*            Send to R3              *)
	
		R3_doTable_PartPreSens		 := diCell3_Table_PartPreSens;
		R3_doGrpCmpnstr_Opened		 := diCell3_GrpCmpnstr_Opened;
		R3_doGrpCmpnstr_Closed		 := diCell3_GrpCmpnstr_Closed;
		R3_doVcmCylndr_Opened		 := diCell3_VcmCylndr_Opened;
		R3_doVcmCylndr_Closed		 := diCell3_VcmCylndr_Closed;
		R3_goRecipe					 := RecipeNo;
		R3_doAuto					 := R3_diAutoON;
		R3_doSM_ClmpOpened			 := diCell3_SM_ClampOpened;
		R3_doSM_ClmpClosed			 := diCell3_SM_ClampClosed;
		R3_doCell3_TableClamp_Opened := diCell3_TableClamp_Opened;
		R3_doCell3_TableClamp_Closed := diCell3_TableClamp_Closed;
		R3_doTool1_PreSens			 := diCell3_Tool1_PreSens;
		R3_doTool2_PreSens			 := diCell3_Tool2_PreSens;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

