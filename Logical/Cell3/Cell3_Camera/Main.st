
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	
	//================================================================================================================
	(*               CAMERA STATUS BITS                   *)
	(*
		Status Bit.0  --> Online
		Status Bit.1  --> Toggle
		Status Bit.2  --> Not Used
		Status Bit.3  --> Busy
		Status Bit.4  --> QualitySubmitErr
		Status Bit.5  --> ImageErr
		Status Bit.6  --> TriggerReady
		Status Bit.7  --> ExposureComplate
		Status Bit.8  --> Not Used
		Status Bit.9  --> Not Used
		Status Bit.10 --> Pass
		Status Bit.11 --> Fail
		Status Bit.12 --> ResultReady
		Status Bit.13 --> LightingNok
		Status Bit.14 --> TurnLightOn
		Status Bit.15 --> Not Used
	*)
	//================================================================================================================

		SendWaitTime := T#250ms;
		PulseTimer.PT := T#500ms;
		Pos1TopWaitSendTimer.PT		 := SendWaitTime;
		SewTopWaitSendTimer.PT		 := SendWaitTime;
		Pos2_3TopWaitSendTimer.PT	 := SendWaitTime;
		SewBottomWaitSendTimer.PT	 := SendWaitTime;
	
	//================================================================================================================
	(*                Cell 3 Camera Top view  Position 1 / Integrity Inspection               *)

		PC1_C3_TView_Pos1_ProdID := 0;
		Pos1TopWaitSendTimer.PT := SendWaitTime;
		CamTrig_C3_TopPos1(
			iCamOnline					 := PC1_C3_TView_Pos1_Sts.0,
			iCamTrigReady				 := PC1_C3_TView_Pos1_Sts.6,
			iCamResultReady				 := PC1_C3_TView_Pos1_Sts.12,
			iTrigger					 := R3_diCam_Check1);
			
		PC1_C3_TView_Pos1_Trigger		 := CamTrig_C3_TopPos1.qCamTrig;
		PC1_C3_TView_Pos1_PLCClear		 := CamTrig_C3_TopPos1.qCamClear;
		LocalDO_C3_CamLightTPos1_Open	 := CamTrig_C3_TopPos1.qLightOn;
		C3_Pos1_StatusNo				 := CamTrig_C3_TopPos1.qStateNo;
		C3_Pos1_AlarmNo					 := CamTrig_C3_TopPos1.qAlarmNo;
		
		
		memcpy(ADR(C3_TopView_XPos1),ADR(PC1_C3_TView_Pos1_X[0]),SIZEOF(PC1_C3_TView_Pos1_X[0]));
		memcpy(ADR(C3_TopView_YPos1),ADR(PC1_C3_TView_Pos1_Y[0]),SIZEOF(PC1_C3_TView_Pos1_Y[0]));
		memcpy(ADR(C3_TopView_0Pos1),ADR(PC1_C3_TView_Pos1_0[0]),SIZEOF(PC1_C3_TView_Pos1_0[0]));
	
		
		C3CamResult_TPos1(
			ReadCamPos_X	 := C3_TopView_XPos1,
			ReadCamPos_Y	 := C3_TopView_YPos1,
			ReadCamPos_0	 := C3_TopView_0Pos1,
			iReadyResult	 := PC1_C3_TView_Pos1_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R3_diCam_Check1);
		
		IF (EDGEPOS (C3CamResult_TPos1.qRobotSendValue))
			THEN
			R3_goCamTop_Edge1PosX 			:= C3CamResult_TPos1.qRobotPos_X;
			R3_doCamTop_Edge1PosXPstvBit 	:= C3CamResult_TPos1.qRobotPos_XPstveBit;
			R3_doCamTop_Edge1PosXNgtvBit 	:= C3CamResult_TPos1.qRobotPos_XNgtveBit;
				
			R3_goCamTop_Edge1PosY 			:= C3CamResult_TPos1.qRobotPos_Y;
			R3_doCamTop_Edge1PosYPstvBit 	:= C3CamResult_TPos1.qRobotPos_YPstveBit;
			R3_doCamTop_Edge1PosYNgtvBit 	:= C3CamResult_TPos1.qRobotPos_YNgtveBit;
				
			R3_goCamTop_Edge1RotZ 			:= C3CamResult_TPos1.qRobotRot_Z;
			R3_doCamTop_Edge1RotZPstvBit 	:= C3CamResult_TPos1.qRobotRot_ZPstveBit;
			R3_doCamTop_Edge1RotZNgtvBit 	:= C3CamResult_TPos1.qRobotRot_ZNgtveBit;
			
			Pos1TopWaitSendTimer.IN := TRUE;
			
		END_IF;
		
		R3_doCam_OK1 := PC1_C3_TView_Pos1_Sts.10; (*Camera Pass Bit*)
		//R3_doCam_NOK1 := PC1_C3_TView_Pos1_Sts.11 (*Camera Fail Bit*)
	
		IF Pos1TopWaitSendTimer.Q
			THEN
			R3_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	

	//================================================================================================================
	
	
	
	//================================================================================================================
	(*                Cell 3 Camera Top View  Sewing Inspection               *)
	
		PC1_C3_TView_Sewing_ProdID := 0;
		SewTopWaitSendTimer.PT := SendWaitTime;
		CamTrig_C3_TopSew(
			iCamOnline					 := PC1_C3_TView_Sewing_Sts.0,
			iCamTrigReady				 := PC1_C3_TView_Sewing_Sts.6,
			iCamResultReady				 := PC1_C3_TView_Sewing_Sts.12,
			iTrigger					 := R3_diCam_Check2);
					
		PC1_C3_TView_Sewing_Trigger		 := CamTrig_C3_TopSew.qCamTrig;
		PC1_C3_TView_Sewing_PLCClear	 := CamTrig_C3_TopSew.qCamClear;
		LocalDO_C3_CamLightTSew_Open	 := CamTrig_C3_TopSew.qLightOn;
		C3_TView_Sew_StatusNo			 := CamTrig_C3_TopSew.qStateNo;
		C3_TView_Sew_AlarmNo			 := CamTrig_C3_TopSew.qAlarmNo;
	
		
		C3CamResult_TSew(
			iSewingOk		 := PC1_C3_TView_Sewing_SewingOK,
			iIntegrityOk	 := 0,
			iReadyResult	 := PC1_C3_TView_Sewing_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R3_diCam_Check2);
			
		IF (EDGEPOS (C3CamResult_TSew.qRobotSendValue))
			THEN
			Local_C3TSew_SewingOk 			:= C3CamResult_TSew.qSewingOk;
				
			SewTopWaitSendTimer.IN := TRUE;
					
		END_IF;
		
		R3_doCam_OK2 := PC1_C3_TView_Sewing_Sts.10; (*Camera Pass Bit*)
		//R3_doCam_NOK2 := PC1_C3_TView_Sewing_Sts.11 (*Camera Fail Bit*)
	
		IF SewTopWaitSendTimer.Q
			THEN
			R3_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	//================================================================================================================
	
	

	//================================================================================================================
	(*                Cell 3 Camera Top view  Position 2-3 / Integrity Inspection               *)

		PC1_C3_TView_Pos2_3_ProdID := 0;
		Pos2_3TopWaitSendTimer.PT := SendWaitTime;
		CamTrig_C3_TopPos2_3(
			iCamOnline					 := PC1_C3_TView_Pos2_3_Sts.0,
			iCamTrigReady				 := PC1_C3_TView_Pos2_3_Sts.6,
			iCamResultReady				 := PC1_C3_TView_Pos2_3_Sts.12,
			iTrigger					 := R3_diCam_Check3);
				
		PC1_C3_TView_Pos2_3_Trigger		 := CamTrig_C3_TopPos2_3.qCamTrig;
		PC1_C3_TView_Pos2_3_PLCClear	 := CamTrig_C3_TopPos2_3.qCamClear;
		LocalDO_C3_CamLightTPos2_3_Open	 := CamTrig_C3_TopPos2_3.qLightOn;
		C3_Pos2_3_StatusNo				 := CamTrig_C3_TopPos2_3.qStateNo;
		C3_Pos2_3_AlarmNo				 := CamTrig_C3_TopPos2_3.qAlarmNo;
			
			
		memcpy(ADR(C3_TopView_XPos2),ADR(PC1_C3_TView_Pos2_X[0]),SIZEOF(PC1_C3_TView_Pos2_X[0]));
		memcpy(ADR(C3_TopView_YPos2),ADR(PC1_C3_TView_Pos2_Y[0]),SIZEOF(PC1_C3_TView_Pos2_Y[0]));
		memcpy(ADR(C3_TopView_0Pos2),ADR(PC1_C3_TView_Pos2_0[0]),SIZEOF(PC1_C3_TView_Pos2_0[0]));
		
		memcpy(ADR(C3_TopView_XPos3),ADR(PC1_C3_TView_Pos3_X[0]),SIZEOF(PC1_C3_TView_Pos3_X[0]));
		memcpy(ADR(C3_TopView_YPos3),ADR(PC1_C3_TView_Pos3_Y[0]),SIZEOF(PC1_C3_TView_Pos3_Y[0]));
		memcpy(ADR(C3_TopView_0Pos3),ADR(PC1_C3_TView_Pos3_0[0]),SIZEOF(PC1_C3_TView_Pos3_0[0]));
		
			
		C3CamResult_TPos2_3(
			ReadCamPos1_X	 := C3_TopView_XPos2,
			ReadCamPos1_Y	 := C3_TopView_YPos2,
			ReadCamPos1_0	 := C3_TopView_0Pos2,
			ReadCamPos2_X	 := C3_TopView_XPos3,
			ReadCamPos2_Y	 := C3_TopView_YPos3,
			ReadCamPos2_0	 := C3_TopView_0Pos3,
			iReadyResult	 := PC1_C3_TView_Pos2_3_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R3_diCam_Check3);
			
		IF (EDGEPOS ( C3CamResult_TPos2_3.qRobotSendValue ))
			THEN
			R3_goCamTop_Edge2PosX 			:= C3CamResult_TPos2_3.qRobotPos1_X;
			R3_doCamTop_Edge2PosXPstvBit	:= C3CamResult_TPos2_3.qRobotPos1_XPstveBit;
			R3_doCamTop_Edge2PosXNgtvBit 	:= C3CamResult_TPos2_3.qRobotPos1_XNgtveBit;
					
			R3_goCamTop_Edge2PosY 			:= C3CamResult_TPos2_3.qRobotPos1_Y;
			R3_doCamTop_Edge2PosYPstvBit 	:= C3CamResult_TPos2_3.qRobotPos1_YPstveBit;
			R3_doCamTop_Edge2PosYNgtvBit 	:= C3CamResult_TPos2_3.qRobotPos1_YNgtveBit;
					
			R3_goCamTop_Edge2RotZ 			:= C3CamResult_TPos2_3.qRobotRot1_Z;
			R3_doCamTop_Edge2RotZPstvBit 	:= C3CamResult_TPos2_3.qRobotRot1_ZPstveBit;
			R3_doCamTop_Edge2RotZNgtvBit 	:= C3CamResult_TPos2_3.qRobotRot1_ZNgtveBit;
			
			R3_goCamTop_Edge3PosX 			:= C3CamResult_TPos2_3.qRobotPos2_X;
			R3_doCamTop_Edge3PosXPstvBit	:= C3CamResult_TPos2_3.qRobotPos2_XPstveBit;
			R3_doCamTop_Edge3PosXNgtvBit 	:= C3CamResult_TPos2_3.qRobotPos2_XNgtveBit;
						
			R3_goCamTop_Edge3PosY 			:= C3CamResult_TPos2_3.qRobotPos2_Y;
			R3_doCamTop_Edge3PosYPstvBit 	:= C3CamResult_TPos2_3.qRobotPos2_YPstveBit;
			R3_doCamTop_Edge3PosYNgtvBit 	:= C3CamResult_TPos2_3.qRobotPos2_YNgtveBit;
						
			R3_goCamTop_Edge3RotZ 			:= C3CamResult_TPos2_3.qRobotRot2_Z;
			R3_doCamTop_Edge3RotZPstvBit 	:= C3CamResult_TPos2_3.qRobotRot2_ZPstveBit;
			R3_doCamTop_Edge3RotZNgtvBit 	:= C3CamResult_TPos2_3.qRobotRot2_ZNgtveBit; 
				
			Pos2_3TopWaitSendTimer.IN := TRUE;
				
		END_IF;
	
		R3_doCam_OK3 := PC1_C3_TView_Pos2_3_Sts.10; (*Camera Pass Bit*)
		//R3_doCam_NOK3 := PC1_C3_TView_Pos2_3_Sts.11 (*Camera Fail Bit*)
	
		IF Pos2_3TopWaitSendTimer.Q
			THEN
			R3_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
		
	
	//================================================================================================================

	

	//================================================================================================================
	(*                Cell 3 Camera Bottom View  Sewing Inspection               *)
	
		PC1_C3_BView_Sewing_ProdID := 0;
		SewBottomWaitSendTimer.PT := SendWaitTime;
		CamTrig_C3_BottomSew(
			iCamOnline					 := PC1_C3_BView_Sewing_Sts.0,
			iCamTrigReady				 := PC1_C3_BView_Sewing_Sts.6,
			iCamResultReady				 := PC1_C3_BView_Sewing_Sts.12,
			iTrigger					 := R3_diCam_Check4);
						
		PC1_C3_BView_Sewing_Trigger		 := CamTrig_C3_BottomSew.qCamTrig;
		PC1_C3_BView_Sewing_PLCClear	 := CamTrig_C3_BottomSew.qCamClear;
		LocalDO_C3_CamLightBSew_Open	 := CamTrig_C3_BottomSew.qLightOn;
		C3_BView_Sew_StatusNo			 := CamTrig_C3_BottomSew.qStateNo;
		C3_BView_Sew_AlarmNo			 := CamTrig_C3_BottomSew.qAlarmNo;
		
			
		C3CamResult_BSew(
			iSewingOk		 := PC1_C3_BView_Sewing_SewingOK,
			iIntegrityOk	 := 0,
			iReadyResult	 := PC1_C3_BView_Sewing_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R3_diCam_Check4);
				
		IF (EDGEPOS (C3CamResult_BSew.qRobotSendValue))
			THEN
			Local_C3BSew_SewingOk 			:= C3CamResult_BSew.qSewingOk;
					
			SewBottomWaitSendTimer.IN := TRUE;
						
		END_IF;
		
		R3_doCam_OK4 := PC1_C3_BView_Sewing_Sts.10; (*Camera Pass Bit*)
		//R3_doCam_NOK4 := PC1_C3_BView_Sewing_Sts.11 (*Camera Fail Bit*)
	
		IF SewBottomWaitSendTimer.Q
			THEN
			R3_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
		END_IF;
	
	//================================================================================================================
	
	
	
	//================================================================================================================		

	

	
	//================================================================================================================		

	IF PulseTimer.Q
		THEN
		R3_doPartValueSended := FALSE;
		Pos1TopWaitSendTimer.IN := FALSE;
		SewTopWaitSendTimer.IN := FALSE;
		Pos2_3TopWaitSendTimer.IN := FALSE;
		SewBottomWaitSendTimer.IN := FALSE;
		PulseTimer.IN := FALSE;
	END_IF;	

	
	
	doCell3_CamLightTop_Open	 := LocalDO_C3_CamLightTPos1_Open
								 OR LocalDO_C3_CamLightTSew_Open
								 OR LocalDO_C3_CamLightTPos2_3_Open
								 OR Man_C3_TCamLightOn;	
	
	doCell3_CamLightBottom_Open	 := LocalDO_C3_CamLightBSew_Open
								 OR Man_C3_BCamLightOn;	
	
	
	PulseTimer();
	Pos1TopWaitSendTimer();
	SewTopWaitSendTimer();
	Pos2_3TopWaitSendTimer();
	SewBottomWaitSendTimer();
	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

