(*********************************************************************************
 * Copyright: B&R Industrial Automation GmbH 
 * Author:    semercis 
 * Created:   December 29, 2020/2:52 PM 
 *********************************************************************************)
PROGRAM _INIT
	Sender.Commands.open_send := FALSE;											(*enable sender by default*)						
END_PROGRAM

(*************cyclic program********************)
PROGRAM _CYCLIC
	IF NOT Sender.Commands.open_send THEN
		Sender.step := FRM_WAIT;	
	END_IF;

	CounterFB.CU		:= diCell2_Dummy48;
	CounterFB.PV		:= 1;
	Cell2TensionData[CounterFB.CV] := TotalTension;
	IF diCell2_Dummy48 THEN	
		Deneme := CounterFB.CV MOD 8;
		IF Deneme = 0 THEN
			i := CounterFB.CV;
			IplusTen := i+10;
			FOR i := CounterFB.CV TO IplusTen DO
				AvgTensionValue := Cell2TensionData[i] + AvgTensionValue;
			END_FOR;
		END_IF
	
		// Average Write Result Array

		IF ((CounterFB.CV MOD 10)= 0) THEN
			index := index +1;
			ResultTensionValue[index] :=  (AvgTensionValue /10);
		END_IF
	END_IF;

	
	CounterFB();
	
	CASE Sender.step OF
		
		//==========================	
		FRM_WAIT:	(*--- wait for command*)
			//==========================	
			IF Sender.Commands.open_send = TRUE THEN								(*command for sender activation*)
				Sender.step := FRM_OPEN;
			END_IF			
		
			(*close will be requested in step FRM_WRITE*)
		
			//==========================	
		FRM_OPEN:	(*--- open serial interface*)
			//==========================	
	
			(*Parameters for FRM_xopen()*)
			Sender.FRM_xopen_0.enable := TRUE;
			Sender.FRM_xopen_0.device := ADR('SL2.IF1.ST3.IF1.ST13.IF1');			(*Devicename --> see your serial interface properties*)
			Sender.FRM_xopen_0.mode := ADR('/PHY=RS422 /BD=9600 /DB=8 /PA=E /SB=1');(*Modestring --> specifies the seria operation mode*)
			Sender.FRM_xopen_0.config := ADR(Sender.xopenConfig) ;					(*Additional Parameters, optional*)
			
			(*Additional Parameters for FRM_xopen()*)
			Sender.xopenConfig.idle := 256;											(*Idle time between two characters*)
			Sender.xopenConfig.delimc := 0;											(*activate two delimeters*)
			Sender.xopenConfig.delim[0] := 0; 										(*CR - carridge return -> dec 13*)
			Sender.xopenConfig.delim[1] := 0; 										(*LF - line feed -> dec 10*)
			Sender.xopenConfig.tx_cnt := 3;											(*number of transmit buffers*)
			Sender.xopenConfig.rx_cnt := 8; 										(*number of receive buffers*)
			Sender.xopenConfig.tx_len := 16; 										(*length of transmit buffers*)
			Sender.xopenConfig.rx_len := 16; 										(*lenght of receive buffers*)
			Sender.xopenConfig.argc := 0;											(*activate additional options*)
			Sender.xopenConfig.argv := 0;											(*parameters for additional options (check help)*)
	
			Sender.FRM_xopen_0();													(*call the FRM_xopen() function*)
			
			IF Sender.FRM_xopen_0.status = 0 THEN
				Sender.step := FRM_GBUF;											(*Interface opend successfully --> next step*)
			ELSIF Sender.FRM_xopen_0.status = BUSY THEN
				Sender.step := FRM_OPEN;											(*operation not finished yet --> call again*)
			ELSE
				Sender.step := FRM_ERROR;											(*function returned errorcode --> check help*)
			END_IF			
	
			//==========================			
		FRM_GBUF:	(*--- aquire sendbuffer for FRM_WRITE*)
			//==========================	
			Sender.FRM_gbuf_0.enable := TRUE;
			Sender.FRM_gbuf_0.ident := Sender.FRM_xopen_0.ident;
			Counter := Counter +1;
			Sender.FRM_gbuf_0();													(*call the FRM_gbuf() function*)
			IF Sender.FRM_gbuf_0.status = 0 THEN
				memset(Sender.FRM_gbuf_0.buffer,0,Sender.FRM_gbuf_0.buflng);		(*clear sendbuffer*)
				IF Counter = 25 THEN
					Counter := 0;
					Sender.Commands.close	:= FALSE;
					Sender.step  			:= FRM_ROBUF;
				ELSE
					Sender.step 			:= FRM_PREPARE_SENDDATA;
				END_IF;								(*system returned a valid buffer --> next step*)
			ELSIF Sender.FRM_gbuf_0.status = BUSY THEN
				Sender.step := FRM_GBUF;											(*operation not finished yet --> call again*)
			ELSE
				Sender.step := FRM_ERROR;											(*function returned errorcode --> check help*)
			END_IF			

			//==========================	
		FRM_PREPARE_SENDDATA: (*--- prepare senddata, copy data to sendbuffer*)
			//==========================			
			Sender.send_data[0] := 128;
			Sender.send_data[1] := 0;
			Sender.send_data[2] := 0;
			Sender.send_data[3] := 0;
			Sender.send_data[4] := 0;
			Sender.send_data[5] := 0;
			Sender.send_data[6] := 0;
			Sender.send_data[7] := 0;
			Sender.send_data[8] := 0;
			Sender.send_data[9] := 0;
			Sender.send_data[10] := 0;
			Sender.send_data[11] := 0;
			Sender.send_data[12] := 0;
			Sender.send_data[13] := 0;
			Sender.send_data[14] := 9;
			Sender.send_data[15] := 119;
			strcpy(Sender.FRM_gbuf_0.buffer,ADR(Sender.send_data));					(*copy senddata to sendbuffer*)
			Sender.step := FRM_WRITE;												(*--> next step*)
		
	
			//==========================		
		FRM_WRITE:	(*--- write data to interface*)
			//==========================			
			(*Parameters for FRM_write()*)
			Sender.FRM_write_0.enable := TRUE;
			Sender.FRM_write_0.ident := Sender.FRM_xopen_0.ident;					(*ident from FRM_xopen()*)
			Sender.FRM_write_0.buffer := Sender.FRM_gbuf_0.buffer;					(*sendbuffer*)
			Sender.FRM_write_0.buflng := strlen(ADR(Sender.send_data));				(*net length of senddata*)
			Sender.FRM_write_0();													(*call the FRM_write() function*)
			
			IF Sender.FRM_write_0.status = 0 THEN
				Sender.step					:= FRM_GBUF;
			ELSIF Sender.FRM_write_0.status = BUSY THEN
				Sender.step := FRM_WRITE;											(*operation not finished yet --> call again*)
			ELSE
				Sender.step := FRM_ROBUF;											(*function returned errorcode --> check help*)
			END_IF			
		
	
			//==========================			
		FRM_READ: // Read receive data in this case
			//==========================	
			Sender.FRM_read_0.enable		:= TRUE;
			Sender.FRM_read_0.ident			:= Sender.FRM_xopen_0.ident;
			
			Sender.FRM_read_0();
			IF Sender.FRM_read_0.status = 0 THEN
				Sender.step				:= FRM_COPY_DATA;
			ELSIF Sender.FRM_read_0.status	= frmERR_NOINPUT THEN	
				Sender.step					:= FRM_READ;
			ELSIF Sender.FRM_read_0.status = frmERR_INPUTERROR THEN
				Sender.step					:= FRM_ERROR; 
			END_IF;

			//==========================				
		FRM_COPY_DATA: // Copy from Buffer to OldReceiveData and Receive_Data byte array
			//==========================		
			memset(ADR(Sender.receiver_data),0,SIZEOF(Sender.receiver_data));
			memcpy(ADR(Sender.receiver_data),(Sender.FRM_read_0.buffer),Sender.FRM_read_0.buflng);
			memcpy(ADR(Sender.Par.OldReceiveData),(Sender.FRM_read_0.buffer),Sender.FRM_read_0.buflng);
			
			TotalTension		:=BYTE_TO_INT(Sender.receiver_data[1])+BYTE_TO_INT(Sender.receiver_data[2]);
			 
			Sender.FRM_rbuf.enable 	:= TRUE;
			Sender.FRM_rbuf.buffer 	:= Sender.FRM_read_0.buffer;
			Sender.FRM_rbuf.buflng 	:= Sender.FRM_read_0.buflng;
			Sender.FRM_rbuf.ident	:= Sender.FRM_xopen_0.ident;		(******************* Read buffer bufring ******************)
			Sender.FRM_rbuf();											(******************* Call the FRM_rbuf() function******************)
			IF Sender.FRM_rbuf.status = 0  THEN  
				Sender.step					:= FRM_READ;
			END_IF;			 
		
			 
	
			//==========================			 		
		FRM_ROBUF:	(*--- release sendbuffer in case of  successful write operation*)
			//==========================		
			Sender.FRM_robuf_0.enable := TRUE;
			Sender.FRM_robuf_0.buffer := Sender.FRM_gbuf_0.buffer;					(*sendbuffer*)
			Sender.FRM_robuf_0.buflng := Sender.FRM_gbuf_0.buflng;					(*buffer length*)
			Sender.FRM_robuf_0.ident := Sender.FRM_xopen_0.ident;					(*ident open*)
			Sender.FRM_robuf_0();													(*call the FRM_robuf() function*)
			 
			IF Sender.FRM_robuf_0.status = 0  THEN
				Sender.step := FRM_READ;											(*released buffer successful --> get next sendbuffer*)
			ELSIF Sender.FRM_robuf_0.status = BUSY THEN
				Sender.step := FRM_ROBUF;											(*operation not finished yet --> call again*)
			ELSE
				Sender.step := FRM_ERROR;											(*function returned errorcode --> check help*)
			END_IF		
			 

			//==========================						
		FRM_CLOSE:	(*--- close the interface*)
			//==========================	
			Sender.FRM_close_0.enable := TRUE;
			Sender.FRM_close_0.ident := Sender.FRM_xopen_0.ident;					(*ident from FRM_xopen()*)			
			
			Sender.FRM_close_0();													(*call the FRM_close() function*)
			
			IF Sender.FRM_close_0.status = 0 THEN
				Sender.Commands.close := FALSE;										(*disable close command*)
				Sender.step := FRM_WAIT;											(*closed interface successfully --> wait step*)
			ELSIF Sender.FRM_close_0.status = BUSY THEN
				Sender.step := FRM_CLOSE;											(*operation not finished yet --> call again*)
			ELSE
				Sender.step := FRM_ERROR;											(*function returned errorcode --> check help*)
			END_IF
			
		FRM_ERROR:	(*--- error handling*)
		; (*not implementet yet, check help for error codes*)
	END_CASE	
	
END_PROGRAM
