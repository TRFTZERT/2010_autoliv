
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	
	//================================================================================================================
	(*               CAMERA STATUS BITS                   *)
	(*
		Status Bit.0  --> Online
		Status Bit.1  --> Toggle
		Status Bit.2  --> Not Used
		Status Bit.3  --> Busy
		Status Bit.4  --> QualitySubmitErr
		Status Bit.5  --> ImageErr
		Status Bit.6  --> TriggerReady
		Status Bit.7  --> ExposureComplate
		Status Bit.8  --> Not Used
		Status Bit.9  --> Not Used
		Status Bit.10 --> Pass
		Status Bit.11 --> Fail
		Status Bit.12 --> ResultReady
		Status Bit.13 --> LightingNok
		Status Bit.14 --> TurnLightOn
		Status Bit.15 --> Not Used
	*)
	//================================================================================================================
	
		SendWaitTime := T#250ms;
		PulseTimer.PT := T#500ms;
		TView_Sew1_WaitSendTimer.PT := SendWaitTime;
		TView_Sew2_WaitSendTimer.PT := SendWaitTime;
		BView_Sew1_WaitSendTimer.PT := SendWaitTime;
		BView_Sew2_WaitSendTimer.PT := SendWaitTime;
	
	//================================================================================================================
	(*        Cell 2 Top View Camera Verification          *)

		PC1_C2_TView_Verifi_ProdID := 0;
		CamTrig_C2_TopView_Verifi(
			iCamOnline					 := PC1_C2_TView_Verifi_Sts.0,
			iCamTrigReady				 := PC1_C2_TView_Verifi_Sts.6,
			iCamResultReady				 := PC1_C2_TView_Verifi_Sts.12,
			iTrigger					 := Local_TView_Verifi_Trigger); // Tetik sinyali konulacak ge�ici olarak Local_TView_Verifi_Trigger konulmustur
		
		PC1_C2_TView_Verifi_Trigger		 := CamTrig_C2_TopView_Verifi.qCamTrig;
		PC1_C2_TView_Verifi_PLCClear	 := CamTrig_C2_TopView_Verifi.qCamClear;
		LocalDO_C2_Verifi_CamLightT_Open := CamTrig_C2_TopView_Verifi.qLightOn;
		C2_TopView_Verifi_StatusNo		 := CamTrig_C2_TopView_Verifi.qStateNo;
		C2_TopView_Verifi_AlarmNo		 := CamTrig_C2_TopView_Verifi.qAlarmNo;
	
		// Result
		//'ilgili sinyal konulacak'		 := PC1_C2_TView_Verifi_Sts.10 (*Camera Pass Bit*) OR
		//									PC1_C2_TView_Verifi_Sts.11 (*Camera Fail Bit*)
		//									AND PC1_C2_TView_Verifi_CamOK = 1; *)
	
	//================================================================================================================	
	
	
	
	//================================================================================================================
	(*       Cell 2 Bottom View Camera Verification        *)

		PC1_C2_BView_Verifi_ProdID := 0;
		CamTrig_C2_BottomView_Verifi(
			iCamOnline					 := PC1_C2_BView_Verifi_Sts.0,
			iCamTrigReady				 := PC1_C2_BView_Verifi_Sts.6,
			iCamResultReady				 := PC1_C2_BView_Verifi_Sts.12,
			iTrigger					 := Local_BView_Verifi_Trigger); // Tetik sinyali konulacak ge�ici olarak Local_BView_Verifi_Trigger konulmustur
			
		PC1_C2_BView_Verifi_Trigger		 := CamTrig_C2_BottomView_Verifi.qCamTrig;
		PC1_C2_BView_Verifi_PLCClear	 := CamTrig_C2_BottomView_Verifi.qCamClear;
		LocalDO_C2_Verifi_CamLightB_Open := CamTrig_C2_BottomView_Verifi.qLightOn;
		C2_BottomView_Verifi_StatusNo	 := CamTrig_C2_BottomView_Verifi.qStateNo;
		C2_BottomView_Verifi_AlarmNo	 := CamTrig_C2_BottomView_Verifi.qAlarmNo;
		
		// Result
		//'ilgili sinyal konulacak'		 := PC1_C2_BView_Verifi_Sts.10 (*Camera Pass Bit*) OR
		//									PC1_C2_BView_Verifi_Sts.11 (*Camera Fail Bit*)
		//									AND PC1_C2_BView_Verifi_CamOK = 1; *)
	
	//================================================================================================================	
	
	
	
	//================================================================================================================
	(*      Cell 2 Top View Camera Top view  Sewing 1 + Integrity        *)
	
		PC1_C2_TView_Sewing1_ProdID := 0;
		CamTrig_C2_TopView_Sewing1(
			iCamOnline					 := PC1_C2_TView_Sewing1_Sts.0,
			iCamTrigReady				 := PC1_C2_TView_Sewing1_Sts.6,
			iCamResultReady				 := PC1_C2_TView_Sewing1_Sts.12,
			iTrigger					 := R2_diCam_Check1);
			
		PC1_C2_TView_Sewing1_Trigger	 := CamTrig_C2_TopView_Sewing1.qCamTrig;
		PC1_C2_TView_Sewing1_PLCClear	 := CamTrig_C2_TopView_Sewing1.qCamClear;
		LocalDO_C2_Sew1_CamLightT_Open	 := CamTrig_C2_TopView_Sewing1.qLightOn;
		C2_TopView_Sewing1_StatusNo		 := CamTrig_C2_TopView_Sewing1.qStateNo;
		C2_TopView_Sewing1_AlarmNo		 := CamTrig_C2_TopView_Sewing1.qAlarmNo;

	
		C2CamResult_TSew1(
			iSewingOk		 := PC1_C2_TView_Sewing1_SewOK,
			iIntegrityOk	 := PC1_C2_TView_Sewing1_IntegOK,
			iReadyResult	 := PC1_C2_TView_Sewing1_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R2_diCam_Check1);
			
		IF (EDGEPOS (C2CamResult_TSew1.qRobotSendValue))
			THEN
			Local_C2_TSew1_SewOk 		:= C2CamResult_TSew1.qSewingOk;
			Local_C2_TSew1_IntegOk		:= C2CamResult_TSew1.qIntegrityOk;
			
			TView_Sew1_WaitSendTimer.IN := TRUE;
						
		END_IF;
		
		R2_doCam_OK1 := PC1_C2_TView_Sewing1_Sts.10; (*Camera Pass Bit*)
		//R2_doCam_NOK2 := PC1_C2_TView_Sewing1_Sts.11 (*Camera Fail Bit*)
	
		IF TView_Sew1_WaitSendTimer.Q
			THEN
			R2_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
			TView_Sew1_WaitSendTimer.IN := FALSE;
		END_IF;
	
	
	//================================================================================================================	
	

	
	//================================================================================================================
	(*      Cell 2 Top View Camera Top view  Sewing 2         *)
	
		PC1_C2_TView_Sewing2_ProdID := 0;
		CamTrig_C2_TopView_Sewing2(
			iCamOnline					 := PC1_C2_TView_Sewing2_Sts.0,
			iCamTrigReady				 := PC1_C2_TView_Sewing2_Sts.6,
			iCamResultReady				 := PC1_C2_TView_Sewing2_Sts.12,
			iTrigger					 := R2_diCam_Check2);
				
		PC1_C2_TView_Sewing2_Trigger	 := CamTrig_C2_TopView_Sewing2.qCamTrig;
		PC1_C2_TView_Sewing2_PLCClear	 := CamTrig_C2_TopView_Sewing2.qCamClear;
		LocalDO_C2_Sew2_CamLightT_Open	 := CamTrig_C2_TopView_Sewing2.qLightOn;
		C2_TopView_Sewing2_StatusNo		 := CamTrig_C2_TopView_Sewing2.qStateNo;
		C2_TopView_Sewing2_AlarmNo		 := CamTrig_C2_TopView_Sewing2.qAlarmNo;
		
	
		C2CamResult_TSew2(
			iSewingOk		 := PC1_C2_TView_Sewing2_SewOK,
			iIntegrityOk	 := 0,
			iReadyResult	 := PC1_C2_TView_Sewing2_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R2_diCam_Check2);
				
		IF (EDGEPOS (C2CamResult_TSew2.qRobotSendValue))
			THEN
			Local_C2_TSew2_SewOk 		:= C2CamResult_TSew2.qSewingOk;
				
			TView_Sew2_WaitSendTimer.IN := TRUE;
							
		END_IF;
		
		R2_doCam_OK2 := PC1_C2_TView_Sewing2_Sts.10; (*Camera Pass Bit*)
		//R2_doCam_NOK2 := PC1_C2_TView_Sewing2_Sts.11 (*Camera Fail Bit*)
	
		IF TView_Sew2_WaitSendTimer.Q
			THEN
			R2_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
			TView_Sew2_WaitSendTimer.IN := FALSE;
		END_IF;
	
	
	//================================================================================================================
	
	
	
	//================================================================================================================
	(*      Cell 2 Bottom View Camera Bottom view  Sewing 1 + Integrity        *)
	
		PC1_C2_BView_Sewing1_ProdID := 0;
		CamTrig_C2_BottomView_Sewing1(
			iCamOnline					 := PC1_C2_BView_Sewing1_Sts.0,
			iCamTrigReady				 := PC1_C2_BView_Sewing1_Sts.6,
			iCamResultReady				 := PC1_C2_BView_Sewing1_Sts.12,
			iTrigger					 := R2_diCam_Check3);
				
		PC1_C2_BView_Sewing1_Trigger	 := CamTrig_C2_BottomView_Sewing1.qCamTrig;
		PC1_C2_BView_Sewing1_PLCClear	 := CamTrig_C2_BottomView_Sewing1.qCamClear;
		LocalDO_C2_Sew1_CamLightB_Open	 := CamTrig_C2_BottomView_Sewing1.qLightOn;
		C2_BottomView_Sewing1_StatusNo	 := CamTrig_C2_BottomView_Sewing1.qStateNo;
		C2_BottomView_Sewing1_AlarmNo	 := CamTrig_C2_BottomView_Sewing1.qAlarmNo;
	
		
		C2CamResult_BSew1(
			iSewingOk		 := PC1_C2_BView_Sewing1_SewOK,
			iIntegrityOk	 := PC1_C2_BView_Sewing1_IntegOK,
			iReadyResult	 := PC1_C2_BView_Sewing1_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R2_diCam_Check3);
				
		IF (EDGEPOS (C2CamResult_BSew1.qRobotSendValue))
			THEN
			Local_C2_BSew1_SewOk 		:= C2CamResult_BSew1.qSewingOk;
			Local_C2_BSew1_IntegOk		:= C2CamResult_BSew1.qIntegrityOk;
				
			BView_Sew1_WaitSendTimer.IN := TRUE;
							
		END_IF;
		
		R2_doCam_OK3 := PC1_C2_BView_Sewing1_Sts.10; (*Camera Pass Bit*)
		//R2_doCam_NOK3 := PC1_C2_BView_Sewing1_Sts.11 (*Camera Fail Bit*)
	
		IF BView_Sew1_WaitSendTimer.Q
			THEN
			R2_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
			BView_Sew1_WaitSendTimer.IN := FALSE;
		END_IF;
	
	
	//================================================================================================================
	

	
	//================================================================================================================
	(*      Cell 2 Bottom View Camera Top view  Sewing 2         *)
	
		PC1_C2_BView_Sewing2_ProdID := 0;
		CamTrig_C2_BottomView_Sewing2(
			iCamOnline					 := PC1_C2_BView_Sewing2_Sts.0,
			iCamTrigReady				 := PC1_C2_BView_Sewing2_Sts.6,
			iCamResultReady				 := PC1_C2_BView_Sewing2_Sts.12,
			iTrigger					 := R2_diCam_Check4);
					
		PC1_C2_BView_Sewing2_Trigger	 := CamTrig_C2_BottomView_Sewing2.qCamTrig;
		PC1_C2_BView_Sewing2_PLCClear	 := CamTrig_C2_BottomView_Sewing2.qCamClear;
		LocalDO_C2_Sew2_CamLightB_Open	 := CamTrig_C2_BottomView_Sewing2.qLightOn;
		C2_BottomView_Sewing2_StatusNo	 := CamTrig_C2_BottomView_Sewing2.qStateNo;
		C2_BottomView_Sewing2_AlarmNo	 := CamTrig_C2_BottomView_Sewing2.qAlarmNo;
			
		
		C2CamResult_BSew2(
			iSewingOk		 := PC1_C2_BView_Sewing2_SewOK,
			iIntegrityOk	 := 0,
			iReadyResult	 := PC1_C2_BView_Sewing2_Sts.12,	// Waiting ResultReady
			iClearDatas		 := R2_diCam_Check4);
						
		IF (EDGEPOS (C2CamResult_BSew2.qRobotSendValue))
			THEN
			Local_C2_BSew2_SewOk 		:= C2CamResult_BSew2.qSewingOk;
					
			BView_Sew2_WaitSendTimer.IN := TRUE;
									
		END_IF;
			
		R2_doCam_OK4 := PC1_C2_BView_Sewing2_Sts.10; (*Camera Pass Bit*)
		//R2_doCam_NOK4 := PC1_C2_BView_Sewing2_Sts.11 (*Camera Fail Bit*)
	
		IF BView_Sew2_WaitSendTimer.Q
			THEN
			R2_doPartValueSended := TRUE;
			PulseTimer.IN := TRUE;
			BView_Sew2_WaitSendTimer.IN := FALSE;
		END_IF;
	
	
	//================================================================================================================
	
	
	IF PulseTimer.Q
		THEN
		R2_doPartValueSended := FALSE;
		PulseTimer.IN := FALSE;
	END_IF;	
	
	
	
	doCell2_CamLightTop_Open := LocalDO_C2_Verifi_CamLightT_Open
 							 OR LocalDO_C2_Sew1_CamLightT_Open
							 OR LocalDO_C2_Sew2_CamLightT_Open
							 OR Man_C2_TView_CamLightOn;
	
	
	doCell2_CamLightBottom_Open := LocalDO_C2_Verifi_CamLightB_Open
								OR LocalDO_C2_Sew1_CamLightB_Open
								OR LocalDO_C2_Sew2_CamLightB_Open
								OR Man_C2_BView_CamLightOn;
	
	
	
	
	PulseTimer();
	TView_Sew1_WaitSendTimer();
	TView_Sew2_WaitSendTimer();
	BView_Sew1_WaitSendTimer();
	BView_Sew2_WaitSendTimer();
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

