
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
		//======================================================================================================================================
	
		IF TRUE
			//AND PC1_C2_TView_Verifi_Sts.0
			//AND PC1_C2_BView_Verifi_Sts.0
			AND PC1_C2_TView_Sewing1_Sts.0
			AND PC1_C2_TView_Sewing2_Sts.0
			AND PC1_C2_BView_Sewing1_Sts.0
			AND PC1_C2_BView_Sewing2_Sts.0
			THEN
			R2_doCam_Running := TRUE;
			R2_doCam_Error := FALSE;
		ELSE
			R2_doCam_Running := FALSE;
			R2_doCam_Error := TRUE;
		END_IF;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================
	
		IF diCell1_LOP_EntryRqst
			OR diCell1_ROP_EntryRqst
			OR diCell2_LOP_EntryRqst
			OR diCell2_ROP_EntryRqst
			THEN
			R2_doOP_EntryRqst := TRUE;
		END_IF;
		
		IF Cell1_2_ProcesEnd = FALSE
			THEN
			R2_doOP_EntryRqst := FALSE;
		END_IF;
	
	//======================================================================================================================================
	
	
	
	//======================================================================================================================================	
	(*            Send to R2              *)
	
		R2_doLPlate_PreSens			 := diCell1_LPlate_PreSens;
		R2_doRPlate_PreSens			 := diCell1_RPlate_PreSens;
		R2_doLPlate_PartPreSens		 := diCell1_LPlate_PartPreSens;
		R2_doRPlate_PartPreSens		 := diCell1_RPlate_PartPreSens;
		R2_goRecipe					 := RecipeNo;
		R2_doAuto					 := R2_diAutoON;
		R2_doTool1_PreSens			 := diCell2_Tool1_PreSens;
		R2_doTool2_PreSens			 := diCell2_Tool2_PreSens;
		
		

	//======================================================================================================================================
	
	
	
	//======================================================================================================================================
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

